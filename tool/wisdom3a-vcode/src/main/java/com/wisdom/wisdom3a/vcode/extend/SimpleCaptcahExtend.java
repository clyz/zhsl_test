package com.wisdom.wisdom3a.vcode.extend;

import com.wisdom.wisdom3a.vcode.CaptchaBean;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author: ziv
 * @date: 2018/12/17 15:24
 * @version: 0.0.1
 */
public class SimpleCaptcahExtend implements CaptchaExtend {
    private final static String VCODE_KEY = "VCODE_KEY";

    @Override
    public String achieve() {
        String attribute = (String) getSession().getAttribute(VCODE_KEY);
        getSession().removeAttribute(VCODE_KEY);
        return attribute;
    }

    private static HttpSession getSession() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
    }

    @Override
    public void dispose(String value, char[] text) { getSession().setAttribute(VCODE_KEY, value);
    }
}
