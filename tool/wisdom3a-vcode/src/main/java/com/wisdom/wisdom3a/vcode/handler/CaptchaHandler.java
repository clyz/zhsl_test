package com.wisdom.wisdom3a.vcode.handler;

import com.wisdom.wisdom3a.vcode.CaptchaBean;
import com.wisdom.wisdom3a.vcode.exception.VcodeException;
import com.wisdom.wisdom3a.vcode.extend.CaptchaExtend;
import com.wisdom.wisdom3a.vcode.gifencoder.AnimatedGifEncoder;
import com.wisdom.wisdom3a.vcode.kit.VcodeKit;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import static com.wisdom.wisdom3a.vcode.kit.VcodeKit.num;

/**
 * @author: ziv
 * @date: 2018/12/17 15:20
 * @version: 0.0.1
 */
public class CaptchaHandler {
    private CaptchaBean captchaBean;
    private CaptchaExtend captchaExtend;
    /**
     * 验证码原型
     */
    private char[] text;
    /**
     * 验证码的值
     */
    private String value = "";

    public CaptchaHandler(CaptchaBean captchaBean, CaptchaExtend captchaExtend) {
        this.captchaBean = captchaBean;
        this.captchaExtend = captchaExtend;
    }

    private static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    private static HttpSession getSession() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
    }

    protected Object invoke(String name) throws VcodeException {
        try {
            if ("text".equalsIgnoreCase(name)) {
                return captchaExtend.achieve();
            } else if ("out".equalsIgnoreCase(name)) {
                out();
                return null;
            } else {
                throw new VcodeException();
            }
        } catch (IOException e) {
            captchaExtend.exception(e);
            throw new VcodeException();
        }
    }

    private float getAlpha(int i, int j) {
        int num = i + j;
        float r = (float) 1 / captchaBean.getSize(), s = (captchaBean.getSize() + 1) * r;
        return num > captchaBean.getSize() ? (num * r - s) : num * r;
    }

    private void gifMark() throws IOException {
        HttpServletResponse response = getResponse();
        response.setContentType("image/gif");
        initText();
        responseHead(response);
        OutputStream outputStream = response.getOutputStream();
        gifVcode(outputStream);
    }

    private void gifVcode(OutputStream os) throws IOException {
        AnimatedGifEncoder gifEncoder = new AnimatedGifEncoder();
        gifEncoder.start(os);
        gifEncoder.setQuality(180);
        gifEncoder.setDelay(100);
        gifEncoder.setRepeat(0);
        BufferedImage frame;
        Color fontcolor[] = new Color[captchaBean.getSize()];
        for (int i = 0; i < captchaBean.getSize(); i++) {
            fontcolor[i] = new Color(20 + num(110), 20 + num(110), 20 + num(110));
        }
        for (int i = 0; i < captchaBean.getSize(); i++) {
            frame = graphicsImage(fontcolor, i);
            gifEncoder.addFrame(frame);
            frame.flush();
        }
        gifEncoder.finish();
        os.flush();
        os.close();
    }

    private void graphicsImage(OutputStream out, int width, int height, Font font) throws IOException {
        boolean ok = false;
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) bi.getGraphics();
        AlphaComposite ac3;
        Color color;
        int len = text.length;
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        captchaExtend.interfere(g, width, height);
        width = width - 5;
        g.setFont(font);
        int h = height - ((height - font.getSize()) >> 1),
                w = (width - len * 2) / len,
                size = w - font.getSize() + 1;
        /* 画字符串 */
        for (int i = 0; i < len; i++) {
            // 指定透明度
            ac3 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f);
            g.setComposite(ac3);
            color = new Color(20 + num(110), 20 + num(110), 20 + num(110));
            g.setColor(color);
            g.drawString(text[i] + "", (width - (len - i) * w) + size + 3, h - 4);
            color = null;
            ac3 = null;
        }
        ImageIO.write(bi, "png", out);
        out.flush();
        out.close();
    }

    private BufferedImage graphicsImage(Color[] fontcolor, int flag) {
        BufferedImage image = new BufferedImage(captchaBean.getWidth(), captchaBean.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = (Graphics2D) image.getGraphics();
        //利用指定颜色填充背景
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, captchaBean.getWidth(), captchaBean.getHeight());
        AlphaComposite ac3;
        int h = captchaBean.getHeight() - ((captchaBean.getHeight() - captchaBean.getFont().getSize()) >> 1);
        int w = captchaBean.getWidth() / captchaBean.getSize();
        g2d.setFont(captchaBean.getFont());
        for (int i = 0; i < captchaBean.getSize(); i++) {
            ac3 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, getAlpha(flag, i));
            g2d.setComposite(ac3);
            g2d.setColor(fontcolor[i]);
            captchaExtend.interfere(g2d, captchaBean.getWidth(), captchaBean.getHeight());
            g2d.drawString(text[i] + "", (captchaBean.getWidth() - (captchaBean.getSize() - i) * w) + (w - captchaBean.getFont().getSize()) + 1, h - 4);
        }
        g2d.dispose();
        return image;
    }

    private void initText() {
        char[] cs = new char[captchaBean.getSize()];
        for (int i = 0; i < captchaBean.getSize(); i++) {
            cs[i] = VcodeKit.alpha();
        }
        text = cs;
        value = new String(cs);
        captchaExtend.dispose(value, text);
    }

    protected void initTextMath() {
        int one = VcodeKit.num(1, 50), two = 0;
        char cs = VcodeKit.plusOrReduce();
        switch (cs) {
            case VcodeKit.PLUS:
                two = VcodeKit.num(50);
                value = String.valueOf(one + two);
                break;
            case VcodeKit.REDUCE:
                two = VcodeKit.num(one);
                value = String.valueOf(one - two);
                break;
            default:
                value = String.valueOf(one + two);
                break;
        }
        text = (String.valueOf(one) + String.valueOf(cs)+ String.valueOf(two)).toCharArray();
    }

    private void out() throws IOException {
        switch (captchaBean.getCaptchaType()) {
            case PNG_MARK:
                initText();
                captchaExtend.dispose(value,text);
                break;
            case GIF_MARK:
                gifMark();
                break;
            case ARITHMETIC:
                initTextMath();
                captchaExtend.dispose(value,text);
                break;
            default:
                break;
        }
    }

    private void png() throws IOException {
        HttpServletResponse response = getResponse();
        response.setContentType("image/png");
        responseHead(response);
        graphicsImage(response.getOutputStream(), captchaBean.getWidth(), captchaBean.getHeight(), captchaBean.getFont());
    }

    private void responseHead(HttpServletResponse response) {
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
    }

}