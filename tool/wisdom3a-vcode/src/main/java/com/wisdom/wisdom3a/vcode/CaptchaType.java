package com.wisdom.wisdom3a.vcode;

/**
 * @author: ziv
 * @date: 2018/12/18 09:36
 * @version: 0.0.1
 */
public enum CaptchaType {
    PNG_MARK, GIF_MARK, ARITHMETIC
}
