package com.wisdom.wisdom3a.vcode.extend;

import java.awt.*;
import java.io.IOException;

import static com.wisdom.wisdom3a.vcode.kit.VcodeKit.num;

/**
 * 验证码，辅助处理类
 *
 * @author: ziv
 * @date: 2018/12/17 15:23
 * @version: 0.0.1
 */
public interface CaptchaExtend {

    /**
     * 获取验证码
     *
     * @return
     */
    String achieve();

    /**
     * @param value 验证值
     * @param text  验证码原型
     */
    void dispose(String value, char[] text);

    /**
     * 当创建/获取验证时出现异常，调用该方法
     * @param e
     */
    default void exception(IOException e){e.printStackTrace();};

    /**
     * 干扰信息
     *
     * @param g
     * @param width
     * @param height
     */
    default void interfere(Graphics2D g, int width, int height) {
        g.drawOval(num(width), num(height), 5 + num(10), 5 + num(10));
    }


}
