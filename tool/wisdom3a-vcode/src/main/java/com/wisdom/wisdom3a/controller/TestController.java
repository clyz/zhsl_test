package com.wisdom.wisdom3a.controller;

import com.wisdom.wisdom3a.vcode.Captcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: ziv
 * @date: 2018/12/19 09:49
 * @version: 0.0.1
 */
@Controller
public class TestController {
    @Autowired
    Captcha captcha;

    @RequestMapping("/vcode")
    public void vcode(){
        captcha.out();
    }
    @RequestMapping("/get")
    @ResponseBody
    public String get(){
        String text = captcha.text();
        System.out.println(text);
        return text;
    }
}
