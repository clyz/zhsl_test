package com.wisdom.wisdom3a.vcode;

import org.springframework.stereotype.Component;

/**
 * 验证码接口
 *
 * @author: ziv
 * @date: 2018/12/17 15:18
 * @version: 0.0.1
 */
@Component
public interface Captcha {
    void out();

    String text();
}
