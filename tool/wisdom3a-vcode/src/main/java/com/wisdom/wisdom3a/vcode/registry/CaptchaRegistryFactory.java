package com.wisdom.wisdom3a.vcode.registry;

import com.wisdom.wisdom3a.vcode.Captcha;
import com.wisdom.wisdom3a.vcode.handler.CaptchaHandler;
import com.wisdom.wisdom3a.vcode.handler.CaptchaProxyHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author: ziv
 * @date: 2018/12/18 09:09
 * @version: 0.0.1
 */
@Configuration
public class CaptchaRegistryFactory implements ImportBeanDefinitionRegistrar {

    private Captcha captcha;

    private CaptchaHandler captchaHandler;

    public void setCaptchaHandler(CaptchaHandler captchaHandler) {
        this.captchaHandler = captchaHandler;
    }

    public Captcha getCaptcha() {
        if (captcha == null) {
            synchronized (CaptchaRegistryFactory.class) {
                if (captcha == null) {
                    captcha = newInstance();
                }
            }
        }
        return captcha;
    }

    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        registry.registerBeanDefinition("captcha",new RootBeanDefinition(getCaptcha().getClass()));
    }

    @Bean
    public Captcha captcha(){
        return getCaptcha();
    }


    private Captcha newInstance() {
        InvocationHandler captchaProxyHandler = new CaptchaProxyHandler(captchaHandler);
        return (Captcha) Proxy.newProxyInstance(Captcha.class.getClassLoader(), new Class[]{Captcha.class}, captchaProxyHandler);
    }
}