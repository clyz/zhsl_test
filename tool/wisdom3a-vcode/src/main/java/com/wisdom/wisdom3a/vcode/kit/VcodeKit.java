package com.wisdom.wisdom3a.vcode.kit;

import java.util.Random;

/**
 * @author: ziv
 * @date: 2018/12/18 13:59
 * @version: 0.0.1
 */
public class VcodeKit {
    public static final char PLUS = '+';
    public static final char REDUCE = '-';
    private static final char[] ALPHA = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'G', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '2', '3', '4', '5', '6', '7', '8', '9'};
    private static Random RANDOM = null;

    public static char alpha() {
        return ALPHA[num(0, ALPHA.length)];
    }

    public static int num(int min, int max) {
        return min + getRandom().nextInt(max - min);
    }

    private static Random getRandom() {
        if (RANDOM == null) {
            synchronized (VcodeKit.class) {
                if (RANDOM == null) {
                    RANDOM = new Random();
                }
            }
        }
        return RANDOM;
    }

    public static int num() {
        return getRandom().nextInt(10);
    }

    public static int num(int num) {
        return getRandom().nextInt(num);
    }

    public static char plusOrReduce() {
        return (char) (getRandom().nextInt(2) == 0 ? '+' : '-');
    }
}
