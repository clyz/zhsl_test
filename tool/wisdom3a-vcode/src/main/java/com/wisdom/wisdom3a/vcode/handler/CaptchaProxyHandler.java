package com.wisdom.wisdom3a.vcode.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 *
 * @author: ziv
 * @date: 2018/12/17 15:20
 * @version: 0.0.1
 */
public class CaptchaProxyHandler implements InvocationHandler {

    private CaptchaHandler captchaHandler;

    public CaptchaProxyHandler(CaptchaHandler captchaHandler) {
        this.captchaHandler = captchaHandler;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return captchaHandler.invoke(method.getName());
    }
}
