package com.wisdom.wisdom3a.vcode;

import java.awt.*;

/**
 * 验证码实体类
 * @author: ziv
 * @date: 2018/12/17 15:14
 * @version: 0.0.1
 */
public class CaptchaBean {
    private Integer height;
    private Integer width;
    private Integer size;
    private Font font;
    private CaptchaType captchaType;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public CaptchaType getCaptchaType() {
        return captchaType;
    }

    public void setCaptchaType(CaptchaType captchaType) {
        this.captchaType = captchaType;
    }

}