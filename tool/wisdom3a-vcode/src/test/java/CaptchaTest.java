import com.wisdom.wisdom3a.vcode.Captcha;
import com.wisdom.wisdom3a.vcode.handler.CaptchaHandler;
import com.wisdom.wisdom3a.vcode.registry.CaptchaRegistryFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring-vcode.xml"})
@ComponentScan
public class CaptchaTest {
    @Autowired
    Captcha captcha;
    @Test
    public void load(){
        captcha.text();
    }
}
