package clyz.ziv.tool.common.io.file.oberverlistener;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.junit.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 使用apache common-io 监控文件变化
 */
public class FileObserverListener {

    @Test
    public void fileObserverListener() throws Exception {
        //  要监听 的文件夹
        File directory = new File("C:\\service_java\\ZHSL\\zhsl_test\\tool\\common-io\\src\\main\\resources");
        // 轮询间隔 5 秒
        long interval = TimeUnit.SECONDS.toMillis(5);
        // 创建一个文件观察器用于处理文件的格式
        FileAlterationObserver observer = new FileAlterationObserver(directory,
                FileFilterUtils.and(FileFilterUtils.fileFileFilter(), FileFilterUtils.suffixFileFilter(".txt")));
        //设置文件变化监听器
        observer.addListener(new FileListener());
        FileAlterationMonitor monitor = new FileAlterationMonitor(interval, observer);
        monitor.start();
        LockSupport.park();
        monitor.stop();
    }
}
