package clyz.ziv.tool.common.io.file.oberverlistener;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;

public class FileListener implements FileAlterationListener {
    public void onStart(FileAlterationObserver fileAlterationObserver) {
        System.out.println("monitor start scan files..");
    }


    public void onDirectoryCreate(File file) {
        System.out.println(file.getName()+" director created.");
    }


    public void onDirectoryChange(File file) {
        System.out.println(file.getName()+" director changed.");
    }


    public void onDirectoryDelete(File file) {
        System.out.println(file.getName()+" director deleted.");
    }


    public void onFileCreate(File file) {
        System.out.println(file.getName()+" created.");
    }


    public void onFileChange(File file) {
        System.out.println(file.getName()+" changed.");
    }


    public void onFileDelete(File file) {
        System.out.println(file.getName()+" deleted.");
    }


    public void onStop(FileAlterationObserver fileAlterationObserver) {
        System.out.println("monitor stop scanning..");
    }
}
