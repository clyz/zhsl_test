package ziv.core;


import org.junit.Test;
import ziv.bean.User;
import ziv.core.groups.GUser;

import java.util.Map;

public class ValidationHandlerTest {

    @Test
    public void valid() throws NoSuchFieldException {
        User user = new User();
        user.setName("张");
        System.out.println(user);
        Map<String, String> valid = ValidationHandler.valid(user, GUser.class);
        System.out.println(valid);
    }
}