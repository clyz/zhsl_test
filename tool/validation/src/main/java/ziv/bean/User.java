package ziv.bean;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import ziv.core.groups.GUser;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;

@Data
public class User {
    private String id;

    @Length(min = 2, max = 4, message = "[2,4]", groups = {Default.class, GUser.class})
    private String name;

    @NotBlank(message = "不能为空", groups = {Default.class})
    private String account;
    private Boolean sex;
    private Integer age;
}
