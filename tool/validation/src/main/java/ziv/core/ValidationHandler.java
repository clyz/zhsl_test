package ziv.core;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * validation 处理
 */
public class ValidationHandler {
    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * 校验
     *
     * @param t   泛型实体bean
     * @param <T> 实体
     * @throws NoSuchFieldException 异常
     */
    public static <T> Map<String, String> valid(T t, Class<?>... clazz) throws NoSuchFieldException {
        Map<String, String> result = new HashMap<String, String>();
        final Set<ConstraintViolation<T>> constraintViolations = VALIDATOR.validate(t, clazz);
        if (constraintViolations != null && !constraintViolations.isEmpty()) {
            for (ConstraintViolation<T> cv : constraintViolations) {
                Field declaredField = t.getClass().getDeclaredField(cv.getPropertyPath().toString());
                if (result.containsKey(declaredField.getName())) {
                    result.put(declaredField.getName(), result.get(declaredField.getName()) + "\t" + cv.getMessage());
                } else {
                    result.put(declaredField.getName(), cv.getMessage());
                }
            }
        }
        return result;
    }

    public static <T> Map<String, String> valid(T t) throws NoSuchFieldException {
        return valid(t, Default.class);
    }
}
