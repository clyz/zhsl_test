import org.apache.commons.logging.Log;

public class SoutLog implements Log {
    public void debug(Object o) {
        System.out.println(o);
    }

    public void debug(Object o, Throwable throwable) {
        System.out.println(o);
    }

    public void error(Object o) {
        System.out.println(o);
    }

    public void error(Object o, Throwable throwable) {
        System.out.println(o);
    }

    public void fatal(Object o) {
        System.out.println(o);
    }

    public void fatal(Object o, Throwable throwable) {
        System.out.println(o);
    }

    public void info(Object o) {
        System.out.println(o);
    }

    public void info(Object o, Throwable throwable) {

    }

    public boolean isDebugEnabled() {
        return true;
    }

    public boolean isErrorEnabled() {
        return true;
    }

    public boolean isFatalEnabled() {
        return true;
    }

    public boolean isInfoEnabled() {
        return true;
    }

    public boolean isTraceEnabled() {
        return true;
    }

    public boolean isWarnEnabled() {
        return true;
    }

    public void trace(Object o) {
        System.out.println(o);
    }

    public void trace(Object o, Throwable throwable) {
        System.out.println(o);
    }

    public void warn(Object o) {
        System.out.println(o);
    }

    public void warn(Object o, Throwable throwable) {
        System.out.println(o);
    }
}
