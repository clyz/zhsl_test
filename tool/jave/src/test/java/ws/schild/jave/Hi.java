package ws.schild.jave;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * 第一次使用
 * <p>
 *
 */
public class Hi {
    private final static String P = "D:\\ZHSL\\FW2.1\\wisdom-tts\\sound\\default\\";
    private FFMPEGExecutor executor;

    @Before
    public void before() {
        DefaultFFMPEGLocator defaultFFMPEGLocator = new DefaultFFMPEGLocator();
        executor = defaultFFMPEGLocator.createExecutor();
    }

    @Test
    public void dome1() throws IOException {
        File one = new File(P + "1.wav");
        File yuan = new File(P + "-7.wav");

        /**

         C:\Users\ADMINI~1\AppData\Local\Temp\jave\ffmpeg-amd64-2.4.6-SNAPSHOT.exe
         -i D:\ZHSL\FW2.1\wisdom-tts\sound\default\1.wav
         -i D:\ZHSL\FW2.1\wisdom-tts\sound\default\-7.wav
         -filter_complex "[0:0] [1:0] concat=n=2:v=0:a=1 [a]" -map [a] output.wav -hide_banner

         *
         */
        //  ffmpeg -i -filter_complex "[0:0] [1:0] concat=n=6:v=0:a=1 [a]" -map [a] output.mp3
        executor.addArgument("-i");
        executor.addArgument(one.getAbsolutePath());
        executor.addArgument("-i");
        executor.addArgument(yuan.getAbsolutePath());
        executor.addArgument("-filter_complex \"[0:0] [1:0] concat=n=2:v=0:a=1 [a]\"");
        executor.addArgument("-map [a] d:/output.wav");
        executor.execute();
        executor.destroy();
    }
}
