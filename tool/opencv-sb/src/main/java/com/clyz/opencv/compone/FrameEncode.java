package com.clyz.opencv.compone;

import org.bytedeco.javacv.Frame;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * @author Zemin.Yang
 */
public class FrameEncode implements Encoder.BinaryStream<Frame> {
    @Override
    public void encode(Frame frame, OutputStream outputStream) throws EncodeException, IOException {
        ByteBuffer byteBuffer = (ByteBuffer) frame.image[0];
        byte[] bytes = JavaCVComponent.conver(byteBuffer);
        byteBuffer.flip();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
