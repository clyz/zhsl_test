package com.clyz.opencv.compone;

import com.clyz.opencv.live.LiveWebSocket;
import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.VideoInputFrameGrabber;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.websocket.Session;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 * @author Zemin.Yang
 */
@Component
@Order
@Slf4j
public class JavaCVComponent implements ApplicationRunner, DisposableBean {
    private VideoInputFrameGrabber grabber;
//    private Java2DFrameConverter converter = new Java2DFrameConverter();

    @Override
    public void destroy() throws Exception {
        grabber.stop();
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        grabber = VideoInputFrameGrabber.createDefault(0);
        grabber.start();
        new Thread(() -> {
            log.info("推送流文件开始");
            while (true) {
                try {
                    Frame frame = grabber.grabFrame();
                    Map<String, Session> sessionMap = LiveWebSocket.sessionMap;
                    for (Session session : sessionMap.values()) {
                        log.info("给sessionKey:{} 推送", session.getId());
                        if (session.isOpen()) {
                            session.getBasicRemote().sendObject(frame);
                        }
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    public static byte[] conver(ByteBuffer byteBuffer) {
        int len = byteBuffer.limit() - byteBuffer.position();
        byte[] bytes = new byte[len];
        if (byteBuffer.isReadOnly()) {
            return null;
        } else {
            byteBuffer.get(bytes);
        }
        return bytes;
    }
}
