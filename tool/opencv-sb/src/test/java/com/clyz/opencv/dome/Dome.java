package com.clyz.opencv.dome;

import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.objdetect.Objdetect;

import java.io.File;

import static org.opencv.imgcodecs.Imgcodecs.imread;

/**
 * @author Zemin.Yang
 * @date 2020/4/22 11:30
 */
public class Dome {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);  //加载动态链接库
    }

    @Test
    public void objdetect(){

    }

    @Test
    public void matImread(){
        String fileName = "C:\\Users\\Administrator\\Pictures\\me\\1579056213622.jpg"; //设置图片的路径
        if (!new File(fileName).exists()){
            System.out.println("文件不存在");
        }else{
            Mat srcImg = imread(fileName);  //opencv读取
            if (srcImg.empty()){
                System.out.println("加载图片失败！");
            }else{
                HighGui.imshow("image",srcImg); //显示
                HighGui.waitKey(0);
            }
        }

    }
}
