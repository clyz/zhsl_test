package com.sms.tool.smsSb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsSbApplication.class, args);
    }

}
