package com.sms.tool.smsSb.test;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.ic.v20190307.IcClient;
import com.tencentcloudapi.ic.v20190307.models.SendSmsRequest;
import com.tencentcloudapi.ic.v20190307.models.SendSmsResponse;

//导入可选配置类
// 导入 SMS 模块的 client
// 导入要请求接口对应的 request response 类

/**
 * Tencent Cloud Sms Sendsms
 * https://cloud.tencent.com/document/product/382/38778
 */
public class AddSmsTemplate {
    public static void main(String[] args) {
        try {

            Credential cred = new Credential("AKIDHWdU384JafpmuyTQuTm21roWDh9L9426", "1wa6MZbgW7VhqKhnhMrY0CKDtLo190zQ");

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("ic.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            IcClient client = new IcClient(cred, "ap-chengdu", clientProfile);

            final SendSmsRequest req = new SendSmsRequest();
            req.setContent("你好");
            //  8986011780153273502J 联通
            //  8986031875028069171HQ 联通
            req.setIccid("8986011780153273502J");
            req.setSdkappid(1400375949L);

            SendSmsResponse resp = client.SendSms(req);

            System.out.println(SendSmsRequest.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }

    }

}
