package ziv.clyz.dplayer.zivdplayer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZivDplayerApplication {

    //  http://127.0.0.1:8080/dplayer/dome/20190526/1/1.m3u8
    public static void main(String[] args) {
        SpringApplication.run(ZivDplayerApplication.class, args);
    }

}
