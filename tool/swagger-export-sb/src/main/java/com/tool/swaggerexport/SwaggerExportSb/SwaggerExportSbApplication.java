package com.tool.swaggerexport.SwaggerExportSb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerExportSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerExportSbApplication.class, args);
    }

}
