# WISDOM-API


<a name="overview"></a>
## Overview
前端测试接口帮助文档


### Version information
*Version* : 0.0.1


### Contact information
*Contact* : xxx


### License information
*License* : wisdomHub-在线测试页面  
*License URL* : http://localhost:8083/  
*Terms of service* : http://localhost:8083/


### URI scheme
*Host* : localhost:8081  
*BasePath* : /


### Tags

* WF_办件中心 : Work Flow Ctrl




<a name="paths"></a>
## Paths

<a name="deletecategoryusingpost"></a>
### c删除1
```
POST /platform/wf/category/del
```


#### Description
没有子项目才能删除


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/category/del
```


##### Request query
```
json :
{
  "id" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="deletecategorysusingpost"></a>
### c删除
```
POST /platform/wf/category/dels
```


#### Description
没有子项目才能删除


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**ids**  <br>*required*|ids|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/category/dels
```


##### Request query
```
json :
{
  "ids" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="getcategoryresponseusingpost"></a>
### C修改时获取
```
POST /platform/wf/category/get
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«CategoryResponse»](#f90fe645eab9d57adf7b4849acfd7e07)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/category/get
```


##### Request query
```
json :
{
  "id" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "category" : "string",
    "code" : "string",
    "description" : "string",
    "id" : 0,
    "name" : "string",
    "parentId" : 0,
    "sort" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listcategorysusingpost"></a>
### c流程配置搜索列表
```
POST /platform/wf/category/list
```


#### Description
查询 category=rule


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[CategoryListRequest](#categorylistrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«CategoryList»](#5b69ad18bd238578fdedea7132d675a9)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/category/list
```


##### Request body
```
json :
{
  "category" : "string",
  "code" : "string",
  "name" : "string",
  "pageNo" : 0,
  "pageSize" : 0,
  "parentId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "category" : "string",
    "categoryCn" : "string",
    "code" : "string",
    "createName" : "string",
    "createTime" : "string",
    "description" : "string",
    "id" : 0,
    "name" : "string",
    "optName" : "string",
    "optTime" : "string",
    "parentId" : 0
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="saveorupdateusingpost"></a>
### C新增|修改流程定义
```
POST /platform/wf/category/saveOrUpdate
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[SUCategoryRequest](#sucategoryrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«CategoryResponse»](#f90fe645eab9d57adf7b4849acfd7e07)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/category/saveOrUpdate
```


##### Request body
```
json :
{
  "category" : "string",
  "code" : "string",
  "description" : "string",
  "id" : 0,
  "name" : "string",
  "parentId" : 0,
  "sort" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "category" : "string",
    "code" : "string",
    "description" : "string",
    "id" : 0,
    "name" : "string",
    "parentId" : 0,
    "sort" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listcategorytreeusingpost"></a>
### C流程定义
```
POST /platform/wf/category/tree
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«List«CategoryTree»»](#9fd0ae6d04a71dca300306ec1dd365e9)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/category/tree
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : [ {
    "category" : "string",
    "code" : "string",
    "id" : 0,
    "name" : "string",
    "parentId" : 0,
    "sons" : [ {
      "category" : "string",
      "code" : "string",
      "id" : 0,
      "name" : "string",
      "parentId" : 0,
      "sons" : [ "..." ]
    } ]
  } ],
  "level" : "string",
  "msg" : "string"
}
```


<a name="cachedefinejsonnodeusingget"></a>
### dt 查询当前流程中缓存配置信息
```
GET /platform/wf/define/cache
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/cache
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="finddefinebydefineidusingget"></a>
### dt 获取缓存中流程定义配置信息
```
GET /platform/wf/define/cache/findDefine
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineId**  <br>*required*|defineId|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/cache/findDefine
```


##### Request query
```
json :
{
  "defineId" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="createdeploymentcacheusingget"></a>
### dt 刷新流程定义配置信息
```
GET /platform/wf/define/cache/put
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineId**  <br>*required*|defineId|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/cache/put
```


##### Request query
```
json :
{
  "defineId" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="runusingget"></a>
### dt 执行流程环节
```
GET /platform/wf/define/cache/run
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**buttonId**  <br>*optional*|string|
|**Query**|**processOpinion**  <br>*optional*|string|
|**Query**|**taskId**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/cache/run
```


##### Request query
```
json :
{
  "buttonId" : "string",
  "processOpinion" : "string",
  "taskId" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="startbyidusingget"></a>
### dt 启动流程
```
GET /platform/wf/define/cache/start
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**applyId**  <br>*optional*|applyId|string|
|**Query**|**key**  <br>*optional*|key|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/cache/start
```


##### Request query
```
json :
{
  "applyId" : "string",
  "key" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="infodefineusingpost"></a>
### d获取
```
POST /platform/wf/define/info
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineId**  <br>*required*|defineId|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«JsonNode»](#7f8ad490921277f1e5b8c5d7dcb99b1d)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/info
```


##### Request query
```
json :
{
  "defineId" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "array" : true,
    "bigDecimal" : true,
    "bigInteger" : true,
    "binary" : true,
    "boolean" : true,
    "containerNode" : true,
    "double" : true,
    "float" : true,
    "floatingPointNumber" : true,
    "int" : true,
    "integralNumber" : true,
    "long" : true,
    "missingNode" : true,
    "nodeType" : "string",
    "null" : true,
    "number" : true,
    "object" : true,
    "pojo" : true,
    "short" : true,
    "textual" : true,
    "valueNode" : true
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listdefinebycategoryidusingpost"></a>
### d获取 通过大类
```
POST /platform/wf/define/listDefineByCategoryId
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**categoryId**  <br>*required*|categoryId|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«List«JsonNode»»](#53e4d64373e9657c733bc65be7097ccf)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/listDefineByCategoryId
```


##### Request query
```
json :
{
  "categoryId" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : [ {
    "array" : true,
    "bigDecimal" : true,
    "bigInteger" : true,
    "binary" : true,
    "boolean" : true,
    "containerNode" : true,
    "double" : true,
    "float" : true,
    "floatingPointNumber" : true,
    "int" : true,
    "integralNumber" : true,
    "long" : true,
    "missingNode" : true,
    "nodeType" : "string",
    "null" : true,
    "number" : true,
    "object" : true,
    "pojo" : true,
    "short" : true,
    "textual" : true,
    "valueNode" : true
  } ],
  "level" : "string",
  "msg" : "string"
}
```


<a name="saveorupdatedefineusingpost"></a>
### d添加修改
```
POST /platform/wf/define/saveOrUpdate
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**json**  <br>*required*|json|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/saveOrUpdate
```


##### Request query
```
json :
{
  "json" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="applyrestnodeinfousingget"></a>
### dt s 存量数据 初始化流程环节节点数据
```
GET /platform/wf/define/store/applyRestNodeInfo
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**applys[0].applyObjName**  <br>*optional*|string|
|**Query**|**applys[0].date**  <br>*optional*|string (date-time)|
|**Query**|**applys[0].id**  <br>*optional*|integer (int64)|
|**Query**|**nodes[0].code**  <br>*optional*|string|
|**Query**|**nodes[0].day**  <br>*optional*|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/store/applyRestNodeInfo
```


##### Request query
```
json :
{
  "applys[0].applyObjName" : "string",
  "applys[0].date" : "string",
  "applys[0].id" : 0,
  "nodes[0].code" : "string",
  "nodes[0].day" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="auditdatasusingget"></a>
### dt s 存量数据 审批
```
GET /platform/wf/define/store/auditDatas
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Query**|**instanceId**  <br>*optional*|< integer (int64) > array(multi)|
|**Query**|**tasks[0].cmpId**  <br>*optional*|integer (int64)|
|**Query**|**tasks[0].nodeId**  <br>*optional*|integer (int64)|
|**Query**|**tasks[0].nodeName**  <br>*optional*|string|
|**Query**|**tasks[0].operator**  <br>*optional*|integer (int64)|
|**Query**|**tasks[0].phrases[0].classifyCode**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].code**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].content**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].createdBy**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].createtime**  <br>*optional*|string (date-time)|
|**Query**|**tasks[0].phrases[0].defaultFlag**  <br>*optional*|integer (int32)|
|**Query**|**tasks[0].phrases[0].description**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].id**  <br>*optional*|integer (int64)|
|**Query**|**tasks[0].phrases[0].itemCode**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].itemName**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].sort**  <br>*optional*|integer (int32)|
|**Query**|**tasks[0].phrases[0].type**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].updatedBy**  <br>*optional*|string|
|**Query**|**tasks[0].phrases[0].updatetime**  <br>*optional*|string (date-time)|
|**Query**|**tasks[0].review**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/store/auditDatas
```


##### Request query
```
json :
{
  "instanceId" : 0,
  "tasks[0].cmpId" : 0,
  "tasks[0].nodeId" : 0,
  "tasks[0].nodeName" : "string",
  "tasks[0].operator" : 0,
  "tasks[0].phrases[0].classifyCode" : "string",
  "tasks[0].phrases[0].code" : "string",
  "tasks[0].phrases[0].content" : "string",
  "tasks[0].phrases[0].createdBy" : "string",
  "tasks[0].phrases[0].createtime" : "string",
  "tasks[0].phrases[0].defaultFlag" : 0,
  "tasks[0].phrases[0].description" : "string",
  "tasks[0].phrases[0].id" : 0,
  "tasks[0].phrases[0].itemCode" : "string",
  "tasks[0].phrases[0].itemName" : "string",
  "tasks[0].phrases[0].sort" : 0,
  "tasks[0].phrases[0].type" : "string",
  "tasks[0].phrases[0].updatedBy" : "string",
  "tasks[0].phrases[0].updatetime" : "string",
  "tasks[0].review" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="getauditdatasusingget"></a>
### dt s 获取存量审批数据
```
GET /platform/wf/define/store/getAuditDatas
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**applys**  <br>*optional*|applys|< integer (int64) > array(multi)|
|**Query**|**defineCode**  <br>*optional*|defineCode|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/store/getAuditDatas
```


##### Request query
```
json :
{
  "applys" : 0,
  "defineCode" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="getauditdatasbyidusingget"></a>
### dt s 获取存量审批数据
```
GET /platform/wf/define/store/getAuditDatasById
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**applys**  <br>*optional*|applys|< integer (int64) > array(multi)|
|**Query**|**defineId**  <br>*optional*|defineId|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/store/getAuditDatasById
```


##### Request query
```
json :
{
  "applys" : 0,
  "defineId" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="pushauditdatausingget"></a>
### dt s 推送存量数据 表示已过审批
```
GET /platform/wf/define/store/pushAuditData
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**applys**  <br>*optional*|applys|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«object»](#7d3c7d7aed68f1c00e3e41f51fb29121)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/store/pushAuditData
```


##### Request query
```
json :
{
  "applys" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "object",
  "level" : "string",
  "msg" : "string"
}
```


<a name="uuidusingget"></a>
### dt 生成唯一ID
```
GET /platform/wf/define/uuid
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«string»](#8129a33ccb6b6d98fda6a350acfd9219)|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/define/uuid
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : "string",
  "level" : "string",
  "msg" : "string"
}
```


<a name="createorupdateformusingpost"></a>
### f流程表单
```
POST /platform/wf/form/createOrUpdate
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[COUFormRequest](#couformrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«FormResponse»](#c9813be7e9482bf9a67e25947141c22e)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/form/createOrUpdate
```


##### Request body
```
json :
{
  "code" : "string",
  "id" : 0,
  "name" : "string",
  "param" : "string",
  "remark" : "string",
  "sort" : 0,
  "status" : "string",
  "url" : "string",
  "wfCategoryId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "id" : 0,
    "name" : "string",
    "param" : "string",
    "remark" : "string",
    "sort" : 0,
    "status" : "string",
    "url" : "string",
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="deleteformsusingpost"></a>
### f删除
```
POST /platform/wf/form/dels
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**ids**  <br>*required*|ids|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/form/dels
```


##### Request query
```
json :
{
  "ids" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="getformusingpost"></a>
### f流程表单修改时获取
```
POST /platform/wf/form/get
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«FormResponse»](#c9813be7e9482bf9a67e25947141c22e)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/form/get
```


##### Request query
```
json :
{
  "id" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "id" : 0,
    "name" : "string",
    "param" : "string",
    "remark" : "string",
    "sort" : 0,
    "status" : "string",
    "url" : "string",
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listformsusingpost"></a>
### f列表查询
```
POST /platform/wf/form/list
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[FormListRequest](#formlistrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«FormList»](#c84eb7bb15deed50e0ea89512df8fd7a)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/form/list
```


##### Request body
```
json :
{
  "code" : "string",
  "name" : "string",
  "pageNo" : 0,
  "pageSize" : 0,
  "status" : "string",
  "wfCategoryId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "code" : "string",
    "createName" : "string",
    "createTime" : "string",
    "id" : 0,
    "name" : "string",
    "optName" : "string",
    "optTime" : "string",
    "status" : "string",
    "url" : "string"
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="getbuttonsandphrasebynodeid2usingpost"></a>
### dt2 通过 流程环节id 获取 流程短语及按钮
```
POST /platform/wf/getButtonsAndPhraseByNodeId
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**nodeId**  <br>*required*|nodeId|integer (int64)|
|**Query**|**step**  <br>*optional*|step|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«NFBResponse»](#08def5ae558adc442e84cb64afbe0a05)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getButtonsAndPhraseByNodeId
```


##### Request query
```
json :
{
  "nodeId" : 0,
  "step" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "auditAdvance" : "string",
    "buttons" : [ [ {
      "api" : "string",
      "buttonId" : 0,
      "code" : "string",
      "id" : 0,
      "imgUrl" : "string",
      "mean" : "string",
      "name" : "string",
      "remark" : "string",
      "sort" : "string",
      "url" : "string"
    } ] ],
    "defineId" : 0,
    "nodeId" : 0,
    "phrases" : [ {
      "classifyCode" : "string",
      "code" : "string",
      "content" : "string",
      "createdBy" : "string",
      "createtime" : "string",
      "defaultFlag" : 0,
      "description" : "string",
      "id" : 0,
      "itemCode" : "string",
      "itemName" : "string",
      "sort" : 0,
      "type" : "string",
      "updatedBy" : "string",
      "updatetime" : "string"
    } ],
    "resourceConfigs" : [ {
      "abbr" : "string",
      "classifyCode" : "string",
      "code" : "string",
      "createdBy" : 0,
      "createtime" : "string",
      "edgeFlag" : "string",
      "id" : 0,
      "itemCode" : "string",
      "itemName" : "string",
      "name" : "string",
      "needFlag" : "string",
      "operationName" : "string",
      "operationTime" : "string",
      "remark" : "string",
      "required" : "string",
      "resourceNum" : 0,
      "resourceType" : "string",
      "sort" : 0,
      "type" : "string",
      "updatedBy" : 0,
      "updatetime" : "string",
      "uploadAfter" : "string",
      "uploadBefore" : "string"
    } ],
    "step" : "string"
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getbuttonsandphrasebytaskid2usingpost"></a>
### dt2 通过 流程执行环节id 获取 流程短语及按钮
```
POST /platform/wf/getButtonsAndPhraseByTaskId
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**taskId**  <br>*required*|taskId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«NFBTaskIdResponse»](#3b95bab42c564baf8616bc330c49ea95)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getButtonsAndPhraseByTaskId
```


##### Request query
```
json :
{
  "taskId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "auditAdvance" : "string",
    "buttons" : [ [ {
      "api" : "string",
      "buttonId" : 0,
      "code" : "string",
      "id" : 0,
      "imgUrl" : "string",
      "mean" : "string",
      "name" : "string",
      "remark" : "string",
      "sort" : "string",
      "url" : "string"
    } ] ],
    "defineId" : 0,
    "nodeId" : 0,
    "phrases" : [ {
      "classifyCode" : "string",
      "code" : "string",
      "content" : "string",
      "createdBy" : "string",
      "createtime" : "string",
      "defaultFlag" : 0,
      "description" : "string",
      "id" : 0,
      "itemCode" : "string",
      "itemName" : "string",
      "sort" : 0,
      "type" : "string",
      "updatedBy" : "string",
      "updatetime" : "string"
    } ],
    "processOpinion" : "string",
    "processResult" : "string",
    "resourceConfigs" : [ {
      "abbr" : "string",
      "classifyCode" : "string",
      "code" : "string",
      "createdBy" : 0,
      "createtime" : "string",
      "edgeFlag" : "string",
      "id" : 0,
      "itemCode" : "string",
      "itemName" : "string",
      "name" : "string",
      "needFlag" : "string",
      "operationName" : "string",
      "operationTime" : "string",
      "remark" : "string",
      "required" : "string",
      "resourceNum" : 0,
      "resourceType" : "string",
      "sort" : 0,
      "type" : "string",
      "updatedBy" : 0,
      "updatetime" : "string",
      "uploadAfter" : "string",
      "uploadBefore" : "string"
    } ],
    "step" : "string",
    "taskId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getdefinebycategoryloginuser2usingpost"></a>
### dt2 通过 流程类别 + 当前使用者  获取 流程 定义
```
POST /platform/wf/getDefineByCategoryLoginUser
```


#### Description
当前使用者 = 当前登录用户


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**categoryId**  <br>*required*|categoryId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«DefineByCategoryLoginUserResponse»](#28805b7ee3689b4c908cbc7c4f3a44dd)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getDefineByCategoryLoginUser
```


##### Request query
```
json :
{
  "categoryId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "form" : {
      "code" : "string",
      "createdBy" : 0,
      "createtime" : "string",
      "designData" : "string",
      "id" : 0,
      "name" : "string",
      "param" : "string",
      "remark" : "string",
      "status" : "string",
      "updatedBy" : 0,
      "updatetime" : "string",
      "url" : "string",
      "wfCategoryId" : 0
    },
    "id" : 0,
    "name" : "string",
    "node" : {
      "code" : "string",
      "createdBy" : 0,
      "createtime" : "string",
      "id" : 0,
      "name" : "string",
      "nodeJson" : "string",
      "nodeType" : "string",
      "updatedBy" : 0,
      "updatetime" : "string",
      "wfCategoryId" : 0,
      "wfDefineId" : 0
    },
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getdefinebycategoryloginusercode2usingpost"></a>
### dt2 通过 流程类别 + 当前使用者  获取 流程 定义 byCode
```
POST /platform/wf/getDefineByCategoryLoginUserCode
```


#### Description
当前使用者 = 当前登录用户


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**categoryCode**  <br>*required*|categoryCode|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«DefineByCategoryLoginUserResponse»](#28805b7ee3689b4c908cbc7c4f3a44dd)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getDefineByCategoryLoginUserCode
```


##### Request query
```
json :
{
  "categoryCode" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "form" : {
      "code" : "string",
      "createdBy" : 0,
      "createtime" : "string",
      "designData" : "string",
      "id" : 0,
      "name" : "string",
      "param" : "string",
      "remark" : "string",
      "status" : "string",
      "updatedBy" : 0,
      "updatetime" : "string",
      "url" : "string",
      "wfCategoryId" : 0
    },
    "id" : 0,
    "name" : "string",
    "node" : {
      "code" : "string",
      "createdBy" : 0,
      "createtime" : "string",
      "id" : 0,
      "name" : "string",
      "nodeJson" : "string",
      "nodeType" : "string",
      "updatedBy" : 0,
      "updatetime" : "string",
      "wfCategoryId" : 0,
      "wfDefineId" : 0
    },
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getfromurlbydefineusingpost"></a>
### 通过 定义 code 获取 该定义得第一个环节 的表单信息
```
POST /platform/wf/getFromUrlByDefine
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineCode**  <br>*required*|流程定义 code|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«NodeFormResponse»](#6e8f3af68e33de828eb3180db6d8a8b8)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getFromUrlByDefine
```


##### Request query
```
json :
{
  "defineCode" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "applyState" : "string",
    "defineCode" : "string",
    "defineId" : 0,
    "formId" : 0,
    "formName" : "string",
    "intanceId" : 0,
    "nodeCalssIfy" : "string",
    "nodeCode" : "string",
    "nodeId" : 0,
    "nodeName" : "string",
    "step" : "string",
    "taskId" : 0,
    "taskState" : "string",
    "url" : "string"
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getfromurlbydefine2usingpost"></a>
### dt2 通过 定义 code 获取 该定义得第一个环节 的表单信息
```
POST /platform/wf/getFromUrlByDefine2
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineCode**  <br>*required*|流程定义 code|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«NodeFormResponse»](#6e8f3af68e33de828eb3180db6d8a8b8)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getFromUrlByDefine2
```


##### Request query
```
json :
{
  "defineCode" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "applyState" : "string",
    "defineCode" : "string",
    "defineId" : 0,
    "formId" : 0,
    "formName" : "string",
    "intanceId" : 0,
    "nodeCalssIfy" : "string",
    "nodeCode" : "string",
    "nodeId" : 0,
    "nodeName" : "string",
    "step" : "string",
    "taskId" : 0,
    "taskState" : "string",
    "url" : "string"
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getinstanceinfousingpost"></a>
### 获取 执行环节 及 环节实例 相关信息
```
POST /platform/wf/getInstanceInfo
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**instanceId**  <br>*required*|instanceId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«InstanceCurrent»](#3d6c7d83e07ee777662c1ea57535a30c)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getInstanceInfo
```


##### Request query
```
json :
{
  "instanceId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "instance" : 0,
    "planEndTime" : "string",
    "remainingTime" : 0,
    "statusApply" : "string",
    "statusApplyCn" : "string",
    "tasks" : [ {
      "nodeName" : "string",
      "status" : "string",
      "statusCn" : "string",
      "taskId" : 0
    } ]
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="gettaskelderusingpost"></a>
### 高龄初始数据准备
```
POST /platform/wf/getTaskElder
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineCode**  <br>*optional*|defineCode|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«TaskElderResponse»](#55b1729abdfcb9acf138c446a8351810)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getTaskElder
```


##### Request query
```
json :
{
  "defineCode" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "nodes" : [ {
      "code" : "string",
      "day" : 0.0,
      "name" : "string"
    } ],
    "sysDefault" : 0.0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="gettaskelder2usingpost"></a>
### dt2 获取流程存量环节信息
```
POST /platform/wf/getTaskElder2
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**defineCode**  <br>*required*|defineCode|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«TaskElderResponse»](#55b1729abdfcb9acf138c446a8351810)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getTaskElder2
```


##### Request query
```
json :
{
  "defineCode" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "nodes" : [ {
      "code" : "string",
      "day" : 0.0,
      "name" : "string"
    } ],
    "sysDefault" : 0.0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="gettaskformusingpost"></a>
### 获取取流程执行环节上得 表单
```
POST /platform/wf/getTaskForm
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**taskId**  <br>*required*|流程执行环节上得Id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«NodeFormResponse»](#6e8f3af68e33de828eb3180db6d8a8b8)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getTaskForm
```


##### Request query
```
json :
{
  "taskId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "applyState" : "string",
    "defineCode" : "string",
    "defineId" : 0,
    "formId" : 0,
    "formName" : "string",
    "intanceId" : 0,
    "nodeCalssIfy" : "string",
    "nodeCode" : "string",
    "nodeId" : 0,
    "nodeName" : "string",
    "step" : "string",
    "taskId" : 0,
    "taskState" : "string",
    "url" : "string"
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="gettaskform2usingpost"></a>
### dt2 获取取流程执行环节上得 表单
```
POST /platform/wf/getTaskForm2
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**taskId**  <br>*required*|流程执行环节上得Id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«NodeFormResponse»](#6e8f3af68e33de828eb3180db6d8a8b8)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getTaskForm2
```


##### Request query
```
json :
{
  "taskId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "applyState" : "string",
    "defineCode" : "string",
    "defineId" : 0,
    "formId" : 0,
    "formName" : "string",
    "intanceId" : 0,
    "nodeCalssIfy" : "string",
    "nodeCode" : "string",
    "nodeId" : 0,
    "nodeName" : "string",
    "step" : "string",
    "taskId" : 0,
    "taskState" : "string",
    "url" : "string"
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="getwfphrasesbytaskidusingpost"></a>
### 获取该环节上的短语
```
POST /platform/wf/getWfPhraseSByTaskId
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|流程环节id|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«List«WfPhraseTipResp»»](#7391e952a2adbfca14c6c01b26688489)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/getWfPhraseSByTaskId
```


##### Request query
```
json :
{
  "id" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : [ {
    "content" : "string",
    "id" : 0
  } ],
  "level" : "string",
  "msg" : "string"
}
```


<a name="listbytaskexcutetodorequestusingpost"></a>
### 待办件
```
POST /platform/wf/list
```


#### Description
分页


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[TaskExcuteToDoRequest](#taskexcutetodorequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«TaskExcuteResponse»](#9fd347b4f12a986be292f2d8e0b12ce2)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/list
```


##### Request body
```
json :
{
  "acceptTimeEnd" : "string",
  "acceptTimeStart" : "string",
  "classify" : "string",
  "cmpId" : 0,
  "code" : "string",
  "confirmTimeEnd" : "string",
  "confirmTimeStart" : "string",
  "expireDayEnd" : 0,
  "expireDayStart" : 0,
  "factTimeEnd" : "string",
  "factTimeStart" : "string",
  "fromType" : "string",
  "householdType" : "string",
  "itemName" : "string",
  "mobile" : "string",
  "name" : "string",
  "nodeName" : "string",
  "operate" : "string",
  "operator" : 0,
  "orgId" : 0,
  "pageNo" : 0,
  "pageSize" : 0,
  "personType" : "string",
  "processResult" : "string",
  "promiseTimeEnd" : "string",
  "promiseTimeStart" : "string",
  "regionCode" : "string",
  "relateId" : 0,
  "startTimeEnd" : "string",
  "startTimeStart" : "string",
  "status" : "string",
  "statusApply" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "acceptTime" : "[localdatetime](#localdatetime)",
    "applyId" : "string",
    "applyStartTime" : "[localdatetime](#localdatetime)",
    "applyType" : "string",
    "applyTypeCn" : "string",
    "categoryCode" : "string",
    "categoryId" : 0,
    "categoryName" : "string",
    "classify" : "string",
    "cmpName" : "string",
    "code" : "string",
    "confirmTime" : 0,
    "enjoyPolicy" : "string",
    "enjoyPolicyCn" : "string",
    "expireDay" : 0,
    "factEndTime" : "[localdatetime](#localdatetime)",
    "factStartTime" : "[localdatetime](#localdatetime)",
    "factTime" : "[localdatetime](#localdatetime)",
    "health" : "string",
    "homeAddr" : "string",
    "homeCn" : "string",
    "householdAddr" : "string",
    "householdType" : "string",
    "id" : 0,
    "itemName" : "string",
    "mobile" : "string",
    "name" : "string",
    "nodeAllowLength" : 0,
    "nodeId" : 0,
    "nodeName" : "string",
    "operator" : 0,
    "operatorName" : "string",
    "org1Name" : "string",
    "orgName" : "string",
    "orgShortName" : "string",
    "planEndTime" : "[localdatetime](#localdatetime)",
    "processResult" : "string",
    "promiseTime" : "[localdatetime](#localdatetime)",
    "regionCode" : "string",
    "regionHome" : "string",
    "shortName" : "string",
    "startTime" : "[localdatetime](#localdatetime)",
    "statusApply" : "string",
    "statusApplyCn" : "string",
    "statusTask" : "string",
    "statusTaskCn" : "string",
    "taskName" : "string",
    "timeRemaining" : 0,
    "wfDefineCode" : "string",
    "wfDefineId" : 0,
    "wfDefineName" : "string"
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="listbytaskexcutealldocumentsrequestusingpost"></a>
### 所有办件
```
POST /platform/wf/listAllDocuments
```


#### Description
分页


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[TaskExcuteToDoRequest](#taskexcutetodorequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«TaskExcuteResponse»](#9fd347b4f12a986be292f2d8e0b12ce2)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/listAllDocuments
```


##### Request body
```
json :
{
  "acceptTimeEnd" : "string",
  "acceptTimeStart" : "string",
  "classify" : "string",
  "cmpId" : 0,
  "code" : "string",
  "confirmTimeEnd" : "string",
  "confirmTimeStart" : "string",
  "expireDayEnd" : 0,
  "expireDayStart" : 0,
  "factTimeEnd" : "string",
  "factTimeStart" : "string",
  "fromType" : "string",
  "householdType" : "string",
  "itemName" : "string",
  "mobile" : "string",
  "name" : "string",
  "nodeName" : "string",
  "operate" : "string",
  "operator" : 0,
  "orgId" : 0,
  "pageNo" : 0,
  "pageSize" : 0,
  "personType" : "string",
  "processResult" : "string",
  "promiseTimeEnd" : "string",
  "promiseTimeStart" : "string",
  "regionCode" : "string",
  "relateId" : 0,
  "startTimeEnd" : "string",
  "startTimeStart" : "string",
  "status" : "string",
  "statusApply" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "acceptTime" : "[localdatetime](#localdatetime)",
    "applyId" : "string",
    "applyStartTime" : "[localdatetime](#localdatetime)",
    "applyType" : "string",
    "applyTypeCn" : "string",
    "categoryCode" : "string",
    "categoryId" : 0,
    "categoryName" : "string",
    "classify" : "string",
    "cmpName" : "string",
    "code" : "string",
    "confirmTime" : 0,
    "enjoyPolicy" : "string",
    "enjoyPolicyCn" : "string",
    "expireDay" : 0,
    "factEndTime" : "[localdatetime](#localdatetime)",
    "factStartTime" : "[localdatetime](#localdatetime)",
    "factTime" : "[localdatetime](#localdatetime)",
    "health" : "string",
    "homeAddr" : "string",
    "homeCn" : "string",
    "householdAddr" : "string",
    "householdType" : "string",
    "id" : 0,
    "itemName" : "string",
    "mobile" : "string",
    "name" : "string",
    "nodeAllowLength" : 0,
    "nodeId" : 0,
    "nodeName" : "string",
    "operator" : 0,
    "operatorName" : "string",
    "org1Name" : "string",
    "orgName" : "string",
    "orgShortName" : "string",
    "planEndTime" : "[localdatetime](#localdatetime)",
    "processResult" : "string",
    "promiseTime" : "[localdatetime](#localdatetime)",
    "regionCode" : "string",
    "regionHome" : "string",
    "shortName" : "string",
    "startTime" : "[localdatetime](#localdatetime)",
    "statusApply" : "string",
    "statusApplyCn" : "string",
    "statusTask" : "string",
    "statusTaskCn" : "string",
    "taskName" : "string",
    "timeRemaining" : 0,
    "wfDefineCode" : "string",
    "wfDefineId" : 0,
    "wfDefineName" : "string"
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="listbytaskexcutealreadydonerequestusingpost"></a>
### 办件中
```
POST /platform/wf/listAlreadyDone
```


#### Description
分页


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[TaskExcuteToDoRequest](#taskexcutetodorequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«TaskExcuteResponse»](#9fd347b4f12a986be292f2d8e0b12ce2)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/listAlreadyDone
```


##### Request body
```
json :
{
  "acceptTimeEnd" : "string",
  "acceptTimeStart" : "string",
  "classify" : "string",
  "cmpId" : 0,
  "code" : "string",
  "confirmTimeEnd" : "string",
  "confirmTimeStart" : "string",
  "expireDayEnd" : 0,
  "expireDayStart" : 0,
  "factTimeEnd" : "string",
  "factTimeStart" : "string",
  "fromType" : "string",
  "householdType" : "string",
  "itemName" : "string",
  "mobile" : "string",
  "name" : "string",
  "nodeName" : "string",
  "operate" : "string",
  "operator" : 0,
  "orgId" : 0,
  "pageNo" : 0,
  "pageSize" : 0,
  "personType" : "string",
  "processResult" : "string",
  "promiseTimeEnd" : "string",
  "promiseTimeStart" : "string",
  "regionCode" : "string",
  "relateId" : 0,
  "startTimeEnd" : "string",
  "startTimeStart" : "string",
  "status" : "string",
  "statusApply" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "acceptTime" : "[localdatetime](#localdatetime)",
    "applyId" : "string",
    "applyStartTime" : "[localdatetime](#localdatetime)",
    "applyType" : "string",
    "applyTypeCn" : "string",
    "categoryCode" : "string",
    "categoryId" : 0,
    "categoryName" : "string",
    "classify" : "string",
    "cmpName" : "string",
    "code" : "string",
    "confirmTime" : 0,
    "enjoyPolicy" : "string",
    "enjoyPolicyCn" : "string",
    "expireDay" : 0,
    "factEndTime" : "[localdatetime](#localdatetime)",
    "factStartTime" : "[localdatetime](#localdatetime)",
    "factTime" : "[localdatetime](#localdatetime)",
    "health" : "string",
    "homeAddr" : "string",
    "homeCn" : "string",
    "householdAddr" : "string",
    "householdType" : "string",
    "id" : 0,
    "itemName" : "string",
    "mobile" : "string",
    "name" : "string",
    "nodeAllowLength" : 0,
    "nodeId" : 0,
    "nodeName" : "string",
    "operator" : 0,
    "operatorName" : "string",
    "org1Name" : "string",
    "orgName" : "string",
    "orgShortName" : "string",
    "planEndTime" : "[localdatetime](#localdatetime)",
    "processResult" : "string",
    "promiseTime" : "[localdatetime](#localdatetime)",
    "regionCode" : "string",
    "regionHome" : "string",
    "shortName" : "string",
    "startTime" : "[localdatetime](#localdatetime)",
    "statusApply" : "string",
    "statusApplyCn" : "string",
    "statusTask" : "string",
    "statusTaskCn" : "string",
    "taskName" : "string",
    "timeRemaining" : 0,
    "wfDefineCode" : "string",
    "wfDefineId" : 0,
    "wfDefineName" : "string"
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="listbyinstanceidusingpost"></a>
### 查询 流程得实例过程记录
```
POST /platform/wf/listByInstanceId
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**instanceId**  <br>*required*|instanceId|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«ApplyTaskHistroy»](#a102b5832bc511eee0bf9d137b8e1188)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/listByInstanceId
```


##### Request query
```
json :
{
  "instanceId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "confirmTime" : 0,
    "consumeDay" : 0,
    "expireDay" : 0,
    "expireFlag" : "string",
    "expireFlagTask" : "string",
    "factTime" : "string",
    "homeCn" : "string",
    "planEndTime" : "string",
    "regionHome" : "string",
    "startTime" : "string",
    "task" : [ {
      "acceptTime" : "string",
      "applyTime" : 0,
      "classify" : "string",
      "expireFlag" : "string",
      "factEndTime" : "string",
      "factPerson" : 0,
      "factPersonName" : "string",
      "factStartTime" : "string",
      "formUrl" : "string",
      "fromType" : "string",
      "id" : 0,
      "lockBy" : 0,
      "lockFlag" : "string",
      "lockTime" : "string",
      "name" : "string",
      "nodeAllowLength" : 0,
      "operate" : "string",
      "orgId" : 0,
      "orgName" : "string",
      "planType" : "string",
      "planer" : "string",
      "planerUsers" : [ {
        "fullName" : "string",
        "nickname" : "string",
        "userId" : 0
      } ],
      "processOpinion" : "string",
      "processResult" : "string",
      "promiseTime" : "string",
      "relateId" : 0,
      "relateTable" : "string",
      "status" : "string",
      "statusCn" : "string",
      "step" : "string",
      "superTime" : 0,
      "taskName" : "string",
      "type" : "string",
      "wfDefineId" : 0,
      "wfInstanceId" : 0,
      "wfNodeId" : 0
    } ]
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listbyapplycompletedusingpost"></a>
### listByApplyCompleted
```
POST /platform/wf/listCompleted
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[ApplyCompletedRequest](#applycompletedrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«ApplyCompletedResponse»](#e286e5d0c181a699a02419fd35012139)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/listCompleted
```


##### Request body
```
json :
{
  "acceptTimeEnd" : "string",
  "acceptTimeStart" : "string",
  "cmpId" : 0,
  "code" : "string",
  "confirmTimeEnd" : "string",
  "confirmTimeStart" : "string",
  "expireDayEnd" : 0,
  "expireDayStart" : 0,
  "factTimeEnd" : "string",
  "factTimeStart" : "string",
  "householdType" : "string",
  "itemName" : "string",
  "mobile" : "string",
  "name" : "string",
  "pageNo" : 0,
  "pageSize" : 0,
  "processResult" : "string",
  "regionCode" : "string",
  "startTimeEnd" : "string",
  "startTimeStart" : "string",
  "status" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "code" : "string",
    "confirmTime" : "[localdatetime](#localdatetime)",
    "expireDay" : 0,
    "factTime" : "[localdatetime](#localdatetime)",
    "homeAddr" : "string",
    "homeCn" : "string",
    "householdType" : "string",
    "itemName" : "string",
    "itemNameCategory" : "string",
    "mobile" : "string",
    "name" : "string",
    "processResult" : "string",
    "regionHome" : "string",
    "startTime" : "[localdatetime](#localdatetime)"
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="deleterulesusingpost"></a>
### r删除规则
```
POST /platform/wf/rule/dels
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**ids**  <br>*required*|ids|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/rule/dels
```


##### Request query
```
json :
{
  "ids" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="getruleresponseusingpost"></a>
### r修改时查询
```
POST /platform/wf/rule/get
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«RuleResponse»](#79b67bb5a55c68217a5a20dd5c870a63)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/rule/get
```


##### Request query
```
json :
{
  "id" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "id" : 0,
    "name" : "string",
    "param" : "string",
    "remark" : "string",
    "returnValue" : "string",
    "sort" : 0,
    "status" : "string",
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listrulesusingpost"></a>
### r规则列表查询
```
POST /platform/wf/rule/list
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[RuleListRequest](#rulelistrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«RuleList»](#bc3039ebc5b8699270ca34db772f0609)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/rule/list
```


##### Request body
```
json :
{
  "categoryId" : 0,
  "code" : "string",
  "name" : "string",
  "pageNo" : 0,
  "pageSize" : 0,
  "status" : "string"
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "code" : "string",
    "id" : 0,
    "name" : "string",
    "optName" : "string",
    "optTime" : "string",
    "param" : "string",
    "remark" : "string",
    "returnValue" : "string",
    "sort" : 0,
    "status" : "string",
    "statusCn" : "string",
    "wfCategoryId" : 0
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```


<a name="saveorupdateruleusingpost"></a>
### r新增|修改规则
```
POST /platform/wf/rule/saveOrUpdate
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[RuleRequest](#rulerequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«RuleResponse»](#79b67bb5a55c68217a5a20dd5c870a63)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/rule/saveOrUpdate
```


##### Request body
```
json :
{
  "code" : "string",
  "id" : 0,
  "name" : "string",
  "param" : "string",
  "remark" : "string",
  "returnValue" : "string",
  "sort" : 0,
  "status" : "string",
  "wfCategoryId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "id" : 0,
    "name" : "string",
    "param" : "string",
    "remark" : "string",
    "returnValue" : "string",
    "sort" : 0,
    "status" : "string",
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="selectselectionusingpost"></a>
### 流程分类 下拉选择查询
```
POST /platform/wf/selectCategorySelection
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«List«WfCategory»»](#44e4e443bc695306d32959cfb327a2e1)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/selectCategorySelection
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : [ {
    "category" : "string",
    "code" : "string",
    "createdBy" : "string",
    "createtime" : "string",
    "description" : "string",
    "id" : 0,
    "name" : "string",
    "parentId" : 0,
    "sort" : 0,
    "updatedBy" : "string",
    "updatetime" : "string"
  } ],
  "level" : "string",
  "msg" : "string"
}
```


<a name="selectnodeselectionusingpost"></a>
### 流程环节 下拉选择查询
```
POST /platform/wf/selectNodeSelection
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«List«WfNode»»](#98111d22ecdd06a81937b38fd85a1ff2)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/selectNodeSelection
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : [ {
    "code" : "string",
    "createdBy" : 0,
    "createtime" : "string",
    "id" : 0,
    "name" : "string",
    "nodeJson" : "string",
    "nodeType" : "string",
    "updatedBy" : 0,
    "updatetime" : "string",
    "wfCategoryId" : 0,
    "wfDefineId" : 0
  } ],
  "level" : "string",
  "msg" : "string"
}
```


<a name="createorupdateusingpost"></a>
### t新增及修改业务实例表
```
POST /platform/wf/table/createOrUpdate
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[InstanceTableRequest](#instancetablerequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«InstanceTableResponse»](#516cae65caaf633b0519628654149170)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/table/createOrUpdate
```


##### Request body
```
json :
{
  "code" : "string",
  "id" : 0,
  "masterTable" : 0,
  "name" : "string",
  "remark" : "string",
  "sort" : 0,
  "tableName" : "string",
  "wfCategoryId" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "createFlag" : "string",
    "id" : 0,
    "masterFlag" : "string",
    "masterName" : "string",
    "masterTable" : 0,
    "name" : "string",
    "remark" : "string",
    "sort" : 0,
    "tableName" : "string",
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="deletetableusingpost"></a>
### t删除
```
POST /platform/wf/table/del
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/table/del
```


##### Request query
```
json :
{
  "id" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="deletetablesusingpost"></a>
### t删除
```
POST /platform/wf/table/dels
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**ids**  <br>*required*|ids|< integer (int64) > array(multi)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«boolean»](#b90525ac5b8e8c2412c03959981e29f6)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/table/dels
```


##### Request query
```
json :
{
  "ids" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : true,
  "level" : "string",
  "msg" : "string"
}
```


<a name="getinstancetableresponseusingpost"></a>
### t修改时详情
```
POST /platform/wf/table/get
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Query**|**id**  <br>*required*|id|integer (int64)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[ResponseInfo«InstanceTableResponse»](#516cae65caaf633b0519628654149170)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/table/get
```


##### Request query
```
json :
{
  "id" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "code" : 0,
  "data" : {
    "code" : "string",
    "createFlag" : "string",
    "id" : 0,
    "masterFlag" : "string",
    "masterName" : "string",
    "masterTable" : 0,
    "name" : "string",
    "remark" : "string",
    "sort" : 0,
    "tableName" : "string",
    "wfCategoryId" : 0
  },
  "level" : "string",
  "msg" : "string"
}
```


<a name="listinstancetableusingpost"></a>
### t列表分页
```
POST /platform/wf/table/list
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**request**  <br>*required*|request|[TableListRequest](#tablelistrequest)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|OK|[PageInfo«TableList»](#f38a317c5e0a5492ed131e49a6914ec1)|
|**201**|Created|No Content|
|**401**|Unauthorized|No Content|
|**403**|Forbidden|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json`


#### Produces

* `*/*`


#### Tags

* WF_办件中心


#### Example HTTP request

##### Request path
```
/platform/wf/table/list
```


##### Request body
```
json :
{
  "categoryId" : 0,
  "code" : "string",
  "createFlag" : "string",
  "name" : "string",
  "pageNo" : 0,
  "pageSize" : 0
}
```


#### Example HTTP response

##### Response 200
```
json :
{
  "endRow" : 0,
  "firstPage" : 0,
  "hasNextPage" : true,
  "hasPreviousPage" : true,
  "isFirstPage" : true,
  "isLastPage" : true,
  "lastPage" : 0,
  "list" : [ {
    "code" : "string",
    "createFlag" : "string",
    "createName" : "string",
    "createTime" : "string",
    "haveSon" : true,
    "id" : 0,
    "name" : "string",
    "optName" : "string",
    "optTime" : "string",
    "sons" : [ {
      "code" : "string",
      "createFlag" : "string",
      "createName" : "string",
      "createTime" : "string",
      "haveSon" : true,
      "id" : 0,
      "name" : "string",
      "optName" : "string",
      "optTime" : "string",
      "sons" : [ "..." ],
      "tableName" : "string"
    } ],
    "tableName" : "string"
  } ],
  "navigateFirstPage" : 0,
  "navigateLastPage" : 0,
  "navigatePages" : 0,
  "navigatepageNums" : [ 0 ],
  "nextPage" : 0,
  "pageNum" : 0,
  "pageSize" : 0,
  "pages" : 0,
  "prePage" : 0,
  "size" : 0,
  "startRow" : 0,
  "total" : 0
}
```




<a name="definitions"></a>
## Definitions

<a name="applycompletedrequest"></a>
### ApplyCompletedRequest

|Name|Description|Schema|
|---|---|---|
|**acceptTimeEnd**  <br>*optional*|接收时间 end  <br>**Example** : `"string"`|string (date-time)|
|**acceptTimeStart**  <br>*optional*|接收时间 start  <br>**Example** : `"string"`|string (date-time)|
|**cmpId**  <br>*optional*|办理单位  <br>**Example** : `0`|integer (int64)|
|**code**  <br>*optional*|办件编号  <br>**Example** : `"string"`|string|
|**confirmTimeEnd**  <br>*optional*|wf：承诺办结时间=流程定义时限（秒）  <br>**Example** : `"string"`|string (date-time)|
|**confirmTimeStart**  <br>*optional*|wf：承诺办结时间=流程定义时限（秒）  <br>**Example** : `"string"`|string (date-time)|
|**expireDayEnd**  <br>*optional*|超时天数  <br>**Example** : `0`|integer (int32)|
|**expireDayStart**  <br>*optional*|超时天数  <br>**Example** : `0`|integer (int32)|
|**factTimeEnd**  <br>*optional*|实际办理时间 end  <br>**Example** : `"string"`|string (date-time)|
|**factTimeStart**  <br>*optional*|实际办理时间 start  <br>**Example** : `"string"`|string (date-time)|
|**householdType**  <br>*optional*|户口性质  <br>**Example** : `"string"`|string|
|**itemName**  <br>*optional*|办理事项 code  <br>**Example** : `"string"`|string|
|**mobile**  <br>*optional*|申请人电话  <br>**Example** : `"string"`|string|
|**name**  <br>*optional*|申请人  <br>**Example** : `"string"`|string|
|**pageNo**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**processResult**  <br>*optional*|办件结果  <br>**Example** : `"string"`|string|
|**regionCode**  <br>*optional*|所在地区  <br>**Example** : `"string"`|string|
|**startTimeEnd**  <br>*optional*|开始时间  <br>**Example** : `"string"`|string (date-time)|
|**startTimeStart**  <br>*optional*|开始时间  <br>**Example** : `"string"`|string (date-time)|
|**status**  <br>*optional*|**Example** : `"string"`|string|


<a name="applycompletedresponse"></a>
### ApplyCompletedResponse

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|办件编号  <br>**Example** : `"string"`|string|
|**confirmTime**  <br>*optional*|wf：承诺办结时间=流程定义时限（秒）  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**expireDay**  <br>*optional*|超时天数  <br>**Example** : `0`|integer (int32)|
|**factTime**  <br>*optional*|实际办理时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**homeAddr**  <br>*optional*|**Example** : `"string"`|string|
|**homeCn**  <br>*optional*|**Example** : `"string"`|string|
|**householdType**  <br>*optional*|户口性质  <br>**Example** : `"string"`|string|
|**itemName**  <br>*optional*|办理事项 code  <br>**Example** : `"string"`|string|
|**itemNameCategory**  <br>*optional*|办理事项 中文  <br>**Example** : `"string"`|string|
|**mobile**  <br>*optional*|申请人电话  <br>**Example** : `"string"`|string|
|**name**  <br>*optional*|申请人  <br>**Example** : `"string"`|string|
|**processResult**  <br>*optional*|办件结果  <br>**Example** : `"string"`|string|
|**regionHome**  <br>*optional*|所在地区  <br>**Example** : `"string"`|string|
|**startTime**  <br>*optional*|开始时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|


<a name="applytaskhistroy"></a>
### ApplyTaskHistroy

|Name|Description|Schema|
|---|---|---|
|**confirmTime**  <br>*optional*|承诺办结时长  <br>**Example** : `0`|integer (int32)|
|**consumeDay**  <br>*optional*|耗时时间，多少天  <br>**Example** : `0`|integer (int32)|
|**expireDay**  <br>*optional*|超期时间，多少天  <br>**Example** : `0`|integer (int32)|
|**expireFlag**  <br>*optional*|是否超期，超期yes，没超期no  <br>**Example** : `"string"`|string|
|**expireFlagTask**  <br>*optional*|超期得项  <br>**Example** : `"string"`|string|
|**factTime**  <br>*optional*|实际办结时间  <br>**Example** : `"string"`|string (date-time)|
|**homeCn**  <br>*optional*|**Example** : `"string"`|string|
|**planEndTime**  <br>*optional*|计划结束时间  <br>**Example** : `"string"`|string (date-time)|
|**regionHome**  <br>*optional*|**Example** : `"string"`|string|
|**startTime**  <br>*optional*|开始时间  <br>**Example** : `"string"`|string (date-time)|
|**task**  <br>*optional*|**Example** : `[ "[taskhistoryresponse](#taskhistoryresponse)" ]`|< [TaskHistoryResponse](#taskhistoryresponse) > array|


<a name="couformrequest"></a>
### COUFormRequest

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|不可修改  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**param**  <br>*optional*|参数  <br>**Example** : `"string"`|string|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**url**  <br>*optional*|连接地址  <br>**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|不可修改  <br>**Example** : `0`|integer (int64)|


<a name="categorylist"></a>
### CategoryList

|Name|Description|Schema|
|---|---|---|
|**category**  <br>*optional*|分类节点normal，流程定义define，流程分类rule（流程实例表；流程表单；流程规则；流程定义浏览-流程定义/流程历史版本）  <br>**Example** : `"string"`|string|
|**categoryCn**  <br>*optional*|**Example** : `"string"`|string|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createName**  <br>*optional*|创建人  <br>**Example** : `"string"`|string|
|**createTime**  <br>*optional*|创建时间  <br>**Example** : `"string"`|string (date-time)|
|**description**  <br>*optional*|说明  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**optName**  <br>*optional*|最近操作人  <br>**Example** : `"string"`|string|
|**optTime**  <br>*optional*|最近更新时间  <br>**Example** : `"string"`|string (date-time)|
|**parentId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="categorylistrequest"></a>
### CategoryListRequest

|Name|Description|Schema|
|---|---|---|
|**category**  <br>*optional*|多个时用，隔开  <br>**Example** : `"string"`|string|
|**code**  <br>*optional*|多个时用，隔开  <br>**Example** : `"string"`|string|
|**name**  <br>*optional*|多个时用，隔开  <br>**Example** : `"string"`|string|
|**pageNo**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**parentId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="categoryresponse"></a>
### CategoryResponse

|Name|Description|Schema|
|---|---|---|
|**category**  <br>*optional*|分类节点normal，流程定义define，流程分类rule（流程实例表；流程表单；流程规则；流程定义浏览-流程定义/流程历史版本）  <br>**Example** : `"string"`|string|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**description**  <br>*optional*|说明  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**parentId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|


<a name="categorytree"></a>
### CategoryTree

|Name|Description|Schema|
|---|---|---|
|**category**  <br>*optional*|分类节点normal，流程定义define，流程分类rule（流程实例表；流程表单；流程规则；流程定义浏览-流程定义/流程历史版本）  <br>**Example** : `"string"`|string|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**parentId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**sons**  <br>*optional*|**Example** : `[ "[categorytree](#categorytree)" ]`|< [CategoryTree](#categorytree) > array|


<a name="definebycategoryloginuserresponse"></a>
### DefineByCategoryLoginUserResponse

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**form**  <br>*optional*|第一环节表单  <br>**Example** : `"[wfform](#wfform)"`|[WfForm](#wfform)|
|**id**  <br>*optional*|定义id  <br>**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**node**  <br>*optional*|第一环节表单  <br>**Example** : `"[wfnode](#wfnode)"`|[WfNode](#wfnode)|
|**wfCategoryId**  <br>*optional*|类别ID  <br>**Example** : `0`|integer (int64)|


<a name="formlist"></a>
### FormList

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createName**  <br>*optional*|创建人  <br>**Example** : `"string"`|string|
|**createTime**  <br>*optional*|创建时间  <br>**Example** : `"string"`|string (date-time)|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**optName**  <br>*optional*|最近操作人  <br>**Example** : `"string"`|string|
|**optTime**  <br>*optional*|最近更新时间  <br>**Example** : `"string"`|string (date-time)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**url**  <br>*optional*|连接地址  <br>**Example** : `"string"`|string|


<a name="formlistrequest"></a>
### FormListRequest

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**pageNo**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="formresponse"></a>
### FormResponse

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**param**  <br>*optional*|参数  <br>**Example** : `"string"`|string|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**url**  <br>*optional*|连接地址  <br>**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="instancecurrent"></a>
### InstanceCurrent

|Name|Description|Schema|
|---|---|---|
|**instance**  <br>*optional*|**Example** : `0`|integer (int64)|
|**planEndTime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**remainingTime**  <br>*optional*|剩余时间  <br>**Example** : `0`|integer (int64)|
|**statusApply**  <br>*optional*|**Example** : `"string"`|string|
|**statusApplyCn**  <br>*optional*|**Example** : `"string"`|string|
|**tasks**  <br>*optional*|**Example** : `[ "[taskcurrent](#taskcurrent)" ]`|< [TaskCurrent](#taskcurrent) > array|


<a name="instancetablerequest"></a>
### InstanceTableRequest

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**masterTable**  <br>*optional*|所属主表  <br>**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**tableName**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="instancetableresponse"></a>
### InstanceTableResponse

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createFlag**  <br>*optional*|是否已生成表，已生成yes，未生成no  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**masterFlag**  <br>*optional*|主从表  <br>**Example** : `"string"`|string|
|**masterName**  <br>*optional*|主表名称  <br>**Example** : `"string"`|string|
|**masterTable**  <br>*optional*|所属主表  <br>**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**tableName**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="jsonnode"></a>
### JsonNode

|Name|Description|Schema|
|---|---|---|
|**array**  <br>*optional*|**Example** : `true`|boolean|
|**bigDecimal**  <br>*optional*|**Example** : `true`|boolean|
|**bigInteger**  <br>*optional*|**Example** : `true`|boolean|
|**binary**  <br>*optional*|**Example** : `true`|boolean|
|**boolean**  <br>*optional*|**Example** : `true`|boolean|
|**containerNode**  <br>*optional*|**Example** : `true`|boolean|
|**double**  <br>*optional*|**Example** : `true`|boolean|
|**float**  <br>*optional*|**Example** : `true`|boolean|
|**floatingPointNumber**  <br>*optional*|**Example** : `true`|boolean|
|**int**  <br>*optional*|**Example** : `true`|boolean|
|**integralNumber**  <br>*optional*|**Example** : `true`|boolean|
|**long**  <br>*optional*|**Example** : `true`|boolean|
|**missingNode**  <br>*optional*|**Example** : `true`|boolean|
|**nodeType**  <br>*optional*|**Example** : `"string"`|enum (ARRAY, BINARY, BOOLEAN, MISSING, NULL, NUMBER, OBJECT, POJO, STRING)|
|**null**  <br>*optional*|**Example** : `true`|boolean|
|**number**  <br>*optional*|**Example** : `true`|boolean|
|**object**  <br>*optional*|**Example** : `true`|boolean|
|**pojo**  <br>*optional*|**Example** : `true`|boolean|
|**short**  <br>*optional*|**Example** : `true`|boolean|
|**textual**  <br>*optional*|**Example** : `true`|boolean|
|**valueNode**  <br>*optional*|**Example** : `true`|boolean|


<a name="nfbresponse"></a>
### NFBResponse

|Name|Description|Schema|
|---|---|---|
|**auditAdvance**  <br>*optional*|**Example** : `"string"`|string|
|**buttons**  <br>*optional*|**Example** : `[ [ "[wfnodebutton](#wfnodebutton)" ] ]`|< < [WfNodeButton](#wfnodebutton) > array > array|
|**defineId**  <br>*optional*|定义ID  <br>**Example** : `0`|integer (int64)|
|**nodeId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**phrases**  <br>*optional*|**Example** : `[ "[wfphrase](#wfphrase)" ]`|< [WfPhrase](#wfphrase) > array|
|**resourceConfigs**  <br>*optional*|**Example** : `[ "[resourceconfig](#resourceconfig)" ]`|< [ResourceConfig](#resourceconfig) > array|
|**step**  <br>*optional*|当前步骤  <br>**Example** : `"string"`|string|


<a name="nfbtaskidresponse"></a>
### NFBTaskIdResponse

|Name|Description|Schema|
|---|---|---|
|**auditAdvance**  <br>*optional*|**Example** : `"string"`|string|
|**buttons**  <br>*optional*|**Example** : `[ [ "[wfnodebutton](#wfnodebutton)" ] ]`|< < [WfNodeButton](#wfnodebutton) > array > array|
|**defineId**  <br>*optional*|定义ID  <br>**Example** : `0`|integer (int64)|
|**nodeId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**phrases**  <br>*optional*|**Example** : `[ "[wfphrase](#wfphrase)" ]`|< [WfPhrase](#wfphrase) > array|
|**processOpinion**  <br>*optional*|办理意见  <br>**Example** : `"string"`|string|
|**processResult**  <br>*optional*|**Example** : `"string"`|string|
|**resourceConfigs**  <br>*optional*|**Example** : `[ "[resourceconfig](#resourceconfig)" ]`|< [ResourceConfig](#resourceconfig) > array|
|**step**  <br>*optional*|当前步骤  <br>**Example** : `"string"`|string|
|**taskId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="node"></a>
### Node

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**day**  <br>*optional*|**Example** : `0.0`|number (double)|
|**name**  <br>*optional*|**Example** : `"string"`|string|


<a name="nodeformresponse"></a>
### NodeFormResponse

|Name|Description|Schema|
|---|---|---|
|**applyState**  <br>*optional*|**Example** : `"string"`|string|
|**defineCode**  <br>*optional*|**Example** : `"string"`|string|
|**defineId**  <br>*optional*|流程定义ID  <br>**Example** : `0`|integer (int64)|
|**formId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**formName**  <br>*optional*|**Example** : `"string"`|string|
|**intanceId**  <br>*optional*|流程实例ID  <br>**Example** : `0`|integer (int64)|
|**nodeCalssIfy**  <br>*optional*|node.calssify  <br>**Example** : `"string"`|string|
|**nodeCode**  <br>*optional*|**Example** : `"string"`|string|
|**nodeId**  <br>*optional*|流程环节ID  <br>**Example** : `0`|integer (int64)|
|**nodeName**  <br>*optional*|**Example** : `"string"`|string|
|**step**  <br>*optional*|当前执行得 步骤环节  <br>**Example** : `"string"`|string|
|**taskId**  <br>*optional*|流程环节执行ID  <br>**Example** : `0`|integer (int64)|
|**taskState**  <br>*optional*|**Example** : `"string"`|string|
|**url**  <br>*optional*|**Example** : `"string"`|string|


<a name="e286e5d0c181a699a02419fd35012139"></a>
### PageInfo«ApplyCompletedResponse»

|Name|Description|Schema|
|---|---|---|
|**endRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**firstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**hasNextPage**  <br>*optional*|**Example** : `true`|boolean|
|**hasPreviousPage**  <br>*optional*|**Example** : `true`|boolean|
|**isFirstPage**  <br>*optional*|**Example** : `true`|boolean|
|**isLastPage**  <br>*optional*|**Example** : `true`|boolean|
|**lastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**list**  <br>*optional*|**Example** : `[ "[applycompletedresponse](#applycompletedresponse)" ]`|< [ApplyCompletedResponse](#applycompletedresponse) > array|
|**navigateFirstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigateLastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatePages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatepageNums**  <br>*optional*|**Example** : `[ 0 ]`|< integer (int32) > array|
|**nextPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**prePage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**size**  <br>*optional*|**Example** : `0`|integer (int32)|
|**startRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**total**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="5b69ad18bd238578fdedea7132d675a9"></a>
### PageInfo«CategoryList»

|Name|Description|Schema|
|---|---|---|
|**endRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**firstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**hasNextPage**  <br>*optional*|**Example** : `true`|boolean|
|**hasPreviousPage**  <br>*optional*|**Example** : `true`|boolean|
|**isFirstPage**  <br>*optional*|**Example** : `true`|boolean|
|**isLastPage**  <br>*optional*|**Example** : `true`|boolean|
|**lastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**list**  <br>*optional*|**Example** : `[ "[categorylist](#categorylist)" ]`|< [CategoryList](#categorylist) > array|
|**navigateFirstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigateLastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatePages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatepageNums**  <br>*optional*|**Example** : `[ 0 ]`|< integer (int32) > array|
|**nextPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**prePage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**size**  <br>*optional*|**Example** : `0`|integer (int32)|
|**startRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**total**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="c84eb7bb15deed50e0ea89512df8fd7a"></a>
### PageInfo«FormList»

|Name|Description|Schema|
|---|---|---|
|**endRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**firstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**hasNextPage**  <br>*optional*|**Example** : `true`|boolean|
|**hasPreviousPage**  <br>*optional*|**Example** : `true`|boolean|
|**isFirstPage**  <br>*optional*|**Example** : `true`|boolean|
|**isLastPage**  <br>*optional*|**Example** : `true`|boolean|
|**lastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**list**  <br>*optional*|**Example** : `[ "[formlist](#formlist)" ]`|< [FormList](#formlist) > array|
|**navigateFirstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigateLastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatePages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatepageNums**  <br>*optional*|**Example** : `[ 0 ]`|< integer (int32) > array|
|**nextPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**prePage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**size**  <br>*optional*|**Example** : `0`|integer (int32)|
|**startRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**total**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="bc3039ebc5b8699270ca34db772f0609"></a>
### PageInfo«RuleList»

|Name|Description|Schema|
|---|---|---|
|**endRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**firstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**hasNextPage**  <br>*optional*|**Example** : `true`|boolean|
|**hasPreviousPage**  <br>*optional*|**Example** : `true`|boolean|
|**isFirstPage**  <br>*optional*|**Example** : `true`|boolean|
|**isLastPage**  <br>*optional*|**Example** : `true`|boolean|
|**lastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**list**  <br>*optional*|**Example** : `[ "[rulelist](#rulelist)" ]`|< [RuleList](#rulelist) > array|
|**navigateFirstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigateLastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatePages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatepageNums**  <br>*optional*|**Example** : `[ 0 ]`|< integer (int32) > array|
|**nextPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**prePage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**size**  <br>*optional*|**Example** : `0`|integer (int32)|
|**startRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**total**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="f38a317c5e0a5492ed131e49a6914ec1"></a>
### PageInfo«TableList»

|Name|Description|Schema|
|---|---|---|
|**endRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**firstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**hasNextPage**  <br>*optional*|**Example** : `true`|boolean|
|**hasPreviousPage**  <br>*optional*|**Example** : `true`|boolean|
|**isFirstPage**  <br>*optional*|**Example** : `true`|boolean|
|**isLastPage**  <br>*optional*|**Example** : `true`|boolean|
|**lastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**list**  <br>*optional*|**Example** : `[ "[tablelist](#tablelist)" ]`|< [TableList](#tablelist) > array|
|**navigateFirstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigateLastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatePages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatepageNums**  <br>*optional*|**Example** : `[ 0 ]`|< integer (int32) > array|
|**nextPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**prePage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**size**  <br>*optional*|**Example** : `0`|integer (int32)|
|**startRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**total**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="9fd347b4f12a986be292f2d8e0b12ce2"></a>
### PageInfo«TaskExcuteResponse»

|Name|Description|Schema|
|---|---|---|
|**endRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**firstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**hasNextPage**  <br>*optional*|**Example** : `true`|boolean|
|**hasPreviousPage**  <br>*optional*|**Example** : `true`|boolean|
|**isFirstPage**  <br>*optional*|**Example** : `true`|boolean|
|**isLastPage**  <br>*optional*|**Example** : `true`|boolean|
|**lastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**list**  <br>*optional*|**Example** : `[ "[taskexcuteresponse](#taskexcuteresponse)" ]`|< [TaskExcuteResponse](#taskexcuteresponse) > array|
|**navigateFirstPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigateLastPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatePages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**navigatepageNums**  <br>*optional*|**Example** : `[ 0 ]`|< integer (int32) > array|
|**nextPage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pages**  <br>*optional*|**Example** : `0`|integer (int32)|
|**prePage**  <br>*optional*|**Example** : `0`|integer (int32)|
|**size**  <br>*optional*|**Example** : `0`|integer (int32)|
|**startRow**  <br>*optional*|**Example** : `0`|integer (int32)|
|**total**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="planeruser"></a>
### PlanerUser

|Name|Description|Schema|
|---|---|---|
|**fullName**  <br>*optional*|**Example** : `"string"`|string|
|**nickname**  <br>*optional*|**Example** : `"string"`|string|
|**userId**  <br>*optional*|用户ID  <br>**Example** : `0`|integer (int64)|


<a name="resourceconfig"></a>
### ResourceConfig

|Name|Description|Schema|
|---|---|---|
|**abbr**  <br>*optional*|**Example** : `"string"`|string|
|**classifyCode**  <br>*optional*|**Example** : `"string"`|string|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createdBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**createtime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**edgeFlag**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**itemCode**  <br>*optional*|**Example** : `"string"`|string|
|**itemName**  <br>*optional*|**Example** : `"string"`|string|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**needFlag**  <br>*optional*|**Example** : `"string"`|string|
|**operationName**  <br>*optional*|**Example** : `"string"`|string|
|**operationTime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**required**  <br>*optional*|**Example** : `"string"`|string|
|**resourceNum**  <br>*optional*|**Example** : `0`|integer (int32)|
|**resourceType**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**type**  <br>*optional*|**Example** : `"string"`|string|
|**updatedBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**updatetime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**uploadAfter**  <br>*optional*|**Example** : `"string"`|string|
|**uploadBefore**  <br>*optional*|**Example** : `"string"`|string|


<a name="a102b5832bc511eee0bf9d137b8e1188"></a>
### ResponseInfo«ApplyTaskHistroy»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[applytaskhistroy](#applytaskhistroy)"`|[ApplyTaskHistroy](#applytaskhistroy)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="f90fe645eab9d57adf7b4849acfd7e07"></a>
### ResponseInfo«CategoryResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[categoryresponse](#categoryresponse)"`|[CategoryResponse](#categoryresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="28805b7ee3689b4c908cbc7c4f3a44dd"></a>
### ResponseInfo«DefineByCategoryLoginUserResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[definebycategoryloginuserresponse](#definebycategoryloginuserresponse)"`|[DefineByCategoryLoginUserResponse](#definebycategoryloginuserresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="c9813be7e9482bf9a67e25947141c22e"></a>
### ResponseInfo«FormResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[formresponse](#formresponse)"`|[FormResponse](#formresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="3d6c7d83e07ee777662c1ea57535a30c"></a>
### ResponseInfo«InstanceCurrent»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[instancecurrent](#instancecurrent)"`|[InstanceCurrent](#instancecurrent)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="516cae65caaf633b0519628654149170"></a>
### ResponseInfo«InstanceTableResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[instancetableresponse](#instancetableresponse)"`|[InstanceTableResponse](#instancetableresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="7f8ad490921277f1e5b8c5d7dcb99b1d"></a>
### ResponseInfo«JsonNode»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[jsonnode](#jsonnode)"`|[JsonNode](#jsonnode)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="9fd0ae6d04a71dca300306ec1dd365e9"></a>
### ResponseInfo«List«CategoryTree»»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `[ "[categorytree](#categorytree)" ]`|< [CategoryTree](#categorytree) > array|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="53e4d64373e9657c733bc65be7097ccf"></a>
### ResponseInfo«List«JsonNode»»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `[ "[jsonnode](#jsonnode)" ]`|< [JsonNode](#jsonnode) > array|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="44e4e443bc695306d32959cfb327a2e1"></a>
### ResponseInfo«List«WfCategory»»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `[ "[wfcategory](#wfcategory)" ]`|< [WfCategory](#wfcategory) > array|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="98111d22ecdd06a81937b38fd85a1ff2"></a>
### ResponseInfo«List«WfNode»»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `[ "[wfnode](#wfnode)" ]`|< [WfNode](#wfnode) > array|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="7391e952a2adbfca14c6c01b26688489"></a>
### ResponseInfo«List«WfPhraseTipResp»»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `[ "[wfphrasetipresp](#wfphrasetipresp)" ]`|< [WfPhraseTipResp](#wfphrasetipresp) > array|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="08def5ae558adc442e84cb64afbe0a05"></a>
### ResponseInfo«NFBResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[nfbresponse](#nfbresponse)"`|[NFBResponse](#nfbresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="3b95bab42c564baf8616bc330c49ea95"></a>
### ResponseInfo«NFBTaskIdResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[nfbtaskidresponse](#nfbtaskidresponse)"`|[NFBTaskIdResponse](#nfbtaskidresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="6e8f3af68e33de828eb3180db6d8a8b8"></a>
### ResponseInfo«NodeFormResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[nodeformresponse](#nodeformresponse)"`|[NodeFormResponse](#nodeformresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="79b67bb5a55c68217a5a20dd5c870a63"></a>
### ResponseInfo«RuleResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[ruleresponse](#ruleresponse)"`|[RuleResponse](#ruleresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="55b1729abdfcb9acf138c446a8351810"></a>
### ResponseInfo«TaskElderResponse»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"[taskelderresponse](#taskelderresponse)"`|[TaskElderResponse](#taskelderresponse)|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="b90525ac5b8e8c2412c03959981e29f6"></a>
### ResponseInfo«boolean»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `true`|boolean|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="7d3c7d7aed68f1c00e3e41f51fb29121"></a>
### ResponseInfo«object»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"object"`|object|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="8129a33ccb6b6d98fda6a350acfd9219"></a>
### ResponseInfo«string»

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `0`|integer (int32)|
|**data**  <br>*optional*|**Example** : `"string"`|string|
|**level**  <br>*optional*|**Example** : `"string"`|string|
|**msg**  <br>*optional*|**Example** : `"string"`|string|


<a name="rulelist"></a>
### RuleList

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|规则编码 不可修改  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|名称  <br>**Example** : `"string"`|string|
|**optName**  <br>*optional*|最近操作人  <br>**Example** : `"string"`|string|
|**optTime**  <br>*optional*|最近更新时间  <br>**Example** : `"string"`|string (date-time)|
|**param**  <br>*optional*|输入参数  <br>**Example** : `"string"`|string|
|**remark**  <br>*optional*|备注  <br>**Example** : `"string"`|string|
|**returnValue**  <br>*optional*|返回值  <br>**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**statusCn**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|流程树ID 不可修改  <br>**Example** : `0`|integer (int64)|


<a name="rulelistrequest"></a>
### RuleListRequest

|Name|Description|Schema|
|---|---|---|
|**categoryId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**pageNo**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|


<a name="rulerequest"></a>
### RuleRequest

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|规则编码 不可修改  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|名称  <br>**Example** : `"string"`|string|
|**param**  <br>*optional*|输入参数  <br>**Example** : `"string"`|string|
|**remark**  <br>*optional*|备注  <br>**Example** : `"string"`|string|
|**returnValue**  <br>*optional*|返回值  <br>**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|流程树ID 不可修改  <br>**Example** : `0`|integer (int64)|


<a name="ruleresponse"></a>
### RuleResponse

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|规则编码 不可修改  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|名称  <br>**Example** : `"string"`|string|
|**param**  <br>*optional*|输入参数  <br>**Example** : `"string"`|string|
|**remark**  <br>*optional*|备注  <br>**Example** : `"string"`|string|
|**returnValue**  <br>*optional*|返回值  <br>**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|流程树ID 不可修改  <br>**Example** : `0`|integer (int64)|


<a name="sucategoryrequest"></a>
### SUCategoryRequest

|Name|Description|Schema|
|---|---|---|
|**category**  <br>*optional*|不可修改 分类节点normal，流程定义define，流程分类rule（流程实例表；流程表单；流程规则；流程定义浏览-流程定义/流程历史版本）  <br>**Example** : `"string"`|string|
|**code**  <br>*optional*|不可修改  <br>**Example** : `"string"`|string|
|**description**  <br>*optional*|说明  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**parentId**  <br>*optional*|不可修改  <br>**Example** : `0`|integer (int64)|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|


<a name="tablelist"></a>
### TableList

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createFlag**  <br>*optional*|**Example** : `"string"`|string|
|**createName**  <br>*optional*|创建人  <br>**Example** : `"string"`|string|
|**createTime**  <br>*optional*|创建时间  <br>**Example** : `"string"`|string (date-time)|
|**haveSon**  <br>*optional*|**Example** : `true`|boolean|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**optName**  <br>*optional*|最近操作人  <br>**Example** : `"string"`|string|
|**optTime**  <br>*optional*|最近更新时间  <br>**Example** : `"string"`|string (date-time)|
|**sons**  <br>*optional*|**Example** : `[ "[tablelist](#tablelist)" ]`|< [TableList](#tablelist) > array|
|**tableName**  <br>*optional*|**Example** : `"string"`|string|


<a name="tablelistrequest"></a>
### TableListRequest

|Name|Description|Schema|
|---|---|---|
|**categoryId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createFlag**  <br>*optional*|是否已生成表，已生成yes，未生成no  <br>**Example** : `"string"`|string|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**pageNo**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|


<a name="taskcurrent"></a>
### TaskCurrent

|Name|Description|Schema|
|---|---|---|
|**nodeName**  <br>*optional*|**Example** : `"string"`|string|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**statusCn**  <br>*optional*|**Example** : `"string"`|string|
|**taskId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="taskelderresponse"></a>
### TaskElderResponse

|Name|Description|Schema|
|---|---|---|
|**nodes**  <br>*optional*|**Example** : `[ "[node](#node)" ]`|< [Node](#node) > array|
|**sysDefault**  <br>*optional*|**Example** : `0.0`|number (double)|


<a name="taskexcuteresponse"></a>
### TaskExcuteResponse

|Name|Description|Schema|
|---|---|---|
|**acceptTime**  <br>*optional*|接受时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**applyId**  <br>*optional*|**Example** : `"string"`|string|
|**applyStartTime**  <br>*optional*|apply 开始时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**applyType**  <br>*optional*|民政对象类型，低保basic_living，特困special_difficulty，临时救助temp_assist，孤儿救助orphan_assist，高龄补贴h_age_allowance，三四级精神智力残疾补贴t_f_mental_dibt，困难残疾人生活补贴difficulty_dibt，重度残疾人护理补贴serious_dibt_nurse  <br>**Example** : `"string"`|string|
|**applyTypeCn**  <br>*optional*|**Example** : `"string"`|string|
|**categoryCode**  <br>*optional*|流程定义大类code  <br>**Example** : `"string"`|string|
|**categoryId**  <br>*optional*|流程定义大类id  <br>**Example** : `0`|integer (int64)|
|**categoryName**  <br>*optional*|大类名称  <br>**Example** : `"string"`|string|
|**classify**  <br>*optional*|任务分类  <br>**Example** : `"string"`|string|
|**cmpName**  <br>*optional*|单位全称  <br>**Example** : `"string"`|string|
|**code**  <br>*optional*|办件编号  <br>**Example** : `"string"`|string|
|**confirmTime**  <br>*optional*|**Example** : `0`|integer (int32)|
|**enjoyPolicy**  <br>*optional*|**Example** : `"string"`|string|
|**enjoyPolicyCn**  <br>*optional*|**Example** : `"string"`|string|
|**expireDay**  <br>*optional*|**Example** : `0`|integer (int32)|
|**factEndTime**  <br>*optional*|apply.plan_end_time 实际结束时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**factStartTime**  <br>*optional*|实际开始时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**factTime**  <br>*optional*|实际办结时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**health**  <br>*optional*|**Example** : `"string"`|string|
|**homeAddr**  <br>*optional*|**Example** : `"string"`|string|
|**homeCn**  <br>*optional*|**Example** : `"string"`|string|
|**householdAddr**  <br>*optional*|籍贯-中文合并  <br>**Example** : `"string"`|string|
|**householdType**  <br>*optional*|户口性质  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|流程执行id  <br>**Example** : `0`|integer (int64)|
|**itemName**  <br>*optional*|办理事项  <br>**Example** : `"string"`|string|
|**mobile**  <br>*optional*|电话  <br>**Example** : `"string"`|string|
|**name**  <br>*optional*|申请人  <br>**Example** : `"string"`|string|
|**nodeAllowLength**  <br>*optional*|节点许可长  <br>**Example** : `0`|integer (int32)|
|**nodeId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**nodeName**  <br>*optional*|**Example** : `"string"`|string|
|**operator**  <br>*optional*|办理人  <br>**Example** : `0`|integer (int64)|
|**operatorName**  <br>*optional*|办理人 name  <br>**Example** : `"string"`|string|
|**org1Name**  <br>*optional*|收件机构：org_id（第一环节的机构）  <br>**Example** : `"string"`|string|
|**orgName**  <br>*optional*|机构名称  <br>**Example** : `"string"`|string|
|**orgShortName**  <br>*optional*|机构简称  <br>**Example** : `"string"`|string|
|**planEndTime**  <br>*optional*|Apply 计划结束时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**processResult**  <br>*optional*|办理结果，正常结束end，取消结束cancel_end，超期结束time_out  <br>**Example** : `"string"`|string|
|**promiseTime**  <br>*optional*|承诺时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**regionCode**  <br>*optional*|区域  <br>**Example** : `"string"`|string|
|**regionHome**  <br>*optional*|区县-街镇-村社  <br>**Example** : `"string"`|string|
|**shortName**  <br>*optional*|单位简称  <br>**Example** : `"string"`|string|
|**startTime**  <br>*optional*|开始时间  <br>**Example** : `"[localdatetime](#localdatetime)"`|[LocalDateTime](#localdatetime)|
|**statusApply**  <br>*optional*|办理中，暂停(复议期)，结束  <br>**Example** : `"string"`|string|
|**statusApplyCn**  <br>*optional*|**Example** : `"string"`|string|
|**statusTask**  <br>*optional*|状态,办理中doing/暂停pause/完成finish  <br>**Example** : `"string"`|string|
|**statusTaskCn**  <br>*optional*|**Example** : `"string"`|string|
|**taskName**  <br>*optional*|申请人  <br>**Example** : `"string"`|string|
|**timeRemaining**  <br>*optional*|剩余时间  <br>**Example** : `0`|integer (int64)|
|**wfDefineCode**  <br>*optional*|流程定义Code  <br>**Example** : `"string"`|string|
|**wfDefineId**  <br>*optional*|流程定义ID  <br>**Example** : `0`|integer (int64)|
|**wfDefineName**  <br>*optional*|流程定义名称  <br>**Example** : `"string"`|string|


<a name="taskexcutetodorequest"></a>
### TaskExcuteToDoRequest

|Name|Description|Schema|
|---|---|---|
|**acceptTimeEnd**  <br>*optional*|接收时间 end  <br>**Example** : `"string"`|string (date-time)|
|**acceptTimeStart**  <br>*optional*|接收时间 start  <br>**Example** : `"string"`|string (date-time)|
|**classify**  <br>*optional*|任务分类  <br>**Example** : `"string"`|string|
|**cmpId**  <br>*optional*|机构所属单位  <br>**Example** : `0`|integer (int64)|
|**code**  <br>*optional*|编号  <br>**Example** : `"string"`|string|
|**confirmTimeEnd**  <br>*optional*|apply.plan_end_time 承诺办结时间  <br>**Example** : `"string"`|string (date-time)|
|**confirmTimeStart**  <br>*optional*|apply.plan_end_time 承诺办结时间  <br>**Example** : `"string"`|string (date-time)|
|**expireDayEnd**  <br>*optional*|超期时间，多少天 End  <br>**Example** : `0`|integer (int32)|
|**expireDayStart**  <br>*optional*|超期时间，多少天 Start  <br>**Example** : `0`|integer (int32)|
|**factTimeEnd**  <br>*optional*|实际办结时间 end  <br>**Example** : `"string"`|string (date-time)|
|**factTimeStart**  <br>*optional*|实际办结时间 start  <br>**Example** : `"string"`|string (date-time)|
|**fromType**  <br>*optional*|任务分类  <br>**Example** : `"string"`|string|
|**householdType**  <br>*optional*|农业farmer，非农业not_farmer  <br>**Example** : `"string"`|string|
|**itemName**  <br>*optional*|办理事项  <br>**Example** : `"string"`|string|
|**mobile**  <br>*optional*|电话  <br>**Example** : `"string"`|string|
|**name**  <br>*optional*|申请人  <br>**Example** : `"string"`|string|
|**nodeName**  <br>*optional*|环节名称  <br>**Example** : `"string"`|string|
|**operate**  <br>*optional*|操作  <br>**Example** : `"string"`|string|
|**operator**  <br>*optional*|办理人  <br>**Example** : `0`|integer (int64)|
|**orgId**  <br>*optional*|执行者机构  <br>**Example** : `0`|integer (int64)|
|**pageNo**  <br>*optional*|**Example** : `0`|integer (int32)|
|**pageSize**  <br>*optional*|**Example** : `0`|integer (int32)|
|**personType**  <br>*optional*|办理人类别,申请人self,委托人代办agent  <br>**Example** : `"string"`|string|
|**processResult**  <br>*optional*|办理结果，正常结束end，取消结束cancel_end，超期结束time_out  <br>**Example** : `"string"`|string|
|**promiseTimeEnd**  <br>*optional*|流程承诺时间 End  <br>**Example** : `"string"`|string (date-time)|
|**promiseTimeStart**  <br>*optional*|流程承诺时间 Start  <br>**Example** : `"string"`|string (date-time)|
|**regionCode**  <br>*optional*|区域  <br>**Example** : `"string"`|string|
|**relateId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**startTimeEnd**  <br>*optional*|开始时间 end  <br>**Example** : `"string"`|string (date-time)|
|**startTimeStart**  <br>*optional*|开始时间 start  <br>**Example** : `"string"`|string (date-time)|
|**status**  <br>*optional*|状态,办理中doing/暂停pause/完成finish  <br>**Example** : `"string"`|string|
|**statusApply**  <br>*optional*|办理中，暂停(复议期)，结束  <br>**Example** : `"string"`|string|


<a name="taskhistoryresponse"></a>
### TaskHistoryResponse

|Name|Description|Schema|
|---|---|---|
|**acceptTime**  <br>*optional*|接收时间  下一个环节得计划开始时间  <br>**Example** : `"string"`|string (date-time)|
|**applyTime**  <br>*optional*|办理时长  <br>**Example** : `0`|integer (int64)|
|**classify**  <br>*optional*|**Example** : `"string"`|string|
|**expireFlag**  <br>*optional*|是否超期,超期yes,没超期no  <br>**Example** : `"string"`|string|
|**factEndTime**  <br>*optional*|实际结束时间 新增时没有  <br>**Example** : `"string"`|string (date-time)|
|**factPerson**  <br>*optional*|**Example** : `0`|integer (int64)|
|**factPersonName**  <br>*optional*|办理人名称  <br>**Example** : `"string"`|string|
|**factStartTime**  <br>*optional*|实际开始时间 新增时没有  <br>**Example** : `"string"`|string (date-time)|
|**formUrl**  <br>*optional*|流程表单URL  <br>**Example** : `"string"`|string|
|**fromType**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|id  <br>**Example** : `0`|integer (int64)|
|**lockBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**lockFlag**  <br>*optional*|锁定状态 锁定yes， 没锁定no  打开流程时就锁上  <br>**Example** : `"string"`|string|
|**lockTime**  <br>*optional*|上锁时间  <br>**Example** : `"string"`|string (date-time)|
|**name**  <br>*optional*|申请人  <br>**Example** : `"string"`|string|
|**nodeAllowLength**  <br>*optional*|环节许可时长  <br>**Example** : `0`|integer (int32)|
|**operate**  <br>*optional*|操作  <br>**Example** : `"string"`|string|
|**orgId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**orgName**  <br>*optional*|机构名称  <br>**Example** : `"string"`|string|
|**planType**  <br>*optional*|任务来源分类,流程类flow；存量类store；项目类project；巡视类 inspect；工单任务类task  <br>**Example** : `"string"`|string|
|**planer**  <br>*optional*|**Example** : `"string"`|string|
|**planerUsers**  <br>*optional*|**Example** : `[ "[planeruser](#planeruser)" ]`|< [PlanerUser](#planeruser) > array|
|**processOpinion**  <br>*optional*|办理意见  审签类型  <br>**Example** : `"string"`|string|
|**processResult**  <br>*optional*|办理结果,同意agree/不同意disagree/无none/取消cance  <br>**Example** : `"string"`|string|
|**promiseTime**  <br>*optional*|流程承诺时间  <br>**Example** : `"string"`|string (date-time)|
|**relateId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**relateTable**  <br>*optional*|**Example** : `"string"`|string|
|**status**  <br>*optional*|状态,办理中doing/暂停pause/完成finish  <br>**Example** : `"string"`|string|
|**statusCn**  <br>*optional*|**Example** : `"string"`|string|
|**step**  <br>*optional*|步骤,在一个环节中多个步骤才能完成（若收件受理；低保现场调查）  <br>**Example** : `"string"`|string|
|**superTime**  <br>*optional*|超时时长 正数：未超时 分钟  <br>**Example** : `0`|integer (int64)|
|**taskName**  <br>*optional*|环节名称  <br>**Example** : `"string"`|string|
|**type**  <br>*optional*|**Example** : `"string"`|string|
|**wfDefineId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**wfInstanceId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**wfNodeId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="wfcategory"></a>
### WfCategory

|Name|Description|Schema|
|---|---|---|
|**category**  <br>*optional*|**Example** : `"string"`|string|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createdBy**  <br>*optional*|**Example** : `"string"`|string|
|**createtime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**description**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**parentId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**updatedBy**  <br>*optional*|**Example** : `"string"`|string|
|**updatetime**  <br>*optional*|**Example** : `"string"`|string (date-time)|


<a name="wfform"></a>
### WfForm

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createdBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**createtime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**designData**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**param**  <br>*optional*|**Example** : `"string"`|string|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**status**  <br>*optional*|**Example** : `"string"`|string|
|**updatedBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**updatetime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**url**  <br>*optional*|**Example** : `"string"`|string|
|**wfCategoryId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="wfnode"></a>
### WfNode

|Name|Description|Schema|
|---|---|---|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**createdBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**createtime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**nodeJson**  <br>*optional*|**Example** : `"string"`|string|
|**nodeType**  <br>*optional*|**Example** : `"string"`|string|
|**updatedBy**  <br>*optional*|**Example** : `0`|integer (int64)|
|**updatetime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**wfCategoryId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**wfDefineId**  <br>*optional*|**Example** : `0`|integer (int64)|


<a name="wfnodebutton"></a>
### WfNodeButton

|Name|Description|Schema|
|---|---|---|
|**api**  <br>*optional*|**Example** : `"string"`|string|
|**buttonId**  <br>*optional*|**Example** : `0`|integer (int64)|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**imgUrl**  <br>*optional*|**Example** : `"string"`|string|
|**mean**  <br>*optional*|**Example** : `"string"`|string|
|**name**  <br>*optional*|**Example** : `"string"`|string|
|**remark**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `"string"`|string|
|**url**  <br>*optional*|**Example** : `"string"`|string|


<a name="wfphrase"></a>
### WfPhrase

|Name|Description|Schema|
|---|---|---|
|**classifyCode**  <br>*optional*|**Example** : `"string"`|string|
|**code**  <br>*optional*|**Example** : `"string"`|string|
|**content**  <br>*optional*|**Example** : `"string"`|string|
|**createdBy**  <br>*optional*|**Example** : `"string"`|string|
|**createtime**  <br>*optional*|**Example** : `"string"`|string (date-time)|
|**defaultFlag**  <br>*optional*|**Example** : `0`|integer (int32)|
|**description**  <br>*optional*|**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|
|**itemCode**  <br>*optional*|**Example** : `"string"`|string|
|**itemName**  <br>*optional*|**Example** : `"string"`|string|
|**sort**  <br>*optional*|**Example** : `0`|integer (int32)|
|**type**  <br>*optional*|**Example** : `"string"`|string|
|**updatedBy**  <br>*optional*|**Example** : `"string"`|string|
|**updatetime**  <br>*optional*|**Example** : `"string"`|string (date-time)|


<a name="wfphrasetipresp"></a>
### WfPhraseTipResp

|Name|Description|Schema|
|---|---|---|
|**content**  <br>*optional*|内容  <br>**Example** : `"string"`|string|
|**id**  <br>*optional*|**Example** : `0`|integer (int64)|





