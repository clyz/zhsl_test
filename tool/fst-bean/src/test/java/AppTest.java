import org.junit.Test;
import org.nustaq.serialization.FSTConfiguration;

import java.util.ArrayList;

/**
 * @author: ziv
 * @date: 2018/12/5 13:17
 * @version: 0.0.1
 */
public class AppTest {
    @Test
    public void test(){
        FSTConfiguration defaultConfiguration = FSTConfiguration.createJsonConfiguration();
        String json = defaultConfiguration.asJsonString(new ArrayList());
        System.err.println(json);
    }
}
