package com.clyz.selenium.qq;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

/**
 * https://blog.csdn.net/qq_22003641/article/details/79137327
 */
@Slf4j
public class QQSpaceTest {
    private RemoteWebDriver webDriver;

    static {
        System.setProperty("webdriver.gecko.driver", "D:\\service_java\\selenium\\firefox\\geckodriver.exe");
        System.setProperty("webdriver.edge.driver", "D:\\service_java\\selenium\\edged\\msedgedriver.exe");
    }

    @BeforeEach
    void before() {
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().deleteAllCookies();
    }

    @Test
    public void qqAuto() throws InterruptedException {
        String Message = "Hello World [selenium] !";//发表内容

        //等浏览器加载完毕，与浏览器同步
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        System.out.println("与浏览器同步成功");

        webDriver.get("https://qzone.qq.com/");
        System.out.println("网站名称:" + webDriver.getTitle());

        //切换到ifram中
        WebElement iframe = webDriver.findElement(By.id("login_frame"));
        webDriver.switchTo().frame(iframe);

        WebElement webElement = webDriver.findElement(By.id("qlogin_show")).findElement(By.className("face"));
        webElement.click();

        System.out.println("登录成功");

        Thread.sleep(10000);
   /*      webDriver.findElement(By.className("music-play")).click();
        System.out.println("成功静止音乐播放");*/
        webDriver.findElement(By.linkText("说说")).click();

//        webDriver.findElement(By.id("tb_menu_panel")).findElement(By.className("menu_item_311")).click();
        System.out.println("成功切换到说说页面");

        Thread.sleep(3000);
        iframe = webDriver.findElement(By.className("app_canvas_frame"));
        webDriver.switchTo().frame(iframe);
        System.out.println("成功进入发表说说iframe");

        webDriver.findElement(By.id("$1_substitutor_content")).click();
        webDriver.findElement(By.id("$1_content_content")).sendKeys(Message);
        System.out.println("成功添加内容");

        webDriver.findElement(By.id("QM_Mood_Poster_Container")).findElement(By.className("op")).click();
        System.out.println("成功发表说说");

    }


    @Test
    public void rainbowFartDome() {
        webDriver.get("https://chp.shadiao.app/");
        log.info("Title: {}", webDriver.getTitle());
        String text = webDriver.findElementById("index").findElement(By.className("mdl-textfield__input")).getText();
        log.info("Text: {}", text);
    }

    @Test
    public void qq() throws InterruptedException {
        String Message = "Hello World [selenium] !";//发表内容
        String Username = "2374619909";//qq账号
        String Passwd = "Zemin886520.CLYZ";//qq密码

        //等浏览器加载完毕，与浏览器同步
        webDriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

        System.out.println("与浏览器同步成功");

        webDriver.get("https://qzone.qq.com/");
        System.out.println("网站名称:" + webDriver.getTitle());

        //切换到ifram中
        WebElement iframe = webDriver.findElement(By.id("login_frame"));
        webDriver.switchTo().frame(iframe);

        WebElement webElement = webDriver.findElement(By.id("switcher_plogin"));
        webElement.click();

        webDriver.findElement(By.id("u")).sendKeys(Username);
        webDriver.findElement(By.id("p")).sendKeys(Passwd);
        webDriver.findElement(By.id("login_button")).click();
        System.out.println("登录成功");

       Thread.sleep(10000);
   /*      webDriver.findElement(By.className("music-play")).click();
        System.out.println("成功静止音乐播放");*/
        webDriver.findElement(By.linkText("说说")).click();

//        webDriver.findElement(By.id("tb_menu_panel")).findElement(By.className("menu_item_311")).click();
        System.out.println("成功切换到说说页面");

        Thread.sleep(3000);
        iframe = webDriver.findElement(By.className("app_canvas_frame"));
        webDriver.switchTo().frame(iframe);
        System.out.println("成功进入发表说说iframe");

        webDriver.findElement(By.id("$1_substitutor_content")).click();
        webDriver.findElement(By.id("$1_content_content")).sendKeys(Message);
        System.out.println("成功添加内容");

        webDriver.findElement(By.id("QM_Mood_Poster_Container")).findElement(By.className("op")).click();
        System.out.println("成功发表说说");

    }

    @AfterEach
    void after() {
        webDriver.close();
        log.info("网页已关闭");
    }
}