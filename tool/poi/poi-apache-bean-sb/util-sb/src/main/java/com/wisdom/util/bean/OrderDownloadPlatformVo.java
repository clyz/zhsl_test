package com.wisdom.util.bean;

import com.wisdom.util.excel.annotion.Excel;

public class OrderDownloadPlatformVo {
    public OrderDownloadPlatformVo() {

    }

    public OrderDownloadPlatformVo(String orderNo, Integer totalPoint, Integer status,
                                   String userName, String userPhone, String userAddress,
                                   String merchantName, String supplierName, String crTime) {
        this.orderNo = orderNo;
        this.totalPoint = totalPoint;
        this.status = status;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.merchantName = merchantName;
        this.supplierName = supplierName;
        this.crTime = crTime;
    }

    /**
     * 订单号
     */
    @Excel(exportName = "订单号", exportFieldWidth = 21)
    private String orderNo;

    /**
     * 总额
     */
    @Excel(exportName = "总额", exportFieldWidth = 21)
    private Integer totalPoint;

    /**
     * 状态，0--未支付，1--支付失败，2--支付成功,3--已取消
     */
    @Excel(exportName = "状态", key = {"0", "1", "2", "3"}, value = {"未支付", "支付失败", "支付成功", "已取消"}, exportFieldWidth = 21)
    private Integer status;

    /**
     * 收货人
     */
    @Excel(exportName = "收货人", exportFieldWidth = 21)
    private String userName;

    /**
     * 收货人联系电话
     */
    @Excel(exportName = "收货人电话", exportFieldWidth = 21)
    private String userPhone;

    /**
     * 收货地址
     */
    @Excel(exportName = "收货地址", exportFieldWidth = 21)
    private String userAddress;

    /**
     * 商户名
     */
    @Excel(exportName = "商户名", exportFieldWidth = 21)
    private String merchantName;

    /**
     * 供应商
     */
    @Excel(exportName = "供应商", exportFieldWidth = 21)
    private String supplierName;

    /**
     * 创建时间
     */
    @Excel(exportName = "下单时间", exportFieldWidth = 35)
    private String crTime;


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(Integer totalPoint) {
        this.totalPoint = totalPoint;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCrTime() {
        return crTime;
    }

    public void setCrTime(String crTime) {
        this.crTime = crTime;
    }

}
