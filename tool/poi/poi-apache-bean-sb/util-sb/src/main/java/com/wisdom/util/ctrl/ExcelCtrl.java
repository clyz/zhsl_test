package com.wisdom.util.ctrl;

import com.wisdom.util.bean.OrderDownloadPlatformVo;
import com.wisdom.util.excel.entity.ExcelTitle;
import com.wisdom.util.excel.util.ExcelExportUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/excel/test/")
public class ExcelCtrl {

    List<OrderDownloadPlatformVo> orderLists = Arrays.asList(
            new OrderDownloadPlatformVo("1", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("2", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("3", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("4", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("5", 1, 1, "",
                    "", "",
                    "", "", new Date().toString())
    );


    @GetMapping("out")
    public void out(HttpServletRequest request, HttpServletResponse response) throws Exception {
        downLoadExcle(request, response, "订单信息", orderLists, "销售1", OrderDownloadPlatformVo.class);
    }


    protected void downLoadExcle(HttpServletRequest request,
                                 HttpServletResponse response, String Title, Collection<?> dataset,
                                 String author, Class clas)
            throws Exception {
        response.reset();
        response.setContentType("application/vnd.ms-excel");
        OutputStream out = null;
        response.setHeader("Content-disposition",
                "attachment; filename=fine.xls");// 设定输出文

        try {
            String newtitle = new String(Title.getBytes("GBK"), "ISO8859-1");
            response.setHeader("content-disposition", "attachment;filename="
                    + newtitle + new Date() + ".xls");

            out = response.getOutputStream();

            HSSFWorkbook workbook = null;

            workbook = ExcelExportUtil.exportExcel(new ExcelTitle(Title, author), clas, dataset);

            workbook.write(out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }
    }
}
