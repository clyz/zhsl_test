package com.wisdom.velocitysb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VelocitySbApplication {

    public static void main(String[] args) {
        SpringApplication.run(VelocitySbApplication.class, args);
    }

}
