package com.wisdom.velocitysb;

import com.wisdom.velocitysb.bean.HurrySupplyApplyInfo;
import com.wisdom.velocitysb.util.velocity.VelocityUtil;
import org.apache.velocity.VelocityContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class VeloctiyPoiCtrl {

    @RequestMapping("/hurrySupplyFormExport")
    public void hurrySupplyCompanyExport(HurrySupplyApplyInfo applyInfo , HttpServletRequest request, HttpServletResponse response) throws IOException {
        VelocityContext vc = new VelocityContext();
        vc.put("info", applyInfo);
        VelocityUtil.createDoc("velocity_poi_1.vm", vc,
                request, response, "velocity_poi_1.xlsx",VelocityUtil.EXCEL_FILE);

    }
}
