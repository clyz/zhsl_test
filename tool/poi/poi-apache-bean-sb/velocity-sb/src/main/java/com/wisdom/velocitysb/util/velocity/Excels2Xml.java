package com.wisdom.velocitysb.util.velocity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Excels2Xml {

    private static HashMap<String, File> map = new HashMap<>();

    /**
     * 输入文件路径,对于嵌套的目录不进行读取
     *
     * @param inPath
     * @return
     */
    public static HashMap<String, File> getFiles(String inPath) {
        if (null == inPath || inPath.trim().length() == 0)
            return null;
        File filex = new File(inPath);
        if (filex.isDirectory()) {
            File[] files = filex.listFiles();
            if (files.length > 0) {
                for (File file : files) {
                    if (file.isFile() && (file.getName().contains(".xlsx") || file.getName().contains(".xls")))
                        map.put(file.getName(), file);
                }
            }
        } else if (filex.isFile()) {
            map.put(filex.getName(), filex);
        }
        return map;
    }

    /**
     *
     * @param map
     * @param outPath
     */
    public static void excels2Xml(HashMap<String, File> map, String outPath) {
        if (map == null)
            return;
        map.forEach((fileName, file) -> {
            BufferedWriter bw = null;
            InputStream is = null;
            try {
                String fn = fileName.replace(".xlsx", ".xml");
                if (fn == fileName) {
                    fn = fileName.replace(".xls", ".xml");
                }
                bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(outPath + fn)), "utf-8"));
                is = new FileInputStream(file.getPath());
                Workbook hssfWorkbook = WorkbookFactory.create(is);
                if (null != hssfWorkbook)
                    bw.write("<xmls>\n");
                for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
                    Sheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
                    if (hssfSheet == null) {
                        continue;
                    }
                    for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                        Row hssfRow = hssfSheet.getRow(rowNum);
                        if (hssfRow != null) {
                            Cell content = hssfRow.getCell(0);
                            Cell channel = hssfRow.getCell(2);
                            bw.write("<m1>\n");
                            bw.write("<content>" + content + "</content>\n");
                            bw.write("<channel>" + channel + "</channel>\n");
                            bw.write("</m1>\n\n");
                        }
                    }
                }
                bw.write("<xmls>\n");
            } catch (FileNotFoundException e) {
                System.out.println("FileNotFoundException...");
                e.printStackTrace();
            } catch (IOException e) {
                System.out.println("IOException...");
                e.printStackTrace();
            } finally {
                try {
                    if (bw != null)
                        bw.close();
                    if (is != null)
                        is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void main(String[] args) throws Exception {
        long begin = System.currentTimeMillis();
        excels2Xml(getFiles("E:\\java_web\\zhsl_obj\\zhsl_test\\poi-apache-bean-sb\\velocity-sb\\src\\main\\resources\\velocity_poi_1.xlsx"),
                "E:\\java_web\\zhsl_obj\\zhsl_test\\poi-apache-bean-sb\\velocity-sb\\src\\main\\resources\\shen");
        long end = System.currentTimeMillis();
        System.out.println("The program consume " + (end - begin) / 1000 / 60.0 + "  seconds");
    }
}