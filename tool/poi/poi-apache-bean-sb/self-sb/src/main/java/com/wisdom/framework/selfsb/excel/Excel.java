package com.wisdom.framework.selfsb.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.ObjectUtils;

import java.util.List;

public class Excel {

    /**
     * 版本
     */
    private ExcelVersion version;

    /**
     * 获取 版本文件的扩展名
     *
     * @return
     */
    public String getExtensionName() {
        return version.getSuffix();
    }

    public void setVersion(ExcelVersion version) {
        this.version = version;
    }

    public Workbook createWorkBook(List<ExcelSheetPo> excelSheets) {
        Workbook workbook = createWorkbook();
        ExcelSheetPo excelSheetPO;
        for (int i = 0; i < excelSheets.size(); i++) {
            excelSheetPO = excelSheets.get(i);
            if (excelSheetPO.getSheetName() == null) {
                excelSheetPO.setSheetName("sheet-" + i);
            }
            // 过滤特殊字符
            assert workbook != null;
            Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName(excelSheetPO.getSheetName()));
            buildSheetData(workbook, sheet, excelSheetPO);
        }
        return workbook;
    }

    private void buildSheetData(Workbook workbook, Sheet sheet, ExcelSheetPo excelSheetPo) {
/*        sheet.setDefaultRowHeight((short) 400);
        sheet.setDefaultColumnWidth((short) 10);*/
        createHeader(sheet.createRow(0), excelSheetPo);
        createBody(sheet, excelSheetPo, workbook);
    }

    private void createBody(Sheet sheet, ExcelSheetPo excelSheetPo, Workbook workbook) {
        Row row;
        List<Object[]> dataList = excelSheetPo.getDataList();
        if (!ObjectUtils.isEmpty(dataList)) {
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            for (Object[] data : dataList) {
                for (int i = 0; i < data.length; i++) {
                    row.createCell(i).setCellValue(data[i].toString());
                }
            }
        }
    }

    private void createHeader(Row headersRow, ExcelSheetPo excelSheetPO) {
        String[] headers = excelSheetPO.getHeaders();
        if (ObjectUtils.isEmpty(headers)) {
            return;
        }
        for (int i = 0; i < headers.length; i++) {
            headersRow.createCell(i).setCellValue(headers[i]);
        }
    }

    private Workbook createWorkbook() {
        switch (version) {
            case V2003:
                return new HSSFWorkbook();
            case V2007:
                return new XSSFWorkbook();
        }
        return null;
    }
}
