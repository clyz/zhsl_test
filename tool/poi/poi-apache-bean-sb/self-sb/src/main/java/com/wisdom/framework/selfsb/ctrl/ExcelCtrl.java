package com.wisdom.framework.selfsb.ctrl;

import com.wisdom.framework.selfsb.excel.Excel;
import com.wisdom.framework.selfsb.excel.ExcelSheetPo;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/test/")
public class ExcelCtrl {

    @GetMapping(value = "out")
    public void out(HttpServletResponse response) throws IOException {

        List<ExcelSheetPo> excelSheets = new ArrayList<>();

        ExcelSheetPo excelSheetPo = new ExcelSheetPo();

        excelSheetPo.setHeaders(new String[]{"Id", "名称"});

        List<Object[]> dataList = new ArrayList<>();
        dataList.add(new Object[]{1,"中国"});
        dataList.add(new Object[]{1,new Date()});
        excelSheetPo.setDataList(dataList);

        excelSheets.add(excelSheetPo);

        response.reset();
        response.setContentType("application/vnd.ms-excel");
        OutputStream out = null;
        response.setHeader("Content-disposition", "attachment; filename=fine.xls");// 设定输出文
        try {
            response.setHeader("content-disposition", "attachment;filename=" + "1.xlsx");
            out = response.getOutputStream();
            Excel excel = new Excel();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename=fine.xlsx");// 设定输出文
            Workbook workBook = excel.createWorkBook(excelSheets);
            workBook.write(out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }

    }
}
