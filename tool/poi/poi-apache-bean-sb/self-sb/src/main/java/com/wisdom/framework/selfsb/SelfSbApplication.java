package com.wisdom.framework.selfsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelfSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SelfSbApplication.class, args);
    }

}

