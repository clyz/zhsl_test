package com.wisdom.framework.selfsb.excel;

public enum ExcelVersion {

    V2003(".xls", 10000, 100), V2007(".xlsx", 100, 100);

    private String suffix;

    private int maxRow;

    private int maxColumn;

    ExcelVersion(String suffix, int maxRow, int maxColumn) {
        this.suffix = suffix;
        this.maxRow = maxRow;
        this.maxColumn = maxColumn;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public int getMaxRow() {
        return maxRow;
    }

    public void setMaxRow(int maxRow) {
        this.maxRow = maxRow;
    }

    public int getMaxColumn() {
        return maxColumn;
    }

    public void setMaxColumn(int maxColumn) {
        this.maxColumn = maxColumn;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

}