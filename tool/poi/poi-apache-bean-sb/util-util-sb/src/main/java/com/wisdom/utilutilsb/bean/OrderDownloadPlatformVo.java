package com.wisdom.utilutilsb.bean;

public class OrderDownloadPlatformVo {
    public OrderDownloadPlatformVo() {

    }

    public OrderDownloadPlatformVo(String orderNo, Integer totalPoint, Integer status,
                                   String userName, String userPhone, String userAddress,
                                   String merchantName, String supplierName, String crTime) {
        this.orderNo = orderNo;
        this.totalPoint = totalPoint;
        this.status = status;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.merchantName = merchantName;
        this.supplierName = supplierName;
        this.crTime = crTime;
    }

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 总额
     */
    private Integer totalPoint;

    /**
     * 状态，0--未支付，1--支付失败，2--支付成功,3--已取消
     */
    private Integer status;

    /**
     * 收货人
     */
    private String userName;

    /**
     * 收货人联系电话
     */
    private String userPhone;

    /**
     * 收货地址
     */
    private String userAddress;

    /**
     * 商户名
     */
    private String merchantName;

    /**
     * 供应商
     */
    private String supplierName;

    /**
     * 创建时间
     */
    private String crTime;


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(Integer totalPoint) {
        this.totalPoint = totalPoint;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCrTime() {
        return crTime;
    }

    public void setCrTime(String crTime) {
        this.crTime = crTime;
    }

}
