package com.wisdom.utilutilsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtilUtilSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(UtilUtilSbApplication.class, args);
    }

}

