package com.wisdom.utilutilsb.ctrl;

import com.wisdom.utilutilsb.bean.ExcelSheetPO;
import com.wisdom.utilutilsb.bean.ExcelVersion;
import com.wisdom.utilutilsb.bean.OrderDownloadPlatformVo;
import com.wisdom.utilutilsb.util.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/excel/test/")
public class ExcelCtrl {

    List<OrderDownloadPlatformVo> orderLists = Arrays.asList(
            new OrderDownloadPlatformVo("1", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("2", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("3", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("4", 1, 1, "",
                    "", "",
                    "", "", new Date().toString()),
            new OrderDownloadPlatformVo("5", 1, 1, "",
                    "", "",
                    "", "", new Date().toString())
    );


    @GetMapping("out")
    public void out(HttpServletResponse response) throws Exception {
        String title = "订单信息";
        String sheetName = "订单";
        String[] rowsName = new String[]{"订单号", "总额", "状态", "收货人", "收货人电话", "收货地址", "下单时间"};
        List<List<Object>> dataList = new ArrayList<>();
        List<Object> objs;

        for (int i = 0; i < orderLists.size(); i++) {
            OrderDownloadPlatformVo man = orderLists.get(i);
            objs = new ArrayList<>();
            objs.add(man.getOrderNo());
            objs.add(man.getTotalPoint());
            objs.add(man.getStatus());
            objs.add(man.getUserName());
            objs.add(man.getUserPhone());
            objs.add(man.getUserAddress());
            objs.add(man.getCrTime());
            dataList.add(objs);
        }

        List<ExcelSheetPO> list = new ArrayList<>();
        ExcelSheetPO excelSheetPO = new ExcelSheetPO();
        excelSheetPO.setDataList(dataList);
        excelSheetPO.setHeaders(rowsName);
        excelSheetPO.setTitle(title);
        excelSheetPO.setSheetName(sheetName);

        response.reset();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition",
                "attachment; filename=fine" + ExcelVersion.V2007.getSuffix());// 设定输出文

        response.setHeader("content-disposition", "attachment;filename="
                + title + new Date() + ExcelVersion.V2007.getSuffix());

        ExcelUtil.createWorkbookAtOutStream(
                ExcelVersion.V2007, list
                , response.getOutputStream(), true);
    }

}
