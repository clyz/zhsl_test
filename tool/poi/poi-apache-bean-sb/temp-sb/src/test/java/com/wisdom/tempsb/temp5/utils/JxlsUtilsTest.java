package com.wisdom.tempsb.temp5.utils;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class JxlsUtilsTest {
    static String TEMPLATE_PATH = "E:\\java_web\\zhsl_obj\\zhsl_test\\poi-apache-bean-sb\\temp-sb\\src\\main\\resources\\temp\\dome.xls";

    public static void main(String[] args) throws Exception {
        // 模板路径和输出流
        OutputStream os = new FileOutputStream("D:/out.xls");
        // 定义一个Map，往里面放入要在模板中显示数据
        Map<String, Object> model = new HashMap<>();
        model.put("id", "001");
        model.put("name", "张三");
        model.put("age", 18);
        //调用之前写的工具类，传入模板路径，输出流，和装有数据Map
        JxlsUtils.exportExcel(TEMPLATE_PATH, os, model);
        os.close();
        System.out.println("完成");
    }
}