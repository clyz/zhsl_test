package com.wisdom.tempsb.vo;

import java.io.Serializable;

public class JxlsVo implements Serializable {
    private String header;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
