package com.wisdom.tempsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TempSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(TempSbApplication.class, args);
    }

}
