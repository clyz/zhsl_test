package com.wisdom.tempsb.ifunc;

@FunctionalInterface
public interface ICreateRow {
    void add();
}
