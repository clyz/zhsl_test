package com.wisdom.tempsb.vo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
@Data
public class ValueVo {
    private String name;
    private Long id;
    private Date createTime;
    private Date updateTime;
    private String tel;

    public ValueVo(String name, Long id, Date createTime, Date updateTime, String tel) {
        this.name = name;
        this.id = id;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.tel = tel;
    }
}
