/*
package com.wisdom.tempsb.temp3;

import com.wisdom.tempsb.product.ValueVo;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/temp3/")
public class ExcelCtrl {

    private final static String PATH = "E:\\java_web\\zhsl_obj\\zhsl_test\\poi-apache-bean-sb\\temp-sb\\src\\main\\resources\\temp\\xo.xlsx";

    @RequestMapping("down")
    public void down(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition", "attachment;fileName=model.xlsx");
        excel(response, new String[]{"ID", "姓名", "电话号码", "创建时间", "更新时间"}, Arrays.asList(
                new ValueVo("杨泽民",1L,new Date(), new Date(),"112"),
                new ValueVo("张三",8695269876526752145L,new Date(), new Date(),"185964956164"),
                new ValueVo("杨泽民",8695269876526752141L,new Date(), new Date(),"5512186498"),
                new ValueVo("杨泽民",8695269876526711111L,new Date(), new Date(),"79863228"),
                new ValueVo("杨泽民",8695269871111111111L,new Date(), new Date(),"186524"),
                new ValueVo("杨泽民",1111111111111111111L,new Date(), new Date(),"789526")
        ));
    }

    private void excel(HttpServletResponse response, String[] header, List<ValueVo> valueVos) throws IOException {
        XSSFWorkbook hworkbook = new XSSFWorkbook(new FileInputStream(PATH));// 用输入流生成poi对象，以读取excel中的内容
        XSSFSheet sheet = hworkbook.getSheetAt(0);// 获取sheet

        //  标题
        XSSFRow row0 = sheet.getRow(0);
        Cell row0Cell0 = row0.getCell(0);
        row0Cell0.setCellValue("XLSX_测试");

        //  公司
        XSSFRow row1 = sheet.getRow(1);
        Cell row1Cell1 = row1.getCell(1);
        row1Cell1.setCellValue("智慧数联");

        //  header
        XSSFRow rowHeader = sheet.getRow(2);
        for (int i = 0; i < header.length; i++) {
            rowHeader.getCell(i).setCellValue(header[i]);
        }

        //  导出人
        XSSFRow row4 = sheet.getRow(4);
        Cell row4Cell1 = row4.getCell(1);
        row4Cell1.setCellValue("张山");

        //  倒出时间
        XSSFRow row5 = sheet.getRow(5);
        Cell row5Cell1 = row5.getCell(1);
        row5Cell1.setCellValue(new Date().toString());

        //  数据
        int count = 3;
        XSSFRow resouceRow = sheet.getRow(count);
        for (ValueVo valueVo : valueVos) {
            sheet.shiftRows(count, sheet.getLastRowNum(), 1);
            XSSFRow dataRow = sheet.createRow(count++);
            dataRow.setRowStyle(resouceRow.getRowStyle());
            dataRow.createCell(0).setCellValue(valueVo.getId());
            dataRow.getCell(0).setCellStyle(resouceRow.getCell(0).getCellStyle());

            dataRow.createCell(1).setCellValue(valueVo.getName());
            dataRow.getCell(1).setCellStyle(resouceRow.getCell(1).getCellStyle());

            dataRow.createCell(2).setCellValue(valueVo.getTel());
            dataRow.getCell(2).setCellStyle(resouceRow.getCell(2).getCellStyle());

            dataRow.createCell(3).setCellValue(valueVo.getCreateTime());
            dataRow.getCell(3).setCellStyle(resouceRow.getCell(3).getCellStyle());

            dataRow.createCell(4).setCellValue(valueVo.getUpdateTime());
            dataRow.getCell(4).setCellStyle(resouceRow.getCell(4).getCellStyle());
        }

        hworkbook.write(response.getOutputStream());
    }

}
*/
