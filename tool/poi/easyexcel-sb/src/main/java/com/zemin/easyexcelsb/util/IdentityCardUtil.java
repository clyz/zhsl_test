package com.zemin.easyexcelsb.util;

import org.apache.commons.lang3.StringUtils;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.stream.Stream;

/**
 * <p>身份证校验</p>
 * <p>
 * 2、地址码(前六位数）
 * 表示编码对象常住户口所在县(市、旗、区)的行政区划代码，按GB/T2260的规定执行。
 * <p>
 * 3、出生日期码（第七位至十四位）
 * <p>
 * 4、顺序码（第十五位至十七位）
 * 表示在同一地址码所标识的区域范围内，对同年、同月、同日出生的人编定的顺序号，
 * 顺序码的奇数分配给男性，偶数分配给女性。
 * <p>
 * 5、校验码（第十八位数）
 * 校验身份证是否号码是否正确
 *
 * @author Zemin.Yang
 */
public class IdentityCardUtil {

    /**
     * 加权因子 Wi=2^(n【n 身份证号码有左到右 数1，2，3】-1) mod 11
     * Ai身份证号码: Ai,A2,A3,.....
     * n:          18,17,16,15,14,13,12,11,10,9, 8, 7, 6, 5, 4, 3, 2 ,1
     * 加权因子Wi:  7, 9, 10, 5, 8, 4, 2, 1, 6, 3,7, 9,10, 5, 8, 4, 2, 1[18位]
     * <p>
     * 初始化 加权因子
     * <p>
     * 校验码【第十八位数】 = (A1*Wi+A2*W2+...+Ai*Wi) 与 11 求余  = 1 mod 11
     * Math.floorMod(1,11)=Math.floorMod( (A1*Wi+A2*W2+...+Ai*Wi) ,11)
     */
    private static final Integer[] WEIGHTING_FACTOR =
            Stream.iterate(18, n -> n - 1).limit(18)
                    .map(e -> Math.floorMod((int) Math.pow(2, e - 1), 11))
                    .toArray(Integer[]::new);

    /**
     * 验证身份证格式是否正确
     *
     * @param identityCardStr 身份证号码
     * @return true 正确，false错误
     */
    public static boolean isIdentityCard(String identityCardStr) {
        if (identityCardStr == null || !(identityCardStr.length() == 15 || identityCardStr.length() == 18)) {
            return false;
        }
        //  校验 二代身份证
        if (isIdentityCardStructure(identityCardStr)) {
            return isIdentityCardCheckCode(identityCardStr);
        }
        //  TODO .. 一代身份证
        return isIdentityCardStructureEarly(identityCardStr);

    }

    /**
     * 校验 身份证 校验码 是否正确
     *
     * @param identityCardStr 身份证号码
     * @return boolean 身份证 校验码
     * true 校验码正确
     * false 校验码错误
     */
    private static boolean isIdentityCardCheckCode(String identityCardStr) {
        Integer[] identityCards = Stream.of(identityCardStr.substring(0, 17).split(""))
                .map(Integer::parseInt).toArray(Integer[]::new);
        int sum = 0;
        for (int i = 0; i < identityCards.length; i++) {
            sum += WEIGHTING_FACTOR[i] * identityCards[i];
        }
        String lastIC = identityCardStr.substring(17);
        if ("x".equalsIgnoreCase(lastIC)) {
            sum += WEIGHTING_FACTOR[17] * 10;
        } else {
            sum += WEIGHTING_FACTOR[17] * Integer.parseInt(lastIC);
        }
        return Math.floorMod(1, 11) == Math.floorMod(sum, 11);
    }

    /**
     * 校验 身份证 的 结构值
     *
     * @param identityCardStr 身份证号码
     * @return boolean 是否满足身份证数字结构
     * true
     * 1.必须是18位
     * 2.只能包含 数字【1-17位】 数字或者X【18位】
     * false
     */
    private static boolean isIdentityCardStructure(String identityCardStr) {
        return (identityCardStr != null && identityCardStr.length() == 18) &&
//                identityCardStr.matches("\\d{16}[0-9][0-9xX]");
                identityCardStr.matches("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[A-Za-z])$");
    }

    /**
     * 一代身份证校验
     */
    private static boolean isIdentityCardStructureEarly(String identityCardStr) {
        return (identityCardStr != null && identityCardStr.length() == 15) &&
                identityCardStr.matches("^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$");
    }
}
