package com.zemin.easyexcelsb.poi.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Zemin.Yang
 */
@Data
public class DibtElderStorePoi implements Serializable {

    @ExcelProperty(value = "名称")
    private String name;

    @ExcelProperty(value = "登记时间")
    private LocalDateTime registerTime;

    @ExcelProperty(value = "身份证号码")
    private String idCard;

    @ExcelProperty(value = "所在地区")
    private String regionCode;

    @ExcelProperty(value = "家庭地址")
    private String homeAddr;

    @ExcelProperty(value = "累计发放金额")
    private Integer issueTotal;

    @ExcelProperty(value = "残疾类别")
    private String disabilityType;

    @ExcelProperty(value = "残疾等级")
    private String disabilityLevel;

    @ExcelProperty(value = "是否代理")
    private String agentGetFlag;

    @ExcelProperty(value = "领取人名称")
    private String receiverName;

    @ExcelProperty(value = "领取账户号")
    private String receiverAccount;

    @ExcelProperty(value = "领取账户机构")
    private String receiverOrg;

    @ExcelProperty(value = "残疾补贴编号")
    private String disabilityNo;
}
