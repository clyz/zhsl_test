package com.zemin.easyexcelsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyexcelSbApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyexcelSbApplication.class, args);
	}

}
