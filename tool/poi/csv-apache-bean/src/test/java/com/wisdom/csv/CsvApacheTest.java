package com.wisdom.csv;

import com.wisdom.bean.User;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author: ziv
 * @date: 2018/12/7 13:33
 * @version: 0.0.1
 */
public class CsvApacheTest {
    private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private List<String[]> dataStr = Arrays.asList(
            new String[]{"1a", "2a", "3a", "4a", "5a"},
            new String[]{"1b", "2b", "3b", "4b", "5b"},
            new String[]{"1c", "2c", "3c", "4c", "5c"}
    );
    private File file = new File("ljq.csv");
    private FileReader fileReader = null;
    private FileWriter fileWriter = null;
    private String[] headers = new String[]{"1", "2", "3", "4", "5"};
    private CSVParser parser = null;
    private CSVPrinter printer = null;
    private List<User> readUserList = null;
    private List<User> writeUserList = Arrays.asList(
            new User(1, "杨泽民", LocalDate.of(2018, 10, 12)),
            new User(2, "杨泽", LocalDate.now()),
            new User(3, "Ziv", LocalDate.now(Clock.systemDefaultZone())),
            new User(4, "よぅさぅ", LocalDate.parse("2018-05-06", dateTimeFormatter)),
            new User(5, "hanâ", LocalDate.now(ZoneId.systemDefault())),
            new User(6, null, LocalDate.now()),
            new User(7, "xo", null),
            new User(null, "a", LocalDate.now()),
            new User(9, null, null),
            new User()
    );

    @After
    public void close() throws IOException {
        if (printer != null) printer.close();
        if (parser != null) parser.close();
        if (fileWriter != null) fileWriter.close();
        if (fileReader != null) fileReader.close();
    }

    @Before
    public void init() throws IOException {
        readORWriter(false, CSVFormat.EXCEL);
    }

    private void readORWriter(boolean bool, CSVFormat formator) throws IOException {
        if (bool) {
            fileReader = new FileReader(file);
            parser = new CSVParser(fileReader, formator);
        } else {
            fileWriter = new FileWriter(file.getAbsoluteFile());
            printer = new CSVPrinter(fileWriter, formator);
        }
    }

    @Test
    public void printer() throws IOException {
        printer.printRecord("id", "userName", "firstName", "lastName", "birthday");
        printer.printRecord(1, "john73", "John", "Doe", LocalDate.of(1973, 9, 15));
        printer.println();
        printer.printRecord(2, "mary", "Mary", "Meyer", LocalDate.of(1985, 3, 29));
    }

    @Test
    public void printerObject() throws IOException {
        printer.printRecord("id", "name", "birth");
        writeUserList.forEach(e -> {
            try {
                printer.printRecord(e.getId(), e.getName(), e.getBrith());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    @Test
    public void readerObject() throws IOException {
        readUserList = new ArrayList<>();
        Optional.ofNullable(parser.getRecords())
                .ifPresent(p ->
                        p.stream().filter(record -> record.getRecordNumber() != 1)
                                .forEach(record -> {
                                    readUserList.add(new User(
                                            Integer.valueOf(
                                                    record.get(0) == null || record.get(0).length() <= 0 ? "0" : record.get(0)
                                            ),
                                            record.get(1) == null || record.get(1).length() <= 1 ? null : record.get(1),
                                            record.get(2) == null || record.get(2).length() <= 0 ? null : LocalDate.parse(record.get(2), dateTimeFormatter)
                                    ));
                                }));
        readUserList.forEach(System.out::println);
    }

    @Test
    public void write() throws IOException {
        //写入列头数据
        printer.printRecord(headers);
        if (null != dataStr) {
            //循环写入数据
            for (String[] lineData : dataStr) {
                printer.printRecord(lineData);
            }
        }
    }
}
