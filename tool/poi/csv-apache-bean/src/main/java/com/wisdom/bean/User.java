package com.wisdom.bean;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author: ziv
 * @date: 2018/12/7 13:53
 * @version: 0.0.1
 */
public class User implements Serializable {
    private LocalDate brith;
    private Integer id;
    private String name;

    public User(Integer id, String name, LocalDate brith) {
        this.id = id;
        this.name = name;
        this.brith = brith;
    }

    public User() {

    }

    public LocalDate getBrith() {
        return brith;
    }

    public void setBrith(LocalDate brith) {
        this.brith = brith;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name=" + name +
                ", brith=" + brith +
                '}';
    }
}
