package com.wisdom.common.basecontroller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.wisdom.common.bean.CompanyHasImgModel;
import com.wisdom.easypoisbs.EasypoiSbsApplicationTests;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExcelOutControllerTest extends EasypoiSbsApplicationTests {

    @Test
    public void logo() throws IOException {
        List list = new ArrayList<CompanyHasImgModel>();
        list.add(new CompanyHasImgModel("百度", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/1.png", "北京市海淀区西北旺东路10号院百度科技园1号楼"));
        list.add(new CompanyHasImgModel("阿里巴巴", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/2.png", "北京市海淀区西北旺东路10号院百度科技园1号楼"));
        list.add(new CompanyHasImgModel("Lemur", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/3.png", "亚马逊热带雨林"));
        list.add(new CompanyHasImgModel("一众", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/4.png", "山东济宁俺家"));

        Workbook sheets = ExcelExportUtil.exportExcel(
                new ExportParams("公司信息", "嘻嘻"),
                CompanyHasImgModel.class, list);

        File savefile = new File("C:\\Users\\23746\\Downloads/ExcelExportHasImgTest.exportCompanyImg.xls");
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        sheets.write(new FileOutputStream(savefile));
    }
}