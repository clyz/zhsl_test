package com.wisdom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasypoiSbsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasypoiSbsApplication.class, args);
    }
}
