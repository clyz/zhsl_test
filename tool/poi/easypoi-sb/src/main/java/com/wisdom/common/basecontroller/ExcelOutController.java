package com.wisdom.common.basecontroller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.exception.excel.ExcelExportException;
import com.wisdom.common.bean.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/")
public class ExcelOutController {
    /**
     * 导出集合
     */
    @GetMapping("course")
    public void course(HttpServletResponse response) throws IOException {
        ArrayList<CourseEntity> c = new ArrayList<>();

        ArrayList<StudentEntity> list = new ArrayList<>();
        list.add(new StudentEntity("1", "ziv", 1));
        list.add(new StudentEntity("2", "掌上", 2));
        c.add(new CourseEntity("1", new TeacherEntity("1234", "major"), "语文", list));
        c.add(new CourseEntity("1", new TeacherEntity("1234", "major"), "数学", list));
        c.add(new CourseEntity("1", new TeacherEntity("1234", "absent"), "政治", list));


        Workbook workbook = ExcelExportUtil.exportExcel(
                new ExportParams("班级", "一班"),
                CourseEntity.class, c);

        downloadExcel(response, workbook);
    }

    /**
     * 导出图片
     */
    @GetMapping("logo2")
    public void logo2() {
    }

    /**
     * 导出图片
     */
    @GetMapping("logo1")
    public void logo1(HttpServletResponse response) throws IOException {
        List list = new ArrayList<CompanyHasImgModel>();
        list.add(new CompanyHasImgModel("百度", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/1.png", "北京市海淀区西北旺东路10号院百度科技园1号楼"));
        list.add(new CompanyHasImgModel("阿里巴巴", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/2.png", "北京市海淀区西北旺东路10号院百度科技园1号楼"));
        list.add(new CompanyHasImgModel("Lemur", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/3.png", "亚马逊热带雨林"));
        list.add(new CompanyHasImgModel("一众", "E:\\java_web\\zhsl_obj\\zhsl_test\\easypoi-sbs\\src\\main\\resources/static/imgs/company/4.png", "山东济宁俺家"));
        downloadExcel(response,
                ExcelExportUtil.exportExcel(
                        new ExportParams("公司信息", "嘻嘻"),
                        CompanyHasImgModel.class, list));
    }

    public void downloadExcel(HttpServletResponse response, Workbook workbook) throws ExcelExportException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=asdf.xls");
        workbook.write(response.getOutputStream());
    }

    /**
     * 导出 实体
     */
    @GetMapping("student")
    public void student(HttpServletResponse response) throws IOException {
        ArrayList<StudentEntity> list = new ArrayList<>();
        list.add(new StudentEntity("1", "ziv", 1));
        list.add(new StudentEntity("2", "掌上", 2));
        Workbook workbook = ExcelExportUtil.exportExcel(
                new ExportParams("计算机一班学生", "学生"),
                StudentEntity.class, list);
        downloadExcel(response, workbook);
    }
}
