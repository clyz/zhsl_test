package com.wisdom.common.bean;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

import java.util.List;

@ExcelTarget("courseEntity")
@Data
public class CourseEntity implements java.io.Serializable {
    /**
     * 主键
     */
    private String id;
    /**
     * 老师主键
     */
    @ExcelEntity(id = "absent")
    private TeacherEntity mathTeacher;
    /**
     * 课程名称
     */
    @Excel(name = "课程名称", orderNum = "1", width = 25,needMerge=true)
    private String name;
    @ExcelCollection(name = "学生", orderNum = "4")
    private List<StudentEntity> students;

    public CourseEntity(String id, TeacherEntity mathTeacher, String name, List<StudentEntity> students) {
        this.id = id;
        this.mathTeacher = mathTeacher;
        this.name = name;
        this.students = students;
    }
}