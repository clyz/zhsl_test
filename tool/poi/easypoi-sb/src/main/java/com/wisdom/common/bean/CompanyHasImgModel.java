package com.wisdom.common.bean;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

@ExcelTarget("companyHasImgModel")
@Data
public class CompanyHasImgModel {
    @Excel(name = "公司")
    private String name;

    @Excel(name = "公司LOGO", type = 2 ,width = 40 , height = 20,imageType = 1)
    private String companyLogo;

    @Excel(name = "位置")
    private String loacl;

    public CompanyHasImgModel(String name, String companyLogo, String loacl) {
        this.name = name;
        this.companyLogo = companyLogo;
        this.loacl = loacl;
    }
}
