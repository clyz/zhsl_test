package com.wisdom.common;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.exception.excel.ExcelExportException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author smileorsilence
 * @date 2018/05/31
 */
@Slf4j
public class EasyPoiKit {

    /**
     * 创建Excel
     */
    public static void exportExcel(String title, String sheetName, boolean isCreateHeader,
                                   Class<?> pojoClass, List<?> data,
                                   HttpServletResponse response, String fileName) throws ExcelExportException {

        ExportParams exportParams = new ExportParams(title, sheetName);
        exportParams.setCreateHeadRows(isCreateHeader);

        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, pojoClass, data);
        if (workbook == null) {
            throw new ExcelExportException();
        }

        try {
            downloadExcel(response, fileName, workbook);
        } catch (ExcelExportException e) {
            throw new ExcelExportException();
        }
    }

    /**
     * 下载Excel
     */
    public static void downloadExcel(HttpServletResponse response, String fileName, Workbook workbook) throws ExcelExportException {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
        try {
            workbook.write(response.getOutputStream());
        } catch (Exception e) {
            throw new ExcelExportException();
        }
    }

}