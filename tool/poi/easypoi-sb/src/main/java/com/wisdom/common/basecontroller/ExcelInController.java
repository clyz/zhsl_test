package com.wisdom.common.basecontroller;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.wisdom.common.bean.StudentEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/in/")
public class ExcelInController {

    @PostMapping("student")
    @ResponseBody
    public List<StudentEntity> student(MultipartFile file) throws Exception {
        ImportParams params = new ImportParams();
        params.setTitleRows(1);
        params.setHeadRows(1);
        long start = System.currentTimeMillis();
        List<StudentEntity> list = ExcelImportUtil.importExcel(file.getInputStream(),StudentEntity.class, params);
        System.out.println(System.currentTimeMillis() - start);
        return list;
    }

}
