package com.wisdom.common.bean;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class StudentEntity implements Serializable {
    @Excel(name = "出生日期", format = "yyyy-MM-dd", isImportField = "true_st", width = 20)
    private LocalDate birthday;
    /**
     * id
     */
    private String id;
    /**
     * 学生姓名
     */
    @Excel(name = "学生姓名", height = 20, width = 30, isImportField = "true_st")
    private String name;

    @Excel(name = "进校日期", format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime registrationDate;
    /**
     * 学生性别
     */
    @Excel(name = "学生性别", replace = {"男_1", "女_2"}, suffix = "生", isImportField = "true_st")
    private int sex;

    public StudentEntity(String id, String name, int sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = LocalDate.now();
        this.registrationDate = LocalDateTime.now();
    }
}