package clyz.ziv.winrm4j;

import io.cloudsoft.winrm4j.client.WinRmClientContext;
import io.cloudsoft.winrm4j.winrm.WinRmTool;
import io.cloudsoft.winrm4j.winrm.WinRmToolResponse;
import org.apache.http.client.config.AuthSchemes;
import org.junit.Test;

/**
 * WIN  winrm 服务
 */
public class VisWinrm4j {

    @Test
    public void visWinrm4j() {
        WinRmClientContext context = WinRmClientContext.newInstance();
        WinRmTool.Builder builder = WinRmTool.Builder
                .builder("192.168.1.108", "Administrator", "");
        builder.setAuthenticationScheme(AuthSchemes.NTLM);
        builder.port(5985);
        builder.useHttps(false);

        builder.context(context);
        WinRmTool tool = builder.build();
        tool.setOperationTimeout(5000L);
        System.out.println("========");
        String command = "dir";
        WinRmToolResponse resp = tool.executeCommand(command);
        System.out.println(resp.getStatusCode());
        String out = resp.getStdOut();
        System.out.println(out);
        context.shutdown();
    }
}
