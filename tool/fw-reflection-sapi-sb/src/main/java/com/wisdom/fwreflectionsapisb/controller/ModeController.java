package com.wisdom.fwreflectionsapisb.controller;

import com.github.xiaour.api_scanner.annotation.SapiGroup;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author: ziv
 * @date: 2018/12/17 09:31
 * @version: 0.0.1
 */
@RequestMapping("/model/")
@Controller
@SapiGroup(title = "model")
public class ModeController {

    @GetMapping("ajax")
    public ModelAndView ajax(ModelAndView ajax) {
        ajax.setViewName("ajax");
        return ajax;
    }

    @GetMapping("index")
    public String index() {
        return "index";
    }
}
