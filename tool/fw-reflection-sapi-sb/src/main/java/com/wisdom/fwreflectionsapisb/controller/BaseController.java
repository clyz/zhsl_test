package com.wisdom.fwreflectionsapisb.controller;

import com.github.xiaour.api_scanner.annotation.SapiGroup;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

@RequestMapping("/base/")
@RestController
@SapiGroup(title = "base")
public class BaseController{
    @GetMapping("list")
    public List list() {
        return Arrays.asList("ziv", "xo");
    }

    @PostMapping("save")
    public Callable save() {
        return () -> null;
    }
}