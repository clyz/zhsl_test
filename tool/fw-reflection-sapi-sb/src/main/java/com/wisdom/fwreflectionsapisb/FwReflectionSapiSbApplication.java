package com.wisdom.fwreflectionsapisb;

import com.github.xiaour.api_scanner.annotation.Sapi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Sapi(controllers = {"com.wisdom.fwreflectionsapisb.controller"})
public class FwReflectionSapiSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(FwReflectionSapiSbApplication.class, args);
    }

}

