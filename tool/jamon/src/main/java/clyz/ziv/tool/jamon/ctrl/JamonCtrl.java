package clyz.ziv.tool.jamon.ctrl;

import com.jamonapi.MonKey;
import com.jamonapi.MonKeyImp;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author YangZemin
 * @date 2019/10/7 16:06
 */
@RequestMapping("/j/")
@RestController
@Slf4j
public class JamonCtrl {
    @GetMapping
    public void index() {
        Monitor monitor = MonitorFactory.start("jamon.test");
        log.info("测试 jamon.test GET /");
        monitor.stop();
    }

    @GetMapping("/t")
    public void t() {
        MonKey key = new MonKeyImp("com.jamonapi.pageHits", "stackTrace", "ms.");
        Monitor monitor = MonitorFactory.add(key, Math.random() * 1000.0);
        monitor.start();
        log.info("测试 jamon.test GET t");
        monitor.stop();
    }
}