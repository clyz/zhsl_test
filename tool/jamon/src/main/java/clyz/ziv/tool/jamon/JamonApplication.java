package clyz.ziv.tool.jamon;

import clyz.ziv.tool.jamon.filter.JF;
import com.jamonapi.JAMonFilter;
import com.jamonapi.MonKeyImp;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 监控
 *
 * @author YangZemin
 * @date 2019/10/7 16:06
 */
@Log
@SpringBootApplication
public class JamonApplication {

    public static void main(String[] args) {
        SpringApplication.run(JamonApplication.class, args);
    }

    @Bean
    public FilterRegistrationBean<JAMonFilter> filterRegistrationBean() {
        FilterRegistrationBean filterBean = new FilterRegistrationBean<JAMonFilter>();
        filterBean.setFilter(new JF());
        filterBean.setName("JAMonFilter");
        filterBean.addUrlPatterns("/*");
        return filterBean;
    }
}