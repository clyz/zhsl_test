package clyz.ziv.tool.jamon.filter;

import com.jamonapi.JAMonFilter;
import com.jamonapi.MonKeyImp;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import lombok.extern.java.Log;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author YangZemin
 * @date 2019/10/7 16:59
 */
@Log
public class JF extends JAMonFilter {
    private static final long serialVersionUID = 5746197114960908454L;

    private FilterConfig filterConfig = null;

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        log.info("   Jamon Filter  START ");
        Monitor allPages = MonitorFactory.start(new MonKeyImp("jammon.webui.allPages", getURI(request), "ms."));
        Monitor monitor = MonitorFactory.start(getURI(request));

        try {
            filterChain.doFilter(request, response);
        } finally {
            monitor.stop();
            allPages.stop();
        }
        log.info("   Jamon Filter  END ");
    }

    @Override
    protected String getURI(ServletRequest request) {
        if (request instanceof HttpServletRequest) {
            return ((HttpServletRequest) request).getRequestURI();
        } else {
            return "Not an HttpServletRequest";
        }
    }
}