package com.clyz.netty.netty3.五指棋.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;

/**
 * 1 连接
 * 0 连接失败
 * 2 开始游戏   2表示白，21表示黑
 * 3 推送游戏信息
 * 4 游戏结束
 */
@Slf4j
public class TextWebSocketFrameHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    boolean start = false;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {
        log.info("消息 {}", msg.text());
        if (!start && "2".equals(msg.text())) {
            for (Channel c : channels) {
                if (ctx.channel() != c) {
                    c.writeAndFlush(new TextWebSocketFrame("21:游戏开始"));
                }
            }
            start = true;
        } else {
            for (Channel c : channels) {
                if (ctx.channel() != c) {
                    c.writeAndFlush(new TextWebSocketFrame(msg.text()));
                }
            }

            if (msg.text().startsWith("4:")) {
                start = false;
                for (Channel c : channels) {
                    channels.remove(c);
                }
            }
        }
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        if (channels.size() == 2) {
            ctx.channel().writeAndFlush(new TextWebSocketFrame("0:已有人在对弈中。。"));
        } else if (channels.size() == 1) {
            channels.add(ctx.channel());
            channels.writeAndFlush(new TextWebSocketFrame("2:游戏开始"));
        } else if (channels.size() == 0) {
            ctx.channel().writeAndFlush(new TextWebSocketFrame("1:等待人加入可以开始博弈了。。"));
            channels.add(ctx.channel());
        } else {
            ctx.channel().writeAndFlush(new TextWebSocketFrame("0:连接失败。。"));
        }
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        channels.remove(ctx.channel());
        ctx.channel().writeAndFlush(new TextWebSocketFrame("1:等待人加入可以开始博弈了。。"));
        start = false;
    }
}
