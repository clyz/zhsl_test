package com.clyz.netty.netty4.client.controller;

import com.clyz.netty.netty4.bean.Msg;
import com.clyz.netty.netty4.client.NettyClient;
import io.netty.channel.Channel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.*;

/**
 * @author Zemin.Yang
 */
@RestController
@Slf4j
public class MsgController {
    private static ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
    private static ExecutorService executorService = Executors.newFixedThreadPool(2);
    private static Map<String, Channel> channelMap = new ConcurrentHashMap<>();

    @GetMapping("/send")
    public boolean send(@RequestParam String name, String str) {
        channelMap.get(name).writeAndFlush(new Msg(name, str));
        return true;
    }

    @SneakyThrows
    @GetMapping("open")
    public boolean open(@RequestParam String name) {
        NettyClient nettyClient = new NettyClient();
        Future<Channel> future = executorService.submit(nettyClient);
        Channel channel = future.get();
        if (null == channel) throw new RuntimeException("netty client start error channel is null");

        while (!nettyClient.isActive()) {
            log.info("NettyClient启动服务 ...");
            Thread.sleep(500);
        }
        log.info("NettyClient连接服务完成 {}", channel.localAddress());

        // Channel状态定时巡检；3秒后每5秒执行一次
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            while (!nettyClient.isActive()) {
                log.info("通信管道巡检：通信管道状态 " + nettyClient.isActive());
                try {
                    log.info("通信管道巡检：断线重连[Begin]");
                    Channel freshChannel = executorService.submit(nettyClient).get();
                    freshChannel.writeAndFlush(new Msg(freshChannel.id().toString(), "上线了"));
                } catch (InterruptedException | ExecutionException e) {
                    log.info("通信管道巡检：断线重连[Error]");
                }
            }
        }, 3, 5, TimeUnit.SECONDS);
        channelMap.put(name, channel);
        return true;
    }
}
