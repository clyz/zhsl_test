package com.clyz.netty.netty1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chat1NettyApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Chat1NettyApplication.class, args);

        StartNetty.netty1();
    }

}
