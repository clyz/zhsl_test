package com.clyz.netty.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class U {
    public static String toJson(Object value) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static <T> T toObject(String value, Class<T> tClass) {
        ObjectMapper mapper = new ObjectMapper();
        T json = null;
        try {
            json = mapper.readValue(value, tClass);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}
