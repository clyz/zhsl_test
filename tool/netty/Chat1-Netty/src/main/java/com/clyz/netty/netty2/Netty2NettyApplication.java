package com.clyz.netty.netty2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Netty2NettyApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Netty2NettyApplication.class, args);
    }

}
