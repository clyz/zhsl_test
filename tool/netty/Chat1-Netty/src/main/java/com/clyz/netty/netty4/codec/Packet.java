package com.clyz.netty.netty4.codec;

import com.clyz.netty.netty4.bean.Msg;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public abstract class Packet {

    private final static Map<Byte, Class<? extends Packet>> packetType = new ConcurrentHashMap<>();

    static {
        packetType.put(Command.msg, Msg.class);
    }

    public static Class<? extends Packet> get(Byte command) {
        return packetType.get(command);
    }

    /**
     * 获取协议指令
     *
     * @return 返回指令值
     */
    public abstract Byte getCommand();

}
