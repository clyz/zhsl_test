package com.clyz.netty.netty2.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TextWebSocketFrameHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {
        log.info("消息 {}", msg.text());
        for (Channel c : channels) {
            if (ctx.channel() != c) {
                c.writeAndFlush(new TextWebSocketFrame(msg.text()));
            }
        }

    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        for (Channel channel : channels) {
            channel.writeAndFlush(new TextWebSocketFrame());
        }
        channels.add(ctx.channel());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpRequest) {
            log.info("准备提取token");
        }
        //接着建立请求
        super.channelRead(ctx, msg);
    }
}
