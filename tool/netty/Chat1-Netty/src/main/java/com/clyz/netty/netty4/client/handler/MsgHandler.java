package com.clyz.netty.netty4.client.handler;

import cn.hutool.json.JSONUtil;
import com.clyz.netty.netty4.bean.Msg;
import com.clyz.netty.netty4.codec.MyBizHandler;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MsgHandler extends MyBizHandler<Msg> {

    @Override
    public void channelRead(Channel channel, Msg msg) {
        log.info("{}", JSONUtil.toJsonStr(msg));
    }

}
