package com.clyz.netty.netty1.handler;

import com.clyz.netty.util.U;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class UserWebSocketFrameHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    static Map<String, User> sessions = new ConcurrentHashMap<>();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        String text = msg.text();

        Map<String, Object> message = new HashMap<>();
        if (text.startsWith("$img")) {
            message.put("type", "img_msg"); //普通消息
            message.put("msg", text.substring(4));
        } else {
            message.put("type", "normal_msg"); //普通消息
            message.put("msg", text);
        }

        Channel incoming = ctx.channel();
        message.put("user", sessions.get(incoming.id().asShortText()));

        String json = U.toJson(message);

        for (Channel channel : channels) {
            channel.writeAndFlush(new TextWebSocketFrame(json));
        }

    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {  // (2)
        Channel incoming = ctx.channel();
        channels.add(ctx.channel());
        log.info("Client:" + incoming.remoteAddress() + "加入");

        String header = head_image[new Random().nextInt(head_image.length)];
        User user = new User(header, incoming.id().asShortText());
        sessions.put(incoming.id().asShortText(), user);

        incoming.writeAndFlush(new TextWebSocketFrame("ID:" + incoming.id().asShortText()));
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {  // (3)
        Channel incoming = ctx.channel();
        log.info("Client:" + incoming.remoteAddress() + "离开");
        channels.remove(ctx.channel());

        sessions.remove(incoming.id().asShortText());
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception { // (5)
        Channel incoming = ctx.channel();
        log.info("Client:" + incoming.remoteAddress() + "在线");
        incoming.writeAndFlush(new TextWebSocketFrame("ID:" + incoming.id().asShortText()));
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception { // (6)
        Channel incoming = ctx.channel();
        log.info("Client:" + incoming.remoteAddress() + "掉线");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        Channel incoming = ctx.channel();
        log.info("Client:" + incoming.remoteAddress() + "异常");
        // 当出现异常就关闭连接
        cause.printStackTrace();
        ctx.close();
    }

    public static class User implements Serializable {
        public String head;
        public String name;

        public User(String header, String name) {
            this.head = header;
            this.name = name;
        }
    }

    String[] head_image = new String[]{"christian.jpg",
            "daniel.jpg",
            "elliot.jpg",
            "helen.jpg",
            "jenny.jpg",
            "lena.png",
            "lindsay.png",
            "mark.png",
            "matt.jpg",
            "molly.png",
            "steve.jpg",
            "stevie.jpg",
            "tom.jpg",
            "veronika.jpg"};

}