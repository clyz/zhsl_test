package com.clyz.netty.netty4.client;

import com.clyz.netty.netty4.Netty4NettyApplication;
import com.clyz.netty.netty4.client.handler.MyChannelInitializer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
public class NettyClient implements Callable<Channel> {


    private final EventLoopGroup workerGroup = new NioEventLoopGroup();
    private Channel channel;

    @Override
    public Channel call() {
        ChannelFuture channelFuture = null;
        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.AUTO_READ, true);
            b.handler(new MyChannelInitializer());
            String inetHost = "127.0.0.1";
            int inetPort = 7397;
            channelFuture = b.connect(inetHost, inetPort).syncUninterruptibly();
            this.channel = channelFuture.channel();
        } catch (Exception e) {
            log.error("socket client start error {}", e.getMessage());
        } finally {
            if (null != channelFuture && channelFuture.isSuccess()) {
                log.info("socket client start done. ");
            } else {
                log.error("socket client start error. ");
            }
        }
        return channel;
    }

    public void destroy() {
        if (null == channel) {
            return;
        }
        channel.close();
        workerGroup.shutdownGracefully();
    }

    public boolean isActive() {
        return channel.isActive();
    }

    public Channel channel() {
        return channel;
    }


}
