package com.clyz.netty.netty3.五指棋;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Netty3NettyApplication {

    public static void main(String[] args) {
        SpringApplication.run(Netty3NettyApplication.class, args);
    }

}
