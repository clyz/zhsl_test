package com.clyz.netty.netty4.bean;

import com.clyz.netty.netty4.codec.Command;
import com.clyz.netty.netty4.codec.Packet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Zemin.Yang
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Msg extends Packet implements Serializable {
    private String msg;
    private String target;

    @Override
    public Byte getCommand() {
        return Command.msg;
    }
}
