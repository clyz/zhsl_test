package com.clyz.netty.netty2.bean;

import com.clyz.netty.util.U;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class User {
    private String name;
    private String head;
    private String msg;
    /**
     * {"name":"昵称","head":"头像","msg":"你是SB"}
     */
    public static void main(String[] args) {
        User user = new User();
        user.setName("昵称");
        user.setHead("头像");
        user.setMsg("你是SB");
        log.info(U.toJson(user));
    }
}
