package com.clyz.netty.netty4;

import com.clyz.netty.netty4.server.NettyWebSocketApplicationRunner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.util.concurrent.ExecutionException;

@SpringBootApplication
@Import(NettyWebSocketApplicationRunner.class)
@Slf4j
public class Netty4NettyApplication {

    public static void main(String[] args){
        SpringApplication.run(Netty4NettyApplication.class, args);
    }

}
