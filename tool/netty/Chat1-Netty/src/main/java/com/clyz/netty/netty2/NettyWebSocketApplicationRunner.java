package com.clyz.netty.netty2;

import com.clyz.netty.netty2.handler.TextWebSocketFrameHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Slf4j
//@Component
public class NettyWebSocketApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) {
        // 创建主线程池组，处理客户端的连接(多反应器)
        NioEventLoopGroup mainGroup = new NioEventLoopGroup();

        // 创建从线程池组，处理客户端的读写(多反应器)
        NioEventLoopGroup subGroup = new NioEventLoopGroup();

        try {
            // 创建netty引导类，配置和串联系列组件（设置线程模型，设置通道类型，设置客户端处理器handler，设置绑定端口号）
            ServerBootstrap bootstrap = new ServerBootstrap();

            bootstrap.group(mainGroup, subGroup);
            bootstrap.channel(NioServerSocketChannel.class);
            bootstrap.childHandler(new ChannelInitializer<Channel>() {
                @Override
                protected void initChannel(Channel channel) throws Exception {
                    // 配置链式解码器
                    ChannelPipeline pipeline = channel.pipeline();

                    // 解码成HttpRequest
                   pipeline.addLast(new HttpServerCodec());

                    // 解码成FullHttpRequest   POST
                    pipeline.addLast(new HttpObjectAggregator(1024 * 10));

                    // 添加WebSocket解编码
                    pipeline.addLast(new WebSocketServerProtocolHandler("/"));

                    // 添加处自定义的处理器
                    pipeline.addLast(new TextWebSocketFrameHandler());
                }
            });

            // 异步绑定端口号，需要阻塞住直到端口号绑定成功
            ChannelFuture channelFuture = bootstrap.bind(7397);
            channelFuture.sync();

            log.info("websocket服务端启动成功啦！");

        } catch (InterruptedException e) {
            log.info("{} websocket服务器启动异常", e.getMessage());
        } finally {
//            mainGroup.shutdownGracefully();
//            subGroup.shutdownGracefully();
        }
    }
}
