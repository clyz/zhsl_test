package com.clyz.netty.netty4.client.handler;

import com.clyz.netty.netty4.codec.ObjDecoder;
import com.clyz.netty.netty4.codec.ObjEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class MyChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        //对象传输处理[解码]
        channel.pipeline().addLast(new ObjDecoder());
        // 在管道中添加我们自己的接收数据实现方法
        channel.pipeline().addLast(new MsgHandler());

        //对象传输处理[编码]
        channel.pipeline().addLast(new ObjEncoder());
    }

}
