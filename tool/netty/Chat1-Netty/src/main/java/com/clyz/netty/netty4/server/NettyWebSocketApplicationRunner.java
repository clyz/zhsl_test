package com.clyz.netty.netty4.server;

import com.clyz.netty.netty4.codec.ObjDecoder;
import com.clyz.netty.netty4.codec.ObjEncoder;
import com.clyz.netty.netty4.server.handler.MyServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

@Slf4j
public class NettyWebSocketApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) {
        NioEventLoopGroup mainGroup = new NioEventLoopGroup();
        NioEventLoopGroup subGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();

            bootstrap.group(mainGroup, subGroup);
            bootstrap.channel(NioServerSocketChannel.class);
            bootstrap.childHandler(new ChannelInitializer<Channel>() {
                @Override
                protected void initChannel(Channel channel) throws Exception {
                    // 配置链式解码器
                    ChannelPipeline pipeline = channel.pipeline();
                    pipeline.addLast(new ObjDecoder());
                    pipeline.addLast(new ObjEncoder());
                    // 添加处自定义的处理器
                    pipeline.addLast(new MyServerHandler());
                }
            });

            // 异步绑定端口号，需要阻塞住直到端口号绑定成功
            ChannelFuture channelFuture = bootstrap.bind(7397);
            channelFuture.sync();

            log.info("websocket服务端启动成功啦！");

        } catch (InterruptedException e) {
            log.info("{} websocket服务器启动异常", e.getMessage());
        } finally {
//            mainGroup.shutdownGracefully();
//            subGroup.shutdownGracefully();
        }
    }
}
