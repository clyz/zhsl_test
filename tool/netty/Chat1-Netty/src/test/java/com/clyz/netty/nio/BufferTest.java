package com.clyz.netty.nio;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.nio.IntBuffer;

/**
 * Buffer（缓存） 介绍
 */
public class BufferTest {
    @Test
    void intBuffer1() {
        //  创建 Buffer 对象 ,20*4 字符
        IntBuffer buffer = IntBuffer.allocate(20);
        for (int i = 0; i < 20; i++) {
            //  写，存值
            buffer.put(i);
        }
        log.info(buffer.limit());
        log.info(buffer.position());

        //  反转，Buffer 记录信息
        buffer.flip();

        for (int i = 0; i < 20; i++) {
            //  读
            System.out.print(buffer.get());
        }
        log.info(buffer.limit());
        log.info(buffer.position());

        //  清楚 信息 清空或者压缩缓冲区 缓冲区会被切换成写入模式
        buffer.clear();
//        buffer.compact();

        //  在进行第二次 读写
        for (int i = 0; i < 5; i++) {
            buffer.put(i);
        }
        buffer.flip();
        for (int i = 0; i < 5; i++) {
            System.out.print(buffer.get());
        }

        log.info("重新读");
        //  再次读 倒带
        buffer.rewind();
        for (int i = 0; i < 5; i++) {
            System.out.print(buffer.get());
        }
        buffer.rewind();

        log.info("重新读2");
        //  记录当前 mark 位置
        buffer.mark();
        for (int i = 0; i < 2; i++) {
            System.out.print(buffer.get());
        }
        //  回复到记录位置
        buffer.reset();
        for (int i = 2; i < 5; i++) {
            System.out.print(buffer.get());
        }
    }

    @AfterEach
    public void after(){
        log.info("");
    }
}
