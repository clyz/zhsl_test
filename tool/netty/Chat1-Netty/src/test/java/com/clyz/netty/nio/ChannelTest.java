package com.clyz.netty.nio;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

public class ChannelTest {
    @Test
    void channelTest1() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("1.txt");
        FileChannel inputChannel = fileInputStream.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(1024);

        //  此时 对于 buffer 而言即为 写
        while (inputChannel.read(buffer) != -1) {
            //  反转 buffer 为 读
            buffer.flip();
            log.info(new String(buffer.array()));
        }

        FileOutputStream fileOutputStream = new FileOutputStream("1.txt");
        FileChannel outputChannel = fileOutputStream.getChannel();

        //  反转为写
        buffer.clear();
        buffer.put("你好!".getBytes(StandardCharsets.UTF_8));
        //  改为读
        buffer.flip();
        while (outputChannel.write(buffer) != 0) {

        }

        inputChannel.close();
        //  强制刷新到磁盘
        outputChannel.force(true);
        fileInputStream.close();
        fileOutputStream.close();

    }

    @AfterEach
    void after() {
        log.info();
    }
}
