package com.clyz.xss.controller;

import com.clyz.xss.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@Slf4j
public class XssController {

    @GetMapping("getUser")
    public User getUser(String html, String param2,Long id) {
        log.info("/getUser html: {},{},{}", html, param2,id);
        return User.builder().id("id").name("name").pwd("pwd").build();
    }
}