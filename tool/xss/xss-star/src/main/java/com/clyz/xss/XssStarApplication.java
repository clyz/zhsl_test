package com.clyz.xss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XssStarApplication {

    public static void main(String[] args) {
        SpringApplication.run(XssStarApplication.class, args);
    }

}
