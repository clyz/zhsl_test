package com.wisdom.device;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;

public interface CLibrary extends Library {
    CLibrary INSTANCE = Native.loadLibrary("/sdtapi.dll", CLibrary.class);

    /**
     * 打开串口/USB。
     *
     * @param iPort [in] 整数，表示端口号。1-16（十进制）为串口，1001-1016（十进制）为USB口，缺省的一个USB设备端口号是1001。
     * @return 0x90 打开端口成功1 打开端口失败/端口号不合法
     */
    int SDT_OpenPort(int iPort);

    /**
     * 关闭串口/USB
     *
     * @param iPort
     * @return
     */
    int SDT_ClosePort(int iPort);

    int SDT_GetSAMIDToStr(int iPort, StringByReference pcSAMID, int iIfOpen);

    int SDT_GetSAMID(int iPort, StringByReference pcSAMID, int iIfOpen);

    int SDT_StartFindIDCard(int iPort, StringByReference pucManaInfo, int iIfOpen);

    int SDT_SelectIDCard(int iPort, StringByReference pucManaMsg, int iIfOpen);

    int SDT_ReadBaseMsg(int iPort,
                        StringByReference pucCHMsg,
                        IntByReference puiCHMsgLen,
                        StringByReference pucPHMsg,
                        IntByReference puiPHMsgLen,
                        int iIfOpen);

    int SDT_ReadBaseMsg(int iPort,
                        ByteByReference[] pucCHMsg,
                        IntByReference puiCHMsgLen,
                        StringByReference pucPHMsg,
                        IntByReference puiPHMsgLen,
                        int iIfOpen);


    int SDT_ReadBaseMsg(int iPort,
                        Pointer pucCHMsg,
                        IntByReference puiCHMsgLen,
                        StringByReference pucPHMsg,
                        IntByReference puiPHMsgLen,
                        int iIfOpen);
}