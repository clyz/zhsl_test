package com.wisdom.device;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import static com.sun.org.apache.xml.internal.serialize.OutputFormat.Defaults.Encoding;

public class CLibraryTest {

    @Test
    public void read1() throws UnsupportedEncodingException {
        int iPort = 1001;
        int iIfOpen = 0;
        int iRet = 0;
        StringByReference pucManaInfo = new StringByReference(4);
        String pucManaMsg = new StringBuilder(8).toString();

        StringByReference pucManaMsgP = new StringByReference(8);

/*        String pucCHMsg =  new StringBuilder(512).toString();   //  文字
        String pucPHMsg =  new StringBuilder(1024).toString();  //  照片
        String pucFPMsg = new StringBuilder(1024).toString();   //  指纹*/

        StringByReference pucCHMsg = new StringByReference(1024);
        StringByReference pucPHMsg = new StringByReference(1024);  //  照片

        IntByReference uiCHMsgLen = new IntByReference();
        IntByReference uiPHMsgLen = new IntByReference();

        CLibrary instance = CLibrary.INSTANCE;

        if (iIfOpen == 0) {
            iRet = instance.SDT_OpenPort(iPort);
        }

        if (iRet != 0x90) {
            System.out.println("SDT_OpenPort        error, error code is: " + iRet);
            instance.SDT_ClosePort(iPort);
            return;
        }

        while (true) {
            iRet = instance.SDT_StartFindIDCard(iPort, pucManaInfo, iIfOpen);
            if (iRet == 0x9f) {
                System.out.println("SDT_StartFindIDCard  " + pucManaInfo.getValue());
                iRet = instance.SDT_SelectIDCard(iPort, pucManaMsgP, iIfOpen);
                System.out.println("SDT_SelectIDCard  "+pucManaMsgP.getValue());
                if (iRet == 0x90) break;
            }
            System.err.print("尚未找到卡,你想继续找卡 000");
        }

        Pointer resultv = Pointer.NULL;
        resultv = new Memory(1024);
        iRet = instance.SDT_ReadBaseMsg(iPort, pucCHMsg, uiCHMsgLen, pucPHMsg, uiPHMsgLen, iIfOpen);



//        EncodingUtils.getString(buffer, "UTF-8");

        if (iRet != 0x90) {
            System.out.println("读取追加信息失败（具体含义参见返回码表）");
            if (iIfOpen == 0) instance.SDT_ClosePort(iPort);
            return;
        } else {
            System.out.println("读取追加信息成功");
        }
        if (iIfOpen == 0) instance.SDT_ClosePort(iPort);

        System.out.println("pucCHMsg " + pucCHMsg.getPointer().getWideString(0));
        System.out.println("pucPHMsg " + pucPHMsg.getValue());
        System.out.println(uiCHMsgLen.getValue());
        System.out.println(uiPHMsgLen.getPointer().getWideString(0));

        System.out.println(
                resultv.getWideString(0)
        );

    }

}