package com.wisdom.device;

import com.sun.jna.ptr.ByReference;

public class ByteArrayByReference extends ByReference {

    protected ByteArrayByReference() {
        this(0);
    }

    public ByteArrayByReference(int value) {
        super(1);
        setValue((byte)value);
    }

    public void setValue(byte value) {
        getPointer().setByte(0,value);
    }

    public byte[] getValue() {
        return getPointer().getByteArray(0,255);
    }
}
