package com.wisdom.device;

import com.sun.jna.Native;

public class JnaUtil {

    static CLibrary INSTANCE;
    public static CLibrary LED_INSTANCE;

    static {
        // 获取jdk位数
        String bits = System.getProperty("sun.arch.data.model");
        // 获取os名称
        String ops = System.getProperty("os.name");
        if (ops.startsWith("win") || ops.startsWith("Win"))//windows
        {
            if ("32".equals(bits)) {
                INSTANCE = (CLibrary) Native.loadLibrary("CCR_SDKx32.dll", CLibrary.class);
            }
            if ("64".equals(bits)) {
                INSTANCE = (CLibrary) Native.loadLibrary("CCR_SDKx64.dll", CLibrary.class);
                LED_INSTANCE = Native.loadLibrary("LEDControl_x64.dll", CLibrary.class);
            }
        } else {
            if ("32".equals(bits)) {
                INSTANCE = (CLibrary) Native.loadLibrary("libCCR_SDKx64-x86_32.so", CLibrary.class);
            }
            if ("64".equals(bits)) {
                INSTANCE = (CLibrary) Native.loadLibrary("libCCR_SDKx64-x86_64.so", CLibrary.class);
                LED_INSTANCE = Native.loadLibrary("libLEDControl-x86_64.so", CLibrary.class);
            }
        }
    }
}