package com.wisdom.device;

import com.sun.jna.ptr.ByReference;

public class StringByReference extends ByReference {
    public StringByReference() {
        this(0);
    }

    public StringByReference(int value) {
        super(255);
        setValue(value);
    }

    public String getValue() {
        return getPointer().getString(0);
    }


    public void setValue(int value) {
        getPointer().setInt(0, value);
    }
}
