package ziv.clyz.tool.ORcode.url.util;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liuxin
 * @version 1.0
 * @Title CreatrQrCode.java
 * @description TODO    生成二维码
 * @time 2019年1月28日 下午3:32:37
 */
@SuppressWarnings({"unchecked", "rawtypes", "restriction", "deprecation"})
public class CreatrQrCode {

    // logo默认边框颜色	WHITE
    public static final Color DEFAULT_BORDERCOLOR = Color.WHITE;
    // logo默认边框宽度
    public static final int DEFAULT_BORDER = 1;
    // logo大小默认为照片的1/6
    public static final int DEFAULT_LOGOPART = 4;

    // 容错率
    public static final double rate = 0.30d;
    /**
     * 容错率 电平
     * L（低） 7%的码字可以被恢复。
     * M级（中） 15%的码字可以被恢复。
     * Q级（四分之一）25%的码字可以被恢复。
     * H级（高） 30%的码字可以被恢复。
     * <p>
     * 越高误差校正水平，越少的存储容量
     */
    private static final ErrorCorrectionLevel E_RATE = ErrorCorrectionLevel.M;
    // 	二维码宽
    public static final int QRCODE_WIDTH = 300;
    // 	二维码高
    public static final int QRCODE_HEIGHT = 200;// 默认宽高相同
    //	二维码页边距
    public static final int QRCODE_MARGIN = 1;// 指定生成条码时要使用的边距(以像素为单位)
    //	二维码内容所使用字符集编码
    public static final String CHARACTER = "UTF-8";

    // logo图片的地址
    public static String LogoPath = "D:" + File.separator + "logo.png";

    private final int border = DEFAULT_BORDER;
    //自定义颜色
    private final Color borderColor;
    // 自定义logo大小
    private final int logoPart;

    /**
     * 颜色上创建一个默认配置,生成正常的黑白条码。
     */
    public CreatrQrCode() {
        this(DEFAULT_BORDERCOLOR, DEFAULT_LOGOPART);
    }

    public CreatrQrCode(Color borderColor, int logoPart) {
        this.borderColor = borderColor;
        this.logoPart = logoPart;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public int getBorder() {
        return border;
    }

    public int getLogoPart() {
        return logoPart;
    }

    /**
     * @param qrPic        二维码文件
     * @param logoPic      logo文件
     * @param creatrQrCode 生成的二维码
     * @return boolean
     * @Title addLogo_QRCode
     * @Description 给二维码图片添加Logo
     * @author liuxin
     * @date 2019年1月28日下午2:06:48
     */
    private static boolean addLogo_QRCode(File qrPic, File logoPic, CreatrQrCode creatrQrCode) {
        try {
            if (!qrPic.isFile() || !logoPic.isFile()) {
                return false;
            }
            //读取二维码图片，并构建绘图对象
            BufferedImage image = ImageIO.read(qrPic);
            Graphics2D g = image.createGraphics();
            //读取Logo图片
            BufferedImage logo = ImageIO.read(logoPic);
            //保持二维码是正方形的
            int widthLogo = image.getWidth() / creatrQrCode.getLogoPart();
            int heightLogo = image.getWidth() / creatrQrCode.getLogoPart();
            // 计算图片放置位置
            int x = (image.getWidth() - widthLogo) / 2;
            int y = (image.getHeight() - heightLogo) / 2;
            //开始绘制图片
            g.drawImage(logo, x, y, widthLogo, heightLogo, null);
            g.drawRoundRect(x, y, widthLogo, heightLogo, 10, 10);
            g.setStroke(new BasicStroke(creatrQrCode.getBorder()));
            g.setColor(creatrQrCode.getBorderColor());
            g.drawRect(x, y, widthLogo, heightLogo);
            g.dispose();
            ImageIO.write(image, "jpeg", qrPic);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @param pressText 二维码需要显示的文字
     * @param newImg    带文字的图片
     * @param targetImg 需要添加文字的图片
     * @param fontStyle 文字样式
     * @param color     颜色
     * @param fontSize  字体大小
     * @param width     图片宽度
     * @param height    图片高度
     * @return boolean
     * @Title pressText
     * @Description 二维码图片添加文字
     * @author liuxin
     * @date 2019年1月28日下午2:02:54
     */
    public static boolean pressText(String pressText, String newImg, String targetImg, int fontStyle, Color color, int fontSize, int width, int height) {
        //计算文字开始的位置
        //x开始的位置（控制左右居中）：（图片宽度-字体大小*字的个数）/2
        //int startX = (width - (fontSize * pressText.length())) / 300;
        int startX = (width - (fontSize * pressText.length())) / 2;
        //y开始的位置（控制上下居中）：图片高度-（图片高度-图片宽度）/2
        int startY = height - (height - width) / 3;
        try {
            File file = new File(targetImg);
            Image src = ImageIO.read(file);
            int imageW = src.getWidth(null);
            int imageH = src.getHeight(null);
            BufferedImage image = new BufferedImage(imageW, imageH, BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            g.drawImage(src, 0, 0, imageW, imageH, null);
            g.setColor(color);
            g.setFont(new Font(null, fontStyle, fontSize));
            g.drawString(pressText, startX, startY);
            g.dispose();
            FileOutputStream out = new FileOutputStream(newImg);
            ImageIO.write(image, "JPEG", out);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image);
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @param qrcPath 用来存放生成的二维码图片
     * @param content 二维码表示的内容
     * @param width   图片完整的宽
     * @param height  图片完整的高
     * @return boolean
     * @Title createQrCode
     * @Description 生成二维码（无logo，无文字）
     * @advice 如果要在二维码下方附上文字，把图片设置为长方形（高大于宽）
     * @author liuxin
     * @date 2019年1月28日下午1:59:03
     */
    public boolean createQrCode(String qrcPath, String content, int width, int height) {
        try {
            File qrcFile = new File(qrcPath);
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            Map<EncodeHintType, Comparable> hints = new HashMap<EncodeHintType, Comparable>();
            //设置UTF-8， 防止中文乱码
            hints.put(EncodeHintType.CHARACTER_SET, CHARACTER);
            //设置二维码四周白色区域的大小
            hints.put(EncodeHintType.MARGIN, QRCODE_MARGIN);
            //设置二维码的容错性
            hints.put(EncodeHintType.ERROR_CORRECTION, E_RATE);
            //画二维码，记得调用multiFormatWriter.encode()时最后要带上hints参数，不然上面设置无效
            BitMatrix bitMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, width, height, hints);
            //开始画二维码
            MatrixToImageWriter.writeToFile(bitMatrix, "jpg", qrcFile);
            //二维码生成成功
            return true;
        } catch (Exception e) {
            //二维码生成失败
            return false;
        }
    }

    /**
     * @param qrcPath  用来存放生成的二维码图片
     * @param content  二维码表示的内容
     * @param width    图片完整的宽
     * @param height   图片完整的高
     * @param logoPath 附加在二维码中的图片（logo）
     * @return boolean
     * @Title createLogoQrCode
     * @Description 生成二维码（有logo，无文字）
     * @author liuxin
     * @date 2019年1月28日下午2:17:51
     */
    public boolean createLogoQrCode(String qrcPath, String content, int width, int height, String logoPath) {
        //	判断二维码是否生成成功
        if (createQrCode(qrcPath, content, width, height)) {
            //	在二维码中加入图片
            CreatrQrCode creatrQrCode = new CreatrQrCode();
            File logoFile = new File(logoPath);
            File qrcFile = new File(qrcPath);
            //	判断logo是否添加成功
            if (!addLogo_QRCode(qrcFile, logoFile, creatrQrCode)) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param newImageWithText 用来存放的带有文字的二维码图片
     * @param targetImage      原二维码图片
     * @param text             附加在图片上的文字信息
     * @param width            图片宽度（用来计算文字x开始位置）
     * @param height           图片高度（用来计算文字y开始位置）
     * @return boolean
     * @Title createWordQrcode
     * @Description 生成二维码（无logo，有文字）
     * @author liuxin
     * @date 2019年1月28日下午2:18:28
     */
    public boolean createWordQrcode(String newImageWithText, String targetImage, String text, int width, int height) {
        //	判断二维码是否生成成功
        if (createQrCode(newImageWithText, text, width, height)) {
            //	字体大小
            int font = 20;
            //	字体风格
            int fontStyle = 4;
            //	在二维码下方添加文字（是否替换原图）
            if (org.apache.commons.lang.StringUtils.isNotBlank(targetImage)) {
                //	二维码图片添加文字是否成功
                if (!pressText(text, newImageWithText, targetImage, fontStyle, Color.red, font, width, height)) {
                    return false;
                }
            } else {
                //	二维码图片添加文字是否成功
                if (!pressText(text, newImageWithText, newImageWithText, fontStyle, Color.red, font, width, height)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param newImageWithText 用来存放带有文字的二维码图片的位置
     * @param targetImage      原二维码图片
     * @param text             附加在二维码上的文字信息
     * @param width            图片宽度（用来计算文字x开始位置）
     * @param height           图片高度（用来计算文字y开始位置）
     * @param logoPath         附加在二维码上的图片
     * @return boolean
     * @Title createWordLogoQrcode
     * @Description 生成二维码（有logo，有文字）
     * @author liuxin
     * @date 2019年1月28日下午2:20:05
     */
    public boolean createWordLogoQrcode(String newImageWithText, String targetImage, String text, int width, int height, String logoPath) {
        //	判断二维码是否生成成功
        if (!createLogoQrCode(newImageWithText, text, width, height, logoPath)) {
            return false;
        }
        //	字体大小
        int font = 20;
        //	字体风格
        int fontStyle = 4;
        //	在二维码下方添加文字（是否替换原图）
        if (org.apache.commons.lang.StringUtils.isNotBlank(targetImage)) {
            if (!pressText(text, newImageWithText, targetImage, fontStyle, Color.red, font, width, height)) {
                return false;
            }
        } else {
            if (!pressText(text, newImageWithText, newImageWithText, fontStyle, Color.red, font, width, height)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param path
     * @return Result
     * @Title readQrCode
     * @Description 解析二维码
     * @author liuxin
     * @date 2019年1月28日下午4:43:24
     */
    public static Result readQrCode(String path) {
        Result result = null;
        try {
            MultiFormatReader multiFormatReader = new MultiFormatReader();
            File file = new File(path);
            BufferedImage image = ImageIO.read(file);
            //定义二维码参数
            Map hints = new HashMap();
            hints.put(EncodeHintType.CHARACTER_SET, CHARACTER);
            //获取读取二维码结果
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
            result = multiFormatReader.decode(binaryBitmap, hints);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String args[]) {
        try {
            CreatrQrCode creatrQrCode = new CreatrQrCode();
            //	图片地址加名称
            String path = "C:\\service_java\\ZHSL\\zhsl_test\\tool\\QR-code\\qr-code-vcard.png";
            //	图片显示的内容
            String text = "**省**市公安**市公安分局**派出所";

            //	Logo图片地址加名称
            String LogoPath = "C:\\service_java\\ZHSL\\zhsl_test\\tool\\QR-code\\qr-code.png";

            //二维码
            creatrQrCode.createQrCode(path,text,300,300);

            //Logo
            creatrQrCode.createLogoQrCode(path,text,300,300,LogoPath);

            //文字
//            creatrQrCode.createWordQrcode(path, "", text, 400, 470);

            //Logo+文字
//            creatrQrCode.createWordLogoQrcode(path, "",text, 400, 470,LogoPath);

            //System.out.println(File.separator);
            Result result = readQrCode(path);
            System.out.println("读取二维码： " + result.toString());
            System.out.println("二维码格式： " + result.getBarcodeFormat());
            System.out.println("二维码内容： " + result.getText());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}