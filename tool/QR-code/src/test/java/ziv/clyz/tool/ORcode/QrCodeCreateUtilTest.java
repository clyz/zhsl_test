package ziv.clyz.tool.ORcode;

import com.google.zxing.WriterException;
import ziv.clyz.tool.ORcode.url.util.QrCodeCreateUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class QrCodeCreateUtilTest {

    @org.junit.Test
    public void createQrCode() throws IOException, WriterException {
        QrCodeCreateUtil.createQrCode(
                new FileOutputStream(new File("qr-code.png")),
                "http://www.baidu.com",
                900, "png");
    }

    @org.junit.Test
    public void readQrCode() throws IOException {
        QrCodeCreateUtil.readQrCode(new FileInputStream(new File("qr-code.png")));
    }

}