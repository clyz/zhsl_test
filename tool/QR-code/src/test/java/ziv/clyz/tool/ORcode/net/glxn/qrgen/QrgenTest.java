package ziv.clyz.tool.ORcode.net.glxn.qrgen;

import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.core.vcard.VCard;
import net.glxn.qrgen.javase.QRCode;
import org.junit.Test;

import java.io.*;

/**
 * <p>
 * net.glxn.qrgen 二维码
 * </p>
 */
public class QrgenTest {

    /**
     * <p>
     * 创建一个二维码
     * </p>
     *
     * @throws IOException
     */
    @Test
    public void createQrCode() throws IOException {
        ByteArrayOutputStream bout = QRCode.from("https://www.baidu.com").withSize(250, 250).to(ImageType.PNG).stream();
        OutputStream out = new FileOutputStream("qr-code.png");
        bout.writeTo(out);
        out.flush();
        out.close();
    }

    /**
     * <p>
     * 创建一个明信片二维码
     * </p>
     */
    @Test
    public void vcard() {
        VCard vCard = new VCard();
        vCard.setName("相信那永恒的");
        vCard.setAddress("street 1, xxxx address");
        vCard.setCompany("company Inc.");
        vCard.setPhoneNumber("+xx/xx.xx.xx");
        vCard.setTitle("title");
        vCard.setEmail("info@gmail.com");
        vCard.setWebsite("https://google.com");
        ByteArrayOutputStream bout = QRCode.from(vCard).withCharset("utf-8").withSize(250, 250).to(ImageType.PNG).stream();
        try {
            OutputStream out = new FileOutputStream("qr-code-vcard.png");
            bout.writeTo(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * 其他二维码模式
     * </p>
     */
    @Test
    public void stream() {
        // get QR file from text using defaults
        File file = QRCode.from("Hello World").file();

        // get QR stream from text using defaults
        ByteArrayOutputStream stream = QRCode.from("Hello World").stream();

        // override the image type to be JPG
        QRCode.from("Hello World").to(ImageType.JPG).file();
        QRCode.from("Hello World").to(ImageType.JPG).stream();

        // override image size to be 250x250
        QRCode.from("Hello World").withSize(250, 250).file();
        QRCode.from("Hello World").withSize(250, 250).stream();

        // override size and image type
        QRCode.from("Hello World").to(ImageType.GIF).withSize(250, 250).file();
        QRCode.from("Hello World").to(ImageType.GIF).withSize(250, 250).stream();

        // supply own outputstream
//        QRCode.from("Hello World").to(ImageType.PNG).writeTo(outputStream);

        // supply own file name
        QRCode.from("Hello World").file("QRCode");

        // supply charset hint to ZXING
        QRCode.from("Hello World").withCharset("UTF-8");

        // supply error correction level hint to ZXING
        QRCode.from("Hello World").withErrorCorrection(ErrorCorrectionLevel.L);

        // supply any hint to ZXING
        QRCode.from("Hello World").withHint(EncodeHintType.CHARACTER_SET, "UTF-8");

        // encode contact data as vcard using defaults
        VCard johnDoe = new VCard("John Doe")
                .setEmail("john.doe@example.org")
                .setAddress("John Doe Street 1, 5678 Doestown")
                .setTitle("Mister")
                .setCompany("John Doe Inc.")
                .setPhoneNumber("1234")
                .setWebsite("www.example.org");
        QRCode.from(johnDoe).file();

        // if using special characters don't forget to supply the encoding
        VCard johnSpecial = new VCard("中文名称")
                .setAddress("北京东路201号");
        QRCode.from(johnSpecial).withCharset("UTF-8").file();
    }

}