package ziv.clyz.tool.ORcode.zxing;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static com.google.zxing.client.j2se.MatrixToImageConfig.BLACK;
import static com.google.zxing.client.j2se.MatrixToImageConfig.WHITE;


public class ZxingTest {

    private int w = 500;
    private int h = 500;


    private File logoFile = new File("C:\\Users\\Administrator\\Pictures\\\uD83C\uDF41.jpg");
    String content = "BEGIN:VCARD\n" +
            "VERSION:3.0\n" +
            "N:李德伟\n" +
            "EMAIL:1606841559@qq.com\n" +
            "TEL:12345678912\n" +
            "ADR:山东济南齐鲁软件园\n" +
            "ORG:济南\n" +
            "TITLE:软件工程师\n" +
//            "URL:http://blog.csdn.net/lidew521\n" +
            "NOTE:呼呼测试下吧。。。\n" +
            "END:VCARD";

    @Test
    public void createImg() throws IOException, WriterException {

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        Map<EncodeHintType, Comparable> hints = new HashMap<EncodeHintType, Comparable>();
        //设置UTF-8， 防止中文乱码
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        //设置二维码四周白色区域的大小    指定生成条码时要使用的边距(以像素为单位)
        hints.put(EncodeHintType.MARGIN, 1);

        /**
         * 容错率 电平
         * L（低） 7%的码字可以被恢复。
         * M级（中） 15%的码字可以被恢复。
         * Q级（四分之一）25%的码字可以被恢复。
         * H级（高） 30%的码字可以被恢复。
         * <p>
         * 越高误差校正水平，越少的存储容量
         * 设置二维码的容错性
         */
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);

        //画二维码，记得调用multiFormatWriter.encode()时最后要带上hints参数，不然上面设置无效
        BitMatrix bitMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, w, h, hints);
        //开始画二维码
        MatrixToImageWriter.writeToPath(bitMatrix, "jpeg", Paths.get("qr-code.png"));

        //读取二维码图片，并构建绘图对象
        BufferedImage image = ImageIO.read(new File("qr-code.png"));
        Graphics2D g = image.createGraphics();
        //读取Logo图片
        BufferedImage logo = ImageIO.read(logoFile);
        //保持二维码是正方形的
        int widthLogo = image.getWidth() / 4;   //  logo大小默认为照片的1/6
        int heightLogo = image.getWidth() / 4;
        // 计算图片放置位置
        int x = (image.getWidth() - widthLogo) / 2;
        int y = (image.getHeight() - heightLogo) / 2;
        //开始绘制图片
        g.drawImage(logo, x, y, widthLogo, heightLogo, null);
        g.drawRoundRect(x, y, widthLogo, heightLogo, 10, 10);
        g.setStroke(new BasicStroke(2)); // logo默认边框宽度
        g.setColor(Color.WHITE);    //  边框颜色
        g.drawRect(x, y, widthLogo, heightLogo);
        g.dispose();
        ImageIO.write(image, "jpeg", new File("qr-code-vcard.png"));
    }

    @Test
    public void createCard() throws WriterException, IOException {
        BitMatrix bitMatrix = new MultiFormatWriter()
                .encode(content, BarcodeFormat.QR_CODE, w, h, hints);// 生成矩阵
        BufferedImage bufImg = toBufferedImage(bitMatrix);
        ImageIO.write(bufImg, "png", new File("qr-code.png"));
    }

    @Test
    public void create() throws WriterException, IOException {
        BitMatrix bitMatrix = new MultiFormatWriter()
                .encode("呼呼测试下吧", BarcodeFormat.QR_CODE, w, h, hints);// 生成矩阵

        BufferedImage bufImg = toBufferedImage(bitMatrix);
        ImageIO.write(bufImg, "png", new File("qr-code.png"));
    }


    // 图片宽度的一半
    private static final int IMAGE_WIDTH = 100;
    private static final int IMAGE_HEIGHT = 100;
    private static final int IMAGE_HALF_WIDTH = IMAGE_WIDTH / 2;
    private static final int FRAME_WIDTH = 2;
    // 二维码写码器
    private static MultiFormatWriter mutiWriter = new MultiFormatWriter();

    // 用于设置QR二维码参数
    private static Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>() {
        private static final long serialVersionUID = 1L;

        {
            put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// 设置QR二维码的纠错级别（H为最高级别）具体级别信息
            put(EncodeHintType.CHARACTER_SET, "utf-8");// 设置编码方式
            put(EncodeHintType.MARGIN, 0);
        }
    };


    private static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
            }
        }
        return image;
    }

}