package com.wisdom.rsabeansb;

import org.bouncycastle.util.encoders.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RsaBeanSbApplicationTests {

    @Test
    public void contextLoads() {
        byte[] en = Base64.decode("123");
        System.out.println(en);
    }

}

