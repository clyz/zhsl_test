package com.wisdom.rsabeansb.ctrl;

import com.wisdom.rsabeansb.kit.RSAKit;
import com.wisdom.rsabeansb.kit.RSAUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;

/**
 * @author: ziv
 * @date: 2018/12/26 13:21
 * @version: 0.0.1
 */
@RestController
public class RSACtrl {
    @RequestMapping(value = "/platform/rsaGet")
    @ResponseBody
    public String generate(HttpServletRequest request) throws Exception {
        KeyPair kp = RSAUtil.generateKeyPair();
        PrivateKey privateKey = kp.getPrivate();
        PublicKey publicKey = kp.getPublic();
        String privateKeyStr = RSAUtil.toBase64(privateKey);
        String publicKeyStr = RSAUtil.toBase64(publicKey);
        HttpSession session = request.getSession();
        session.setAttribute("sPub", publicKeyStr);
        session.setAttribute("sPriv", privateKeyStr);
        return publicKeyStr;
    }

    @RequestMapping(value = "/sham/jiemi", method = RequestMethod.GET)
    @ResponseBody
    public String receiveMessage(
            HttpServletRequest request,
            @RequestParam(name = "msg") String message) throws Exception {

        HttpSession session = request.getSession();
        String sPub = (String) session.getAttribute("sPub");
        System.out.println("SPUB:" + sPub);
        PublicKey clientPublicKey = RSAUtil.getPublicRSAKey(sPub);

        String sPriv = (String) session.getAttribute("sPriv");
        PrivateKey privateKey = RSAUtil.getPrivateRSAKey(sPriv);
        System.out.println(message);
        String decryptedText = RSAUtil.decryptToString(message, privateKey);

        System.out.println("服务端处理成功，客户端发送过来的内容为：" + decryptedText);

        return decryptedText;
    }

    @GetMapping("s/g")
    public String sg(HttpSession session) throws Exception {
        Map<String, Object> map = RSAKit.generateKeyPair();
        String publicKey = RSAKit.getPublicKey(map);
        String privateKey = RSAKit.getPrivateKey(map);
        session.setAttribute("pub", publicKey);
        session.setAttribute("pri", privateKey);
        return publicKey;
    }

    @GetMapping("s/send")
    public String ssend(String msg, HttpSession session) throws Exception {
        String sPriv = (String) session.getAttribute("pri");
        System.out.println("MSG::" + msg);
        String segmentdecrypt = RSAKit.segmentdecrypt(msg, sPriv);
        return segmentdecrypt;
    }

    @GetMapping("ts")
    public void ts(HttpSession session){
        session.setAttribute("t","xxxxxxx");
    }

    @GetMapping("tg")
    public String tg(HttpSession session){
        return (String) session.getAttribute("t");
    }
}
