package com.wisdom.rsabeansb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsaBeanSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(RsaBeanSbApplication.class, args);
    }

}

