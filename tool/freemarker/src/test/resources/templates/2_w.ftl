<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                    xmlns:o="urn:schemas-microsoft-com:office:office"
                    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                    xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                    xmlns:v="urn:schemas-microsoft-com:vml"
                    xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                    xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                    xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                    xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                    mc:Ignorable="w14 w15 w16se w16cid w16 w16cex wp14">
                <w:body>
                    <w:p w14:paraId="6BCA09F0" w14:textId="421D1AE8" w:rsidR="00B029D4" w:rsidRDefault="00B029D4">
                        <w:pPr>
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00BE4572">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>村社：</w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>三凤镇</w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">   </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00BE4572">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>姓名：</w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>吴秀珍</w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r w:rsidRPr="004C38A0">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>身份证号：</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>51092********</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">9116    </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00BE4572">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>联系电话：</w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>13980211245</w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="0021605C">
                            <w:rPr>
                                <w:noProof/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="719E0135" w14:textId="77777777" w:rsidR="00B029D4" w:rsidRDefault="00B029D4"/>
                    <w:p w14:paraId="364F7CCE" w14:textId="36581B91" w:rsidR="00BC2329" w:rsidRDefault="00B029D4">
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                            </w:rPr>
                            <w:drawing>
                                <wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="27A25078" wp14:editId="18F5D5A0">
                                    <wp:extent cx="749300" cy="749300"/>
                                    <wp:effectExtent l="0" t="0" r="0" b="0"/>
                                    <wp:docPr id="12" name="图片 12" descr="C:\Users\Administrator\AppData\Local\Microsoft\Windows\INetCache\Content.Word\2.jpg"/>
                                    <wp:cNvGraphicFramePr>
                                        <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/>
                                    </wp:cNvGraphicFramePr>
                                    <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                        <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                            <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                <pic:nvPicPr>
                                                    <pic:cNvPr id="0" name="Picture 13" descr="C:\Users\Administrator\AppData\Local\Microsoft\Windows\INetCache\Content.Word\2.jpg"/>
                                                    <pic:cNvPicPr>
                                                        <a:picLocks noChangeAspect="1" noChangeArrowheads="1"/>
                                                    </pic:cNvPicPr>
                                                </pic:nvPicPr>
                                                <pic:blipFill>
                                                    <a:blip r:embed="rId4">
                                                        <a:extLst>
                                                            <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                                <a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/>
                                                            </a:ext>
                                                        </a:extLst>
                                                    </a:blip>
                                                    <a:srcRect/>
                                                    <a:stretch>
                                                        <a:fillRect/>
                                                    </a:stretch>
                                                </pic:blipFill>
                                                <pic:spPr bwMode="auto">
                                                    <a:xfrm>
                                                        <a:off x="0" y="0"/>
                                                        <a:ext cx="749300" cy="749300"/>
                                                    </a:xfrm>
                                                    <a:prstGeom prst="rect">
                                                        <a:avLst/>
                                                    </a:prstGeom>
                                                    <a:noFill/>
                                                    <a:ln>
                                                        <a:noFill/>
                                                    </a:ln>
                                                </pic:spPr>
                                            </pic:pic>
                                        </a:graphicData>
                                    </a:graphic>
                                </wp:inline>
                            </w:drawing>
                        </w:r>
                        <w:r>
                            <w:br/>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="6D2649D0" w14:textId="7DC8A8F9" w:rsidR="00B029D4" w:rsidRDefault="001B3264">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:noProof/>
                            </w:rPr>
                            <w:drawing>
                                <wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="160748A1" wp14:editId="524B49D5">
                                    <wp:extent cx="952500" cy="952500"/>
                                    <wp:effectExtent l="0" t="0" r="0" b="0"/>
                                    <wp:docPr id="3" name="图片 3"/>
                                    <wp:cNvGraphicFramePr>
                                        <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/>
                                    </wp:cNvGraphicFramePr>
                                    <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                        <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                            <pic:pic
                                                    xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                <pic:nvPicPr>
                                                    <pic:cNvPr id="3" name="图片 3"/>
                                                    <pic:cNvPicPr/>
                                                </pic:nvPicPr>
                                                <pic:blipFill>
                                                    <a:blip r:embed="rId5">
                                                        <a:extLst>
                                                            <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                                <a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/>
                                                            </a:ext>
                                                        </a:extLst>
                                                    </a:blip>
                                                    <a:stretch>
                                                        <a:fillRect/>
                                                    </a:stretch>
                                                </pic:blipFill>
                                                <pic:spPr>
                                                    <a:xfrm>
                                                        <a:off x="0" y="0"/>
                                                        <a:ext cx="952500" cy="952500"/>
                                                    </a:xfrm>
                                                    <a:prstGeom prst="rect">
                                                        <a:avLst/>
                                                    </a:prstGeom>
                                                </pic:spPr>
                                            </pic:pic>
                                        </a:graphicData>
                                    </a:graphic>
                                </wp:inline>
                            </w:drawing>
                        </w:r>
                    </w:p>
                    <w:sectPr w:rsidR="00B029D4" w:rsidSect="006E2EF7">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1440" w:right="1080" w:bottom="1440" w:left="1080" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
                <Relationship Id="rId7" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId6"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId5" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"
                              Target="media/image2.gif"/>
                <Relationship Id="rId4" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"
                              Target="media/image1.jpeg"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image1.jpeg" pkg:contentType="image/jpeg" pkg:compression="store">
        <pkg:binaryData>/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcU
            FhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgo
            KCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCABjAGMDASIA
            AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
            AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
            ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
            p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
            AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
            BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
            U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
            uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2n4uf
            Eu1+G1hptzd6bc34vZjCiQMAQQM964D/AIaOH/Qh+IP0/wAKd+1d97wD/wBhf+grnvjx8YvEngXx
            x/Y+iJYfZFtopf3sAc5I55oA3Zf2kY4I3km8D69HGoyWcgAD8q6jUPjTp1l8NNM8ZPpN01rfz+Ql
            usi71OSMk9O1ebeCPiPrPxE+F/xFbX0tAbGzTyvIh2ffEmc/98Cul+FHgfSvHv7P3h7S9c877Msj
            yjyX2HIc0Aek698QNJ0vwxLq9u6ahKkQkFnbTKZXz2HvXmmkftIWep6xHp0XhHWRcNIsbjep8vJx
            kivNfH3gd/h18XfC9p8OUkm1C4iMsUd4+8F8kd/ao1l+Inw48eDxHrljYW83iG6jt5jgOOSM4A6U
            AfU/j7xT/wAIl4Yk1j+zrrUNhUfZ7b75zXNfDv4s2Pi37Z9t0250HyNu3+0HCebn0qf43+LNQ8Hf
            Dm51nSRAbtHjA81Ny8n0rxnxn4r8EfFbw3pVncXtx/wlwg8u2hgUxxm5cdD2xmgD3HSfiBb6p46u
            vDlpp1zJFDD5o1FWBhf2B9a85b9pG0a8u7e08IazdfZpTEzwspGQcVr/AACv7fw9o8PgTVm8vxPa
            LJNNDjICk5HzV5v8KtduvDHwz+Jut6csRvLTUC8XmruGd4HT8aAOz/4aM/6kPxB+n+FbHgH45Wvi
            3xla+HG8O6lp13PG0ga5YYAAz069q8J0z9o7xpcahaQSppWySVUOLUdCcetes6lj/hrXQiABnTGP
            A/6ZmgD6CooooA+f/wBrKRIV8CSSsFjTVcsx6AYFYPxY8D+EviF4pGuH4gabp+beOLyfLEn3R1zv
            FeiftBzeCodG0k/EG1vLm189vs4t92Q23nOPavK/AmjfBHxp4gi0fR9K1T7XKpYebJIowPfNAE+h
            +H/DHw8+HPjm1tPGmn6vPqloAqqBGQUD8feOc769C/Z/1Ww0n4GaFLqd5DZxP5kayStgZLmquufB
            H4ZaNo97qV1pdw0FpGZJRHcuTge2a1vDnhvwL4w+F2l2dpbTDw7C5mghlkKSAhj179c0AeQfE74V
            6hpMrazH49uNR15VMun2YiImcE9Iz5hP5Cu4svDcXxD+H/hKy1XxALXWdCKXl7HMvmSgg8h+QR06
            15t498YN43+MHhdfhs5i1GwiNrE10uAGBPr1GK6HRINR+FGv634j+KzpLH4hjNsTZfOXbvwOnFAH
            b/tI6nYar8Eb+fS7uG7gWeKPzImyMg9K4vxx8O9JvPhz4R1XStRttG1qDT0mgihhHm3suwEAEEHO
            frWR4N1Lw74v8cw+BNAhuU8CXUZuZbSYFZDcDknceccCtD4gNJ4LDt41ImksN3/CKm1+byNn3fMx
            +HWgDB/ZkGrj4zXv/CR/av7R+wv5n2rPmdsZzWh8LP7G1TwT8Q/Dmsa5a6RJqGoMFlmIJABznGRn
            pXG/Cj4sLYfFC68VeNppJpLi1MLNBHyTxjgV0tzrHwHup57mbStZZ5GLyMPMxknPrQBHZfBTwja3
            lvcf8LO01/KcSY+zjnBz/wA9K7xNV0/V/wBqzRJ9Ku4byBdNdPMibIyI2rV8OfB/4Wa/4Zi1yw0y
            5azliMqg3DhsD2z7VjfBq7+E7fEKzi8HafqMGt7JPKafdtxsOevtQB9J0UUUAc945s7a78Kap9qg
            im2WsrJ5ig4Ow8iviH4MWni3RfEMfijw34cm1aGIyRDHCE9DzX3T4lvLGz0S8fVCn2byW3oWxvGO
            QK+WNa+Nei6N4BuNL+Gelaloknn71nZQ0YJPzcknrQB20lv4u+H8b6ho+jXXiaXxGPPvLSc/JYkc
            7B7HzD/3zWLY+BLb4iatPcaj4iuvDevyL5lxoVr0tgOOme45/Gu9+H3xIk8e+BtQt7GaXTNctrWN
            Bd3uI0klYH509RkfrXhegaJ4+vvjRrNjp3iW1/4SMW+bm/XBSRMDgcfSgDsPDXwq0nw40/i7wJrc
            3ifUtIl4swgUSSf3Ca9C8eaZYeNfCvhG78d3Y8OXguVnFqRu3Sf88+aofDr4eeJvBPgTxDpEOuWM
            PiLUZ/tFtcI3Q4wTgj+lauovdWvhyAeI7hdROn4M9/LErFpv9kY4ANTKSirm+Gw8q8uWJzev6bF4
            Y/aCsPElxbx2HhqHThFJebQkQkO/jjv0qK2Hw6WPxWt545tr3+292DON/wBlzn/V5+taPhbV28Z6
            Fc2eqRJq8S5N1aSRgBkz8u30NfPF54PsLb42waOkBl0Nr+IlcHaIWOcE9uKmFRVFdG2MwM8HNwn0
            Om8MfAqDxF4zuLTTtRu5vDAh3w6tHECsr91qrp/hXxrofhTxJ4Yg8EzXkV/LiO+kjHmRgHgr9cfr
            X0v4J8Jap4c8VTHRtRtk8DiEi306I7tknc5/+vWD4j+Ky658LfGereF/tNje6O3kCWQDO7eBkda0
            OI800CLUPgZ4W0bXb03F1PrBFrPp92xRLUZzkYr0rQLXwNrnxd07xH4f8Q2b6lHbtEun2yBQ3ynL
            V5h8GNRv/ipd6pB8TPO1rS7C1+026yrsAbuQRjPFdP8ABfXPhrqPjy1i8I+Er2w1HbII7t8si4U5
            GcntQB9J0UUUAeSftC6d4S1DR9JTxtrVzpNss7GF4c/vG28g49q+f/HofTPhhc6Z4LgTUvAvnLId
            WkXEvnZ5T6Zr6B/aIv7Gy8M2iX3hiTxA87SRwiOLzDbNs+/Xn/wx8A3/AIt/Z2n8Oyl9MuJb95B9
            oiIwA+elAHL+AEsvi54VuW8WXDaRZeFII0SWx4MiODkv648sfma9A+H+gfD34b2yeOrXxFdzafeq
            baOecEhufpntXD/s8y6f4TsfiXp+sCDUDAYohaEgG62eaCFB61z3xR+Idv4v8KQeEPD/AIPvNLNn
            MLj7Osf3B3+QcjrQB6l4u8W+FfFninTNa8G6w9/4qsY/LsdPAKxznOec1oePdR1O00m40/VbSVEv
            /KZTGuVjbq3A/L8K8F+B3j3R/BckqXnhV9X1oz77aaNQZYuMbR3r6i8QXN/4u8G6Jf2V1Y6BPdnc
            8WqRAnn+AZ71FSHOrI9DLsXHC1lKcbr/ACPOvBL33hbVrvTbSOS6vLpIyHi+4vzZ6n1BxU3j2yPh
            bxLpmnIFkfxlssb2QjmEDjMfoeaztY8U+Mvhz8QIrfU9MGu2XkCRnsLIjOcgDdjtisjxR4f8TRfE
            HwX4j1rULu7028vUvEhlD40+MkHDk8DFRRpezN81zKONleKs+r7sb4n+KGr/AAZ1ibwToUEF9YWY
            DpNd5Mh3cnNYvgW28RWuqnwJ4l09bLTvGcpuHlBBkC/fyv5d69u+K/w/8OfE6zZNCvtHi10uJHu0
            IkkKgdPl5r5m0rSfG2qePNNt9T1PU7J7SRreHVLoOI7ZBnkMeg/xrY8g+hvizrMvwX+G2j6f4dgh
            uVkLWZe4HzbcE54rj/2R77xN5UdrFpMD+G3mleW/OPMWTHT869UktvDVx8P4NM8Xazo+vXtpA5E8
            86OWkwfmGT1ryT9kvTL9rlL+PxMkenLLKh0bzeZDj7+ygD6wooooA4X4qy+NYbCxPw/gsprkyn7Q
            LroExxj8av8Aw2fxRJ4bB8bxWsWrea2VtvubO1eX/tW3mpW9l4RttJ1G5sHu9QMDyQSFDggdcVy2
            p/DeXR7kW2sfGKSxudobybm98tsHocF6AL3xM+Cep2Xi/TPEPw5gEt99okubr7VN8ofIKYH/AH1X
            R2dppvgGFvHvxI323iHUAbW6Fr88XPAwv0Arz7VfAV8nhjWNX0P4q3Orf2bCZXS1u/Mx1wDh+M4N
            df4R16+HwC8O3134fm8XXMs5V4XQzMPnPzng9KAOG0hfh02vLdfDO41Cbxk0haxjuxiIyH19qrXl
            58RfiZ41g8N6rBYfaPD15HcTrEdmzkZ5711PxY0m/wDDfxO8I+IfCXg25uIba28yaGxtDt8wnodg
            6157/ZOua14w8T+JNcvL/wACi5ja4hNzvt/tDD/lmCcZPFAH1s3jLRYPGUHhOWVv7ZkgEwj8v5dv
            Pf8AA1wvx08R3lnrXhjwzD5X9neIZTaXmV+byycHaex5r5j0TRdT13w1L4m0/wATXV54rimNvFp8
            MpkuniGPnAB345NdD4x+H+v2Oq+A49W8R6jNe6xIgXzy/mWTHHTJzkUAadl4c1zwT8dNT0f4XpHL
            cQ2gIF62RsOM81vfFO4+MLeAdZHiez0iPR/KH2hoSN4G8dPxxVf4bW0Hw+/aC1O08T+Io5fLscG/
            v5gm4nGBljW54xutF0f4R+N7L/hPLHX7rUD5sEX2xZGQbx8ijJoA8m/Zw8A6T8QPEGqWWvtP5Vvb
            iRPKk2HOcV754O8D/D3wL8V7HTtLm1EeJDBJJFHI2YyhQ5yfpmvNNfA0D4OfDm/0T/QL27uhHcT2
            37uSUZ6OR1r3G58W2Mfxr0vw2+jW8l/NZGYaiQPMUbSdtAHqNFFFAHgX7Vv3/AH/AGF/6CvFv2to
            3f4rsUViPsMHQe1e7ftO6Br+uWXhebwzpU+pz2F81w8cQ6DAxmsGXxp8Trlg118KYZpAAN8kQc/q
            aAPOP2fkZPhZ8VN6kf6HD1HtLXq3wr8c6X4A/Z98PaprSzPbu7RAQrk5LmsDXvEPxP1Xw9qWkxfD
            EWUd9CYpHt0CHn8ah1zwH4nn/Z18NaFFol02q2175k1qAN6pvY5NAHXf8NOeDMgfZtVyeg8kf40v
            xi8N3fxl8A+Hbzws0UMckhuALs+WdpGPzq748+HWP7J8WaRpoudY0e0VI9KWFNlw/fdXnfiq6+KP
            jC40C1m8E3ekWdneRys1sdg25GQRnpQBwPhK0l+B/wAXbRvFiGZY7YyH7IPM4fIH8qz/ABD478T/
            ABA+I+mfZLr5odQ/4lZliCeVl/kz+lfXd7c6tP8AEa202fwtb3OhNah5NUljDFZOfk5/zzR4sa/0
            jxDoEPh7wpaXtrcz7by6SFQbVc/eoA+UNX8jTfi5qCfGSN9Tn8gBvsX9/HydMVyHxM8C3/gzUbVb
            vyjDqEZubZUJJEZPAPvX1j8RvBl/oXiy58deG9KbxBq1yFtm06RAVVMff/SofCvhjWPH/ifTvFHj
            rRm0O60ZvLgsSoaOZSDyc59aAPL9A+Jfw9m+HvhzQvF+nancTaT86+WmAJM/Xmtrwl4y0/xz+01o
            uq6PDcx2i2MkX75MHIjNP+NmieMPE/iS20vTPAwj0mwvRJHe28KjzV46+1eypPqlj8RrDTLLwrbp
            oT2xeXVI4wpSTB+TIoA9AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/9k=
        </pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image2.gif" pkg:contentType="image/gif" pkg:compression="store">
        <pkg:binaryData>R0lGODlhZABkAOYAANywk////5p2YMyOg5t2Y9CWh9Sfi7uTecuhhqqEbP3Prtinj9esj9ioj555
            Y9OojLKLc8+licOaf8y6r7OYh6J9ZqaAafj29eXc16B+adajjb+WfPLt65t2YqaHc7aPdtmpkKyP
            fb+pm+zl4cedg66Ib9OcirmgkdeljtnLw5p2Ytuukt/UzdqrkcaxpdmqkM+UhrSYitutksuih9qs
            kc26seS4mtLDuZp2Yb+Xfc2QhNKaiaF+bObc2K2PgMaxp9PDus6ShaaBafDDpNWhjLKMc9qtkdmr
            kLqgk9GYiNemjtipj6yGbvbJqd/UztnLxNetkJt2YduvkqeHdtWijNGoi62PfrmgkqB7ZMCpnbiR
            eNGZiNCXh9Sgi9akjaeHdd6zlcWcgtOeisOagKB+a+q+n6aAa6+IcaJ9Z6uEbqaHdKB+ar+Wfa2P
            f7OYiaaAaqqEbQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/
            C05FVFNDQVBFMi4wAwEAAAAh+QQEAAAAACwAAAAAZABkAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKT
            lJWWl5iZmpucnZ6foKGio6SlpqeoqaqrrK2ur7CxsrO0tba3uLm6u7y9vr/AwcLDxMXGx8jJysvM
            zc7P0NHSkA8IB9fY2dcSCKEMCBva4hsIDJ8MbGloODgC7u/w8BUfG+aZY2dm7PH87w4QBx5kipAA
            hwoCCAmouDKhoUOHJ0LEO2BvEgMIAg4mVGHloUcRFOKVEFhpg4OMCRPGCMCypUuWGCaccFehmyQE
            FVCmJMDjwsufHCaIyCDAwQZKB9ypQdihacKfUFmmICpAQiQE7jwkbNoBYY+oP0d4cAfhprsJI7Z2
            5QkWKgeJDv8iPGJw8sQFHkzXEhjR9uUFEe6sQsp5giWSnQSA9P15gWiCRxg9+ASCGMninyEdVGSE
            VYBPlpQRTvl6+WUKdyQZuWPRsgdenopLu+QQ+FFSCi859CAt2+UFdzYXdfa7+3PvlhIPPIogIMTx
            y8AdnTT+PGpI5Y9yYqgO9reA4IswTuAONjkkrB74Rh2Bob377WBvFJ3rIEMKsBzeu6f+coQ7uZBI
            4I4ID50wVj/wUJCCfi648wEkEZxEwUMiSITgOyFMoN8NRFkwSVIXVpDAiCTmdKEAFWzWSGcIOkAi
            iRac+B8lEWhDziIMSJAAPxCo6AgD4pQjXAInwZMAgNMkqeSgkkyeQgIJTSqSE3hREoLVY0wykMBR
            hsSYmjJaclnIlYfsSCUyZBqSlIeG5ISkMsw5UKY7YgqSFJbNnFQnAMy9Y8E1EJxkgY/JgJhANicJ
            gQWPhCpTAoI2DJEDE+4ElGSN2cTIRBFFkFWlIA8USVajSlaDAAJffqrqqqy26uqrsMYq66y01mrr
            rbjmquuuvPbq66/ABivssMQWa+yxyIYSCAAh+QQEAAAAACwKACIAVQAhAAAH/4AAgoODEQcWAomK
            ixYJByQPhJKTlJUADxIJi5sCFQkfEhGWkw8fDiqoqJyrn5Gjr4MMGxU4qSqrnBYQCKMPEAI4BMLD
            PhcBx8cXGCkiIYuusJUMBw4CKsPCPCPIyBgsLhSLEpUS1dbYw0Dc6wEjNx4CDiTRlBEVidfoBEjs
            3Bwp4QQcmEQiUQgMwzooFFajH7sbGQTSI/SgWoYUMYQp7CAshsN1GOCVYEAIkYhjPvQRcPJx3YiI
            GyYK+iAghLEfKn+05HbBWYlBGwScQNaDBzp+O7mNSCSKXgQBHowFuDAF3RSpSafCiwngHgd/P2LE
            QMIyK7cJAiBM/JWCJxCxMf+eYDWLoZMgqGbz1q0wUdPXvFkTPXggIAPgrBcSTby37fBOZ7yqtXX8
            sa6AiSUEnKT8MRGvA4X/HhuxbIIICs42haAwod8JAQkmIkjEwp+3CeACLvJAwYVoZDcUA2CAiDcF
            ChFx4cogYoLz1PPWFj5OAZ5yXCecT3gt4EOszLkOHEDQlBCCTLi8ywQAmlOFD494SYpA4tcqC5MQ
            iBdfHhZ9CAkEmMA46wnywH6PTPQAAh8I6AhJBUYooSArVLiCFBO+IkWFRtBAwxEvvLBEAyS+kOEr
            K5CoIolLhHiEhxbCsoKHIIq44o04NnDiKEbk6OOKIb6wAiEp/mgkiUbsOAr5DSGOeGSOGArS448t
            vuAhDTEqOQgJ0VGyIYcc0tgkjktIMqOHRqxgRJodfjimikNqeY98k7yQY5Av0sBmmnFOMuWTKtKg
            JQCzxUaJk4CS2OckiAK6RJQyMZAAV5IgAg0hUgTZ6I9HWPJnA0FeySeklkhKqXmwUaIJnRqG6aaL
            pA4qSaGUgIbfJPf0J+uuADzF1ySaSEQIaIbyaiwA1bBK6CIJkIAACfdYAOGxvILmCXyHJJIDFpxA
            MC21vNq3iRZNNJHDIqeCaywJJQT4QWY5KCCvAkMk8q266hImABhNKGADE2nhK/Ag4iriwKUDg8vA
            B4jEA4Gu1AYCACH5BAQAAAAALAoAIQBVACUAAAf/gACCg4SCEQgHiYqJJBGFj5CRkoIPCBuLihII
            k5wPGxUCoaKjoQ4lGwycqpAMEgmksKEJBw+rghIlAiq7u2QUv8CkprW2nAgQDry7OMDAGaQJm5MI
            rwIE19gETgHc3RgpFKPExY8PEKEq2dc/3d0jLCfPoRKSH6EeU+rXMe39HCkeBDiQRm6QBAcCMrTR
            d61fuwsswgnYAOlchhsBkFzrwPEaEocOb4QiUfBWKBcXgGzsSIAHyH4Ynn0odEBAiAvcnDDc9rJd
            CoGOyCFIOIIbBx762PV094yeIAYOMuDslkVdlqX9JgiwUPAVhnZP1PmYijUABoHENgiY4DBljRoc
            /8r2C+h0VQQBFEA+edtDbjsRAiAIelXUr1wXgYvVxGi4LAsBFQYLwIqhsuXCPbUmUCzga88Rli1T
            DiUoFwuQGCjIg0VhQlyHiDfbUusCJIcTAWN5mOC532MBgoZ68HwBg4jVsURlcN0vhIADxaAKSOFt
            Qu7ko1z05hZ70LnkGwgOYkDiAKhQIVxMmOC8wrhVErBDQJCqEKJqAjyIWH8ilPiao8y3ygbICPOe
            LSQgJEoCdU1CwgfnjUJSIZUgIJ4t5CmCSklPWUhfSYgociCHJBZEQwsorlAiJyugCMICC6CggQZE
            GGCjBityQoONPNpIxYxKwIhiC6rQAKMSM1LR4/+STBqQ4yQvNClljzNqQAMhO06ppY0gPDlJA0lu
            2WQXKgoS5ZQ/agDjAkN6WUyLKL4QJ4wy0sgkFYW0sKacIPB5ZJg8EummBRbU94gXTVYZ5AJ8goBi
            JC90IWaPC7gJgD3QQaLkpDZ2cWUkNXJqABVlegmVA5HI4MWMoW6phCQgSHrjjHumuAoCIwLwQGKP
            vHKhJDLMuQCSM6JQqqWP3FUCJCQIIFshviIrrX3OQlITV4UwIJCh00qrrQDcCnJegwcJ0GC30r7y
            wTgMABgKBIlUcy66yN6VXBVFkGJBrvRa+oAFsMyggAJQzDCDEOb2q3AhuUAx8MNMCPDrwujGl8Mt
            wwo0QRrFFO+KBcZQCLAsxxS/ogUUNoThH8kUNzuKA5myvLAEBySQAAT8dhsIACH5BAQAAAAALAoA
            IQBUACcAAAf/gACCg4SCCCQHHwmLixAbEYWRkpOUghEIBweMiyUHCJWVEhACpKWmpRUfkKCskggf
            DqeyAg4ln60AEhWkKgS+BDwsGMMpLiGlJRK4rQgJvL8EKkDDGCwTFKUWGwyhsQJrvdAEMQHl5hcp
            HqQJ3MuuFqQZ4dA8F+bmLNgCFauRowIhUvTw1aHgr3sIMWCr8MBdIQnxblz4VbCDrx4IzXEQQYuE
            P1I3yo2gaBFYxnsjjllo5xCiABH2AvAgWJLAiJPlLpyg1Q8ACVIp7iERRwAIznPqEjgEwICUiHtA
            iCI5am6nA5ajXGSM6osHRqoiMwg40BJgxh4zfRkFG+DCMaUA/x4I8ICTQ4+vbMtNoMWy1S4OJy/c
            jZkXA6lPELXmzXuBlDJcckMsXnwMAoADAibgTCFCnakMJyTixEYWFwIBFHBiMCaLwgTAJ/cqxaz4
            XAp9s0o9zaiuNDOzCFlwzF2KAux7HJVCzAB7tdh1EhBIl74BVikXN8sZFnCrlVwBLETe8CzAgqfp
            CCQc2EXqRPhyHHoLgifLwYfuklySCkFBn29cH+QGwWOTIMDeXP2JVUI7D2zgjQOO9BXKgaR8sBQD
            EtAnQCcNMeOMKewsVckD03Uo4omDMDBdTyi2iMsRIMS4gouTrBCjEhpoYIAJJmxRwI8m0EjJET8W
            +eMOPBKRY/+MIIByRI5E8LiDkVRWWYCQkyxg5ZZG8mjCEYMQyeWYPyqB5SReSEmmlVzMCICWXCJp
            Qo4aMHlmKzbGuICeOe7YY5U7EAICnXsqUSiUahbZJJYfWDiJGFZ6qaQGhSoRoyQLcLGmkRqc6QyB
            hUy56Y9cgCmJj6MWsIObQspVwSQ0iMEjqmQSMYkSmgLJI6EytiiXZZE4wyIlNPCpQZQ8GsDqnYT8
            KslPcBXiDH7M3nlatIRgVoGEAMRiYrV3yuWAJOwtOMgDzjgKbrW7gMoAZqXYlwgpwK5b7U+kWLBI
            KVUUcYoDoNrLrH6mzKCAAlAUUYQQYwksMAMbZEKCMzYcbPFGDA07rDEAJQhQhcUHY/zfxutuIEAO
            ICughQABk1ztaUyA3AQWAgzrMrOxNHHxPjc7PIoWZSgARkc9P0xhzUVr7CAt1NobCAAh+QQEAAAA
            ACwKACAAVAAsAAAH/4AAgoOEhAwIiIkMhYyNjo+NiYkPkJWDDBIOApucmxYfEZailQgVnZ0VECSj
            hQglmioEBCo4FLYhnBYSrLwPEKY4srRWthScDhuLohIWm8Gy0DEXAQEXKSeeCLykJZyx0ARTI9QB
            LCIZAhUblREJmxku3+AENeTUI8YCH9uNDBCcIr7MkxXDXgAOLjYlUMYok4AME6hBixKlg6wfBu/h
            gsCPUARTAkRMiyGrQ0VZPjJWw2aB4SAJmyhwIOdjIIEeKstt4tgxgiYPGMj9sFlP5QgPAloSQrBJ
            hMEePOZlyUktxaZd2xhoCjGN3IUp83x0NYqOp6B/IVRy+BEjBpJxVP+p3RDgwOWyhzMNXgDSNoaT
            uPc2aQPwYFMKwIjJ4TqwzZTTxIgTJhC04SFVDBNOFKMg4gZVFul4RdgENyOHCSI2n5gwVu8mSv8o
            qMSA9BQneK3JbeIFU4DafLablrZnTNsHAbLtYcAmQJXLBxt+snAtgBeJTQY5nBOQQAKlS8w2eTaI
            SxvTDCkwYLi2c7CjA5tOBD2IzaworQImqDeHrjskpgKEcFg1E4R2lm0luAdJBM2cohRvtulyXzen
            OBDKIBskoCEovGDSYHN2jYKAhgmosg0JFHJ3YUcstujiizCK0sICNMboCI0oGGCACQUUAMMAQBZg
            YyMtAGlkkD3qSAT/jSBU0oKOBvRYwJFUVjlAk0MSsoCVXBqpQ49JLDBIkV2WOYAOBmTJiAZSmsll
            C4Js2SWYSjKppiU0LgBCnjry6GOVOsgwiAZQ4ogjlG0CqQMKdzqyA6BSFroACijQKGghCwTh5pFJ
            DPnAd41MuamijDby46gDFIBljBU4AGohLezQ46lmdtoIEV7SqWOejQJQmX2VyMCnjlImsWqvlRyg
            4CCmhIjsi8pGwl0j7iz7rItMTcZIZdoW0oy117LIlAWN/CPAskxVEO6QrxXCQCcHLOIPXSuu++I/
            CRxAAiIfaMJEcBXUa6+LDIB0ihBD2CBEJyUMrGa/nphigwIU22BDXg4CMOZwr//MQPHHM2S8ca+V
            FfExxSFrPLKahQnQxMkpr9yoKVDALLLMDwuQw8lFnIuzmqMJMMTHCwv8M4zu7KzAELsdnaVPAlRx
            sQANO51lb5yAa7WLDxygIVbXBgIAIfkEBAAAAAAsCgAfAFQALQAAB/+AAIKDhIMRJAeJiiQMhY6P
            kJGEDwiKihIPkpqCCBACn6ChFhuZm6aQDwehqwIVHxGngwwHDgI4KgS5Ki4TEy4hnw4QjbGnEhW2
            uLpXvRMUoCWwmwyenzi52AQ8IwHdHDcZnwfFmrSg2dhP3QEXKcACJcSQErUCJyzYHR3YP+vrEx4E
            WJBHzhAyASFS+Milbx8BH/66sQDmQJqjaiEwdMunL1eNiN0uPBtYcJCETxlSdIvBsCOBGCC7uRBQ
            8eKnCf6QoMvFIWYADuEslARw0t6FdUB2ElAXUyTNUoKKqvTHYSE2Hkx9jgj3oeCDTyJA6szWzyc7
            YEIHJbDn00mNGkD/zPrD8AnqKVUejoIc8baGXrkXwkkQFOGTRrmI/Z0QAIFcLZyJI99oJQivTw7O
            woHycIJbzBE0iyH41BNkOwoBQWWgwMJsOFglBFCIOQGUhQS4ExzMGPNTsQ2fYrLQXCF3AgufPNyI
            +QwBAFWz/U0IV0ECQcKxM7j4+zN0rNECIrII6OCAXUEPPtQ6UXpdQFhfBYjoJSKcA0ySECDzMAED
            hhQBjVMMMhT04kJqB1j0CAPIZSCCfyw8k4BJqziwwXWQUMNKY+REUA8oH5wXCXCrkGTIIl59kNtg
            Jc2SiHXkMLBBbgdgONSNOOao44489uijIyAsIKQMPwoig5AaGGDA/w4FFBDEAFAWUOQgIEBpJZQw
            NGmCkkIuoAkISprQJAxXlmnmAFMKgsKZbF7ZZAEgUNnmnFZqkKYgRIxJ55k6EAnAmm1mWYCSBnT5
            4wMiDnKkkCgwqiSTTpoJAyELENqoBpeGqaeVXvJYgQOJApDEmW9uacClGgj5CAo67HmlAT0Cx6Ej
            ZLoKpQ5xPvKkrQPA4Cc5BzjnCDI2tpBEk7vSaQIkGrQaZZOWDmlKsI+MNqEjawm7SQuOGiBmkzv8
            mqa1jwB3bSHIaXvnjqOlVUg16gIwWgXr/lhXIQyEUiMAGtZUb4+eJHAACQggoJ4ATLDSioL/6sjA
            QasIMYQNQoRSQm3DUx4sEDI2KOCxDTbkIICAGN/pyQwepzzDyCWvC1wRKXu8MsktFxlfEzHPXPOd
            yECRM8s7ZyxADjEXIUC8QfNYmABDpFwxw0nzuBbRCgzhW9RFeihAFSHDg/WURYGC9Nc7poIbi2Sn
            rfbahQQCACH5BAQAAAAALAsAHwBTAC0AAAf/gACCg4SFhoeIiYqLjI2FDAiRkgyOlY2Skg+WhwwS
            DgKgoaAWHxGbpwAIFaKiFRAkpwglnyoEBCo4FLohoRYSqIwPEKs4trhWuhShDhuUjBIWoMW21DEX
            AQEXKSejCMCHsqG11ARTI9gBLCIZAhUbihEJoBku4+QENejYI8oCH9+DGEAIJeLLPVsx9AXg4AJU
            AmeFPAnIMAEbtShROtj6oXAfLwgAI6wSIOJaDFsdMtry0TEbNwsQBUkARYEDOh8HCfRomQ4USFQR
            PnnAgO5HznwtR3gQAHMQAlAiFPbgcS8LT2wpQP3axOBTiGvoLky55wNsUnY/AQwM0ZLDjxgx/5Cc
            u4rthgAHMRvNzGBT4QUgcGM4obsPlLcHoFIQXoyO14FNq6IyXtwwAYANE69imHAiGQURN66yaGcp
            Aqi5HTlMEOH5xASzfkEJE0ChJYalrELRg40OlKWZAtr2yw0VtT5lCD7QVoiBm4BXMR9sEMoitgBL
            JEAp5LBOQAIJmgJCAxVaIa9IE1NgwLDNp7dEB0CdILqQW9pGXQVMWK+O3XdFTwkQgmLZTECaWrmV
            8N4iEUTDSlO/5eYLfiXk5oApgmyQwIalWNKJg8/l5QgCGybwyiYkVOgQhgC16OKLMAIDwgI0yhDj
            IDLQqIEBBuxQQAFBDCBkATcWAoKQSAoJw/+PJvBI4wKKgMCjCT/CkOSVWA5QJCEoZOllkj8WAMIg
            X5aJpAZbEkJElWZmqYONAJS5ZAE8GvBkmonkSCMKe/LoI5BYwjDIAnXyqYGhU7KJJJR4FpJElmE2
            aYChGtBoCAo6tJmkATE+EN4hVmoqpA5jGhKkqAPAAOeLFTjwaSEtJPHjqWaacIgGmQ75Y6E1ponZ
            fYu00KcBVP64w6qNwrcgIauImOw3ByzrlHeHyCPts8A8ZZkhmG1bSDTXYhsLU4cMJIC0T1Ug7ouy
            PSLKAZQIdBeL634zUAIHkBDJB58wQVwF9NYLDAMjsSLEEDYIIUoJAsfI7yir2KDAxDbYkINbAI81
            vOVAM0zs8QwYa7wlZkV4PDHIGYvcKShNmIyyykWuAoXLIcPssAA5mFzEuTbHaJoAQ3iscMA9AyRP
            zgoM4VvRMAYlQBUWC8Aw0zACF0q4VAPzwAEbbvVsIAAh+QQEAAAAACwLAB8AUwAtAAAH/4AAgoOE
            ghEkB4mKJAyFjo+QkYQPCIqKEg+SmgAIEAKfoKEWG5mbppAPB6GrAhUfEaeCDAcOAjgqBLkqLhMT
            LiGfDhCNsacSFba4ule9ExSgJbCaDJ6fOLnYBDwjAd0cNxmfB8WatKDZ2E/dARcpwAIlxI8StQIn
            LNgdHdg/6+sTHgRYkEduUARkAkKk8JFL3z4CPvx1YwHMgbRC1UJg6JZPX64aErtdeDaw4CAJnzKk
            6BajoUcCMUJ2cyHAIsZPE/whQZeLg8wAHMJZMAkApb0L64DwJKBO5siapYp+WumPA0NsPJr+HBHu
            Q8EHn0SE3Jmt3092wIYKSmDvp5MaNf+AnPWH4VPUU6o8IA05Am6NvXMvhJMAIMKnjXMT+zshAAK5
            WjkVS77RCkDenxychQPl4QQ3mSNqFkPwyWfIdhQCgspAgcXZcBFKCKAgcwIoCwlyJ0CoUeanYhs+
            yWSxuYLuBBY+ebgh81ml2RInhKsggaAh2RlcAAYqOhZpARJZBHRw4K6gBx9qnTC9LmAEsAJE9BIR
            zgEmSQiQeZiAAUOKgOMUgwwFvbig2gEXPcJAchmI0B8LzyQgiFGgOLCBdZBQw4pj5ERQDygfmBdJ
            cKuUZMgiX32gG2EmzZJIdeQwsIFuB2BI1I045qjjjjz26OMgICwgpAw/AiCDkBoYYMD/DgUUEMQA
            UBZQJJBQVgklDE2aoKSQC0gCgpImNAmDlWSWOcCUgqBg5ppWNlkACIOwKWeVGqApCBFizmmmDkQC
            ICeWBShpAJc/PiDiIEcKiYKiSjLpZJkwDLKAoItqUCmYeVbZJY8VOHAoAEmY6aaWBlSqgZCOoKCD
            nlYa0GNwHDoyJqtQ6gCnI0/SOgAMfZJzAAKPIGNjC0k0meucJjyiwapRNknpkKb8+ghpEjrCFrCm
            tMCoAWE2uUOvU1L7SHDVFpIctnbuSJpaNwmAriCkVZCuj3YVwkAoNQKgoU3z8uhJAgeQgAAC6QnA
            BCutJNhvjgwgtIoQQ9ggRCglLFxkacECIWODAhzbYEMOAgRoMZqezMDxyTOEPLKdwRVxMscpi7xy
            oZ808XLMM6OJDBQ3q5zzxQLk8HIR7v5cpGECDHHyxAobvSNbQiswxG9O/+ihAFV8DE/VRVL4ybtc
            85hKbiyGbfbZaA8SCAAh+QQEAAAAACwLAB8AUwAtAAAH/4AAgoOEhYaHiImKi4yNhQwIkZIMjpWN
            kpIPlocMEg4CoKGgFh8Rm6cACBWiohUQJKcIJZ8qBAQqOBS6IaEWEqiMDxCrOLa4VroUoQ4blIwS
            FqDFttQxFwEBFyknowjAh7KhtdQEUyPYASwiGQIVG4oRCaAZLuPkBDXo2CPKAh/fgxhACCXiyz1b
            MfQF4OACVAJnhTwJyDABG7UoUTrY+qFwHy8IACOsEiDiWgxbHTLa8tExGzcLEAVJAEWBAzofBwn0
            aJkOFEhUET55wIDuR858LUd4EABzEAJQIhT24HEvC09sKUD92sTgU4hr6C5MuecDbFJ2PwEMDNGS
            w48YMf+QnLuK7YYABzEbzcxgU+EFIHBjOKG7D5S3B6BSEF6MjteBTauiMl7cMAGADROvYphwIhkF
            ETeusmhnKQKouR05TBDh+cQEs35BCRNAoSWGpaxC0YONDpSlmQLa9ssNFbU+ZQg+0FaIgZuAVzEf
            bBDKIrYASyRAKeSwTkACCZoCQgMVWiGvSBNTYMCwzae3RAdAnSC6kFvaRl0FTFivjt13RU8JEIJi
            2UxAmlq5lfDeIhFEw0pTv+XmC34l5OaAKYJskMCGpVjSiYPP5eUIAhsm8MomJFToEIYAtejiizAC
            A8ICNMoQ4yAy0KiBAQbsUEABQQwgZAE3FgKCkEgKCcP/jybwSOMCioDAowk/wpDklVgOUCQhKGTp
            ZZI/FgDCIF+WiaQGWxJCRJVmZqmDjQCUuWQBPBrwZJqJ5EgjCnvy6COQWMIwyAJ18qmBoVOyiSSU
            eBaSRJZhNmmAoRrQaAgKOrSZpAExPhDeIVZqKqQOYxoSpKgDwADnixU48GkhLSTx46lmmnCIBpkO
            +WOhNaaJ2X2LtNCnAVT+uMOqjcK3ICGriJjsNwcs65R3h8gj7bPAPGWZIZhtW0g012IbC1OHDCSA
            tE9VIO6Lsj0iygGUCHQXi+t+M1ACB5AQyQefMEFcBfTWCwwDI7EixBA2CCFKCQLHyO8oq9igwMQ2
            2JCDWwCPNbzlQDNM7PEMGGu8JWZFeDwxyBmL3CkoTZiMsspFrgKFyyHD7LAAOZhcxLk2x2iaAEN4
            rHDAPQMkT84KDOFb0TAGJUAVFgvAMNMwAhdKuFQD88ABG271bCAAIfkEBAAAAAAsCwAfAFMALQAA
            B/+AAIKDhIIRJAeJiiQMhY6PkJGEDwiKihIPkpoACBACn6ChFhuZm6aQDwehqwIVHxGnggwHDgI4
            KgS5Ki4TEy4hnw4QjbGnEhW2uLpXvRMUoCWwmgyenzi52AQ8IwHdHDcZnwfFmrSg2dhP3QEXKcAC
            JcSPErUCJyzYHR3YP+vrEx4EWJBHblAEZAJCpPCRS98+Aj78dWMBzIG0QtVCYOiWT1+uGhK7XXg2
            sOAgCZ8ypOgWo6FHAjFCdnMhwCLGTxP8IUGXi4PMABzCWTAJAKW9C+uA8CSgTubImqWKflrpjwND
            bDya/hwR7kPBB59EhNyZrd9PdsCGCkpg76eTGjX/gJz1h+FT1FOqPCANOQJujb1zL4STACDCp41z
            E/s7IQACuVo5FUu+0QpA3p8cnIUD5eEEN5kjahZD8MlnyHYUAoLKQIHF2XARSgigIHMCKAsJcidA
            qFHmp2IbPslksbmC7gQWPnm4IfNZpdkSJ4SrIIGgIdkZXAAGKjoWaQESWQR0cOCuoAcfap0wvS5g
            BLACRPQSEc4BJkkIkHmYgAFDioDjFIMMBb24oNoBFz3CQHIZiNAfC88kIIhRoDiwgXWQUMOKY+RE
            UA8oH5gXSXCrlGTIIl99oBthJs2SSHXkMLCBbgdgSNSNOOao44489ujjICAsIKQMPwIgg5AaGGDA
            /w4FFBDEAFAWUCSQUFYJJQxNmqCkkAtIAoKSJjQJg5VkljnAlIKgYOaaVjZZAAiDsClnlRqgKQgR
            Ys5ppg5EAiAnlgUoaQCXPz4g4iBHComCokoy6WSZMAyygKCLalApmHlW2SWPFThwKABJmOmmlgZU
            qoGQjqCgg55WGtBjcBw6MiarUOoApyNP0joADH2ScwACjyBjYwtJNJnrnCY8osGqUTZJ6ZCm/PoI
            aRI6whawprTAqAFhNrlDr1NS+0hw1RaSHLZ27kiaWjcJkCAnlaXbo12FMFAhixraJC+PniRwAAkI
            IJCeAEyw0sq7++LIAEKrCDGEDUKEUkLCRQ4sEGgyNiigsQ025CBAgBSj6ckMGpc8w8ch2xlcESVr
            fDLIKRf6SRMtvxwzmshAUTPKN1csQA4tFyEAuj33aJgAQ5QcMcJF68gW0AoM8VvTP3ooQBUdw0N1
            kRR+QvTWPKaSG4tgl2322YMEAgAh+QQEAAAAACwLAB8AUwAtAAAH/4AAgoOEhYaHiImKi4yNhQwI
            kZIMjpWNkpIPlocMEg4CoKGgFh8Rm6cACBWiohUQJKcIJZ8qBAQqOBS6IaEWEqiMDxCrOLa4VroU
            oQ4blIwSFqDFttQxFwEBFyknowjAh7KhtdQEUyPYASwiGQIVG4oRCaAZLuPkBDXo2CPKAh/fgxhA
            CCXiyz1bMfQF4OACVAJnhTwJyDABG7UoUTrY+qFwHy8IACOsEiDiWgxbHTLa8tExGzcLEAVJAEWB
            AzofBwn0aJkOFEhUET55wIDuR858LUd4EABzEAJQIhT24HEvC09sKUD92sTgU4hr6C5MuecDbFJ2
            PwEMDNGSw48YMf+QnLuK7YYABzEbzcxgU+EFIHBjOKG7D5S3B6BSEF6MjteBTauiMl7cMAGADROv
            YphwIhkFETeusmhnKQKouR05TBDh+cQEs35BCRNAoSWGpaxC0YONDpSlmQLa9ssNFbU+ZQg+0FaI
            gZuAVzEfbBDKIrYASyRAKeSwTkACCZoCQgMVWiGvSBNTYMCwzae3RAdAnSC6kFvaRl0FTFivjt13
            RU8JEIJi2UxAmlq5lfDeIhFEw0pTv+XmC34l5OaAKYJskMCGpVjSiYPP5eUIAhsm8MomJFToEIYA
            tejiizACA8ICNMoQ4yAy0KiBAQbsUEABQQwgZAE3FgKCkEgKCcP/jybwSOMCioDAowk/wpDklVgO
            UCQhKGTpZZI/FgDCIF+WiaQGWxJCRJVmZqmDjQCUuWQBPBrwZJqJ5EgjCnvy6COQWMIwyAJ18qmB
            oVOyiSSUeBaSRJZhNmmAoRrQaAgKOrSZpAExPhDeIVZqKqQOYxoSpKgDwADnixU48GkhLSTx46lm
            mnCIBpkO+WOhNaaJ2X2LtNCnAVT+uMOqjcK3ICGriJjsNwcs65R3h8gj7bPAPGWZIZhtW0g012Ib
            C1OHDCSAtE9VIO6Lsj0iygGUCHQXi+t+M1ACB5AQyQefMEFcBfTWCwwDI7EixBA2CCFKCQLHyO8o
            q9igwMQ22JCDWwCPNbzlQDNM7PEMGGu8JWZFeDwxyBmL3CkoTZiMsspFrgKFyyHD7LAAOZhcxLk2
            x2iaAEN4rHDAPQMkT84KDOFb0TAGJUAVFgvAMNMwAhdKuFQD88ABG271bCAAIfkEBAAAAAAsCwAf
            AFMALQAAB/+AAIKDhIIRJAeJiiQMhY6PkJGEDwiKihIPkpoACBACn6ChFhuZm6aQDwehqwIVHxGn
            ggwHDgI4KgS5Ki4TEy4hnw4QjbGnEhW2uLpXvRMUoCWwmgyenzi52AQ8IwHdHDcZnwfFmrSg2dhP
            3QEXKcACJcSPErUCJyzYHR3YP+vrEx4EWJBHblAEZAJCpPCRS98+Aj78dWMBzIG0QtVCYOiWT1+u
            GhK7XXg2sOAgCZ8ypOgWo6FHAjFCdnMhwCLGTxP8IUGXi4PMABzCWTAJAKW9C+uA8CSgTubImqWK
            flrpjwNDbDya/hwR7kPBB59EhNyZrd9PdsCGCkpg76eTGjX/gJz1h+FT1FOqPCANOQJujb1zL4ST
            ACDCp41zE/s7IQACuVo5FUu+0QpA3p8cnIUD5eEEN5kjahZD8MlnyHYUAoLKQIHF2XARSgigIHMC
            KAsJcidAqFHmp2IbPslksbmC7gQWPnm4IfNZpdkSJ4SrIIGgIdkZXAAGKjoWaQESWQR0cOCuoAcf
            ap0wvS5gBLACRPQSEc4BJkkIkHmYgAFDioDjFIMMBb24oNoBFz3CQHIZiNAfC88kIIhRoDiwgXWQ
            UMOKY+REUA8oH5gXSXCrlGTIIl99oBthJs2SSHXkMLCBbgdgSNSNOOao44489ujjICAsIKQMPwIg
            g5AaGGDA/w4FFBDEAFAWUCSQUFYJJQxNmqCkkAtIAoKSJjQJg5VkljnAlIKgYOaaVjZZAAiDsCln
            lRqgKQgRYs5ppg5EAiAnlgUoaQCXPz4g4iBHComCokoy6WSZMAyygKCLalApmHlW2SWPFThwKABJ
            mOmmlgZUqoGQjqCgg55WGtBjcBw6MiarUOoApyNP0joADH2ScwACjyBjYwtJNJnrnCY8osGqUTZJ
            6ZCm/PoIaRI6whawprTAqAFhNrlDr1NS+0hw1RaSHLZ27kiaWjcJkCAnlaXbo12FMFAhixraJC+P
            niRwAAkIIJCeAEyw0sq7++LIAEKrCDGEDUKEUkLCRQ4sEGgyNiigsQ025CBAgBSj6ckMGpc8w8ch
            2xlcESVrfDLIKRf6SRMtvxwzmshAUTPKN1csQA4tFyEAuj33aJgAQ5QcMcJF68gW0AoM8VvTP3oo
            QBUdw0N1kRR+QvTWPKaSG4tgl2322YMEAgAh+QQEAAAAACwLAB8AUwAtAAAH/4AAgoOEhYaHiImK
            i4yNhQwIkZIMjpWNkpIPlocMEg4CoKGgFh8Rm6cACBWiohUQJKcIJZ8qBAQqOBS6IaEWEqiMDxCr
            OLa4VroUoQ4blIwSFqDFttQxFwEBFyknowjAh7KhtdQEUyPYASwiGQIVG4oRCaAZLuPkBDXo2CPK
            Ah/fgxhACCXiyz1bMfQF4OACVAJnhTwJyDABG7UoUTrY+qFwHy8IACOsEiDiWgxbHTLa8tExGzcL
            EAVJAEWBAzofBwn0aJkOFEhUET55wIDuR858LUd4EABzEAJQIhT24HEvC09sKUD92sTgU4hr6C5M
            uecDbFJ2PwEMDNGSw48YMf+QnLuK7YYABzEbzcxgU+EFIHBjOKG7D5S3B6BSEF6MjteBTauiMl7c
            MAGADROvYphwIhkFETeusmhnKQKouR05TBDh+cQEs35BCRNAoSWGpaxC0YONDpSlmQLa9ssNFbU+
            ZQg+0FaIgZuAVzEfbBDKIrYASyRAKeSwTkACCZoCQgMVWiGvSBNTYMCwzae3RAdAnSC6kFvaRl0F
            TFivjt13RU8JEIJi2UxAmlq5lfDeIhFEw0pTv+XmC34l5OaAKYJskMCGpVjSiYPP5eUIAhsm8Mom
            JFToEIYAtejiizACA8ICNMoQ4yAy0KiBAQbsUEABQQwgZAE3FgKCkEgKCcP/jybwSOMCioDAowk/
            wpDklVgOUCQhKGTpZZI/FgDCIF+WiaQGWxJCRJVmZqmDjQCUuWQBPBrwZJqJ5EgjCnvy6COQWMIw
            yAJ18qmBoVOyiSSUeBaSRJZhNmmAoRrQaAgKOrSZpAExPhDeIVZqKqQOYxoSpKgDwADnixU48Gkh
            LSTx46lmmnCIBpkO+WOhNaaJ2X2LtNCnAVT+uMOqjcK3ICGriJjsNwcs65R3h8gj7bPAPGWZIZht
            W0g012IbC1OHDCSAtE9VIO6Lsj0iygGUCHQXi+t+M1ACB5AQyQefMEFcBfTWCwwDI7EixBA2CCFK
            CQLHyO8oq9igwMQ22JCDWwCPNbzlQDNM7PEMGGu8JWZFeDwxyBmL3CkoTZiMsspFrgKFyyHD7LAA
            OZhcxLk2x2iaAEN4rHDAPQMkT84KDOFb0TAGJUAVFgvAMNMwAhdKuFQD88ABG271bCAAIfkEBAAA
            AAAsCwAfAFMALQAAB/+AAIKDhIIRJAeJiiQMhY6PkJGEDwiKihIPkpoACBACn6ChFhuZm6aQDweh
            qwIVHxGnggwHDgI4KgS5Ki4TEy4hnw4QjbGnEhW2uLpXvRMUoCWwmgyenzi52AQ8IwHdHDcZnwfF
            mrSg2dhP3QEXKcACJcSPErUCJyzYHR3YP+vrEx4EWJBHblAEZAJCpPCRS98+Aj78dWMBzIG0QtVC
            YOiWT1+uGhK7XXg2sOAgCZ8ypOgWo6FHAjFCdnMhwCLGTxP8IUGXi4PMABzCWTAJAKW9C+uA8CSg
            TubImqWKflrpjwNDbDya/hwR7kPBB59EhNyZrd9PdsCGCkpg76eTGjX/gJz1h+FT1FOqPCANOQJu
            jb1zL4STACDCp41zE/s7IQACuVo5FUu+0QpA3p8cnIUD5eEEN5kjahZD8MlnyHYUAoLKQIHF2XAR
            SgigIHMCKAsJcidAqFHmp2IbPslksbmC7gQWPnm4IfNZpdkSJ4SrIIGgIdkZXAAGKjoWaQESWQR0
            cOCuoAcfap0wvS5gBLACRPQSEc4BJkkIkHmYgAFDioDjFIMMBb24oNoBFz3CQHIZiNAfC88kIIhR
            oDiwgXWQUMOKY+REUA8oH5gXSXCrlGTIIl99oBthJs2SSHXkMLCBbgdgSNSNOOao44489ujjICAs
            IKQMPwIgg5AaGGDA/w4FFBDEAFAWUCSQUFYJJQxNmqCkkAtIAoKSJjQJg5VkljnAlIKgYOaaVjZZ
            AAiDsClnlRqgKQgRYs5ppg5EAiAnlgUoaQCXPz4g4iBHComCokoy6WSZMAyygKCLalApmHlW2SWP
            FThwKABJmOmmlgZUqoGQjqCgg55WGtBjcBw6MiarUOoApyNP0joADH2ScwACjyBjYwtJNJnrnCY8
            osGqUTZJ6ZCm/PoIaRI6whawprTAqAFhNrlDr1NS+0hw1RaSHLZ27kiaWjcJkCAnlaXbo12FMFAh
            ixraJC+PniRwAAkIIJCeAEyw0sq7++LIAEKrCDGEDUKEUkLCRQ4sEGgyNiigsQ025CBAgBSj6ckM
            Gpc8w8ch2xlcESVrfDLIKRf6SRMtvxwzmshAUTPKN1csQA4tFyEAuj33aJgAQ5QcMcJF68gW0AoM
            8VvTP3ooQBUdw0N1kRR+QvTWPKaSG4tgl2322YMEAgAh+QQEAAAAACwKAB8AVAAtAAAH/4AAgoOE
            hYaHiImKi4yNhwwIkZIMjpWNkpIPlogMEg4CoKGgFh8Rm6cACBWiohUQJKgIJZ8qBAQqOBS6IaEW
            EqiMDxCrOLa4VroUoQ4blI0SFqDFttQxFwEBFyknowjAh7KhtdQEUyPYASwiGQIVG4sRCaAZLuPk
            BDXo2CPKAh/fgxhACCXiyz1bMfQF4OACVAJnhjwJyDABG7UoUTrY+qFwHy8IACOsEiDiWgxbHTLa
            8tExGzcLEAdJAEWBAzofBwn0aJkOFEhUET55wIDuR858LUd4EACTEAJQIhT24HEvC09sKUD92sTg
            U4hr6C5MuecDbFJ2PwUNDNGSw48YMf+QnLuK7YYABzGfTbSp8AIQuDGc0N0HyhuAB6BSDF6MjteB
            TauiMl7cMIGgDROvYphwIhkFETeusmhnKQKouR05TBDh+cQEs31BaRpIoSWGpaxC0YONDpSlmQLa
            9ssNFbU+Zd4+CKitDwM3Aa9iPtgglEVsAZZIgFLIYZ2ABBI0BYQGKrRCXt6eZkiBAcM2n4YRHQB1
            guhCbmkbdRUwob06duAp8pQAISiWzQSkqZVbCfEpEkE0rDT1W26+6FdCbg6YMsgGCXRYiiWdQAhd
            Xo4g0GECr2xCwoUOaQjQizDGKCNALSxg44yG2IiCAQaYUEABMAwgZAE4FtKCkEgO+SP/j0TYCMIi
            LfBowI8FJGnllQM8WaQgC2DpJZI6/JjEAoMc+eWZA+hgwJaEaEAlml62wOWZYjLpJJuK2LgACHry
            6COQV+ogwyAaSKmjjlK+KaQOKOBpyA6BUmnoAiigYOOghSwQBJxJJjFjBC4aUiWnizZ6SJCkDlCA
            ljF+AsshLezwI6poenoIEWDWyaOeeCrnQCUy9MkjlUmw6qgiBzQoyH6vHhsjAwm8Y8hTlhkij7TO
            wkjtIZhVW0g04mX7omkVHDIQXoXM5K24L36iLAO9iBcBi6Gy+818FRxAQiQffMIEcRXUa+83A+Um
            xBA2CCFKfgPDGAEEJ34ChgIUUzyDXgCPNczmQGFUbDHGGrM5ExMeK3BxxiHjiJgATXh8cspbRgOF
            x2EIgC3MMmJWhMdFCKAszi+uPETFWAgQLtAxDrSzAmXchTSO+2kxw78MPz3uSHcdbXWMCBxwgMBs
            BgIAIfkEBAAAAAAsCgAfAFQALQAAB/+AAIKDhIQPCAeJiQiFjY6PkI0MCBuKByQMkZoADAcWAqCh
            ohYbD5unkBIJoqwCFR8RqIMPEKAqBLgEKlcTEyIeoA4QmbKnnQ4COLm4Vr0uIaElsZu0oWTLuDwc
            AdwjLhmgB8WRx6Br2LhO3AEcN8ACJcSPB6EnHLkdHbk16+sT4BbkjRtEApmAEBhi4Mqnj0CMftxY
            AHMwrVEtARRGcOOxMB8ufhADXIAWcOAgCaA8sOCGpGPDhyEDiBBA0SIoEf1+oCPQIyY7gCYBoDx4
            YZ2TnSBDXqBA09RJUBMgXvCxjMcTn93AfRj4ANSJov2yYPuBVSRJQvQo+HRSowaQsuv/MIByKguB
            gAxgIY5oWyMv1gvgJAxygBeu4XUnBEAYV0IAhsOHb7gSREIAzpgcJlAAF8rDCY0xR9AsxkCAB58X
            UlB4ByoDhZU+wcWiFzXkhFAWEuhOUAEUwpigitlVG5IF5wq7E3wyfSMmU0YbMEL850qCQEERGmdw
            4ZfDaFkR7vqVSPMA3VkfkNmDCCxWVwEneokA50DC+UYIemcQgQFDCmDiFNNbCM68c0BFjjCw3Aks
            YMACUwk8JYoDG1z3CAPpsbLYOBEYFMoH90GyQW+jCBSBIiQMxIAquglmUieJWDcQCSXodoCFQeWo
            44489ujjj0A2cgQIRK4QpCArEKmE/wYaGGCCCVsUIKUJRw5yhJRYSrnDk0QwSSQImhzBJBFP7pDl
            mWgWUKUgC6TpZpZPmnCElW/WiaUSawriRZl2psmFkQC0+eaWJjCpwZd5OpIkkQswyqSTUKK5AyEg
            GNqoEpeOySeWYPr4wVaPiJFmnF1qcKkSRDqyABd9ZqnBj6u42IiZrUrJxZyORFlrATsA2mNXFTxC
            gxhP6monEY8oweqUT1papI5dbVjIKghGQoOjGpD5pAG+rhmtI5VF2MgqjCTqo13iFkJPBRYiE6K5
            QXXlgCMkxjPLKqDC22NvsnJCTygOfHDAB6BIqy+PlYGS2yqgVFEEK/UdDORQrMyggGQCUBRRhBAC
            BCjxjwxUcskqNlxs8gwdf5xnY1WYfDHKHqscZHQ5uKyAFgL0K/OPdjHhchNYCFDtzj4i08TJkxF9
            ZC1alKEAGDSlqHSQDJAYytBT+7gBMg6Um/XXYIct9thk6xgIACH5BAQAAAAALAoAHwBUACkAAAf/
            gACCg4SFhoeIiYqLjI2EDCQHkpOTCA+OmIgIlJwkEZmEEQcWAqWmp6UVECSgjg8SCaiyAg4lG5iw
            AjgqBL0EVhPBEyIhpxYHDK2aEKW8vmTCEy4Up6qXiQikur7cTgHf3xcpIqYVycqDDyWm3L4/4OAs
            Lhmmn4cfpR4pMe29MfDwOEygV8GeMgkOBGSY8KNfL4DgxHmgJcEQsww3vvEj0KGjP4gBizkwmElC
            KRcXAtTo1bGDL5DwTpSqOMhkBg7gnjisARMch2IWzmF6UIqFT4f/en6TOXJQBQE44Tnh4cuHN6Xf
            RtA7AIpZCoAjfPjiAQRruGIJBJkUAfICkBpA/1Ka/ZaCViaiHmA+qVEj6lwOpT4xMzq38EQEuAS4
            KFyYHNdYGHqOiBbsBga5IB1jOiBgQk8ONyhPwOAXYt20sb4CxOBilqkMFFQDbM3KEWe2YG/Qcy2A
            wgTM8QR8ALBBYVQOKSaaSsC8ubZSFFhgvjDxWqMIRcOxoGbKQvPmp0LcKC2zIoPnqBIgPsQAAbN8
            LoLRg9DVtYVbmg4kVCgiWLEK6cRSDgTWKeKeLBa0wgA+ptSy3iKi7NdggQBEgAACJLkiwSQ0KfPA
            hQ9iAskkGwiFzokopqjiiobQ0MKLK7BYyAovgrDAAihooAERBvSogYwt9ihkj1ToqMSNL7awCP8N
            NyqhIxVDRimlAUAW8sKUWA6powY0DEJDlmAKCUKVhTTwZJhTdhEjAFdmWaQGNy6QJJmK0PjiC3fe
            mOOOUlJBSAtx4gmCoE2eKaSSLFoQFCJeTLnlkQsICsKLh7zQBZpDLiAjPlwdAiWmPXbR5SE8gmoA
            FWuuyIADDiAigxc6lhqmEoiAcKmPOgYKYyOWHEIUfYbEEqIiMuS5gJM6opAqnRUKUMIhJAiQVrAC
            DMusighIewhnCRbCAC0mXqvitwKEC8BTAnQIAELpiltlLB9YxwBnpkAgiYDqursidq5VUQQqFlCo
            74oPoFfKDAooAMUMMwjR7sDurgNFwhQzUS0zxO6alAPFCjRRCsbuEoUFx1A4C7K7sWgBhQ1hlGLt
            yUBGe4oDncJ87YbMEWjzzjz3DGQgACH5BAQAAAAALAoAIABVACYAAAf/gACCg4SFhoeIiYqLjI2E
            DAgHkpOUJAgMjpmGCBuUnhIID5qDEQdvAqipqqoWEJejiw9jcKu1qRUlJKKLDBIJAioEwgQqIhPH
            xy4UHqoOELCHJBDAw8QUyMgUIaslu9EVqNXDNQHl5uUcLCcZqB/QpL/U4jHn5xcYIswCCZiGH6ge
            3AjrQFAYvXoIUzDjB02CAwEZrgwsKAwhQgwUBFSIUGgDKhEBgEzsIAyJRYQXMlrolykCqhAXRowU
            xuMkQhcCHHAUxABcCnQ8xBFwYrPehW3uNJUQALKcD6E/itYTIcDCoAMCXJzr8VTYFCBSjbJDkAmB
            gBDnOCAZxuPHhbDn/zIeEGRBAAeEPfLCRThBwDNH/37WG5H37V5zGDQKQnV4b2Krjn5haAwXFSZU
            dykXTSwg0y/Bmm2iIrv0hEUOGFJMOEGhdesTKTAYrndjXyaPHmabu8diggjXrm9gyDwYlSCzTDEo
            T3Fim61aE24QH4E0E4OHIZRjYPH7eS1jI3afqDpIgvcEJQ6AQsAegYQSdZ//bfnQloUEBza0Zy8t
            nn2WAJAADioWfOCNIgjAtwoEADoSgX8VuNLIAxD4h0oCO71zSATtHaghNA+0l+GHJDqywokrSFEi
            IlKcaAQNNBzxwgtLNGDjCysisoKNPNq4xIxHwIgiIyvAKCONPSapZP8DOR5ixJJQ9jjjCysQsmOU
            WNpoRJOH0DBjjVkuqaIgT0b54wsw0jAklySQkEiLLrpo5JdKLlFIkTAasYIRer4YI508VpkjOGQd
            8sKSUwZJQ596CmpImWHySEOTZiWACJiR2uioIZhGusSYmjCQwAaH1OWhIFJM2WmURyQCaQNTptko
            qImISuomthnyS6GMwOnnkS8cQSuXhFR6CFaQFQLOiMQS61IFh8QzFyFYWdrstYI8xOtxqiRgiYBV
            NYhtk1hVkMAHksSXAxa1MDjuuNPUokUTTeSgyq3vjktCCQmcu1QOCgSswBCW5WvwIA+gAkYTCtjA
            hF8HRwxAvM2cKvEStQx8EJ8zzF7s8ccghyzyuIEAACH5BAQAAAAALAoAIQBVACIAAAf/gACCg4SF
            hoeIiYqLjI2DDwgHkpOUkhIIjpmEDAgblZ8bCAyaDGxpaDg4Aqusra0VHxujmoljZ2aprrqsDhAH
            D4oRCTgqBMYEKlcTy8zMJyGuB7O0ggwQAsXHKlbN3SIUriXAhhsO2MfHMQHr7O3rGBMnqxWY1AgV
            5+gEPBfu/hwTRGQQ4GBDoQOr1BjrwPCYv4frUgwUIIEWglUejjHsYKwHRH8jPKyCMOiigAkjNHLc
            9/EhB2gOImRiYO7EBR4LVxIY0dLdBRGrKgLAd2IdEn0EgPT0d2FggkzXPPQDghTJUn/gHHBa1W8d
            VWNTPF51l2LVOEarWLDrgXOf0rHt/zgERUjBHYceYuG2u7Cq3iKTPvF21csO2oEIAkIQvtrXkbnB
            iyGCOzBUAIbIH/kK8Lvo2gTMHw0DuOiBJ8QRGFKrvvzxBsGZDjKk+MhhtWrI7kaskglAwioRzU6I
            3NWKQgrbLlZ90BTBHIVmIqARZxVigu0bAy0QQji9QoLv4PFNF1BhmiOTxB2AB29h/O5CESqFWsRA
            QgJdEMzP/CTqbwJzrSTAGzUEFmjggQgmqOCCDDbo4IMQRugICSRIaKEm+HB24YaHXPQUhxcykIBB
            hrR3FogGikhiIR4ecp+GKNojwIcHCaCdIfgMGCOBiDng4iorCoIQjTsWaE6QACDGikAFkkBgjgX6
            FUkLdwlQYo4QWOAXpZS0lECcDUPkwMQqv3CpYHyUtMdEEUWMZGaEDwA40pZvKggJAgicWOeeCwYC
            ADs=
        </pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="4472C4"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main"
                                           name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}"
                                           vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"/>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex">
                <w:zoom w:percent="100"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:proofState w:spelling="clean" w:grammar="clean"/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridHorizontalSpacing w:val="105"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="15"/>
                    <w:compatSetting w:name="overrideTableStyleFontSizeAndJustification"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="enableOpenTypeFeatures" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="1"/>
                    <w:compatSetting w:name="doNotFlipMirrorIndents" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="1"/>
                    <w:compatSetting w:name="differentiateMultirowTableHeaders"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="006E2EF7"/>
                    <w:rsid w:val="001B3264"/>
                    <w:rsid w:val="00476B6D"/>
                    <w:rsid w:val="006E2EF7"/>
                    <w:rsid w:val="009D52DF"/>
                    <w:rsid w:val="00B029D4"/>
                    <w:rsid w:val="00BC2329"/>
                    <w:rsid w:val="00D6702B"/>
                    <w:rsid w:val="00E4758C"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="1026"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="08F4CB3A"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{4906BE41-7533-4DC7-84E9-48A928693AE1}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                      xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                      mc:Ignorable="w14 w15 w16se w16cid w16 w16cex">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorEastAsia"
                                      w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
                            <w:kern w:val="2"/>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="22"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="376">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1"
                                    w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="39"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:styleId="a3">
                    <w:name w:val="Table Grid"/>
                    <w:basedOn w:val="a1"/>
                    <w:uiPriority w:val="39"/>
                    <w:rsid w:val="009D52DF"/>
                    <w:tblPr>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        </w:tblBorders>
                    </w:tblPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                           xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                           xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                           mc:Ignorable="w14 w15 w16se w16cid w16 w16cex">
                <w:optimizeForBrowser/>
                <w:relyOnVML/>
                <w:allowPNG/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                     xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                     mc:Ignorable="w14 w15 w16se w16cid w16 w16cex">
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title/>
                <dc:subject/>
                <dc:creator>Administrator</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>qing</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2020-12-24T09:42:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2020-12-24T09:42:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>1</Pages>
                <Words>11</Words>
                <Characters>66</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>1</Lines>
                <Paragraphs>1</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company/>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>76</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>