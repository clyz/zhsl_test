package ${package}.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
* @author Zemin.Yang
* @date ${date}
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ${className} {
    private Long id;

    /**
    * true 通过验证
    */
    public boolean createValidate() {
        return ture;
    }

    public boolean updateValidate() {
        return ture;
    }

    public boolean deleteValidate() {
        return ture;
    }
}