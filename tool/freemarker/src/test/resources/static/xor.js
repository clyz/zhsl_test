function xorf(val, key) {
    if (typeof key == 'number') key = [key];
    var output = '';
    for (var i = 0; i < val.length; i++) {
        var c = val.charCodeAt(i);
        var k = key[i % key.length];
        output += String.fromCharCode(c ^ k);
    }
    return output;
}

function xor(val, factor) {
    var key = factor.substring(0, 2) + factor.substring(4, 6) + factor.substring(2, 4);
    var key2 = factor.split('').reverse().join('');
    return xorf(xorf(val, key), key2);
}
