package clyz.tool.freemarker.word;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Person {
    private String community;
    private String name;
    private String card;
    private String tel;
    private String qrCode;
    private String id;
}
