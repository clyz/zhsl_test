package clyz.tool.freemarker;

import clyz.tool.freemarker.word.Person;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import freemarker.core.XMLOutputFormat;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.*;
import java.time.LocalDate;
import java.util.*;

@SpringBootTest
public class FreemarkerApplicationTests {

    @Autowired
    FreeMarkerConfigurationFactoryBean freeMarkerConfigurationFactoryBean;

    private static String xorf(String val, String key) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < val.length(); i++) {
            char c = val.charAt(i);
            char k = key.charAt(i % key.length());
            stringBuilder.append((char)(c ^ k));
        }
        return stringBuilder.toString();
    }

    private static String xor(String value, String factor) {
        String key1 = factor.substring(0, 2) + factor.substring(4, 6) + factor.substring(2, 4);
        List<String> list = Arrays.asList(factor.split(""));
        Collections.reverse(list);
        String key2 = String.join("", list);
        return xorf(xorf(value, key1), key2);
    }

    private static String xorJs(String value, String factor) {
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("javascript");
            engine.eval(new FileReader(FreemarkerApplicationTests.class.getClass().getResource("/").getPath() + "/static/xor.js"));
            if (engine instanceof Invocable) {
                Invocable in = (Invocable) engine;
                return (String) in.invokeFunction("xor", value, factor);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        throw new RuntimeException("id 加密失败");
    }

    public static void main(String[] args) {
        System.out.println(xor("elder_1183735", "510903"));
        String xorJs = xorJs("elder_1183735", "510903");
        System.out.println(xorJs);
    }

    /**
     * 测试类 Class 导出
     */
    @Test
    public void word() throws IOException, TemplateException {
        final Configuration configuration = freeMarkerConfigurationFactoryBean.createConfiguration();
        configuration.setDefaultEncoding("UTF-8");
        configuration.setOutputFormat(XMLOutputFormat.INSTANCE);
        final ObjectWrapper wrapper = configuration.getObjectWrapper();
        final Template template = configuration.getTemplate("word_word.ftl");
        File docFile = new File("word.doc");
        final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(docFile));

        Map<String, Object> p = new HashMap<>();
        p.put("list", createList());

        template.process(p, outputStreamWriter, wrapper);

    }

    private List<Person> createList() {
        //  246
        QrConfig config = new QrConfig(250, 250);
// 设置边距，既二维码和背景之间的边距51092********9116
        config.setMargin(2);


        List<Person> list = new ArrayList<>();
        for (int i = 0; i < 990; i++) {
            String addr = RandomUtil.randomString(6);
            String tel = RandomUtil.randomNumbers(11);
            String name = RandomUtil.randomString(3);
            String id = RandomUtil.randomString(20);
            String base64 = QrCodeUtil.generateAsBase64("姓名:" + name + "\n电话:" + tel + "\n内部编码:" + xor(id,addr), config, ImgUtil.IMAGE_TYPE_PNG);
            list.add(Person.builder()
                    .id(id)
                    .card(RandomUtil.randomNumbers(18).replaceAll("(\\d{5})\\d{9}(\\d{4})", "$1********$2"))
                    .community(addr)
                    .tel(tel)
                    .name(name)
                    .qrCode(base64.replaceFirst("data:image/png;base64,", ""))
                    .build());
        }
        return list;
    }

    /**
     * 测试类 Class 导出
     */
    @Test
    public void clazz() throws IOException, TemplateException {
        final Configuration configuration = freeMarkerConfigurationFactoryBean.createConfiguration();
        final ObjectWrapper wrapper = configuration.getObjectWrapper();
        final Template template = configuration.getTemplate("Class_java.ftl");
        File docFile = new File("AutoCodeDemo.java");
        final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(docFile));

        Map<String, Object> p = new HashMap<>();
        p.put("package", "com.freemark.hello");
        p.put("className", "AutoCodeDemo");
        p.put("date", LocalDate.now().toString());

        template.process(p, outputStreamWriter, wrapper);
    }

}
