package clyz.tool.freemarker;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Script;
import org.beetl.core.Template;
import org.beetl.core.resource.StringTemplateResourceLoader;
import org.beetl.sql.core.SQLReady;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class BeetlTest {

    @Test
    public void domeSql(){
        String updateSql = "update department set name=? where id =?";
        String name="lijz";
        SQLReady updateSqlReady = new SQLReady(updateSql,new Object[]{name,"1"});
    }


    @Test
    public void dome() throws IOException {
        //初始化代码
        StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
        Configuration cfg = Configuration.defaultConfiguration();
        GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);

        Script script = gt.getScript("var a = 1;\n" +
                "var b = date();\n" +
                "var c = '2';\n" +
                "return a+1;");
        script.binding("name", "beetl");
        String render = script.render();
        System.out.println(render);

        Template t = gt.getTemplate("select * from user where name like #{'%'+name+'%'}\n");
        t.binding("name", "beetl");
//渲染结果
        String str = t.render();
        System.out.println(str);
    }
}
