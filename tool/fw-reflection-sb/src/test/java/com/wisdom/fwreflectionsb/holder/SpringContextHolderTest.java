package com.wisdom.fwreflectionsb.holder;

import com.wisdom.fwreflectionsb.FwReflectionSbApplicationTests;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Iterator;
import java.util.Map;

public class SpringContextHolderTest extends FwReflectionSbApplicationTests {

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;

    @Test
    public void getBean() {
        Map<RequestMappingInfo, HandlerMethod> map = this.handlerMapping.getHandlerMethods();
        map.keySet().stream().forEach(e->{
            System.out.println(map.get(e).getBean().getClass().getName());
        });
    }
}