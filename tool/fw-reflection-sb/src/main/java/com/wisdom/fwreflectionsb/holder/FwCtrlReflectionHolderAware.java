package com.wisdom.fwreflectionsb.holder;

import com.wisdom.fwreflectionsb.ctrl.CtrlReflectionHandler;
import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @author: ziv
 * @date: 2018/12/14 11:23
 * @version: 0.0.1
 */
@Configuration
@ConditionalOnProperty(name = "fw.ctrl.scan", havingValue = "true")
public class FwCtrlReflectionHolderAware implements ApplicationContextAware {

    @Bean
    public CtrlReflectionHandler ctrlReflectionHandler() {
        return new CtrlReflectionHandler();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctrlReflectionHandler().handler(applicationContext.getBean(RequestMappingHandlerMapping.class));
    }

}
