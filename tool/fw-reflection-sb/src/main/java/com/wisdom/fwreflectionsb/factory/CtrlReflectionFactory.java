package com.wisdom.fwreflectionsb.factory;

import org.springframework.beans.factory.FactoryBean;

public class CtrlReflectionFactory implements FactoryBean<CtrlReflectionFactory> {
    @Override
    public CtrlReflectionFactory getObject() {
        return new CtrlReflectionFactory();
    }

    @Override
    public Class<?> getObjectType() {
        return CtrlReflectionFactory.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
