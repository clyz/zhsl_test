package com.wisdom.fwreflectionsb.kit;

import java.lang.reflect.Method;

/**
 * @author: ziv
 * @date: 2018/12/14 14:28
 * @version: 0.0.1
 */
public class ReflectionKit {
    public static Method[] getUniqueDeclaredMethods(Class clazz) {
        return clazz.getDeclaredMethods();
    }
}
