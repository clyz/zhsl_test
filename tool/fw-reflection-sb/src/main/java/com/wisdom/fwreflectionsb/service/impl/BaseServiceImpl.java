package com.wisdom.fwreflectionsb.service.impl;

import com.wisdom.fwreflectionsb.service.IBaseService;
import org.springframework.stereotype.Service;

/**
 * @author: ziv
 * @date: 2018/12/14 10:04
 * @version: 0.0.1
 */
@Service
public class BaseServiceImpl implements IBaseService {
}
