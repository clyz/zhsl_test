package com.wisdom.fwreflectionsb.ctrl;

import java.util.Set;

/**
 * Controller 实体类
 * @author: ziv
 * @date: 2018/12/14 14:47
 * @version: 0.0.1
 */
public class CtrlInfo {
    /**
     * 方法名称
     */
    private String name;
    /**
     * restful type
     */
    private Set<String> restful;
    /**
     * return type
     */
    private Class<?> type;
    /**
     * return type name;
     */
    private String typeName;
    /**
     * url
     */
    private Set<String> url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getRestful() {
        return restful;
    }

    public void setRestful(Set<String> restful) {
        this.restful = restful;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Set<String> getUrl() {
        return url;
    }

    public void setUrl(Set<String> url) {
        this.url = url;
    }
}