package com.wisdom.fwreflectionsb.ctrl;

public interface Controller {
    String FORWARD = "forward:";
    String REDIRECT = "redirect:";
}