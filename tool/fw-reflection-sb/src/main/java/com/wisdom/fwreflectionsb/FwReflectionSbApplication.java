package com.wisdom.fwreflectionsb;

import com.github.xiaour.api_scanner.annotation.Sapi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Sapi(controllers = "com.wisdom.fwreflectionsb.controller")
public class FwReflectionSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(FwReflectionSbApplication.class, args);
    }

}

