package com.wisdom.fwreflectionsb.ctrl;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

/**
 * @author: ziv
 * @date: 2018/12/14 13:41
 * @version: 0.0.1
 */
public class CtrlMethodHander {
    private CtrlInfo ctrlInfo;
    private Method method;

    public CtrlMethodHander(Method method, Set<String> url, Set<String> restful) {
        this.method = method;
        initCtrlInfo(url, restful);
    }

    private void initCtrlInfo(Set<String> url, Set<String> restful) {
        CtrlInfo ctrlInfo = new CtrlInfo();
        ctrlInfo.setName(method.getName());
        ctrlInfo.setType(method.getReturnType());
        ctrlInfo.setTypeName(method.getReturnType().getTypeName());
        ctrlInfo.setUrl(url);
        ctrlInfo.setRestful(restful);
        this.ctrlInfo = ctrlInfo;
    }

    public CtrlInfo getCtrlInfo() {
        return ctrlInfo;
    }

    public Method getMethod() {
        return method;
    }

    public Class getMethodClass(){
        return method.getDeclaringClass();
    }

}
