package com.wisdom.fwreflectionsb.ctrl;

import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author: ziv
 * @date: 2018/12/14 11:25
 * @version: 0.0.1
 */
public class CtrlReflectionHandler {

    private Map<Class, List<CtrlMethodHander>> ctrlMethodInfo = new ConcurrentHashMap<>();

    public void handler(RequestMappingHandlerMapping rmhm) {
        Optional.ofNullable(rmhm.getHandlerMethods())
                .ifPresent(rmhmMap -> setCtrlMethodInfo(
                        rmhmMap.keySet().stream().filter(requestMappingInfo ->
                                !"basicErrorController".equals(rmhmMap.get(requestMappingInfo).getBean())
                        ).map(requestMappingInfo -> new CtrlMethodHander(
                                rmhmMap.get(requestMappingInfo).getMethod(),
                                requestMappingInfo.getPatternsCondition().getPatterns(),
                                requestMappingInfo.getMethodsCondition().getMethods()
                                        .stream().map(Enum::name).collect(Collectors.toSet())))
                                .collect(Collectors.groupingBy(CtrlMethodHander::getMethodClass))
                ));
    }

    public void setCtrlMethodInfo(Map<Class, List<CtrlMethodHander>> ctrlMethodInfo) {
        this.ctrlMethodInfo = ctrlMethodInfo;
    }
}
