package com.wisdom.fwreflectionsb.controller;

import com.wisdom.fwreflectionsb.ctrl.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

@RequestMapping("/base/")
@RestController
public class BaseController implements Controller {
    @GetMapping("list")
    public List list() {
        return Arrays.asList("ziv", "xo");
    }

    @PostMapping("save")
    public Callable save() {
        return () -> null;
    }
}