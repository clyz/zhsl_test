package com.wisdom.jobxxl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobxxlApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobxxlApplication.class, args);
    }
}
