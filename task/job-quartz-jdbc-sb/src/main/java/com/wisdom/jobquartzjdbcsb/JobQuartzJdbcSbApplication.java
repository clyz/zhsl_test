package com.wisdom.jobquartzjdbcsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobQuartzJdbcSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobQuartzJdbcSbApplication.class, args);
    }


}