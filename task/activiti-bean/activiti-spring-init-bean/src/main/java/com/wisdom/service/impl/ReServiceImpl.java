package com.wisdom.service.impl;

import com.wisdom.service.IReService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReServiceImpl implements IReService {

    private RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
    private RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
    private TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();

    public Deployment deploy(String name, String bpmn, String png) {
        return repositoryService.createDeployment()
                .name(name)
                .addClasspathResource(bpmn)
                .addClasspathResource(png)
                .deploy();
    }

    public ProcessInstance startByKey(String key) {
        return runtimeService.startProcessInstanceByKey(key);
    }

    public List<Task> queryTask(String assignee){
        return taskService.createTaskQuery()
                .taskAssignee(assignee).list();
    }

}
