package com.wisdom.service;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import java.util.List;

/**
 * 流程定义
 */
public interface IReService {

    /**
     * <p>
     * 部署 流程图
     * </p>
     * <p>
     * classPath 方式
     * </p>
     *
     * @param name 流程图名称
     * @param bpmn 流程图 bpmn文件
     * @param png  流程图图片
     * @return 该部署的 ID
     */
    Deployment deploy(String name, String bpmn, String png);

    /**
     * <p>
     * 通过key 开启 一个流程
     * 默认启用最新版本
     * </p>
     *
     * @param key
     * @return
     */
    ProcessInstance startByKey(String key);

    /**
     * <p>
     * 指定个人业务
     * </p>
     *
     * @param assignee
     * @return
     */
    List<Task> queryTask(String assignee);
}