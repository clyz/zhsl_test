package com.wisdom.service;

import com.wisdom.CommonTest;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IReServiceTest extends CommonTest {
    @Autowired
    private IReService reService;
    @Test
    public void queryTask(){
        reService.queryTask("ziv").forEach(System.err::println);
    }
    @Test
    public void startByKey(){
        ProcessInstance myProcess = reService.startByKey("PROCESS_1");
        System.err.println(myProcess);
    }
    @Test
    public void deploy(){
        System.err.println(reService.deploy("test", "test.bpmn", "test.png"));
    }
}