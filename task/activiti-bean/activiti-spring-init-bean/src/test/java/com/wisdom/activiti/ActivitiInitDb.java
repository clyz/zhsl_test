package com.wisdom.activiti;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.TaskQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author: ziv
 * @date: 2018/12/20 14:31
 * @version: 0.0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:activiti.cfg.xml"})
public class ActivitiInitDb {
    private ProcessEngine processEngine;

    @Before
    public void before() {
        processEngine = ProcessEngines.getDefaultProcessEngine();
    }

    @Test
    public void processEngine() {
        //  流程引擎
        ProcessEngineConfiguration engineConfiguration = ProcessEngineConfiguration
                .createStandaloneProcessEngineConfiguration();
        engineConfiguration.setJdbcDriver("com.mysql.jdbc.Driver");
        engineConfiguration.setJdbcPassword("WisdomQa*");
        engineConfiguration.setJdbcUrl("jdbc:mysql://132.232.150.196:3360/activiti");
        engineConfiguration.setJdbcUsername("tiger");

        //配置数据库表的初始化方式
        engineConfiguration.setDatabaseSchemaUpdate("drop-create");

        //得到流程引擎
        processEngine = engineConfiguration.buildProcessEngine();

        processEngine.getRepositoryService().createDeployment().name("test")
                .addClasspathResource("test.bpmn")
                .deploy();

        ProcessInstance processInstance = processEngine.getRuntimeService()
                .startProcessInstanceByKey("PROCESS_1");
    }

    @Test
    public void setProcessEngineXml(){

    }

    @Test
    public void repository() {
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deploy = repositoryService.createDeployment().name("test")
                .addClasspathResource("test.bpmn")
                .deploy();
    }

    @Test
    public void task(){
        TaskService taskService = processEngine.getTaskService();
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery.list().forEach(System.err::println);
    }

    @Test
    public void management(){
        ManagementService managementService = processEngine.getManagementService();
    }

    @Test
    public void start(){
        ProcessInstance instance = processEngine.getRuntimeService()
                .startProcessInstanceByKey("PROCESS_1");
    }

    @Test
    public void next(){
        TaskService taskService = processEngine.getTaskService();
        taskService.complete("7502");
    }

    @Test
    public void service() {
        //  历史记录服务
        processEngine.getHistoryService();
        //  工作流用户服务
//        processEngine.getIdentityService();
        //  流程运行服务
        processEngine.getRuntimeService();
        //  job服务
        processEngine.getTaskService();
        //  页面表单提交服务
//        processEngine.getFormService();
        //  流程图服务
        processEngine.getRepositoryService();
        //  管理器
        processEngine.getManagementService();
    }
}
