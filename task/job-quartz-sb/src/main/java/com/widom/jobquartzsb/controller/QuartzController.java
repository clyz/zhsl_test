package com.widom.jobquartzsb.controller;

import com.widom.jobquartzsb.service.IQuartzService;
import com.widom.jobquartzsb.util.ClassKit;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/")
public class QuartzController {

    @Autowired
    IQuartzService quartzService;

    @RequestMapping("update")
    public Date update(String clazz, @RequestParam(defaultValue = "0/1 * * * * ?") String core) throws ClassNotFoundException, SchedulerException {
        return quartzService.updateCron(ClassKit.getJob(clazz),core);
    }

    @RequestMapping("lg")
    public List<String> listGroup() throws SchedulerException {
        return quartzService.listGroup();
    }

    @RequestMapping("run")
    public Date run(String clazz, @RequestParam(defaultValue = "0/5 * * * * ?") String core) throws Exception {
        return quartzService.buildCoreTriggerTimer(ClassKit.getJob(clazz), core);
    }

    @RequestMapping("stop")
    public boolean stop(String clazz) throws SchedulerException, ClassNotFoundException {
        return quartzService.unscheduleJob(ClassKit.getJob(clazz));
    }
}
