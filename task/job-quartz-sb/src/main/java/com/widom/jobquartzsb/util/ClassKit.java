package com.widom.jobquartzsb.util;

import org.quartz.Job;
import org.quartz.JobKey;

public class ClassKit {
    public static Class<Job> getJob(String clazz) throws ClassNotFoundException {
        return  (Class<Job>) Class.forName("com.widom.jobquartzsb.timers." + clazz);
    }

    public static JobKey getJobKey(String clazz) throws ClassNotFoundException {
        return new JobKey(getJob(clazz).getName());
    }
}
