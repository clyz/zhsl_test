package com.widom.jobquartzsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobQuartzSbApplication {
    public static void main(String[] args) {
        SpringApplication.run(JobQuartzSbApplication.class, args);
    }
}
