package com.widom.jobquartzsb.timers;

import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDateTime;

/**
 * test one
 */
public class WelcomeTimer extends QuartzJobBean {
    static Logger log = LoggerFactory.getLogger(WelcomeTimer.class);
    @Override
    protected void executeInternal(org.quartz.JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("welcome.{}", LocalDateTime.now());
    }
}
