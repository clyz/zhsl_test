package com.widom.jobquartzsb.timers;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class DescTimer implements Job {
    static Logger log = LoggerFactory.getLogger(DescTimer.class);
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Desc{}:", UUID.randomUUID().toString());
    }
}