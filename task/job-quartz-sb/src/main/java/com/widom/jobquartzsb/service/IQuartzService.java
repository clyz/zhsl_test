package com.widom.jobquartzsb.service;

import org.quartz.Job;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import java.util.Date;
import java.util.List;

public interface IQuartzService {
    /**
     *  开启所有
     */
    void startJobs() throws SchedulerException;

    /**
     * 关闭所有
     */
    void shutdownJobs(Scheduler sched) throws SchedulerException;

    /**
     * 更新一个cron 定时任务
     * 不更改trigger的triggerKey
     */
    Date updateSimple(Class<? extends Job> clazz, int cron) throws SchedulerException;
    /**
     * 更新一个cron 定时任务
     */
    Date updateCron(Class<? extends Job> clazz, String cron) throws SchedulerException;
    /**
     * 获取所有组别
     */
    List<String> listGroup() throws SchedulerException;

    /**
     * 停止调度Job任务
     */
    boolean unscheduleJob(Class<? extends Job> clazz) throws SchedulerException;

    /**
     * 停止调度多个Job任务
     */
    boolean unscheduleJobs(List<Class<? extends Job>> clazz) throws SchedulerException;

    /**
     * 开启一个Job
     */
    Date rescheduleJob(Class<? extends Job> clazz) throws SchedulerException;

    boolean delete(JobKey jobKey,Class<? extends Job> clazz) throws SchedulerException;

    /**
     * 触发器 版本的定时任务
     *  System.currentTimeMillis() + 1000 * 60  设置开始时间为1分钟后
     */
    Date buildCoreTriggerTimer(Class<? extends Job> clazz, String core) throws Exception;

    Date buildTimeTriggerTimer(Class<? extends Job> clazz, long startAtTime) throws Exception;
}
