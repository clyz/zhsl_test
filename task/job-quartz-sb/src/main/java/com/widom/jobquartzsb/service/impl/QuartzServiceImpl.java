package com.widom.jobquartzsb.service.impl;

import com.widom.jobquartzsb.service.IQuartzService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class QuartzServiceImpl implements IQuartzService {
    /**
     * 注入任务调度器
     */
    @Autowired
    private Scheduler scheduler;

    public void listJob() throws SchedulerException {
        List<JobExecutionContext> currentlyExecutingJobs = scheduler.getCurrentlyExecutingJobs();
    }

    @Override
    public void shutdownJobs(Scheduler sched) throws SchedulerException {
        if (!scheduler.isShutdown()) {
            scheduler.shutdown();
        }
    }
    @Override
    public void startJobs() throws SchedulerException {
        scheduler.start();
    }

    @Override
    public Date updateSimple(Class<? extends Job> clazz, int cron) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(clazz);
        SimpleTrigger trigger = (SimpleTrigger) scheduler.getTrigger(triggerKey);
        long oldCron = trigger.getRepeatInterval();
        if (oldCron != cron) {
            TriggerBuilder<SimpleTrigger> builder = TriggerBuilder.newTrigger()
                    .withIdentity(clazz.getName())
                    .withSchedule(SimpleScheduleBuilder.repeatHourlyForever(cron))
                    .startNow();
            return scheduler.rescheduleJob(triggerKey, builder.build());
        }
        return null;
    }

    @Override
    public Date updateCron(Class<? extends Job> clazz, String cron) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(clazz);
        CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
        if (!ObjectUtils.isEmpty(trigger)) {
            String oldCron = trigger.getCronExpression();
            if (!oldCron.equalsIgnoreCase(cron)) {
                TriggerBuilder<CronTrigger> builder = TriggerBuilder
                        //  触发器
                        .newTrigger()
                        //  触发器名,触发器组
                        .withIdentity(clazz.getName())
                        // 立即执行
                        .startNow()
                        // 触发器时间设定,修改一个任务的触发时间
                        .withSchedule(CronScheduleBuilder.cronSchedule(cron));
                return scheduler.rescheduleJob(triggerKey, builder.build());
            }
        }
        return null;
    }

    @Override
    public List<String> listGroup() throws SchedulerException {
        return scheduler.getJobGroupNames();
    }

    @Override
    public boolean unscheduleJob(Class<? extends Job> clazz) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(clazz);
        return scheduler.unscheduleJob(triggerKey);
    }

    @Override
    public boolean unscheduleJobs(List<Class<? extends Job>> clazz) throws SchedulerException {
        if(!ObjectUtils.isEmpty(clazz)){
            List triggerKey = new ArrayList<TriggerKey>();
            clazz.forEach(e->triggerKey.add(getTriggerKey(e)));
            return scheduler.unscheduleJobs(triggerKey);
        }
        return false;
    }
    @Override
    public Date rescheduleJob(Class<? extends Job> clazz) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(clazz);
        return scheduler.rescheduleJob(triggerKey, scheduler.getTrigger(triggerKey));
    }

    @Override
    public boolean delete(JobKey jobKey, Class<? extends Job> clazz) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(clazz);
        // 停止触发器
        scheduler.pauseTrigger(triggerKey);
        //  停止调度job
        scheduler.unscheduleJob(triggerKey);
        //  移除
        return scheduler.deleteJob(jobKey);
    }

    @Override
    public Date buildCoreTriggerTimer(Class<? extends Job> clazz, String core) throws Exception {
        //任务名称
        //任务所属分组
        String name = clazz.getName();
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(core);
        //创建任务
        JobDetail jobDetail = JobBuilder.newJob(clazz).withIdentity(name).build();
        //创建任务触发器
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(name).withSchedule(scheduleBuilder).build();
        //将触发器与任务绑定到调度器内
        return scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public Date buildTimeTriggerTimer(Class<? extends Job> clazz, long startAtTime) throws Exception {
        //任务名称
        //任务所属分组
        String name = clazz.getName();
        //创建任务
        JobDetail jobDetail = JobBuilder.newJob(clazz).withIdentity(name).build();
        //创建任务触发器
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(name).startAt(new Date(startAtTime)).build();
        //将触发器与任务绑定到调度器内
        return scheduler.scheduleJob(jobDetail, trigger);
    }

    /**
     * 获取默认 triggerkey
     */
    private TriggerKey getTriggerKey(Class<? extends Job> clazz) {
        return TriggerKey.triggerKey(clazz.getName());
    }
}
