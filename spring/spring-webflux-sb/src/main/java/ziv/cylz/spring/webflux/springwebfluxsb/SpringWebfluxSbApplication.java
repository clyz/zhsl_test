package ziv.cylz.spring.webflux.springwebfluxsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebfluxSbApplication.class, args);
    }

}
