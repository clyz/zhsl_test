/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : springbatch

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-07-10 13:05:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for c
-- ----------------------------
DROP TABLE IF EXISTS `c`;
CREATE TABLE `c` (
  `id` tinyint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` bit(1) DEFAULT NULL,
  `birth` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c
-- ----------------------------
INSERT INTO `c` VALUES ('1', '杨泽民', '1', '2019-07-03 11:39:06');
INSERT INTO `c` VALUES ('2', '暂时', '0', '2019-07-04 11:40:21');
INSERT INTO `c` VALUES ('3', '玩儿', '1', '2019-07-04 11:40:21');
INSERT INTO `c` VALUES ('4', '大', '0', '2019-07-04 6:1:2');
INSERT INTO `c` VALUES ('5', '暂爱好', '1', '2017-07-04 11:10:21');
INSERT INTO `c` VALUES ('6', '系列', '0', '2019-07-06 10:4:21');
INSERT INTO `c` VALUES ('7', '狗屎', '1', '2019-07-04 7:40:21');
