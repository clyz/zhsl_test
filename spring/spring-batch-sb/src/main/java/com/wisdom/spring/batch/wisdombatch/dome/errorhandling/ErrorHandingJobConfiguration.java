package com.wisdom.spring.batch.wisdombatch.dome.errorhandling;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * job 执行 时的异常处理
 * <p>
 * job 其中 一个 step 出错后就会停止，下一次将从上一次错误的 step 开始执行
 */
//@Configuration
public class ErrorHandingJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job errorHandingJob() {
        return jobBuilderFactory
                .get("errorHandingJob")
                .start(step1())
                .next(step2())
                .build();
    }

    private Step step2() {
        return stepBuilderFactory
                .get("step2")
                .tasklet((contribution, chunkContext) -> {
                    if (false) {
                        return RepeatStatus.FINISHED;
                    } else {
                        throw new RuntimeException("Run Exception");
                    }
                })
                .build();
    }

    private Step step1() {
        return stepBuilderFactory
                .get("step1")
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("STEP! -> OK");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }
}
