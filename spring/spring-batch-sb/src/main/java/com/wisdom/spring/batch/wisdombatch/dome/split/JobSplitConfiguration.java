package com.wisdom.spring.batch.wisdombatch.dome.split;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

/**
 * split 异步执行
 */
//@Configuration
public class JobSplitConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job splitJob() {
        return jobBuilderFactory.get("splitJob")
                .start(jobSplitFlow1())
                .split(new SimpleAsyncTaskExecutor())   //  异步执行
                .add(jobSplitFlow2())
                .end()
                .build();
    }

    private Flow jobSplitFlow2() {
        return new FlowBuilder<Flow>("jobSplitFlow2")
                .start(
                        stepBuilderFactory
                                .get("jobSplitFlow2Step1")
                                .tasklet(printTasklet())
                                .build())
                .next(
                        stepBuilderFactory
                                .get("jobSplitFlow2Step2")
                                .tasklet(printTasklet())
                                .build())
                .build();
    }

    private Flow jobSplitFlow1() {
        return new FlowBuilder<Flow>("jobSplitFlow1")
                .start(
                        stepBuilderFactory
                                .get("jobSplitFlow1Step1")
                                .tasklet(printTasklet())
                                .build())
                .next(
                        stepBuilderFactory
                                .get("jobSplitFlow1Step2")
                                .tasklet(printTasklet())
                                .build())
                .build();
    }

    @Bean
    public Tasklet printTasklet() {
        return new PrintTasklet();
    }

    // step执行的任务类（可以写为外部类，此处为了方便，我们写为内部类）
    class PrintTasklet implements Tasklet {

        @Override
        public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
            System.out.println("has been execute on stepName:" + chunkContext.getStepContext().getStepName()
                    + ",has been execute on thread:" + Thread.currentThread().getName());
            return RepeatStatus.FINISHED;
        }

    }
}
