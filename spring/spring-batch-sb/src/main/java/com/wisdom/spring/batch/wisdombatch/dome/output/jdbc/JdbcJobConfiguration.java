package com.wisdom.spring.batch.wisdombatch.dome.output.jdbc;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.SqlTypeValue;
import org.springframework.jdbc.core.StatementCreatorUtils;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 写入数据到 DB 中
 */
//@Configuration
public class JdbcJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;

    @Bean
    public Job jdbcOutputJob() {
        return jobBuilderFactory.get("jdbcOutputJob")
                .start(jdbcOutputStep()).build();
    }

    private Step jdbcOutputStep() {
        return stepBuilderFactory.get("jdbcOutputStep")
                .<C, C>chunk(5)
                .reader(readerCsvC())
                .writer(jdbcWriterC())
                .build();
    }

    private JdbcBatchItemWriter<C> jdbcWriterC() {
        final JdbcBatchItemWriter<C> jdbcBatchItemWriter = new JdbcBatchItemWriter<>();
        jdbcBatchItemWriter.setDataSource(dataSource);
        //  BUG- 时间 -BUG (java.util.Date -> java.sql.Date 不等)
        jdbcBatchItemWriter.setSql("INSERT INTO c(id,name,sex,birth) values(?,?,?,?)");
        jdbcBatchItemWriter.setItemPreparedStatementSetter((item, ps) -> {
            StatementCreatorUtils.setParameterValue(ps, 1, SqlTypeValue.TYPE_UNKNOWN, item.getId());
            StatementCreatorUtils.setParameterValue(ps, 2, SqlTypeValue.TYPE_UNKNOWN, item.getName());
            StatementCreatorUtils.setParameterValue(ps, 3, SqlTypeValue.TYPE_UNKNOWN, item.getSex());
            StatementCreatorUtils.setParameterValue(ps, 4, SqlTypeValue.TYPE_UNKNOWN, item.getBirth());
        });

        /*jdbcBatchItemWriter.setSql("INSERT INTO c(id,name,sex) values(:id,:name,:sex)");
        jdbcBatchItemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());    //  参数提供者*/

        jdbcBatchItemWriter.afterPropertiesSet();
        return jdbcBatchItemWriter;
    }

    private FlatFileItemReader<C> readerCsvC() {
        final FlatFileItemReader<C> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new ClassPathResource("static/doc/input/c_1.csv"));
        flatFileItemReader.setLinesToSkip(1); //  跳过第一行

        /** 解析文件 */
        final DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer(); //  解析方式
        delimitedLineTokenizer.setNames("id", "name", "sex", "birth");   //通过表头分割
        final DefaultLineMapper<C> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSet -> {
            try {
                return new C(
                        fieldSet.readLong("id"),
                        fieldSet.readString("name"),
                        fieldSet.readBoolean("sex"),
                        new SimpleDateFormat("yyyy-MM-DD hh:mm:ss").parse(fieldSet.readString("birth"))
                );
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        });   //  读取文件

        flatFileItemReader.setLineMapper(defaultLineMapper);
        return flatFileItemReader;
    }
}
