package com.wisdom.spring.batch.wisdombatch.dome.param;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

//@Configuration
public class JobParametersConfiguretion implements StepExecutionListener {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    private Map<String, JobParameter> parames;

    @Bean
    public Job MyParametersJobThree() {
        return jobBuilderFactory.get("MyParametersJobThree")
                .start(MyParametersJobStep3())
                .build();
    }

    private Step MyParametersJobStep3() {
        return stepBuilderFactory.get("MyParametersJobStep3")
                .listener(this)
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("parame is: "+parames.get("info"));
                    return RepeatStatus.FINISHED;
                }).build();
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        System.out.println(stepExecution.getStepName()+"运行之前...........");

        parames = stepExecution.getJobParameters().getParameters();

        parames.put("info",new JobParameter("info",true));
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println(stepExecution.getStepName()+"运行之完毕...........");
        return null;
    }
}
