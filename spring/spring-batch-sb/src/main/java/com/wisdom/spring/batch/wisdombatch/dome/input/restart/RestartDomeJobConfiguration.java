package com.wisdom.spring.batch.wisdombatch.dome.input.restart;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

/**
 * ItemStreamReader open(),close(),update()
 * 重新，继续执行，上一次的
 */
//@Configuration
public class RestartDomeJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Qualifier("restartDemoReader")
    private ItemReader<C> reader;

    @Bean
    public Job retartDomeJob() {
        return jobBuilderFactory.get("retartDomeJob")
                .start(retartDomeStep())
                .build();
    }

    @Bean
    public Step retartDomeStep() {
        return stepBuilderFactory.get("retartDomeStep")
                .<C, C>chunk(5)
                .reader(reader)
                .writer(writerRestartC())
                .build();
    }

    @Bean
    public ItemWriter<? super C> writerRestartC() {
        return items -> {
            items.forEach(item -> {
                System.out.println("GET:Multiple_CSV_C::    Writing item: " + item);
            });
        };
    }
}
