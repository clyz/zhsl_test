package com.wisdom.spring.batch.wisdombatch.dome.decision;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * decision 决策器 通过 返回状态判断是否 继续执行    JobExecutionDecider
 */
//@Configuration
public class JobDecisionConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    //编写Job
    @Bean
    public Job FlowDecisionJob() {
        return jobBuilderFactory.get("FlowDecisionJob")
                .start(firstStep()).next(myDecider())
                .from(myDecider()).on("EVEN").to(evenStep())
                .from(myDecider()).on("ODD").to(oddStep())
                .from(oddStep()).on("*").to(myDecider())
                .end()
                .build();
    }

    @Bean
    public Step firstStep() {
        return stepBuilderFactory.get("firstStep").tasklet((contribution, chunkContext) -> {
            System.out.println("Hello firstStep..");
            return RepeatStatus.FINISHED;
        }).build();

    }

    @Bean
    public Step evenStep() {
        return stepBuilderFactory.get("evenStep").tasklet((contribution, chunkContext) -> {
            System.out.println("Hello evenStep..");
            return RepeatStatus.FINISHED;
        }).build();

    }

    @Bean
    public Step oddStep() {
        return stepBuilderFactory.get("oddStep").tasklet((contribution, chunkContext) -> {
            System.out.println("Hello oddStep..");
            return RepeatStatus.FINISHED;
        }).build();

    }

    @Bean
    public JobExecutionDecider myDecider() {
        return new MyDecider();
    }
}
