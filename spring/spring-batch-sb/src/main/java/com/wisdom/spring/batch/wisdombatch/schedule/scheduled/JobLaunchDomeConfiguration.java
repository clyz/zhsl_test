package com.wisdom.spring.batch.wisdombatch.schedule.scheduled;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.converter.DefaultJobParametersConverter;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.JobParametersNotFoundException;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Map;

//@Configuration
public class JobLaunchDomeConfiguration implements StepExecutionListener, ApplicationContextAware {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    private Map<String, JobParameter> parameterMap;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private JobRegistry jobRegistry;
    @Autowired
    private JobRepository jobRepository;

    private ApplicationContext context;

    @Autowired
    private IncrementerSchedulerJob incrementerSchedulerJob;

    //  定时任务执行 5s
    @Scheduled(fixedDelay = 5000)
    public void scheduler() throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException, JobParametersNotFoundException, NoSuchJobException {
        jobOperator().startNextInstance("schedulerJob");
    }

    @Bean
    public Job schedulerJob(){
        return jobBuilderFactory
                .get("schedulerJob")
                .incrementer(incrementerSchedulerJob)
                .start(schedulerStep())
                .build();
    }

    @Bean
    public Step schedulerStep() {
        return stepBuilderFactory.get("schedulerStep")
                .listener(this)
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("scheduler Step RUN !  " + parameterMap.get("now"));
                    return RepeatStatus.FINISHED;
                }).build();
    }

    //  Job 与 名称 关系 JobOperator 通过名称找到对应的 Job-Bean
    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor() throws Exception {
        final JobRegistryBeanPostProcessor processor = new JobRegistryBeanPostProcessor();
        processor.setJobRegistry(jobRegistry);
        processor.setBeanFactory(context.getAutowireCapableBeanFactory());
        processor.afterPropertiesSet();
        return processor;
    }

    @Bean
    public JobOperator jobOperator() {
        final SimpleJobOperator operator = new SimpleJobOperator();
        operator.setJobLauncher(jobLauncher);
        operator.setJobParametersConverter(new DefaultJobParametersConverter());   //  参数传化方式
        operator.setJobRepository(jobRepository);   //  Job名称如何管理Bean
        operator.setJobExplorer(jobExplorer);   //  如何 更新 数据
        operator.setJobRegistry(jobRegistry);    //  如何 持久化
        return operator;
    }

    @Bean("jobLaunchDome")
    public Job jobLaunchDome() {
        return jobBuilderFactory.get("jobLaunchDome")
                .start(jobLaunchStep())
                .build();
    }

    @Bean
    public Step jobLaunchStep() {
        return stepBuilderFactory.get("jobLaunchStep")
                .listener(this)
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("Job_Launch_Step : Run !" + parameterMap.get("param"));
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        parameterMap = stepExecution.getJobParameters().getParameters();
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
