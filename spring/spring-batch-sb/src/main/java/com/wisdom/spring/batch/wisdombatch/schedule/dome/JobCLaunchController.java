package com.wisdom.spring.batch.wisdombatch.schedule.dome;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobInstanceAlreadyExistsException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
@RequestMapping("job/claunch")
public class JobCLaunchController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobOperator jobOperator;

    @Autowired
    private Job jobLaunchDome;

    @GetMapping("runJob1_{param}")
    public String runJob1(@PathVariable("param") String param) throws JobParametersInvalidException,
            JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        System.out.println("Run Job1 PARAM::  " + param);
        //  配置 JOB 参数
        final JobParameters jobParam = new JobParametersBuilder()
                .addString("param", param)
                .toJobParameters();
        jobLauncher.run(jobLaunchDome, jobParam);
        return "Job RUN " + param;
    }

    @GetMapping("runJob2_{param}")
    public String runJob2(@PathVariable("param") String param) throws JobParametersInvalidException,
            JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobInstanceAlreadyExistsException, NoSuchJobException {
        System.out.println("Run Job2 PARAM::  " + param);
        jobOperator.start("jobLaunchDome","param="+param);
        return "Job RUN " + param;
    }
}
