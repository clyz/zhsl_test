package com.wisdom.spring.batch.wisdombatch.dome.output.multiple;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.support.ClassifierCompositeItemWriter;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.classify.Classifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.oxm.xstream.XStreamMarshaller;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;

/**
 * 写入 多个文件中
 */
//@Configuration
public class MultipleJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;
    @Value("static/doc/output/c.xml")
    private Resource resourceXml;
    @Value("static/doc/output/c.csv")
    private Resource resourceCsv;

    @Bean
    public Job multipleOutputJob() throws Exception {
        return jobBuilderFactory.get("multipleOutputJob")
                .start(multipleOutputStep2()).build();
    }

    private Step multipleOutputStep() throws Exception {
        return stepBuilderFactory.get("multipleOutputStep")
                .<C, C>chunk(3)
                .reader(readerMultipleC())
                .writer(multipleWriterC())
                .build();
    }

    private Step multipleOutputStep2() throws Exception {
        final StaxEventItemWriter<C> staxEventItemWriter = xmlWriterC();
        final FlatFileItemWriter<C> flatFileItemWriter = csvWriterC();
        return stepBuilderFactory.get("multipleOutputStep2")
                .<C, C>chunk(3)
                .reader(readerMultipleC())
                .writer(classifierCompositeItemWriter(staxEventItemWriter, flatFileItemWriter))
                .stream(staxEventItemWriter)    //  打开流
                .stream(flatFileItemWriter)
                .build();
    }

    //  方式 一 放入 多文件中
    private CompositeItemWriter<C> multipleWriterC() throws Exception {
        final CompositeItemWriter<C> compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(Arrays.asList(xmlWriterC(), csvWriterC()));
        compositeItemWriter.afterPropertiesSet();
        return compositeItemWriter;
    }

    //  方式 二 分类放入 不同文件中
    private ClassifierCompositeItemWriter<C> classifierCompositeItemWriter(StaxEventItemWriter<C> staxEventItemWriter, FlatFileItemWriter<C> flatFileItemWriter) {
        final ClassifierCompositeItemWriter<C> classifierCompositeItemWriter = new ClassifierCompositeItemWriter<>();
        classifierCompositeItemWriter.setClassifier(
                new Classifier<C, ItemWriter<? super C>>() {
                    @Override
                    public ItemWriter<? super C> classify(C c) {
                        return c.getId() % 2 == 0 ? staxEventItemWriter : flatFileItemWriter;
                    }
                }
        );
        return classifierCompositeItemWriter;
    }

    public StaxEventItemWriter<C> xmlWriterC() throws Exception {
        final StaxEventItemWriter<C> flatFileItemWriter = new StaxEventItemWriter<>();
        flatFileItemWriter.setEncoding("utf-8");
        flatFileItemWriter.setResource(resourceXml);
        flatFileItemWriter.setRootTagName("cs");
        final HashMap<String, Class> map = new HashMap<>();
        map.put("c", C.class);
        final XStreamMarshaller streamMarshaller = new XStreamMarshaller();
        streamMarshaller.setAliases(map);

        flatFileItemWriter.setMarshaller(streamMarshaller);
        flatFileItemWriter.afterPropertiesSet();
        return flatFileItemWriter;
    }

    public FlatFileItemWriter<C> csvWriterC() throws Exception {
        final FlatFileItemWriter<C> flatFileItemWriter = new FlatFileItemWriter<>();
        flatFileItemWriter.setEncoding("utf-8");
        flatFileItemWriter.setResource(resourceCsv);
        flatFileItemWriter.setLineAggregator(new DelimitedLineAggregator<>());    //  写的方式
        flatFileItemWriter.afterPropertiesSet();
        return flatFileItemWriter;
    }

    private JdbcPagingItemReader<C> readerMultipleC() throws Exception {
        final JdbcPagingItemReader<C> jdbcPagingItemReader = new JdbcPagingItemReader<>();
        jdbcPagingItemReader.setDataSource(dataSource);
        jdbcPagingItemReader.setFetchSize(5);   //  每次读取的个数
        jdbcPagingItemReader.setRowMapper((resultSet, i) ->
                C.builder()
                        .id(resultSet.getLong("id"))
                        .sex(resultSet.getBoolean("sex"))
                        .name(resultSet.getNString("name"))
                        .birth(resultSet.getDate("birth"))
                        .build());    //  如何映射成 Mapper 对象

        /** 读取的SQL 语句 */
        final MySqlPagingQueryProvider mySqlPagingQueryProvider = new MySqlPagingQueryProvider();
        mySqlPagingQueryProvider.setSelectClause("id,name,sex,birth");  //  查询的字段
        mySqlPagingQueryProvider.setFromClause("FROM C");   //  查表
        final HashMap<String, Order> sortKeys = new HashMap<>();
        sortKeys.put("id", Order.DESCENDING);    //  降序排序
        mySqlPagingQueryProvider.setSortKeys(sortKeys); //  排序策略

        jdbcPagingItemReader.setQueryProvider(mySqlPagingQueryProvider);
        jdbcPagingItemReader.afterPropertiesSet();
        return jdbcPagingItemReader;
    }
}
