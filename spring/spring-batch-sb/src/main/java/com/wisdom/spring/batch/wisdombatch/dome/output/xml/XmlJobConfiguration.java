package com.wisdom.spring.batch.wisdombatch.dome.output.xml;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.oxm.xstream.XStreamMarshaller;

import javax.sql.DataSource;
import java.util.HashMap;

//@Configuration
public class XmlJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;
    @Value("static/doc/output/c.xml")
    private Resource resource;

    @Bean
    public Job xmlOutputJob() throws Exception {
        return jobBuilderFactory.get("xmlOutputJob")
                .start(xmlOutputStep()).build();
    }

    private Step xmlOutputStep() throws Exception {
        return stepBuilderFactory.get("xmlOutputStep")
                .<C, C>chunk(1)
                .reader(readerXmlC())
                .writer(xmlWriterC())
                .build();
    }

    private StaxEventItemWriter<C> xmlWriterC() throws Exception {
        final StaxEventItemWriter<C> flatFileItemWriter = new StaxEventItemWriter<>();
        flatFileItemWriter.setEncoding("utf-8");
        flatFileItemWriter.setResource(resource);
        flatFileItemWriter.setRootTagName("cs");
        System.out.println(resource.getFile().getAbsolutePath());
        final HashMap<String, Class> map = new HashMap<>();
        map.put("c", C.class);
        final XStreamMarshaller streamMarshaller = new XStreamMarshaller();
        streamMarshaller.setAliases(map);

        flatFileItemWriter.setMarshaller(streamMarshaller);
        flatFileItemWriter.afterPropertiesSet();
        return flatFileItemWriter;
    }

    private JdbcPagingItemReader<C> readerXmlC() throws Exception {
        final JdbcPagingItemReader<C> jdbcPagingItemReader = new JdbcPagingItemReader<>();
        jdbcPagingItemReader.setDataSource(dataSource);
        jdbcPagingItemReader.setFetchSize(5);   //  每次读取的个数
        jdbcPagingItemReader.setRowMapper((resultSet, i) ->
                C.builder()
                        .id(resultSet.getLong("id"))
                        .sex(resultSet.getBoolean("sex"))
                        .name(resultSet.getNString("name"))
                        .birth(resultSet.getDate("birth"))
                        .build());    //  如何映射成 Mapper 对象

        /** 读取的SQL 语句 */
        final MySqlPagingQueryProvider mySqlPagingQueryProvider = new MySqlPagingQueryProvider();
        mySqlPagingQueryProvider.setSelectClause("id,name,sex,birth");  //  查询的字段
        mySqlPagingQueryProvider.setFromClause("FROM C");   //  查表
        final HashMap<String, Order> sortKeys = new HashMap<>();
        sortKeys.put("id", Order.DESCENDING);    //  降序排序
        mySqlPagingQueryProvider.setSortKeys(sortKeys); //  排序策略

        jdbcPagingItemReader.setQueryProvider(mySqlPagingQueryProvider);
        jdbcPagingItemReader.afterPropertiesSet();
        return jdbcPagingItemReader;
    }
}
