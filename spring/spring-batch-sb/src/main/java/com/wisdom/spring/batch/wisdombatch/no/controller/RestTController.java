package com.wisdom.spring.batch.wisdombatch.no.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("read")
public class RestTController {

    @GetMapping(value = "up")
    public String idex(String id, String cookie, @RequestParam(defaultValue = "100") int num) {
        System.out.println("GOGO");
        String url = "http://132.232.150.196/wisdom3A/platform/content/updateReadNumIncreaseOfNews";
        MultiValueMap<String, String> param = new LinkedMultiValueMap<>();
        param.add("id", id);

        HttpHeaders headers = new HttpHeaders();

        List<String> cookieList = new ArrayList<>();
        cookieList.add("sennId=" + cookie);
        headers.put(HttpHeaders.COOKIE, cookieList);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(param, headers);
        final RestTemplate restTemplate = new RestTemplate();
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < num; i++) {
            final ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
            stringBuilder.append(entity.getBody());
        }
        return stringBuilder.toString();
    }
}
