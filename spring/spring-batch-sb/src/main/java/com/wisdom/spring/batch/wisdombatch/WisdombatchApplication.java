package com.wisdom.spring.batch.wisdombatch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = {})
//@EnableBatchProcessing
//@EnableScheduling
public class WisdombatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(WisdombatchApplication.class, args);
    }

}
