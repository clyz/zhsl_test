package com.wisdom.spring.batch.wisdombatch.dome.input.jdbc;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;

/**
 * 从数据库中读取 ItemReader,ItemWriter
 */
//@Configuration
public class DBjdbcDomeJobConfguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;

    @Bean
    public Job dBjdbcDomeJob() {
        return jobBuilderFactory.get("dBjdbcDomeJob")
                .start(dBjdbcDomeStep())
                .build();
    }

    public Step dBjdbcDomeStep() {
        return stepBuilderFactory.get("dBjdbcDomeStep")
                .<C, C>chunk(5)
                .reader(readerJdbcC())
                .writer(writerJdbcC())
                .build();
    }

    /**
     * jdbc 方式读取
     */
    @Bean   //  创建 工厂BEAN
    @StepScope  //  @Scope(value = "step", proxyMode = ScopedProxyMode.TARGET_CLASS) 默认代理模式,bean的生命周期
    public JdbcPagingItemReader<C> readerJdbcC() {
        final JdbcPagingItemReader<C> jdbcPagingItemReader = new JdbcPagingItemReader<>();
        jdbcPagingItemReader.setDataSource(dataSource);
        jdbcPagingItemReader.setFetchSize(5);   //  每次读取的个数
        jdbcPagingItemReader.setRowMapper((resultSet, i) ->
                new C(resultSet.getLong("id"),
                        resultSet.getNString("name"),
                        resultSet.getBoolean("sex"),
                        resultSet.getDate("birth")));    //  如何映射成 Mapper 对象

        /** 读取的SQL 语句 */
        final MySqlPagingQueryProvider mySqlPagingQueryProvider = new MySqlPagingQueryProvider();
        mySqlPagingQueryProvider.setSelectClause("id,name,sex,birth");  //  查询的字段
        mySqlPagingQueryProvider.setFromClause("FROM C");   //  查表
        final HashMap<String, Order> sortKeys = new HashMap<>();
        sortKeys.put("id", Order.DESCENDING);    //  降序排序
        mySqlPagingQueryProvider.setSortKeys(sortKeys); //  排序策略

        jdbcPagingItemReader.setQueryProvider(mySqlPagingQueryProvider);
        return jdbcPagingItemReader;
    }

    @Bean
    public ItemWriter<? super C> writerJdbcC() {
        return items -> {
            items.forEach(item -> {
                System.out.println("GET:JDBC_C::    Writing item: " + item);
            });
        };
    }

    //  实体Bean
//    @Builder    //lombok  创建对象,构建器模式
    class C {
        private Long id;
        private String name;
        private Boolean sex;
        private Date birth;

        public C() {
        }

        public C(Long id, String name, Boolean sex, Date birth) {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.birth = birth;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getSex() {
            return sex;
        }

        public void setSex(Boolean sex) {
            this.sex = sex;
        }

        public Date getBirth() {
            return birth;
        }

        public void setBirth(Date birth) {
            this.birth = birth;
        }

        @Override
        public String toString() {
            return "C{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", sex=" + sex +
                    ", birth=" + birth +
                    '}';
        }
    }
}
