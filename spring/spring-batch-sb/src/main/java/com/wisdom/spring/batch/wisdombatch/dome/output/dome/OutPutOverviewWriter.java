package com.wisdom.spring.batch.wisdombatch.dome.output.dome;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class OutPutOverviewWriter implements ItemWriter<String> {

    @Override
    public void write(List<? extends String> items) throws Exception {
        System.out.println("Chunk size is: " + items.size());   //  这一批次的个数
        items.forEach(e -> System.out.print(e + "\t"));
        System.out.println("-------------------");
    }
}