package com.wisdom.spring.batch.wisdombatch.dome.input.csv;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 从csv中读取 ItemReader,ItemWriter
 */
//@Configuration
public class CsvDomeJobConfguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job csvDomeJob() {
        return jobBuilderFactory.get("csvDomeJob")
                .start(csvDomeStep())
                .build();
    }

    public Step csvDomeStep() {
        return stepBuilderFactory.get("csvDomeStep")
                .<C, C>chunk(5)
                .reader(readerCsvC())
                .writer(writerCsvC())
                .build();
    }

    /**
     * file_csv 方式读取
     */
    @Bean   //  创建 工厂BEAN
    @StepScope  //  @Scope(value = "step", proxyMode = ScopedProxyMode.TARGET_CLASS) 默认代理模式,bean的生命周期
    public FlatFileItemReader<C> readerCsvC() {
        final FlatFileItemReader<C> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new ClassPathResource("static/doc/input/c.csv"));
        flatFileItemReader.setLinesToSkip(1); //  跳过第一行

        /** 解析文件 */
        final DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer(); //  解析方式
        delimitedLineTokenizer.setNames("id", "name", "sex", "brith");   //通过表头分割
        final DefaultLineMapper<C> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSet -> {
            try {
                return new C(
                        fieldSet.readLong("id"),
                        fieldSet.readString("name"),
                        fieldSet.readBoolean("sex"),
                        new SimpleDateFormat("yyyy-MM-DD hh:mm:ss").parse(fieldSet.readString("brith"))
                );
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        });   //  读取文件

        flatFileItemReader.setLineMapper(defaultLineMapper);
        return flatFileItemReader;
    }

    @Bean
    public ItemWriter<? super C> writerCsvC() {
        return items -> {
            items.forEach(item -> {
                System.out.println("GET:CSV_C::    Writing item: " + item);
            });
        };
    }

    //  实体Bean
//    @Builder    //lombok  创建对象,构建器模式
    class C {
        private Long id;
        private String name;
        private Boolean sex;
        private Date birth;

        public C() {
        }

        public C(Long id, String name, Boolean sex, Date birth) {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.birth = birth;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getSex() {
            return sex;
        }

        public void setSex(Boolean sex) {
            this.sex = sex;
        }

        public Date getBirth() {
            return birth;
        }

        public void setBirth(Date birth) {
            this.birth = birth;
        }

        @Override
        public String toString() {
            return "C{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", sex=" + sex +
                    ", birth=" + birth +
                    '}';
        }
    }
}
