package com.wisdom.spring.batch.wisdombatch.schedule.scheduled;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 设置参数
 */
@Component
public class IncrementerSchedulerJob implements JobParametersIncrementer {

    @Override
    public JobParameters getNext(JobParameters parameters) {
        final JobParameters jobParameters = parameters == null ? new JobParameters() : parameters;
        return new JobParametersBuilder(jobParameters).addDate("now", new Date()).toJobParameters();
    }
}
