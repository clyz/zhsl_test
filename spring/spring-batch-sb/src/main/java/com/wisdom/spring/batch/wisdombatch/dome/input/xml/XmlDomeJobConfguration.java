package com.wisdom.spring.batch.wisdombatch.dome.input.xml;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.xstream.XStreamMarshaller;

import java.util.HashMap;

/**
 * 从xml中读取 ItemReader,ItemWriter
 */
//@Configuration
public class XmlDomeJobConfguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job xmlDomeJob() {
        return jobBuilderFactory.get("xmlDomeJob")
                .start(xmlDomeStep())
                .build();
    }

    @Bean
    public Step xmlDomeStep() {
        return stepBuilderFactory.get("xmlDomeStep")
                .<C, C>chunk(5)
                .reader(readerXmlC())
                .writer(writerXmlC())
                .build();
    }

    @Bean
    @StepScope
    public StaxEventItemReader<C> readerXmlC() {
        final StaxEventItemReader<C> staxEventItemReader = new StaxEventItemReader<>();
        staxEventItemReader.setResource(new ClassPathResource("static/doc/input/c.xml"));
        staxEventItemReader.setFragmentRootElementName("data"); //  批量处理的节点

        final XStreamMarshaller xStreamMarshaller = new XStreamMarshaller();
        final HashMap<String, Class> map = new HashMap<>();
        map.put("data", C.class);
        xStreamMarshaller.setAliases(map);  //  设置 节点对应对象
        staxEventItemReader.setUnmarshaller(xStreamMarshaller);
        return staxEventItemReader;
    }

    @Bean
    public ItemWriter<? super C> writerXmlC() {
        return items -> {
            items.forEach(item -> {
                System.out.println("GET:XML_C::    Writing item: " + item);
            });
        };
    }
}
