package com.wisdom.spring.batch.wisdombatch.dome.input.dome;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

/**
 * ItemReader,ItemWriter
 */
//@Configuration
public class InputOverviewDemoJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    //监听Job执行
    @Bean
    public Job myListenerJob() {
        return jobBuilderFactory.get("inputOverviewDemoJob")
                .start(inputOverviewDemoStep())
                .build();
    }

    private Step inputOverviewDemoStep() {
        return stepBuilderFactory.get("inputOverviewDemoStep")
                .<String, String>chunk(2)   //  区块的处理个数 每次处理两个 及 IO_INPUT,IO_OUTPUT
                .faultTolerant()
                .reader(
                        new ListItemReader<>(Arrays.asList("暂时", "你好", "他好", "zhouenlai", "liushaoqi"))
                )   //  ItemReader<? extends String> 读 数据 IO_INPUT STRING
                .writer(
                        items -> {
                            items.forEach(item -> {
                                System.out.println("Writing item: " + item);
                            });
                        }
                )   //  ItemWriter<? super String> 写 数据 IO_OUTPUT STRING
                .build();
    }
}
