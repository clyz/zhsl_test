package com.wisdom.spring.batch.wisdombatch.dome.test;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

//@Configuration
public class JobTestConfiguration {

    @Bean
    public Job helloWorldJob(JobBuilderFactory jobBuilderFactory,StepBuilderFactory stepBuilderFactory) {
        return jobBuilderFactory.get("helloWorldJob").start(step1(stepBuilderFactory)).build();
    }

    @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get("step1").tasklet((stepContribution, chunkContext) -> RepeatStatus.FINISHED).build();
    }

    @Bean(name = "dataSource")
    @Qualifier("dataSource")
    @Primary
    public DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
        dataSourceBuilder.url("jdbc:mysql://localhost:3306/springbatch");
        dataSourceBuilder.username("root");
        dataSourceBuilder.password("zemin886520");
        return dataSourceBuilder.build();
    }

    @Bean
    public JobLauncherTestUtils jobLauncherTestUtils() {
        return new JobLauncherTestUtils();
    }

    @Bean
    public JobRepositoryTestUtils jobRepositoryTestUtils(JobRepository jobRepository, @Qualifier("dataSource") DataSource dataSource) {
        return new JobRepositoryTestUtils(jobRepository, dataSource);
    }
}
