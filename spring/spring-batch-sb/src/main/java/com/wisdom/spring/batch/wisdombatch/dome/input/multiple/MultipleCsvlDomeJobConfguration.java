package com.wisdom.spring.batch.wisdombatch.dome.input.multiple;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import com.wisdom.spring.batch.wisdombatch.dome.input.csv.CsvDomeJobConfguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.xstream.XStreamMarshaller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * 多文件读取数据
 * 读取 ItemReader,ItemWriter
 */
//@Configuration
public class MultipleCsvlDomeJobConfguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Value("classpath:/static/doc/input/c*.csv")
    Resource[] resoucres;

    @Bean
    public Job multipleCsvDomeJob() {
        return jobBuilderFactory.get("multipleCsvDomeJob")
                .start(multipleCsvDomeStep())
                .build();
    }

    @Bean
    public Step multipleCsvDomeStep() {
        return stepBuilderFactory.get("multipleCsvDomeStep")
                .<C, C>chunk(5)
                .reader(readerMultipleCsvC())
                .writer(writerMultipleCsvC())
                .build();
    }

    @Bean
    @StepScope
    public MultiResourceItemReader<C> readerMultipleCsvC() {
        final MultiResourceItemReader<C> multiResourceItemReader = new MultiResourceItemReader<>();
        multiResourceItemReader.setDelegate(readerCsvC());  //  设置 代理处理
        multiResourceItemReader.setResources(resoucres);
        return multiResourceItemReader;
    }

    private FlatFileItemReader<C> readerCsvC() {
        final FlatFileItemReader<C> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setLinesToSkip(1); //  跳过第一行

        /** 解析文件 */
        final DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer(); //  解析方式
        delimitedLineTokenizer.setNames("id", "name", "sex", "brith");   //通过表头分割
        final DefaultLineMapper<C> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSet -> {
            try {
                return new C(
                        fieldSet.readLong("id"),
                        fieldSet.readString("name"),
                        fieldSet.readBoolean("sex"),
                        new SimpleDateFormat("yyyy-MM-DD hh:mm:ss").parse(fieldSet.readString("brith"))
                );
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        });   //  读取文件

        flatFileItemReader.setLineMapper(defaultLineMapper);
        return flatFileItemReader;
    }

    @Bean
    public ItemWriter<? super C> writerMultipleCsvC() {
        return items -> {
            items.forEach(item -> {
                System.out.println("GET:Multiple_CSV_C::    Writing item: " + item);
            });
        };
    }
}
