package com.wisdom.spring.batch.wisdombatch.dome.output.dome;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

//@Configuration
public class OutputOverviewJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job outputOverviewJob() {
        return jobBuilderFactory.get("outputOverviewJob")
                .start(outputOverviewStep()).build();
    }

    private Step outputOverviewStep() {
        return stepBuilderFactory.get("outputOverviewStep")
                .<String, String>chunk(5)
                .reader(new ListItemReader<String>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K")))
                .writer(new OutPutOverviewWriter())
                .build();
    }
}