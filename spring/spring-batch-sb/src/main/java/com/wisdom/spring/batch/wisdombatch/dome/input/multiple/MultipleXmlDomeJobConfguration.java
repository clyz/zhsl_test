package com.wisdom.spring.batch.wisdombatch.dome.input.multiple;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.xstream.XStreamMarshaller;

import java.util.HashMap;

/**
 * 多文件读取数据
 * 读取 ItemReader,ItemWriter
 */
//@Configuration
public class MultipleXmlDomeJobConfguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Value("classpath:/static/doc/input/c*.xml")
    Resource[] resoucres;

    @Bean
    public Job multipleXmlDomeJob() {
        return jobBuilderFactory.get("multipleXmlDomeJob")
                .start(multipleXmlDomeStep())
                .build();
    }

    @Bean
    public Step multipleXmlDomeStep() {
        return stepBuilderFactory.get("multipleXmlDomeStep")
                .<C, C>chunk(5)
                .reader(readerMultipleXmlC())
                .writer(writerMultipleXmlC())
                .build();
    }

    @Bean
    @StepScope
    public MultiResourceItemReader<C> readerMultipleXmlC() {
        final MultiResourceItemReader<C> multiResourceItemReader = new MultiResourceItemReader<>();
        multiResourceItemReader.setDelegate(readerXmlC());  //  设置 代理处理
        multiResourceItemReader.setResources(resoucres);
        return multiResourceItemReader;
    }

    private StaxEventItemReader<C> readerXmlC() {
        final StaxEventItemReader<C> staxEventItemReader = new StaxEventItemReader<>();
        staxEventItemReader.setFragmentRootElementName("data"); //  批量处理的节点

        final XStreamMarshaller xStreamMarshaller = new XStreamMarshaller();
        final HashMap<String, Class> map = new HashMap<>();
        map.put("data", C.class);
        xStreamMarshaller.setAliases(map);  //  设置 节点对应对象
        staxEventItemReader.setUnmarshaller(xStreamMarshaller);
        return staxEventItemReader;
    }

    @Bean
    public ItemWriter<? super C> writerMultipleXmlC() {
        return items -> {
            items.forEach(item -> {
                System.out.println("GET:Multiple_XML_C::    Writing item: " + item);
            });
        };
    }
}
