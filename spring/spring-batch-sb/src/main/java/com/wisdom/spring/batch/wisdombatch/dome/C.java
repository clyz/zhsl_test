package com.wisdom.spring.batch.wisdombatch.dome;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

//  实体Bean
@Builder    //lombok  创建对象,构建器模式
@Data
@AllArgsConstructor
@NoArgsConstructor
public class C {
    private Long id;
    private String name;
    private Boolean sex;
    private Date birth;
}
