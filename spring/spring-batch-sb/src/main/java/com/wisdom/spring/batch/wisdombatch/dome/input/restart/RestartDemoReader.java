package com.wisdom.spring.batch.wisdombatch.dome.input.restart;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.item.*;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

/**
 * 节点继续 读取数据
 * open() -> read()
 * -> update()
 * -> close()
 * <p>
 * 表中会记录当前执行到的位置 batch_step_execution_context
 * SHORT_CONTEXT:
 * {"batch.taskletType":"org.springframework.batch.core.step.item.ChunkOrientedTasklet","batch.stepType":"org.springframework.batch.core.step.tasklet.TaskletStep","curLine":["java.lang.Long",15]}
 */
//@Component("restartDemoReader")
public class RestartDemoReader implements ItemStreamReader<C> {

    private Long curLine = 0L;
    private boolean restart = false;
    private FlatFileItemReader<C> reader = new FlatFileItemReader<>();
    private ExecutionContext executionContext;

    public RestartDemoReader() {
        reader.setResource(new ClassPathResource("static/doc/input/c.csv"));
        reader.setLinesToSkip(1);

        /** 解析文件 */
        final DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer(); //  解析方式
        delimitedLineTokenizer.setNames("id", "name", "sex", "brith");   //通过表头分割
        final DefaultLineMapper<C> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSet -> {
            try {
                return
                        C.builder()
                                .id(fieldSet.readLong("id"))
                                .name(fieldSet.readString("name"))
                                .sex(fieldSet.readBoolean("sex"))
                                .birth(new SimpleDateFormat("yyyy-MM-DD hh:mm:ss").parse(fieldSet.readString("brith")))
                                .build();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
                return null;
            }
        });   //  读取文件
        defaultLineMapper.afterPropertiesSet();

        reader.setLineMapper(defaultLineMapper);
    }

    @Override
    public C read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        C c = null;
        this.curLine++;
        if (restart) {
            reader.setLinesToSkip(this.curLine.intValue() - 1);
            restart = false;
            System.out.println("Start reading from line: " + this.curLine);
        }
        reader.open(executionContext);
        c = reader.read();
        if (c != null) {
            if (c.getId().equals(1131L)) {  //  自定义一个BUG.. 节点
                throw new RuntimeException("Something wrong. C id " + c.getId() + "  " + this.curLine);
            }
        } else
            this.curLine--;
        return c;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        this.executionContext = executionContext;
        if (executionContext.containsKey("curLine")) {    //  重新执行时，获取到数据库上一步保存的位置状态
            this.curLine = executionContext.getLong("curLine");
            this.restart = true;
        } else {
            this.restart = false;
            this.curLine = 0L;
            executionContext.put("curLine", this.curLine.intValue());
        }
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        System.out.println("update curLine: " + this.curLine);  //  本次的开始点上，并不是本条数据的位置上
        executionContext.put("curLine", this.curLine);
    }

    @Override
    public void close() throws ItemStreamException {

    }
}
