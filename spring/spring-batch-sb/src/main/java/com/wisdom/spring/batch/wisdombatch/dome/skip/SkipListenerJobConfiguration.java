package com.wisdom.spring.batch.wisdombatch.dome.skip;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.batch.item.support.ListItemWriter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * skip 发生错误 时跳过
 * 运用 Listener 记录 错误的数据
 */
//@Configuration
public class SkipListenerJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job skipJob() {
        return jobBuilderFactory
                .get("skipJob")
                .start(step1())
                .next(step2())
                .build();
    }

    private Step step2() {
        return stepBuilderFactory
                .get("step2")
                .<String, String>chunk(1)
                .reader(new ListItemReader<>(Arrays.asList("1", "2", "3", "4", "5", "6")))
                .faultTolerant()//  retry
                .skip(Exception.class) //  指定异常类
                .skipLimit(10) //  指定 跳过次数
                .writer(new ListItemWriter<>())
                .faultTolerant()
                .retryLimit(1)
                .noRetry(RuntimeException.class)
                .retry(Exception.class)
                .build();
    }

    private Step step1() {
        return stepBuilderFactory
                .get("step1")
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("STEP! -> OK");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }
}
