package com.wisdom.spring.batch.wisdombatch.dome.processor;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;

/**
 * processor 过滤 处理 如 验证，删选，写入写出数据前的操作
 */
//@Configuration
public class ProcessorDemoJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;
    @Value("static/doc/output/c.csv")
    private Resource resource;

    @Bean
    public Job processorDemoJob() throws Exception {
        return jobBuilderFactory.get("processorDemoJob")
                .start(processorDemoStep())
                .build();
    }

    private Step processorDemoStep() throws Exception {
        return stepBuilderFactory.get("processorDemoStep")
                .<C, C>chunk(2)
                .reader(readerJdbcC())
                .processor(itemProcessor())
                .writer(writerC())
                .build();
    }

    //  过度处理数据
    private CompositeItemProcessor<C, C> itemProcessor() {
        final CompositeItemProcessor<C, C> itemProcessor = new CompositeItemProcessor<>();
        //  代理处理过滤 如果 方法null  过滤掉
        itemProcessor.setDelegates(Arrays.asList(
                item -> {
                    item.setName(item.getName().toUpperCase());
                    return item;
                },
                (ItemProcessor<C, C>) item -> item.getName().length() > 10 ? item : null
        ));
        return itemProcessor;
    }

    private FlatFileItemWriter<C> writerC() throws Exception {
        final FlatFileItemWriter<C> flatFileItemWriter = new FlatFileItemWriter<>();
        flatFileItemWriter.setEncoding("utf-8");
        flatFileItemWriter.setResource(resource);
        flatFileItemWriter.setLineAggregator(new DelimitedLineAggregator<>());    //  写的方式
        flatFileItemWriter.afterPropertiesSet();
        return flatFileItemWriter;
    }

    private JdbcPagingItemReader<C> readerJdbcC() throws Exception {
        final JdbcPagingItemReader<C> jdbcPagingItemReader = new JdbcPagingItemReader<>();
        jdbcPagingItemReader.setDataSource(dataSource);
        jdbcPagingItemReader.setFetchSize(5);   //  每次读取的个数
        jdbcPagingItemReader.setRowMapper((resultSet, i) ->
                new C(resultSet.getLong("id"),
                        resultSet.getNString("name"),
                        resultSet.getBoolean("sex"),
                        resultSet.getDate("birth")));    //  如何映射成 Mapper 对象

        /** 读取的SQL 语句 */
        final MySqlPagingQueryProvider mySqlPagingQueryProvider = new MySqlPagingQueryProvider();
        mySqlPagingQueryProvider.setSelectClause("id,name,sex,birth");  //  查询的字段
        mySqlPagingQueryProvider.setFromClause("FROM C");   //  查表
        final HashMap<String, Order> sortKeys = new HashMap<>();
        sortKeys.put("id", Order.DESCENDING);    //  降序排序
        mySqlPagingQueryProvider.setSortKeys(sortKeys); //  排序策略

        jdbcPagingItemReader.setQueryProvider(mySqlPagingQueryProvider);
        jdbcPagingItemReader.afterPropertiesSet();
        return jdbcPagingItemReader;
    }
}