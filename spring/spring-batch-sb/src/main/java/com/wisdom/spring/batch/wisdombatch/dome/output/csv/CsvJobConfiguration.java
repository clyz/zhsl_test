package com.wisdom.spring.batch.wisdombatch.dome.output.csv;

import com.wisdom.spring.batch.wisdombatch.dome.C;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.util.HashMap;

/**
 * 写入数据到 CSV 中
 */
//@Configuration
public class CsvJobConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private DataSource dataSource;

    @Value("static/doc/output/c.csv")
    private Resource resource;

    @Bean
    public Job csvOutputJob() throws Exception {
        return jobBuilderFactory.get("csvOutputJob")
                .start(csvOutputStep()).build();
    }

    private Step csvOutputStep() throws Exception {
        return stepBuilderFactory.get("csvOutputStep")
                .<C, C>chunk(5)
                .reader(readerJdbcC())
                .writer(csvWriterC())
                .build();
    }

    private FlatFileItemWriter<C> csvWriterC() throws Exception {
        final FlatFileItemWriter<C> flatFileItemWriter = new FlatFileItemWriter<>();
        flatFileItemWriter.setEncoding("utf-8");
        flatFileItemWriter.setResource(resource);
        flatFileItemWriter.setLineAggregator(new DelimitedLineAggregator<>());    //  写的方式
        flatFileItemWriter.afterPropertiesSet();
        return flatFileItemWriter;
    }

    private JdbcPagingItemReader<C> readerJdbcC() throws Exception {
        final JdbcPagingItemReader<C> jdbcPagingItemReader = new JdbcPagingItemReader<>();
        jdbcPagingItemReader.setDataSource(dataSource);
        jdbcPagingItemReader.setFetchSize(5);   //  每次读取的个数
        jdbcPagingItemReader.setRowMapper((resultSet, i) ->
                new C(resultSet.getLong("id"),
                        resultSet.getNString("name"),
                        resultSet.getBoolean("sex"),
                        resultSet.getDate("birth")));    //  如何映射成 Mapper 对象

        /** 读取的SQL 语句 */
        final MySqlPagingQueryProvider mySqlPagingQueryProvider = new MySqlPagingQueryProvider();
        mySqlPagingQueryProvider.setSelectClause("id,name,sex,birth");  //  查询的字段
        mySqlPagingQueryProvider.setFromClause("FROM C");   //  查表
        final HashMap<String, Order> sortKeys = new HashMap<>();
        sortKeys.put("id", Order.DESCENDING);    //  降序排序
        mySqlPagingQueryProvider.setSortKeys(sortKeys); //  排序策略

        jdbcPagingItemReader.setQueryProvider(mySqlPagingQueryProvider);
        jdbcPagingItemReader.afterPropertiesSet();
        return jdbcPagingItemReader;
    }
}
