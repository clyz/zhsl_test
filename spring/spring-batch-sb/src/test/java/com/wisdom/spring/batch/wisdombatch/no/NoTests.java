package com.wisdom.spring.batch.wisdombatch.no;

import com.wisdom.spring.batch.wisdombatch.WisdombatchApplicationTests;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class NoTests extends WisdombatchApplicationTests {

    @Test
    public void test(){
        System.out.println("GOGO");
        String url = "http://132.232.150.196/wisdom3A/platform/content/updateReadNumIncreaseOfNews";
        MultiValueMap<String, String> param = new LinkedMultiValueMap<>();
        param.add("id", "66023268366680064");

        HttpHeaders headers = new HttpHeaders();

        List<String> cookieList = new ArrayList<>();
        cookieList.add("sennId=6a8a799f-c29b-454a-8ae1-3533ab48d86f;Path=/wisdom3A;expires=2019-07-24T13:06:33.000Z");
        headers.put(HttpHeaders.COOKIE, cookieList);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(param, headers);
        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        System.out.println(entity);
    }

}
