package com.wisdom.spring.batch.wisdombatch;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
//@EnableBatchProcessing
public class WisdombatchApplicationTests {

    @Test
    public void contextLoads() {
    }

}
