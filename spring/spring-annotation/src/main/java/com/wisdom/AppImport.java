package com.wisdom;

import com.wisdom.bean.User;
import com.wisdom.spring.importBeanDefinitionRegistrar.BeanImportBeanDefinitionRegistrar;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author: ziv
 * @date: 2018/11/28 15:45
 * @version: 0.0.1
 */
@Configuration
@Import(value = {User.class, BeanImportBeanDefinitionRegistrar.class})
public class AppImport {

}
