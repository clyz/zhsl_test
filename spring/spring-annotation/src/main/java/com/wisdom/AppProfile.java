package com.wisdom;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author: ziv
 * @date: 2018/12/4 09:15
 * @version: 0.0.1
 */
@ComponentScan({"com.wisdom.config"})
public class AppProfile {

}
