package com.wisdom.config;

import com.wisdom.bean.Book;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @author: ziv
 * @date: 2018/11/27 13:09
 * @version: 0.0.1
 */
@Configuration
public class BookConfig {

    @Lazy   //  懒加载
    @Bean(destroyMethod = "destroy", initMethod = "init")
    public Book book() {
        return new Book(1, "语文");
    }
}
