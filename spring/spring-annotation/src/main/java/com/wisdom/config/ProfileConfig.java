package com.wisdom.config;

import com.wisdom.vo.ProfileVo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author: ziv
 * @date: 2018/12/4 09:13
 * @version: 0.0.1
 */
@Configuration
public class ProfileConfig {

    @Profile("default")
    @Bean
    public ProfileVo defaultProfileVo() {
        return new ProfileVo();
    }

    @Profile("test")
    @Bean("testProfileVo")
    public ProfileVo testProfileVo() {
        return new ProfileVo("test");
    }

    @Profile("dev")
    @Bean("devProfileVo")
    public ProfileVo devProfileVo() {
        return new ProfileVo("dev");
    }

    @Profile("pro")
    @Bean("proProfileVo")
    public ProfileVo proProfileVo() {
        return new ProfileVo("pro");
    }
}
