package com.wisdom.config;

import com.wisdom.bean.Admin;
import com.wisdom.spring.condition.AdminCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ziv
 * @date: 2018/11/28 08:56
 * @version: 0.0.1
 */
@Configuration
public class AdminConfig {

    @Conditional(AdminCondition.class)  //  设置创建bean的条件
    @Bean("adminVo")
    public Admin admin() {
        return new Admin("admin");
    }
}
