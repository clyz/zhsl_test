package com.wisdom.config;

import com.wisdom.spring.factorybean.OrderFactorybean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ziv
 * @date: 2018/11/28 15:56
 * @version: 0.0.1
 */
@Configuration
public class OrderFactoryBeanConfig {

    @Bean
    public OrderFactorybean orderFactorybean() {
        return new OrderFactorybean();
    }
}
