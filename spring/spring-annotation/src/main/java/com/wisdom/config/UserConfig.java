package com.wisdom.config;

import com.wisdom.bean.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author: ziv
 * @date: 2018/11/27 16:51
 * @version: 0.0.1
 */
@Configuration
public class UserConfig {

    @Scope("prototype") //  多例与单例设置
    @Bean
    public User user() {
        return new User("张山");
    }
}
