package com.wisdom;

import com.wisdom.spring.beanpostprocessor.UserBeanPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;

/**
 * @author: ziv
 * @date: 2018/11/29 17:11
 * @version: 0.0.1
 */
@ComponentScan({"com.wisdom.bean"})
@Configuration
public class AppProcessor {
    @Bean
    @Async
    public BeanPostProcessor userBeanPostProcessor() {
        return new UserBeanPostProcessor();
    }
}
