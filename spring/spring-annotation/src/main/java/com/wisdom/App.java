package com.wisdom;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ziv
 * @date: 2018/11/27 16:52
 * @version: 0.0.1
 */
@Configuration
/*@ComponentScans(
        @ComponentScan(
                useDefaultFilters = false,
                value = {"com.wisdom.controller","com.wisdom.config"},
                lazyInit = true,
                excludeFilters = {
                        @ComponentScan.Filter(
                                type = FilterType.ASSIGNABLE_TYPE,
                                classes = UserConfig.class),
                        @ComponentScan.Filter(
                                type = FilterType.ANNOTATION,
                                classes = Controller.class
                        ),
                        @ComponentScan.Filter(
                                type = FilterType.CUSTOM,
                                classes = CustomTypeFilter.class
                        )
                })
)*/
@ComponentScan
public class App {

}
