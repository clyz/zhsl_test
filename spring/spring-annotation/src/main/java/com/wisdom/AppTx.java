package com.wisdom;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author: ziv
 * @date: 2018/11/27 16:52
 * @version: 0.0.1
 */
@Configuration
@ComponentScan
@EnableTransactionManagement
public class AppTx {

}
