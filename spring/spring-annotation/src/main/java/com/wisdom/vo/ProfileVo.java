package com.wisdom.vo;

/**
 * @author: ziv
 * @date: 2018/12/4 09:12
 * @version: 0.0.1
 */
public class ProfileVo {
    private String name;

    public ProfileVo() {
    }

    public ProfileVo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProfileVo{" +
                "name='" + name + '\'' +
                '}';
    }
}
