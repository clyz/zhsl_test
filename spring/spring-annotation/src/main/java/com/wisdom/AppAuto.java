package com.wisdom;

import com.wisdom.bean.User;
import com.wisdom.service.IAdminService;
import com.wisdom.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;

/**
 * @author: ziv
 * @date: 2018/12/3 09:08
 * @version: 0.0.1
 */
@ComponentScan("com.wisdom")
public class AppAuto {
    @Qualifier("adminservice1")
    @Autowired
    IAdminService adminService;
    @Autowired
    IUserService userService;
}
