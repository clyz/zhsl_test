package com.wisdom.bean;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @PostConstruct ->  初始化bean 之前执行
 * @PreDestroy ->    摧毁bean 时执行
 * @author: ziv
 * @date: 2018/11/29 17:06
 * @version: 0.0.1
 */
@Component
public class LiveAnnotation {
    private Integer id;

    @PostConstruct
    public void afterPropertiesSet() {
        System.err.println("@PostConstruct   ->  "
                + this.getClass().getName());
    }

    @PreDestroy
    public void destroy() {
        System.err.println("@PreDestroy ->  "
                + this.getClass().getName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Live{" +
                "id=" + id +
                '}';
    }
}
