package com.wisdom.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * InitializingBean ->  初始化bean 之前执行
 * DisposableBean ->    摧毁bean 时执行
 *
 * @author: ziv
 * @date: 2018/11/29 17:06
 * @version: 0.0.1
 */
@Component
public class LiveInitializingDisposableBean implements InitializingBean, DisposableBean {
    private Integer id;

    public void afterPropertiesSet() {
        System.err.println("InitializingBean.afterPropertiesSet()   ->  "
                + this.getClass().getName());
    }

    public void destroy() {
        System.err.println("DisposableBean.destroy()"
                + this.getClass().getName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Live{" +
                "id=" + id +
                '}';
    }
}
