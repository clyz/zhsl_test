package com.wisdom.bean;

/**
 * @author: ziv
 * @date: 2018/11/27 13:08
 * @version: 0.0.1
 */
public class Book {
    private Integer id;
    private String name;

    public Book(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void init() {
        System.err.println(getClass().getName() + "::init");
    }

    public void destroy() {
        System.err.println(getClass().getName() + "::destroy");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
