package com.wisdom.bean;

/**
 * @author: ziv
 * @date: 2018/11/28 15:42
 * @version: 0.0.1
 */
public class Order {
    private Integer id;

    public Order() {
    }

    public Order(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                '}';
    }
}
