package com.wisdom.bean;

/**
 * @author: ziv
 * @date: 2018/11/28 08:55
 * @version: 0.0.1
 */
public class Admin {
    private String name;

    public Admin() {
    }

    public Admin(String admin) {
        this.name = admin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "name='" + name + '\'' +
                '}';
    }
}
