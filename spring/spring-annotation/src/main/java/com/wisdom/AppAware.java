package com.wisdom;

import com.wisdom.spring.aware.CarAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: ziv
 * @date: 2018/12/3 09:08
 * @version: 0.0.1
 */
@ComponentScan("com.wisdom.spring.aware")
public class AppAware {
    @Bean
    public CarAware carAware() {
        return new CarAware("car");
    }
}
