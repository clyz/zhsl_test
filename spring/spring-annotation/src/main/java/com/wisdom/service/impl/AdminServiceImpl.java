package com.wisdom.service.impl;

import com.wisdom.service.IAdminService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * @author: ziv
 * @date: 2018/12/3 09:13
 * @version: 0.0.1
 */
@Primary
@Service(value = "adminservice1")
public class AdminServiceImpl implements IAdminService {
}
