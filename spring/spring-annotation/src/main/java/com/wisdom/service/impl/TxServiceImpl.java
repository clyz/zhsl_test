package com.wisdom.service.impl;

import com.wisdom.service.ITxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: ziv
 * @date: 2018/12/11 09:25
 * @version: 0.0.1
 */
@Service
public class TxServiceImpl implements ITxService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Transactional(rollbackFor = Exception.class)
    public void saveRole(String name) {
        int row = jdbcTemplate.update("INSERT INTO role(name) VALUES (?)", name);
        if (row > 0) {
            System.err.println("OK....");
        } else {
            System.err.println("NO.....");
        }
        int i = 1 / 0;
    }
}
