package com.wisdom.service;

/**
 * @author: ziv
 * @date: 2018/12/11 09:23
 * @version: 0.0.1
 */
public interface ITxService {
    void saveRole(String name);
}
