package com.wisdom.spring.beanDefinitionRegistryPostProcessor;

import com.wisdom.bean.User;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * @author: ziv
 * @date: 2018/12/3 16:53
 * @version: 0.0.1
 */
public class DruidBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        BeanDefinitionBuilder builder1 = BeanDefinitionBuilder.genericBeanDefinition(User.class);
        registry.registerBeanDefinition("xyz", builder1.getBeanDefinition());
        for (int i = 0; i < 5; i++) {//这里动态注入5个Bean
            //获取基于MyBean2 的定义
            BeanDefinitionBuilder builder2 = BeanDefinitionBuilder.genericBeanDefinition(User.class);
            builder2.addPropertyValue("name", "zhangsan" + i);
            //属性依赖MyBean,确保MyBean一再容器中
            //builder2.addPropertyReference("abc", "createMyBean");//@Bean(name="createMyBean")
            builder2.addPropertyReference("abc", "xyz");//动态手动注入MyBean实例
            registry.registerBeanDefinition(i + "", builder2.getBeanDefinition());
        }
    }

    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
