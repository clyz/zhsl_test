package com.wisdom.spring.applicationListener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 容器发布 ApplicationEvent 事件时 触发
 */
@Component
public class AppApplicationListener implements ApplicationListener<ApplicationEvent> {
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("xxxxxxxx" + event);
    }
}
