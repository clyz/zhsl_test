package com.wisdom.spring.importSelector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author: ziv
 * @date: 2018/11/28 10:27
 * @version: 0.0.1
 */
public class BeanImportSelector implements ImportSelector {
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.wisdom.controller.BookCtrl"};
    }
}
