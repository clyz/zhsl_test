package com.wisdom.spring.condition;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author: ziv
 * @date: 2018/11/28 09:17
 * @version: 0.0.1
 */
public class AdminCondition implements Condition {

    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Environment environment = context.getEnvironment();
        String property = environment.getProperty("os.name");
        System.err.println(property);
        String[] defaultProfiles = environment.getActiveProfiles();
        for (String profile :
                defaultProfiles) {
            System.err.print(profile + ",");
        }
        //  bean注册工厂
        BeanDefinitionRegistry registry = context.getRegistry();
        String[] beanDefinitionNames = registry.getBeanDefinitionNames();
        for (String name :
                beanDefinitionNames) {
            System.err.println(name);
        }
        return true;
    }
}