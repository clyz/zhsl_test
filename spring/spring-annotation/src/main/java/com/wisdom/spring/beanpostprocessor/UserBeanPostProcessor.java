package com.wisdom.spring.beanpostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 都要执行这个接口的方法
 * BeanPostProcessor    ->  实现bean创建前后置通知
 *
 * @author: ziv
 * @date: 2018/11/29 17:19
 * @version: 0.0.1
 */
public class UserBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.err.println("BeanPostProcessor   ->  " +
                bean.getClass().getName() + " ->  " + beanName);
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.err.println("BeanPostProcessor   ->  " +
                bean.getClass().getName() + " ->  " + beanName);
        return bean;
    }
}
