package com.wisdom.spring.aware;

import lombok.extern.java.Log;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.util.StringValueResolver;

/**
 * @author: ziv
 * @date: 2018/12/3 15:35
 * @version: 0.0.1
 */
@Log
public class CarAware implements ApplicationContextAware, BeanNameAware, EmbeddedValueResolverAware {

    private ApplicationContext applicationContext;
    private String name;

    public CarAware(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * 解析String value 占位符
     *
     * @param resolver
     */
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        String s = resolver.resolveStringValue("OS.Name::${os.name}");
        System.err.println(s);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void setBeanName(String name) {
        log.info(name);
    }

    @Override
    public String toString() {
        return "CarAware{" +
                "name='" + name + '\'' +
                '}';
    }
}
