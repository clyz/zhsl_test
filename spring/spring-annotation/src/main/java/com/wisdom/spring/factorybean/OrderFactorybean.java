package com.wisdom.spring.factorybean;

import com.wisdom.bean.Order;
import org.springframework.beans.factory.FactoryBean;

/**
 * 创建工厂类
 *
 * @author: ziv
 * @date: 2018/11/28 15:41
 * @version: 0.0.1
 */
public class OrderFactorybean implements FactoryBean<Order> {
    public Order getObject() {
        System.err.println("ORDER_FACTORY_BEAN::" + this.getClass().getName());
        return new Order();
    }

    public Class<Order> getObjectType() {
        return Order.class;
    }

    public boolean isSingleton() {
        return true;
    }
}
