package com.wisdom.spring.importBeanDefinitionRegistrar;

import com.wisdom.bean.User;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author: ziv
 * @date: 2018/11/28 10:30
 * @version: 0.0.1
 */
public class BeanImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        registry.registerBeanDefinition("user", new RootBeanDefinition(User.class));
    }
}
