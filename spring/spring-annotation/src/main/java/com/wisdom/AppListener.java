package com.wisdom;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ziv
 * @date: 2018/12/11 10:33
 * @version: 0.0.1
 */
@ComponentScan(value = {"com.wisdom.spring.applicationListener"})
@Configuration
public class AppListener {
}