package com.wisdom;

import com.wisdom.vo.ProfileVo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppProfileTest {
    AnnotationConfigApplicationContext configApplicationContext = null;

    @After
    public void beanDefinitionNames() {
        String[] beanNamesForType = configApplicationContext.getBeanNamesForType(ProfileVo.class);
        for (String name :
                beanNamesForType) {
            System.out.println(name);
        }
    }

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext();
        configApplicationContext.getEnvironment().setActiveProfiles("dev", "test");
        configApplicationContext.register(AppProfile.class);
        configApplicationContext.refresh();
    }

    @Test
    public void profile() {
    }
}