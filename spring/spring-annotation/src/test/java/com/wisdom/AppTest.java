package com.wisdom;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppTest {

    ApplicationContext configApplicationContext = null;

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext(App.class);
    }

    @Test
    public void beanDefinitionNames() {
        String[] beanDefinitionNames = configApplicationContext.getBeanDefinitionNames();
        for (String name :
                beanDefinitionNames) {
            System.out.println(name);
        }
    }

    @Test
    public void Lazy() {
        Object book = configApplicationContext.getBean("book");
        System.err.println(book);
    }

    @Test
    public void single() {
        Object userOne = configApplicationContext.getBean("user");
        Object userTwo = configApplicationContext.getBean("user");
        System.err.println(userOne);
        System.err.println(userTwo);
        System.err.println(userOne == userTwo);
    }

    @Test
    public void factorybean() {
        Object orderFactorybean1 = configApplicationContext.getBean("orderFactorybean");
        System.err.println(orderFactorybean1);
        Object orderFactorybean2 = configApplicationContext.getBean("orderFactorybean");
        System.err.println(orderFactorybean1.getClass().getName());
        System.err.println(orderFactorybean1 == orderFactorybean2);
        Object orderFactorybean3 = configApplicationContext.getBean("&orderFactorybean");
        System.err.println(orderFactorybean3);
    }
}