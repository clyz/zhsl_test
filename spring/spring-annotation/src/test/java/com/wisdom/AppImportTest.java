package com.wisdom;

import com.wisdom.bean.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppImportTest {

    ApplicationContext configApplicationContext = null;

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext(AppImport.class);
    }

    @Test
    public void beanDefinitionNames() {
        String[] beanDefinitionNames = configApplicationContext.getBeanDefinitionNames();
        for (String name :
                beanDefinitionNames) {
            System.out.println(name);
        }
    }

    @Test
    public void bean() {
        Object admin = configApplicationContext.getBean(User.class);
        System.err.println(admin);
    }
}