package com.wisdom;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppAutoTest {
    ApplicationContext configApplicationContext = null;

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext(AppAuto.class);
    }

    @Test
    public void beanDefinitionNames() {
        String[] beanDefinitionNames = configApplicationContext.getBeanDefinitionNames();
        for (String name :
                beanDefinitionNames) {
            System.out.println(name);
        }
    }
}