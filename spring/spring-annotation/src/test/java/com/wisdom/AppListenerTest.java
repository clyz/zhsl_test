package com.wisdom;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppListenerTest {
    ApplicationContext configApplicationContext = null;

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext(AppListener.class);
        //  发布一个事件监听器
        configApplicationContext.publishEvent(new ApplicationEvent[0]);
    }

    @Test
    public void beanDefinitionNames() {
        String[] beanDefinitionNames = configApplicationContext.getBeanDefinitionNames();
        for (String name :
                beanDefinitionNames) {
            System.out.println(name);
        }
    }
}