package com.wisdom.config;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BookConfigTest {
    ApplicationContext configApplicationContext = null;

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext(BookConfig.class);
    }

    @Test
    public void book() {
        Object book = configApplicationContext.getBean("book");
        System.err.println(book);
    }
}