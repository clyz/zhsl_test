package com.wisdom;

import com.wisdom.service.ITxService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppTxTest {
    ApplicationContext configApplicationContext = null;

    @Before
    public void before() {
        configApplicationContext = new AnnotationConfigApplicationContext(AppTx.class);
    }

    @Test
    public void tx() {
        ITxService txService = configApplicationContext.getBean(ITxService.class);
        txService.saveRole("ziv");
    }

    @Test
    public void beanDefinitionNames() {
        String[] beanDefinitionNames = configApplicationContext.getBeanDefinitionNames();
        for (String name :
                beanDefinitionNames) {
            System.out.println(name);
        }
    }
}