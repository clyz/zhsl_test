package clyz.ziv.springsessionsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSessionSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSessionSbApplication.class, args);
    }

}
