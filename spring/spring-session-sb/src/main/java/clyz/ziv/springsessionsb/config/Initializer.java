package clyz.ziv.springsessionsb.config;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * @author YangZemin
 * @date 2020/3/19 11:41
 */
public class Initializer extends AbstractHttpSessionApplicationInitializer {
    public Initializer() {
        super(Config.class);
    }
}
