package clyz.ziv.springshellsb.commands;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

/**
 * 是时候添加我们的第一个命令了。
 * 创建一个新类（随便命名）并使用@ShellComponent（@Component的一种变体，用于限制扫描候选命令的类集）对其进行注释。
 * 然后，创建一个采用两个整数（a和b）并返回其总和的add方法。使用@ShellMethod对其进行注释，
 * 并在注释中提供命令的描述（唯一需要的信息）：
 * @author YangZemin
 * @date 2020/3/19 9:25
 */
@ShellComponent
public class AddCommands {
    @ShellMethod("Add two integers together.")
    public int add(int a, int b) {
        System.out.println("Add two integers together.");
        return a + b;
    }
}
