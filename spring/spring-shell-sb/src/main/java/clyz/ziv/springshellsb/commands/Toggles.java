package clyz.ziv.springshellsb.commands;

import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

import java.util.Calendar;

import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.SUNDAY;

/**
 * @author YangZemin
 * @date 2020/3/19 10:08
 */
@ShellComponent
public class Toggles {
    @ShellMethodAvailability
    public Availability availabilityOnWeekdays() {
        return Calendar.getInstance().get(DAY_OF_WEEK) == SUNDAY
                ? Availability.available()
                : Availability.unavailable("today is not Sunday");
    }

    @ShellMethod("foo")
    public void foo() {}

    @ShellMethod("bar")
    public void bar() {}
}
