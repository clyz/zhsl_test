package clyz.ziv.springshellsb.commands;

import org.springframework.shell.Availability;
import org.springframework.shell.standard.*;

import javax.validation.constraints.Size;

/**
 * @author YangZemin
 * @date 2020/3/19 9:31
 */
@ShellComponent
@ShellCommandGroup("more")
public class MoreCommands {

    @ShellMethod(value = "Add numbers.", key = "sum")
    public int add(int a, int b) {
        return a + b;
    }

    @ShellMethod("Display stuff.")
    public String echo(int a, int b, int c) {
        return String.format("You said a=%d, b=%d, c=%d", a, b, c);
    }

    @ShellMethod(value = "Display stuff.", prefix = "-")
    public String echos(int a, int b, @ShellOption("--third") int c) {
        return String.format("You said a=%d, b=%d, c=%d", a, b, c);
    }

    /*@ShellMethod("Describe a command.")
    public String help(@ShellOption({"-C", "--command"}) String command) {
        return command;
    }*/

    @ShellMethod("Say hello.")
    public String greet(@ShellOption(defaultValue = "World") String who) {
        return "Hello " + who;
    }

    @ShellMethod("Add Numbers.")
    public float addF3(@ShellOption(arity = 3) float[] numbers) {
        return numbers[0] + numbers[1] + numbers[2];
    }

    @ShellMethod("Terminate the system.")
    public String shutdown(boolean force) {
        return "You said " + force;
    }

    @ShellMethod("Terminate the system.")
    public String shutdownF1(@ShellOption(arity = 1, defaultValue = "false") boolean force) {
        return "You said " + force;
    }

    @ShellMethod("Change password.")
    public String changePassword(@Size(min = 8, max = 40) String password) {
        return "Password successfully set to " + password;
    }

    /**
     * ==========================================================
     * ==========================================================
     * ===========    ShellMethodAvailability      ==============
     * ===================== START ==============================
     * ==========================================================
     */

    /**
     * isP Availability.available() 是 可用
     */
    @ShellMethodAvailability("isP")
    @ShellMethod("Download the nuclear codes.")
    public String p() {
        return "可以使用";
    }

    public Availability isP() {
        return isP ? Availability.available() : Availability.unavailable("you are not connected");
    }
    @ShellMethod("set P")
    public boolean sP(@ShellOption(arity = 1, defaultValue = "false") boolean p) {
        isP = p;
        return isP;
    }

    private boolean isP;

    /**
     * ==========================================================
     * ==========================================================
     * ===========    ShellMethodAvailability      ==============
     * =====================  END  ==============================
     * ==========================================================
     */

}