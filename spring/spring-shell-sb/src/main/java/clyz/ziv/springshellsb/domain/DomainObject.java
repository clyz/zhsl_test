package clyz.ziv.springshellsb.domain;

/**
 * @author YangZemin
 * @date 2020/3/19 10:45
 */
public class DomainObject {

    private final String value;

    public DomainObject(String value) {
        this.value = value;
    }

    public String toString() {
        return value;
    }
}
