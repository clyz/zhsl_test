package clyz.ziv.springshellsb.prompt;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.shell.jline.FileInputProvider;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.shell.jline.ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED;

/**
 * @author YangZemin
 * @date 2020/3/19 10:41
 */
@Order(InteractiveShellApplicationRunner.PRECEDENCE - 100) // Runs before InteractiveShellApplicationRunner
public class ScriptShellApplicationRunner implements ApplicationRunner {

    /*@Override
    public void run(ApplicationArguments args) throws Exception {
        List<File> scriptsToRun = args.getNonOptionArgs().stream()
                .filter(s -> s.startsWith("@"))
                .map(s -> new File(s.substring(1)))
                .collect(Collectors.toList());

        boolean batchEnabled = environment.getProperty(SPRING_SHELL_SCRIPT_ENABLED, boolean.class, true);

        if (!scriptsToRun.isEmpty() && batchEnabled) {
            InteractiveShellApplicationRunner.disable(environment);
            for (File file : scriptsToRun) {
                try (Reader reader = new FileReader(file);
                     FileInputProvider inputProvider = new FileInputProvider(reader, parser)) {
                    shell.run(inputProvider);
                }
            }
        }
    }*/

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}
