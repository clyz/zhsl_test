package clyz.ziv.springshellsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

@SpringBootApplication
public class SpringShellSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringShellSbApplication.class, args);
    }

    /*public static void main(String[] args) throws Exception {
        //  关闭help
        String[] disabledCommands = {"--spring.shell.command.help.enabled=false"};
        String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);
        SpringApplication.run(SpringShellSbApplication.class, fullArgs);
    }*/

}
