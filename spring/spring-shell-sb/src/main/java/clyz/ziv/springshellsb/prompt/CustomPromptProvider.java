package clyz.ziv.springshellsb.prompt;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.context.event.EventListener;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

/**
 * 提示 提供者
 *
 *
 * @author YangZemin
 * @date 2020/3/19 10:39
 */
//@Component
public class CustomPromptProvider implements PromptProvider {

    /*private ConnectionDetails connection;

    @Override
    public AttributedString getPrompt() {
        if (connection != null) {
            return new AttributedString(connection.getHost() + ":>",
                    AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
        }
        else {
            return new AttributedString("server-unknown:>",
                    AttributedStyle.DEFAULT.foreground(AttributedStyle.RED));
        }
    }

    @EventListener
    public void handle(ConnectionUpdatedEvent event) {
        this.connection = event.getConnectionDetails();
    }*/

    @Override
    public AttributedString getPrompt() {
        return null;
    }
}