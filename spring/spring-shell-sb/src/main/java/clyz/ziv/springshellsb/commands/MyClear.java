package clyz.ziv.springshellsb.commands;

import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.commands.Clear;

/**
 * @author YangZemin
 * @date 2020/3/19 10:16
 */
public class MyClear implements Clear.Command {

    @ShellMethod("Clear the screen, only better.")
    public void clear() {
        // ...
    }
}
