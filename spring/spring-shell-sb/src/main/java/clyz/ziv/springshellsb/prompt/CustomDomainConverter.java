package clyz.ziv.springshellsb.prompt;

import clyz.ziv.springshellsb.domain.DomainObject;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author YangZemin
 * @date 2020/3/19 10:46
 */
//@Component
class CustomDomainConverter implements Converter<String, DomainObject> {

    @Override
    public DomainObject convert(String source) {
        return new DomainObject(source);
    }
}
