package ziv.clyz.email.mail;

import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;

public class MailSenderImplTest {

    @Test
    public void sendText() throws MessagingException {
        final MailTransport mailTransport = new MailTransport();
        mailTransport.setUser("2374619909@qq.com", "ygkooyaezolheacf");
        final MailSenderImpl mailSender = new MailSenderImpl(mailTransport);

        mailSender.sendText("我又来了", "欢迎您", new String[]{"zemin.ziv@qq.com"}, null, null);
    }

    @Test
    public void send() throws MessagingException, UnsupportedEncodingException {
        final MailTransport mailTransport = new MailTransport();
        mailTransport.setUser("2374619909@qq.com", "ygkooyaezolheacf");
        final MailSenderImpl mailSender = new MailSenderImpl(mailTransport);
        final MimeMessage message = mailSender.getMessage();

        mailSender.setRescipients(message, new String[]{"zemin.ziv@qq.com"}, null, null);

        MimeMultipart mimeMultipart = mailSender.mimeMultipart(0, "mixed",
                mailSender.mimeBodyPartFile("C:\\Users\\Administrator\\Pictures\\\uD83C\uDF41.jpg"),
                mailSender.mimeBodyPartFile("C:\\Users\\Administrator\\Pictures\\\uD83C\uDF41.jpg")
        );

        mimeMultipart.addBodyPart(
                mailSender.mimeBodyPart(
                        mailSender.mimeMultipart(0, "related",
                                mailSender.mimeBodyPart("xxx这是女的xxxx<br/><img src='cid:aaa.jpg'>"),
                                mailSender.mimeBodyPartFile("C:\\Users\\Administrator\\Pictures\\\uD83C\uDF41.jpg", "aaa.jpg")
                        )
                )
        );
        message.setContent(mimeMultipart);
        message.saveChanges();

        mailSender.send(message);
    }

    @Test
    public void retrievalHost() {
        System.out.println(retrievalHost("2374619909@qq.com"));
    }

    private String retrievalHost(String user) {
        if (user != null) {
            return "smtp." + user.substring(user.indexOf("@") + 1);
        }
        return null;
    }
}