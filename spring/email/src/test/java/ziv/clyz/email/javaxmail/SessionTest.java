package ziv.clyz.email.javaxmail;

import org.junit.Before;
import org.junit.Test;
import sun.text.normalizer.ICUBinary;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SessionTest {

    private Properties properties;

    @Before
    public void before() throws IOException {
        properties = new Properties();
        properties.load(
                new FileInputStream(new File("C:\\service_java\\ZHSL\\zhsl_test\\spring\\email/src/main/resources/mail.properties"))
        );
    }

    @Test
    public void load() {
        Session mailSession = Session.getInstance(properties);
        MimeMessage message=new MimeMessage(mailSession);

    }

    class MyAuthenticator extends Authenticator {
        protected PasswordAuthentication getPasswordAuthentication(){
            return new PasswordAuthentication(getDefaultUserName(), null);
        }
    }
}
