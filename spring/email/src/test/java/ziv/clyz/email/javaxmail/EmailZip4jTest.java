package ziv.clyz.email.javaxmail;

import org.junit.Test;
import ziv.clyz.email.util.CompressUtil;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.FileOutputStream;
import java.security.Security;
import java.util.Date;
import java.util.Properties;

public class EmailZip4jTest {

    @Test
    public void send() {
        sendEmil("zemin.ziv@qq.com", "加密了的");
    }

    /**
     * 使用加密的方式,利用465端口进行传输邮件,开启ssl
     *
     * @param to      为收件人邮箱
     * @param message 发送的消息
     */
    public static void sendEmil(String to, String message) {
        try {
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
            //设置邮件会话参数
            Properties props = new Properties();
            //邮箱的发送服务器地址
            props.setProperty("mail.smtp.host", "smtp.sina.com");
            props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            //邮箱发送服务器端口,这里设置为465端口
            props.setProperty("mail.smtp.port", "465");
            props.setProperty("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.auth", "true");

            final String username = "yzmziv@sina.com";
            final String password = "zemin886520";

            //获取到邮箱会话,利用匿名内部类的方式,将发送者邮箱用户名和密码授权给jvm
            Session session = Session.getDefaultInstance(props, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
            //通过会话,得到一个邮件,用于发送
            Message msg = new MimeMessage(session);
            //设置发件人
            msg.setFrom(new InternetAddress(username));
            //设置收件人,to为收件人,cc为抄送,bcc为密送
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(to, false));
            msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(to, false));
            msg.setSubject("邮件主题");
            //设置发送的日期
            msg.setSentDate(new Date());
            msg.setContent("这是一封邮件正文带图片cid:xoxo.zip的邮件", "text/html;charset=UTF-8");


            msg.writeTo(new FileOutputStream("D:a.eml"));
            final String zip = CompressUtil.zip("D:a.eml", "1");

            MimeBodyPart attach = new MimeBodyPart();
            DataHandler dh = new DataHandler(new FileDataSource(zip));
            attach.setDataHandler(dh);
            attach.setFileName(dh.getName());
            attach.setContentID("xoxo.zip");

            MimeMultipart mp2 = new MimeMultipart();
            mp2.addBodyPart(attach);

            msg.setContent(mp2);
            mp2.setSubType("related");
            Transport.send(msg);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
