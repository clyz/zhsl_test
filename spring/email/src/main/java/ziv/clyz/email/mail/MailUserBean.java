package ziv.clyz.email.mail;

public class MailUserBean {
    private String user;

    private String host;

    private String password;

    public MailUserBean() {
    }

    public MailUserBean(String host, String user, String password) {
        this.user = user;
        this.host = host;
        this.password = password;
    }

    /**
     * //        ts.connect("smtp.qq.com", "2374619909@qq.com", "ygkooyaezolheacf");
     * //        ts.connect("smtp.163.com", "18581591460@163.com", "zemin886520");
     * //        ts.connect("smtp.sina.com", "yzmziv@sina.com", "zemin886520");
     * //        ts.connect("smtp.exmail.qq.com","yangzemin@wisdomdata.net","Zemin886520");
     * 自动检索 host 不支持 企业邮箱！
     *
     * @param user     用户名
     * @param password 密码
     */
    public MailUserBean(String user, String password) {
        this.user = user;
        this.password = password;
        this.host = retrievalHost();
    }

    private String retrievalHost() {
        if (user != null) {
            return "smtp." + user.substring(user.indexOf("@") + 1);
        }
        return null;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
