package ziv.clyz.email.mail;

import org.apache.log4j.Logger;
import org.springframework.util.ObjectUtils;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Objects;

public class MailSenderImpl implements IMailSender {

    private static Logger log = Logger.getLogger("system");

    private MailTransport mailTransport;

    public MailSenderImpl(MailTransport mailTransport) {
        if (mailTransport.getSession() == null) {
            log.error("Mail session create fail!");
        }
        this.mailTransport = mailTransport;
    }

    /**
     * <p>
     * 发送 简单 内容 的邮箱
     * </p>
     *
     * @param subject
     * @param content
     * @param toAddresses
     * @param ccAddresses
     * @param bccAddresses
     * @throws MessagingException
     */
    public void sendText(String subject, String content,
                         String[] toAddresses, String[] ccAddresses, String[] bccAddresses)
            throws MessagingException {
        MimeMessage message = getMessage();
        setRescipients(message, toAddresses, ccAddresses, bccAddresses);
        message.setSubject(subject);
        message.setContent(content, "text/html;charset=UTF-8");
        send(message);
    }

    public MimeBodyPart mimeBodyPartFile(String filePath, String cid) throws MessagingException,
            UnsupportedEncodingException {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        DataHandler dataHandler = new DataHandler(new FileDataSource(filePath));
        mimeBodyPart.setDataHandler(dataHandler);
        mimeBodyPart.setContentID(cid);
        return mimeBodyPart;
    }

    public MimeBodyPart mimeBodyPartFile(String filePath) throws MessagingException, UnsupportedEncodingException {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        DataHandler dataHandler = new DataHandler(new FileDataSource(filePath));
        mimeBodyPart.setDataHandler(dataHandler);
        mimeBodyPart.setFileName(MimeUtility.encodeText(dataHandler.getName()));
        return mimeBodyPart;
    }

    public MimeBodyPart mimeBodyPart(Object o) throws MessagingException, UnsupportedEncodingException {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(o, "text/html;charset=UTF-8");
        return mimeBodyPart;
    }

    /**
     * @param startIndex
     * @param subType       "related"/"mixed"(外层)
     * @param mimeBodyParts
     * @return
     * @throws MessagingException
     */
    public MimeMultipart mimeMultipart(int startIndex, String subType, MimeBodyPart... mimeBodyParts) throws MessagingException {
        MimeMultipart mimeMultipart = new MimeMultipart();
        for (int i = 0; i < mimeBodyParts.length; i++) {
            mimeMultipart.addBodyPart(mimeBodyParts[i], i + startIndex);
        }
        mimeMultipart.setSubType(subType);
        return mimeMultipart;
    }

    public MimeMessage getMessage() {
        return new MimeMessage(mailTransport.getSession());
    }

    public void send(MimeMessage message) throws MessagingException {
        mailTransport.send(message);
    }

    /**
     * <p>
     * 指向 发送人
     * </p>
     *
     * @param message      消息对象
     * @param toAddresses  to
     * @param ccAddresses  cc
     * @param bccAddresses bcc
     * @throws MessagingException
     */
    public void setRescipients(MimeMessage message, String[] toAddresses, String[] ccAddresses, String[] bccAddresses) throws MessagingException {
        if (!ObjectUtils.isEmpty(toAddresses)) {
            message.setRecipients(Message.RecipientType.TO, getInternetAddress(toAddresses));
        }
        if (!ObjectUtils.isEmpty(ccAddresses)) {
            message.setRecipients(Message.RecipientType.CC, getInternetAddress(ccAddresses));
        }
        if (!ObjectUtils.isEmpty(bccAddresses)) {
            message.setRecipients(Message.RecipientType.BCC, getInternetAddress(bccAddresses));
        }
    }

    private InternetAddress[] getInternetAddress(String[] addresses) {
        return Arrays.stream(addresses).distinct()
                .map(username -> {
                    try {
                        return new InternetAddress(username);
                    } catch (AddressException e) {
                        // TODO... 对 谁 发送邮箱失败！
                        e.printStackTrace();
                    }
                    return null;
                }).filter(Objects::nonNull)
                .toArray(InternetAddress[]::new);
    }

}
