package ziv.clyz.email.mail;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * <p>
 * Transport 会制动 去 配置 session  host 与 port
 * DEBUG SMTP: useEhlo true, useAuth true
 * DEBUG SMTP: trying to connect to host "smtp.qq.com", port 25, isSSL false
 * </p>
 *
 * @author yangzemin
 * @date 2019年6月18日 16:50:12
 */
public class MailTransport {

    private Session session;

    private Transport transport;

    private MailUserBean user;

    private static Logger log = Logger.getLogger("system");

    public MailTransport() {
        final MailSessionBean mailSessionBean = new MailSessionBean();
        session = getSession(mailSessionBean);
        session.setDebug(mailSessionBean.isDebug());
    }

    public MailTransport(MailSessionBean mailSessionBean) {
        session = getSession(mailSessionBean);
        session.setDebug(mailSessionBean.isDebug());
    }

    public MailTransport(Properties properties) {
        session = getSession(properties);
    }

    public MailTransport(String propertiesPath) {
        session = getSession(propertiesPath);
    }

    /**
     * <p>
     * 是否开启 日志
     * </p>
     *
     * @param debug true / false
     */
    public void setDebug(boolean debug) {
        session.setDebug(debug);
    }

    /**
     * //3、连上邮件服务器
     * //        ts.connect("smtp.qq.com", "2374619909@qq.com", "ygkooyaezolheacf");
     * //        ts.connect("smtp.163.com", "18581591460@163.com", "zemin886520");
     * //        ts.connect("smtp.sina.com", "yzmziv@sina.com", "zemin886520");
     * //        ts.connect("smtp.exmail.qq.com","yangzemin@wisdomdata.net","Zemin886520");
     *
     * @param user
     * @throws MessagingException
     */
    private void transportConnect(MailUserBean user) throws MessagingException {
        if (transport == null || transport.isConnected()) {
            transport = session.getTransport();
        }
        if (user == null) {
            transport.connect();
        } else {
            transport.connect(user.getHost(), user.getUser(), user.getPassword());
        }
    }

    public void send(Message message) throws MessagingException {
        send(message, user);
    }

    /**
     * <p>
     * 发送 邮箱
     * </p>
     *
     * @param message
     * @throws MessagingException
     */
    public void send(Message message, MailUserBean user) throws MessagingException {
        //指明邮件的发件人
        if (user != null) {
            message.setFrom(new InternetAddress(user.getUser()));
        }
        transportConnect(user);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    public Session getSession() {
        return session;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setUser(MailUserBean user) {
        this.user = user;
    }

    public void setUser(String host, String user, String password) {
        this.user = new MailUserBean(host, user, password);
    }

    public void setUser(String user, String password) {
        this.user = new MailUserBean(user, password);
    }

    /**
     * <p>
     * 创建 session
     * </p>
     *
     * @param mailSessionBean 配置文件Bean
     * @return session
     */
    private static Session getSession(MailSessionBean mailSessionBean) {
        Properties prop = new Properties();
        if (StringUtils.isNotBlank(mailSessionBean.getHost())) {
            prop.setProperty("mail.host", mailSessionBean.getHost());
        }
        if (StringUtils.isNotBlank(mailSessionBean.getProtocol())) {
            prop.setProperty("mail.transport.protocol", mailSessionBean.getProtocol());
        }
        prop.setProperty("mail.smtp.auth", String.valueOf(mailSessionBean.isAuth()));
        return getSession(prop);
    }

    /**
     * <p>
     * 创建 session
     * </p>
     * <p>
     * prop.setProperty("mail.host", "smtp.sohu.com");
     * prop.setProperty("mail.transport.protocol", "smtp");
     * prop.setProperty("mail.smtp.auth", "true");
     * </p>
     *
     * @param prop 配置文件
     * @return session
     */
    private static Session getSession(Properties prop) {
        return Session.getInstance(prop);
    }

    /**
     * <p>
     * 创建 session
     * </p>
     *
     * @param propPath 配置文件路径
     * @return session
     */
    private static Session getSession(String propPath) {
        Properties prop = new Properties();
        try (FileReader fileReader = new FileReader(new File(propPath))) {
            prop.load(fileReader);
            return getSession(prop);
        } catch (IOException e) {
            log.error("mail properties is not found!", e);
            return null;
        }
    }
}
