package ziv.clyz.email.mail;

/**
 * <p>
 * 默认
 * "mail.transport.protocol", "smtp"
 * "mail.smtp.auth", "true"
 * debug true
 * </p>
 *
 * @author yangzemin
 * @date 2019年6月18日 16:50:12
 */
public class MailSessionBean {
    //"mail.host", "smtp.sohu.com"
    private String host = "smtp";
    //"mail.transport.protocol", "smtp"
    private String protocol;
    //"mail.smtp.auth", "true"
    private boolean auth = true;

    private boolean debug = true;

    public MailSessionBean() {
    }

    /**
     * <p>
     *
     * </p>
     *
     * @param host "mail.host", "smtp.sohu.com"
     */
    public MailSessionBean(String host) {
        this.host = host;
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
