package spring.cloud.eureka.hystrix.service;


import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.springframework.web.client.RestTemplate;

/**
 * @author YangZemin
 * @date 2019/12/30 15:41
 */
public class HelloServiceCommand extends HystrixCommand<String> {

    private RestTemplate restTemplate;

    public HelloServiceCommand(String group, RestTemplate restTemplate) {
        super(HystrixCommandGroupKey.Factory.asKey(group));
        this.restTemplate = restTemplate;
    }

    //服务调用
    @Override
    protected String run() throws Exception {
        System.out.println(Thread.currentThread().getName());
        return restTemplate.getForEntity("http://HELLO-SERVICE/hello", String.class).getBody();
    }

    //服务降级时所调用的Fallback()
    @Override
    protected String getFallback() {
        return "error";
    }
}
