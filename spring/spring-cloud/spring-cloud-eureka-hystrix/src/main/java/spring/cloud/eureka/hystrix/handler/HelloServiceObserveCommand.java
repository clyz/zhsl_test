package spring.cloud.eureka.hystrix.handler;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixObservableCommand;
import org.springframework.web.client.RestTemplate;
import rx.Observable;
import rx.Subscriber;

/**
 * 这时候Hystrix又给我们提供了另外一种模式HystrixObservableCommand来让我们继承这个类，
 * 其实这种模式就运用了Java的RX编程中的观察者模式
 *
 * @author YangZemin
 * @date 2019/12/30 15:54
 */
public class HelloServiceObserveCommand extends HystrixObservableCommand<String> {

    private RestTemplate restTemplate;

    public HelloServiceObserveCommand(String commandGroupKey, RestTemplate restTemplate) {
        super(HystrixCommandGroupKey.Factory.asKey(commandGroupKey));
        this.restTemplate = restTemplate;
    }

    @Override
    protected Observable<String> construct() {
        //观察者订阅网络请求事件
        return Observable.create(subscriber -> {
            try {
                if (!subscriber.isUnsubscribed()) {
                    System.out.println("方法执行....");
                    String result = restTemplate.getForEntity("http://HELLO-SERVICE/hello", String.class).getBody();
                    //这个方法监听方法，是传递结果的，请求多次的结果通过它返回去汇总起来。
                    subscriber.onNext(result);
                    String result1 = restTemplate.getForEntity("http://HELLO-SERVICE/hello", String.class).getBody();
                    //这个方法是监听方法，传递结果的
                    subscriber.onNext(result1);
                    subscriber.onCompleted();
                }
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    //服务降级Fallback
    @Override
    protected Observable<String> resumeWithFallback() {
        return Observable.create(subscriber -> {
            try {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext("error");
                    subscriber.onCompleted();
                }
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }
}