package spring.cloud.eureka.hystrix.controller;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import rx.Observable;
import rx.Observer;
import spring.cloud.eureka.hystrix.handler.HelloServiceObserveCommand;
import spring.cloud.eureka.hystrix.service.HelloService;
import spring.cloud.eureka.hystrix.service.HelloServiceCommand;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author YangZemin
 * @date 2019/12/30 15:15
 */
@RestController
public class ConsumerController {

    @Autowired
    private HelloService helloService;

    @GetMapping(value = {"/consumer", "/"})
    public String helloConsumer(@RequestParam(defaultValue = "http://HELLO-SERVICE/hello") String url) {
        return helloService.helloService(url);
    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = {"h/consumer", "/h"})
    public String helloConsumerH() {
        HelloServiceCommand command = new HelloServiceCommand("group", restTemplate);
        return command.execute();
    }

    @GetMapping(value = {"o/consumer", "/o"})
    public String helloConsumerObserve() throws ExecutionException, InterruptedException {
        List<String> list = Lists.newArrayList();
        HelloServiceObserveCommand command = new HelloServiceObserveCommand("hello", restTemplate);
        /**
         *         热执行
         * 　所谓的热执行就是不管你事件有没有注册完(onCompleted()，onError，onNext这三个事件注册)，
         * 就去执行我的业务方法即(HystrixObservableCommand实现类中的construct()方法).
         * 我们可以在上面的代码中sleep（10000）一下清楚看出热执行
         */
        Observable<String> observable = command.observe();
        /**
         *         冷执行
         * 　所谓的冷执行就是，先进行事件监听方法注册完成后，才执行业务方法
         */
//        Observable<String> observable =command.toObservable();
//        Thread.sleep(10000);
        //订阅
        observable.subscribe(new Observer<String>() {
            //请求完成的方法
            @Override
            public void onCompleted() {
                System.out.println("会聚完了所有查询请求");
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            //订阅调用事件，结果会聚的地方，用集合去装返回的结果会聚起来。
            @Override
            public void onNext(String s) {
                System.out.println("结果来了.....");
                list.add(s);
            }
        });

        return list.toString();

    }
}