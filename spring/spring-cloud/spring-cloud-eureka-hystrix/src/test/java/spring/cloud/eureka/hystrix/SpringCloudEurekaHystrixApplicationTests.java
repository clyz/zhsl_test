package spring.cloud.eureka.hystrix;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.ServletContext;

@AutoConfigureMockMvc
@SpringBootTest
class SpringCloudEurekaHystrixApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void consumer() throws Exception {
        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/")).andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    void consumer_h() throws Exception {
        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/h")).andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    void consumer_o() throws Exception {
        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/o")).andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

}