package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayStatus {
    PAY(0, "已支付"),
    WIT(1, "待支付");
    private Integer code;
    private String msg;
}
