package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class R<T> {
    private Integer code;
    private String msg;
    private T data;

    public static <T> R<T> OK(T data) {
        return new R<T>(1, "成功", data);
    }

    public static <T> R<T> NO(T data) {
        return new R<T>(0, "失败", data);
    }
}
