package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductInfoStatusEnum {
    UP(0, "在架"),
    DOWN(1, "下架");
    private Integer code;
    private String message;
}
