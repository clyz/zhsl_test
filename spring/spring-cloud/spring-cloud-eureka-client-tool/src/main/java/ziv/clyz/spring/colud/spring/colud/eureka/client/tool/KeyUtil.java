package ziv.clyz.spring.colud.spring.colud.eureka.client.tool;

import java.util.Random;

public class KeyUtil {
    public static synchronized String genUniqueKey() {
        return String.valueOf(System.currentTimeMillis() + (new Random().nextInt(900000) + 1000000));
    }
}
