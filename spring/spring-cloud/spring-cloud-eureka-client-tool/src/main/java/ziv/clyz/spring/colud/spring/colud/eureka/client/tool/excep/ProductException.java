package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.excep;

import lombok.Getter;

@Getter
public class ProductException extends RuntimeException {
    private Integer code;

    public ProductException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public ProductException(RE re) {
        this(re.getMsg(), re.getCode());
    }
}
