package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductVo implements Serializable {
    private static final long serialVersionUID = 42L;

    @JsonProperty("name")
    private String categoryName;

    @JsonProperty("type")
    private String categoryType;

    @JsonProperty("list")
    List<ProductInfoVo> productInfoVoList;
}
