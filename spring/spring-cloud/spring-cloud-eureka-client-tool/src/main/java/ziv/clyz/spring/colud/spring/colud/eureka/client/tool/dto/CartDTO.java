package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartDTO {
    private String productId;
    private Integer productQuantity;
}
