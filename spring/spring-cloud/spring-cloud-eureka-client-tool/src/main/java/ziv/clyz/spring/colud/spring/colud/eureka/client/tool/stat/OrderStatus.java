package ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatus {
    NEW(1, "下单"),
    FINSISHED(2, "完成"),
    CANCEL(3, "取消");
    private Integer code;
    private String msg;
}
