package ziv.clyz.spring.colud.eureka.config.server;

import org.jboss.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer //  统一配置文件管理 将配置文件 保存到 GIT 中 从GIT 中读取配置文件
/**
 * spring-cloud-config  读取GIT中的文件 到 config-server 中
 *
 * http://localhost:8764/[分支默认master]/[文件名称]-[环境dev|test|].[yml|properties|json]
 * http://localhost:8764/[文件名称]/[环境dev]
 *
 * EG:
 *  http://localhost:8764/spring-colud-eureka-client-order.properties
 *  http://localhost:8764/spring-colud-eureka-client-order.yml
 *  http://localhost:8764/spring-colud-eureka-client-order.json
 *  http://localhost:8764/eureka-client-order/dev
 *
 */
public class SpringColudEurekaClientConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringColudEurekaClientConfigServerApplication.class, args);
    }

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.DEBUG;
    }

}
