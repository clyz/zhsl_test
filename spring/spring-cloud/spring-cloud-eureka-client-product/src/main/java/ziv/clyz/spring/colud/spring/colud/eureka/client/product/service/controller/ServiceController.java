package ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("product/service/")
public class ServiceController {
    @GetMapping("product")
    public String product() {
        return "'product':'你好呀！'";
    }
}
