package ziv.clyz.spring.colud.spring.colud.eureka.client.product.app;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class B implements ApplicationContextAware {

    private static ApplicationContext b;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        b = applicationContext;
    }

    public static <T> T b(Class<T> c) {
        return b.getBean(c);
    }

}
