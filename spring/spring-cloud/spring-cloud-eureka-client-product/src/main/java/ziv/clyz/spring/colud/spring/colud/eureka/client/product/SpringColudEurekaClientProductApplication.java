package ziv.clyz.spring.colud.spring.colud.eureka.client.product;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableDistributedTransaction
public class SpringColudEurekaClientProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringColudEurekaClientProductApplication.class, args);
    }

}
