package ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductInfo;

import java.util.List;

public interface ProductInfoRepository extends JpaRepository<ProductInfo, String> {

    List<ProductInfo> findAllByProductStatusEquals(Integer productStatus);

    List<ProductInfo> findAllByProductIdIsIn(List<String> productInfos);
}
