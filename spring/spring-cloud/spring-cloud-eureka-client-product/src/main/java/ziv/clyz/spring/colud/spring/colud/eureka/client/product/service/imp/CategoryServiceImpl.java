package ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductCategory;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository.ProductCategoryRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.CategoryService;

import java.util.List;
import java.util.Set;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Override
    public List<ProductCategory> findProductCategoriesByCategoryIdIn(Set<Integer> categoryIds) {
        return productCategoryRepository.findProductCategoriesByCategoryIdIn(categoryIds);
    }
}
