package ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
public class ProductCategory {
    @Id
    @GeneratedValue //  默认自增
    private Integer categoryId;

    /*类目 名称*/
    private String categoryName;
    /*类目 编号*/
    private String categoryType;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private Date createTime;

    @UpdateTimestamp
    @Column(updatable = false)
    private Date updateTime;
}
