package ziv.clyz.spring.colud.spring.colud.eureka.client.product.app;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductCategory;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductInfo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository.ProductCategoryRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository.ProductInfoRepository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * 服务启动后立即执行
 */
@Component
public class InitRunner implements ApplicationRunner, Ordered {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        final ProductCategoryRepository categoryRepository = B.b(ProductCategoryRepository.class);
        final ProductInfoRepository productInfoRepository = B.b(ProductInfoRepository.class);

        final List<ProductCategory> in = categoryRepository.findProductCategoriesByCategoryIdIn(Arrays.asList(1, 2));
        if (in == null || in.size() <= 0) {
            categoryRepository.save(
                    ProductCategory.builder()
                            .categoryId(2)
                            .categoryName("蔬菜")
                            .categoryType("Vegetables")
                            .build());
            categoryRepository.save(
                    ProductCategory.builder()
                            .categoryId(1)
                            .categoryName("冰淇淋")
                            .categoryType("ice")
                            .build());
            if (productInfoRepository.findAll().size() <= 0) {
                productInfoRepository.save(
                        ProductInfo.builder()
                                .productName("花心大萝卜")
                                .productStatus(0)
                                .productStock(100)
                                .productDescription("好吃不腻的花心大萝卜。")
                                .categoryType(2)
                                .productPrice(new BigDecimal(4.69))
                                .build());
                productInfoRepository.save(
                        ProductInfo.builder()
                                .productName("性感大白菜")
                                .productStatus(0)
                                .productStock(100)
                                .productDescription("我家的白菜就是不一样")
                                .categoryType(2)
                                .productPrice(new BigDecimal(7.00))
                                .build());
                productInfoRepository.save(
                        ProductInfo.builder()
                                .productName("脑子奶茶")
                                .productStatus(0)
                                .productStock(100)
                                .productDescription("脑子奶茶，不一样的奶茶")
                                .categoryType(1)
                                .productPrice(new BigDecimal(11.56))
                                .build());
            }
        }
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
