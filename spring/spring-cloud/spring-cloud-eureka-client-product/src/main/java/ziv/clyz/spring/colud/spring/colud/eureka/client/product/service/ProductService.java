package ziv.clyz.spring.colud.spring.colud.eureka.client.product.service;

import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductInfo;

import java.util.List;

public interface ProductService {

    List<ProductInfo> findUpAll();

    List<ProductInfo> findByProductIdIn(List<String> productInfos);

    void decreaseStock(List<CartDTO> cartDTOS);
}
