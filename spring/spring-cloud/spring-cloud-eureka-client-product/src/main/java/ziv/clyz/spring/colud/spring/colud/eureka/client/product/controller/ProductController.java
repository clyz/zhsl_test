package ziv.clyz.spring.colud.spring.colud.eureka.client.product.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductInfo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.CategoryService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.ProductService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductInfoVo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductVo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.R;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;

    /**
     * 查询所有在架子的订单
     * 获取类目列表
     * 查询类目
     * 构建数据
     */
    @GetMapping("list")
    public R<List<ProductVo>> list() {
        final Map<Integer, List<ProductInfo>> productInfos = productService.findUpAll().stream()
                .collect(Collectors.groupingBy(ProductInfo::getCategoryType));

        return R.OK(categoryService
                .findProductCategoriesByCategoryIdIn(productInfos.keySet())
                .stream()
                .map(pc -> ProductVo
                        .builder()
                        .categoryName(pc.getCategoryName())
                        .categoryType(pc.getCategoryType())
                        .productInfoVoList(
                                productInfos.get(pc.getCategoryId()).stream()
                                        .map(pi -> {
                                                    final ProductInfoVo productInfoVo = ProductInfoVo.builder().build();
                                                    BeanUtils.copyProperties(pi, productInfoVo);
                                                    return productInfoVo;
                                                }
                                        ).collect(Collectors.toList())
                        )
                        .build())
                .collect(Collectors.toList()));
    }

    /**
     * 获取 商品列表
     */
    @GetMapping("listForOrder")
    public List<ProductInfo> listForOrder(@RequestParam("productInfos") List<String> productInfos) {
        return productService.findByProductIdIn(productInfos);
    }

    //  更新 库存
    @PostMapping("decreaseStock")
    public void decreaseStock(@RequestBody List<CartDTO> cartDTOS){
        productService.decreaseStock(cartDTOS);
    }
}
