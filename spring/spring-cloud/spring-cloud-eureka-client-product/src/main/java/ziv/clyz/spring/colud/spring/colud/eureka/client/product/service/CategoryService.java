package ziv.clyz.spring.colud.spring.colud.eureka.client.product.service;

import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductCategory;

import java.util.List;
import java.util.Set;

public interface CategoryService {
    List<ProductCategory> findProductCategoriesByCategoryIdIn(Set<Integer> categoryIds);
}
