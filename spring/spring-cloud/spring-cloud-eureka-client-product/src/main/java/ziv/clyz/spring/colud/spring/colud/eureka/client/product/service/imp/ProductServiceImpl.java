package ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.imp;

import com.codingapi.txlcn.tc.annotation.DTXPropagation;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.codingapi.txlcn.tc.annotation.TxcTransaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.touk.throwing.ThrowingFunction;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductInfo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository.ProductInfoRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.service.ProductService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.excep.ProductException;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.excep.RE;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat.ProductInfoStatusEnum;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat.amqp.Queues;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductInfoRepository productInfoRepository;
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public List<ProductInfo> findUpAll() {
        return productInfoRepository.findAllByProductStatusEquals(ProductInfoStatusEnum.UP.getCode());
    }

    @Override
    public List<ProductInfo> findByProductIdIn(List<String> productInfos) {
        return productInfoRepository.findAllByProductIdIsIn(productInfos);
    }

    @Override
    @Transactional
    @TxcTransaction(propagation = DTXPropagation.SUPPORTS)
    public void decreaseStock(List<CartDTO> cartDTOS) {
        final List<ProductInfo> infos = cartDTOS.stream()
                .map(ThrowingFunction.unchecked(
                        cart -> {
                            final Optional<ProductInfo> info = productInfoRepository.findById(cart.getProductId());
                            if (!info.isPresent()) throw new ProductException(RE.PRODUCT_NOT_EXIST);
                            final int numStock = info.get().getProductStock() - cart.getProductQuantity();  //  剩余商品
                            if (numStock < 0) throw new ProductException(RE.PRODUCT_STOCK_ERROR);
                            info.get().setProductStock(numStock);
                            return info.get();
                        })
                ).collect(Collectors.toList());

        //  更新库存
        ObjectMapper mapper = new ObjectMapper();
        final List<ProductInfo> collect = infos.stream()
                .map(ThrowingFunction.unchecked(info -> productInfoRepository.save(info)))
                .collect(Collectors.toList());

        //  发送 库存 更新 消息
        try {
            amqpTemplate.convertAndSend(Queues.PRODUCT_INFO_STOCK, mapper.writeValueAsString(collect));
        } catch (JsonProcessingException e) {
            log.error("消息发送 转换 JSON 异常", e);
        }
    }
}
