package ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Builder
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class ProductInfo {
    @Id
    @Column(length = 50)
    @GeneratedValue(generator = "productId")
    @GenericGenerator(name = "productId", strategy = "uuid.hex")
    private String productId;
    /*  名称 */
    private String productName;
    /*  价格 */
    private BigDecimal productPrice;
    /*  库存 */
    private Integer productStock;
    /*描述*/
    private String productDescription;
    /*小图*/
    private String productIcon;
    /*状态 0正常 1 下架*/
    private Integer productStatus;
    /*类目类型编号*/
    private Integer categoryType;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private Date createTime;

    @UpdateTimestamp
    @Column(updatable = false)
    private Date updateTime;
}
