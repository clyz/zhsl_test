package ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductCategory;

import java.util.List;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Integer> {

    List<ProductCategory> findProductCategoriesByCategoryIdIn(Iterable<Integer> categoryIds);

}
