package ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.SpringColudEurekaClientProductApplicationTests;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductInfo;

import java.math.BigDecimal;
import java.util.Arrays;

public class ProductInfoRepositoryTest extends SpringColudEurekaClientProductApplicationTests {

    @Autowired
    private ProductInfoRepository productInfoRepository;

    @Test
    public void save(){
        final ProductInfo productInfo = productInfoRepository.save(
                ProductInfo.builder()
                        .productName("花心大萝卜")
                        .productStatus(0)
                        .productStock(100)
                        .productDescription("好吃不腻的花心大萝卜。")
                        .categoryType(2)
                        .productPrice(new BigDecimal(4.69))
                        .build()
        );
        System.out.println(productInfo);
    }

    @Test
    public void findAllByProductStatusEquals(){
        productInfoRepository.findAllByProductStatusEquals(0)
                .forEach(System.out::println);
    }

    @Test
    public void findAllByProductIdIsIn(){
        productInfoRepository
                .findAllByProductIdIsIn(
                        Arrays.asList("4028e3816c07eb3b016c07eb58be0000","4028e3816c07ec58016c07ec75c00000"))
                .forEach(System.out::println);
    }
}