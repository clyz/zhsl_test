package ziv.clyz.spring.colud.spring.colud.eureka.client.product.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.SpringColudEurekaClientProductApplicationTests;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.entity.ProductCategory;

import java.util.Arrays;
import java.util.List;

public class ProductCategoryRepositoryTest extends SpringColudEurekaClientProductApplicationTests {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Test
    public void save() {
        final ProductCategory productCategory = productCategoryRepository.save(
                ProductCategory.builder()
                        .categoryName("蔬菜")
                        .categoryType("Vegetables")
                        .build()
        );
        System.out.println(productCategory);
    }

    @Test
    public void findProductCategoriesByCategoryIdIn() {
        final List<ProductCategory> categoryIdIn = productCategoryRepository
                .findProductCategoriesByCategoryIdIn(Arrays.asList(1, 2, 3));
    }
}