package ziv.clyz.spring.colud.spring.colud.eureka.client.product.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.SpringColudEurekaClientProductApplicationTests;
import ziv.clyz.spring.colud.spring.colud.eureka.client.product.app.B;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.excep.ProductException;

import java.util.Arrays;

@Slf4j
public class ProductServiceTest extends SpringColudEurekaClientProductApplicationTests {

    @Test
    public void decreaseStock() {
        try {
            B.b(ProductService.class)
                    .decreaseStock(Arrays.asList(
                            new CartDTO("4028e3816c07ec58016c07ec75c00000", 13),
                            new CartDTO("4028e3816c07ed60016c07ed82b90000", 80)
                    ));
        }catch (Exception e){
            ProductException productException = (ProductException) e.getCause();
            log.info("{},{},{}",e.getMessage(),productException.getCode(),productException.getMessage());
        }
    }

}