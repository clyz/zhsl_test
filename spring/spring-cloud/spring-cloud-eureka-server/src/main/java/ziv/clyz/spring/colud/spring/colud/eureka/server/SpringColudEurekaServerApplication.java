package ziv.clyz.spring.colud.spring.colud.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringColudEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringColudEurekaServerApplication.class, args);
    }

}
