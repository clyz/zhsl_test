package ziv.clyz.spring.colud.eureka.client.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * route 代理服务端
 */
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class SpringCloudEurekaClientZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaClientZuulApplication.class, args);
    }

}
