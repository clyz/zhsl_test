package ziv.clyz.spring.cloud.config.client.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ziv.clyz.spring.cloud.config.client.config.Girl;

/**
 * curl -v -X POST "http://127.0.0.1:8764/actuator/bus-refresh"
 */
@RestController
@RefreshScope
public class EnvController {
    @Value("${env}")
    private String env;

    @Autowired
    private Girl girl;

    @GetMapping(value = {"/env", "print/env", "/"})
    public String getEnv() {
        return env;
    }

    /**
     * 直接 将 Bean 类 返回 Json 会 抛出 异常.
     *
     * @return
     */
    @GetMapping(value = {"/girl", "/g"})
    public Girl girl() {
        Girl jsonGirl = new Girl();
        BeanUtils.copyProperties(girl, jsonGirl);
        return jsonGirl;
    }
}
