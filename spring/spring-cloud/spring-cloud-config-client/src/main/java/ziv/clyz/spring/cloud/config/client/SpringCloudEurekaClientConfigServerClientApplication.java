package ziv.clyz.spring.cloud.config.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class SpringCloudEurekaClientConfigServerClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaClientConfigServerClientApplication.class, args);
    }

}
