package ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.order;

import org.junit.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.SpringColudEurekaClientOrderApplicationTests;

import java.util.Date;

/**
 * MQ 发送端测试
 */
public class OrderReceiverTest extends SpringColudEurekaClientOrderApplicationTests {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Test
    public void orderComputerExChangeMQQueue() {
        amqpTemplate.convertAndSend("orderFruitExChange","fruit", "now: " + new Date().toString());
    }

    @Test
    public void orderFruitExChangeMQQueue() {
        amqpTemplate.convertAndSend("orderComputerExChange","computer", "now: " + new Date().toString());
    }

    @Test
    public void orderExChangeMQQueue() {
        amqpTemplate.convertAndSend("orderExChangeMQQueue", "now: " + new Date().toString());
    }

    @Test
    public void orderQueue() {
        amqpTemplate.convertAndSend("orderQueue", "now: " + new Date().toString());
    }

    @Test
    public void orderMQQueue() {
        amqpTemplate.convertAndSend("orderMQQueue", "now: " + new Date().toString());
    }

}