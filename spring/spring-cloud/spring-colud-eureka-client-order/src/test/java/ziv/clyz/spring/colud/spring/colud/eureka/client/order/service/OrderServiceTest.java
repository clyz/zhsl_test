package ziv.clyz.spring.colud.spring.colud.eureka.client.order.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.SpringColudEurekaClientOrderApplicationTests;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.DetailDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.OrderDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.service.ProductService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.R;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductInfoVo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductVo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class OrderServiceTest extends SpringColudEurekaClientOrderApplicationTests {
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    @Test
    public void create() {
        final OrderDTO dto = new OrderDTO();
        dto.setBuyerName("杨泽民");
        dto.setOrderAmount(new BigDecimal(2));
        dto.setBuyerAddress("四川成都华阳");
        dto.setBuyerOpenid("WXasohfqepori");
        dto.setBuyerPhone("18581591460");
        dto.setOrderStatus(1);
        dto.setPayStatus(1);

        List<DetailDTO> arrayList = new ArrayList<>();
        final R<List<ProductVo>> list = productService.list();
        for (ProductVo productVo : list.getData()) {
            final List<ProductInfoVo> infoVoList = productVo.getProductInfoVoList();
            if (infoVoList != null) {
                final ProductInfoVo infoVo = infoVoList.get(0);
                arrayList.add(DetailDTO.builder()
                        .productId(infoVo.getProductId())
                        .productIcon(infoVo.getProductIcon())
                        .productName(infoVo.getProductName())
                        .productPrice(infoVo.getProductPrice())
                        .productQuantity(2)
                        .build());
            }
        }

        dto.setDetails(arrayList);
        final OrderDTO orderDTO = orderService.create(dto);
        log.info("ORDERDTO {}",orderDTO);
    }
}