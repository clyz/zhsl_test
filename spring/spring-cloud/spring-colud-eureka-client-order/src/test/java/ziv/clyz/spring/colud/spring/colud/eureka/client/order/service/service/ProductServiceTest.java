package ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.service;

import org.junit.Test;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.SpringColudEurekaClientOrderApplicationTests;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.app.B;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;

public class ProductServiceTest extends SpringColudEurekaClientOrderApplicationTests {

    @Test
    public void productMsg() {
        final String s = B.b(ProductService.class).productMsg();
        System.out.println(s);
    }

    @Test
    public void listForOrder() {
        B.b(ProductService.class).listForOrder(
                new String[]{"4028e3816c07eb3b016c07eb58be0000", "4028e3816c07ec58016c07ec75c00000"}
        ).forEach(System.out::println);
    }

    @Test
    public void decreaseStock() {
        B.b(ProductService.class).decreaseStock(
                new CartDTO[]{
                        new CartDTO("4028e3816c07ec58016c07ec75c00000", 13),
                        new CartDTO("4028e3816c07ed60016c07ed82b90000", 20)
                }
        );
    }
}