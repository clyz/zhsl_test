package ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.stream.order;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.SpringColudEurekaClientOrderApplicationTests;

import java.util.Date;

/**
 * Stream
 */
public class OrderStreamReceiverTest extends SpringColudEurekaClientOrderApplicationTests {

    @Autowired
    private IOrderStreamClientService orderStreamClientService;

    //  发送
    @Test
    public void orderOutputStreamMessage() {
        orderStreamClientService.orderOutputStreamMessage().send(MessageBuilder.withPayload(new Date().toString()).build());
    }
    // read BUG ...
    @Test
    public void orderInputStreamMessage() {
        orderStreamClientService.orderInputStreamMessage().send(MessageBuilder.withPayload("{'name':'1'}").build());
    }
}