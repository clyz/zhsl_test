package ziv.clyz.spring.colud.spring.colud.eureka.client.order;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
//@EnableEurekaClient
//@EnableFeignClients //  service 层方式通讯,声明式客服端调用，不是RPC
public class SpringColudEurekaClientOrderApplicationTests {

    @Test
    public void contextLoads() {
    }

}
