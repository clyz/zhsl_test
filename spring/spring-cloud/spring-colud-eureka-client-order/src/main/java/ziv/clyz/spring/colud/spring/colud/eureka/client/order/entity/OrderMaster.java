package ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Builder
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
public class OrderMaster {

    @Id
    @Column(length = 50)
    @GeneratedValue(generator = "orderId")
    @GenericGenerator(name = "orderId", strategy = "uuid.hex")
    private String orderId;

    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    /*卖家微信号*/
    private String buyerOpenid;
    /*总金额*/
    private BigDecimal orderAmount;
    /*订单状态 0 新下单*/
    private Integer orderStatus;
    /*支付状态，0未支付默认*/
    @Column(insertable = false,columnDefinition = "int default 0")
    private Integer payStatus;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private Date createTime;

    @UpdateTimestamp
    @Column(updatable = false)
    private Date updateTime;
}
