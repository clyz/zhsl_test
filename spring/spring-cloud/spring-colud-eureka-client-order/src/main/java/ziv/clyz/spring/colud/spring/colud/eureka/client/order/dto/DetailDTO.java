package ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DetailDTO {

    private String productId;

    private String productName;

    private BigDecimal productPrice;
    /*商品数量*/
    private Integer productQuantity;

    private String productIcon;
}
