package ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.service.ProductService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.app.B;

/**
 * 调用 Product client 的 服务接口
 */
@RestController
@RequestMapping("order/client/")
public class ClientController {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @GetMapping("product1")
    public String product1() {
        return restTemplate.getForObject("http://127.0.0.1:8762/product/service/product", String.class);
    }

    @GetMapping("product2")
    public String product2() {
        final ServiceInstance serviceInstance = loadBalancerClient.choose("EUREKA-CLIENT-PRODUCT");
        final String format = String.format("http://%s:%s", serviceInstance.getHost(), serviceInstance.getPort());
        return restTemplate.getForObject(format + "/product/service/product", String.class);
    }

    @GetMapping("product3")
    public String product3() {
        return restTemplate.getForObject("http://EUREKA-CLIENT-PRODUCT/product/service/product", String.class);
    }

    @GetMapping("product4")
    public String product4() {
        return B.b(ProductService.class).productMsg();
    }
}
