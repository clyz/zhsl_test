package ziv.clyz.spring.colud.spring.colud.eureka.client.order.controller.amqp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.stream.order.IOrderStreamClientService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.DetailDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.OrderDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.OrderService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.service.ProductService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.R;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductInfoVo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductVo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("amqp")
public class StreamController {
    @Autowired
    private IOrderStreamClientService orderStreamClientService;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;

    //  测试 ORDER 创建 发送
    @GetMapping(value = {"order", "o"})
    public OrderDTO order() {
        final OrderDTO dto = new OrderDTO();
        dto.setBuyerName("杨泽民");
        dto.setOrderAmount(new BigDecimal(2));
        dto.setBuyerAddress("四川成都华阳");
        dto.setBuyerOpenid("WXasohfqepori");
        dto.setBuyerPhone("18581591460");
        dto.setOrderStatus(1);
        dto.setPayStatus(1);

        List<DetailDTO> arrayList = new ArrayList<>();
        final R<List<ProductVo>> list = productService.list();
        for (ProductVo productVo : list.getData()) {
            final List<ProductInfoVo> infoVoList = productVo.getProductInfoVoList();
            if (infoVoList != null) {
                final ProductInfoVo infoVo = infoVoList.get(0);
                arrayList.add(DetailDTO.builder()
                        .productId(infoVo.getProductId())
                        .productIcon(infoVo.getProductIcon())
                        .productName(infoVo.getProductName())
                        .productPrice(infoVo.getProductPrice())
                        .productQuantity(2)
                        .build());
            }
        }

        dto.setDetails(arrayList);
        return orderService.create(dto);
    }

    //  发送
    @GetMapping("/")
    public void send() {
        orderStreamClientService.orderOutputStreamMessage()
                .send(
                        MessageBuilder.withPayload(
                                new Date().toString()
                        ).build()
                );
    }

    //  发送 对象
    @GetMapping(value = {"b", "obj", "dto"})
    public void sendB() {
        orderStreamClientService.orderOutputStreamMessageObj()
                .send(
                        MessageBuilder.withPayload(
                                DetailDTO.builder()
                                        .productName("dTo").productIcon("icon")
                                        .build()
                        ).build()
                );
    }
}
