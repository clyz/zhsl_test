package ziv.clyz.spring.colud.spring.colud.eureka.client.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.OrderMaster;

public interface OrderMasterRepository extends JpaRepository<OrderMaster,String> {
}
