package ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.ProductInfo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.R;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.vo.product.ProductVo;

import java.util.List;

/**
 * Feign 动态代理 HTTP
 */
@FeignClient(name = "EUREKA-CLIENT-PRODUCT")    //  调用的那个服务
@RequestMapping("product")
public interface ProductService {

    //  调用的那个方法 URL
    @GetMapping("/service/product")
    String productMsg();

    //  Feign 不支持 List 必须使用Array
    @GetMapping("listForOrder")
    List<ProductInfo> listForOrder(@RequestParam("productInfos") String[] productInfos);

    @PostMapping(value = "decreaseStock")
    void decreaseStock(@RequestBody CartDTO[] cartDTOS);

    @GetMapping("list")
    R<List<ProductVo>> list();

}