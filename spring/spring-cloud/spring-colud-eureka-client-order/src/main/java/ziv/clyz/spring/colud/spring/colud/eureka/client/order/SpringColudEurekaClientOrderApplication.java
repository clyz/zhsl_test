package ziv.clyz.spring.colud.spring.colud.eureka.client.order;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.bus.ConditionalOnBusEnabled;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//@EnableEurekaClient
@EnableFeignClients //  service 层方式通讯,声明式客服端调用，不是RPC
@EnableDiscoveryClient
@EnableDistributedTransaction
public class SpringColudEurekaClientOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringColudEurekaClientOrderApplication.class, args);
    }

}
