package ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfo {
    private String productId;
    /*  名称 */
    private String productName;
    /*  价格 */
    private BigDecimal productPrice;
    /*  库存 */
    private Integer productStock;
    /*描述*/
    private String productDescription;
    /*小图*/
    private String productIcon;
    /*状态 0正常 1 下架*/
    private Integer productStatus;
    /*类目类型编号*/
    private Integer categoryType;

    private Date createTime;

    private Date updateTime;
}
