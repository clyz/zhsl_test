package ziv.clyz.spring.colud.spring.colud.eureka.client.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> {
}
