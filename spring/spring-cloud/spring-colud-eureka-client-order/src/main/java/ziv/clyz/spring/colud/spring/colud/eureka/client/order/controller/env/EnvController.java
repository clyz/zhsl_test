package ziv.clyz.spring.colud.spring.colud.eureka.client.order.controller.env;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("env")
@RefreshScope   //  开启BUS 参数值 自动刷新值
public class EnvController {
    @Value("${env}")
    private String env;

    @GetMapping("print")
    public String getEnv(){
        return env;
    }
}
