package ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.impl;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.app.B;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.DetailDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.OrderDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.OrderDetail;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.OrderMaster;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.ProductInfo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.repository.OrderDetailRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.repository.OrderMasterRepository;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.OrderService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.service.service.ProductService;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.dto.CartDTO;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat.OrderStatus;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat.PayStatus;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMasterRepository orderMasterRepository;
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Autowired
    private ProductService productService;

    @Override
    @Transactional
    @LcnTransaction
    public OrderDTO create(OrderDTO order) {
        order.setOrderStatus(OrderStatus.NEW.getCode());
        order.setPayStatus(PayStatus.WIT.getCode());

        //  计算总价格
        final List<ProductInfo> productInfos = B.b(ProductService.class)
                .listForOrder(order.getDetails().stream()
                        .map(DetailDTO::getProductId).toArray(String[]::new));

        BigDecimal bigDecimal = new BigDecimal(BigInteger.ZERO);
        for (DetailDTO dto : order.getDetails()) {
            for (ProductInfo p : productInfos) {
                if (p.getProductId().equals(dto.getProductId())) {
                    bigDecimal = p.getProductPrice()
                            .multiply(new BigDecimal(dto.getProductQuantity()))
                            .add(bigDecimal);
                }
            }
        }
        order.setOrderAmount(bigDecimal);

        //  更新 对应商品的库存
        productService.decreaseStock(order.getDetails().stream()
                .map(detail -> CartDTO.builder()
                        .productId(detail.getProductId())
                        .productQuantity(detail.getProductQuantity())
                        .build()).toArray(CartDTO[]::new));

        //  创建订单
        final OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(order, orderMaster);
        final OrderMaster saveOrderMaster = orderMasterRepository.save(orderMaster);

        //  创建 订单 的基本详情
        orderDetailRepository.saveAll(
                order.getDetails().stream()
                        .map(
                                de -> {
                                    final OrderDetail orderDetail = new OrderDetail();
                                    BeanUtils.copyProperties(de, orderDetail);
                                    orderDetail.setOrderId(saveOrderMaster.getOrderId());
                                    return orderDetail;
                                }
                        ).collect(Collectors.toList())
        );

        return order;
    }
}
