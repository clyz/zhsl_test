package ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import pl.touk.throwing.ThrowingConsumer;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity.ProductInfo;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat.amqp.Queues;
import ziv.clyz.spring.colud.spring.colud.eureka.client.tool.stat.cache.Redis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class ProductReceiver {

    @Autowired
    private StringRedisTemplate redisTemplate;

    //  接收消息
    @RabbitListener(queuesToDeclare = @Queue(Queues.PRODUCT_INFO_STOCK))
    public void productInfoStock(String message) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            final List<ProductInfo> productInfo = mapper.readValue(message,
                    mapper.getTypeFactory().constructParametricType(ArrayList.class, ProductInfo.class));
            log.info(Queues.PRODUCT_INFO_STOCK + " 接收到消息 {}", productInfo);

            //  stock TO redis
            productInfo.forEach(
                    ThrowingConsumer.unchecked(
                            info -> redisTemplate.opsForValue()
                                    .set(String.format(Redis.PRODUCT_INFO_STOCK_NUM_FID, info.getProductId()),
                                            String.valueOf(info.getProductStock())
                                    ))
            );

        } catch (IOException e) {
            log.error(Queues.PRODUCT_INFO_STOCK + " 接收 消息 转换为Json 格式失败", e);
        }
    }

}