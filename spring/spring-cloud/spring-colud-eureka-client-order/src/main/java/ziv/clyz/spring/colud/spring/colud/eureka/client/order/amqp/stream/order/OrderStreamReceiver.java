package ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.stream.order;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.DetailDTO;

@Component
@EnableBinding(value = IOrderStreamClientService.class)
@Slf4j
public class OrderStreamReceiver {

    @StreamListener("orderOutputStreamMessage")
    public void orderOutputStreamMessage(Object message) {
        log.info("orderOutputStreamMessage: {}", message);
    }

    @StreamListener("orderInputStreamMessage")
    public void orderInputStreamMessage(Object message) {
        log.info("orderInputStreamMessage: {}", message);
    }

    @StreamListener("orderOutputStreamMessageObj")
    @SendTo("orderOutputStreamMessage") //  消费完了 回执 到 :orderOutputStreamMessage
    public String orderOutputStreamMessageObj(DetailDTO detailDTO) {
        log.info("orderOutputStreamMessageObj: {}", detailDTO);
        return "我执行完成 了一个消息 现在 回执消息";
    }
}
