package ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderDTO {

    private String orderId;

    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    /*卖家微信号*/
    private String buyerOpenid;
    /*总金额*/
    private BigDecimal orderAmount;
    /*订单状态 0 新下单*/
    private Integer orderStatus;
    /*支付状态，0未支付默认*/
    private Integer payStatus;

    private List<DetailDTO> details;
}
