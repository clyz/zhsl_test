package ziv.clyz.spring.colud.spring.colud.eureka.client.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Builder
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail {

    @Id
    @Column(length = 50)
    @GeneratedValue(generator = "orderId")
    @GenericGenerator(name = "orderId", strategy = "uuid.hex")
    private String detailId;

    private String orderId;

    private String productId;

    private String productName;

    private BigDecimal productPrice;
    /*商品数量*/
    private Integer productQuantity;

    private String productIcon;
}
