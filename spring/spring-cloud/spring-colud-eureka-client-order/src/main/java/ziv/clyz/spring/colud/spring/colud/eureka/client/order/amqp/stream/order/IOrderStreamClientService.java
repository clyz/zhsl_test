package ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.stream.order;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * spring-cloud-starter-stream-rabbit
 */
public interface IOrderStreamClientService {

    @Input("orderInputStreamMessage")
    SubscribableChannel orderInputStreamMessage();

    @Output("orderOutputStreamMessage")
    MessageChannel orderOutputStreamMessage();

    @Output("orderOutputStreamMessageObj")
    MessageChannel orderOutputStreamMessageObj();
}
