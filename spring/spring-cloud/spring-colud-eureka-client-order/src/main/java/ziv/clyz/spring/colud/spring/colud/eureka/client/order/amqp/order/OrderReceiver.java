package ziv.clyz.spring.colud.spring.colud.eureka.client.order.amqp.order;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息接收
 */
@Slf4j
@Component
public class OrderReceiver {

    //  指定接收
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "orderComputerExChangeMQQueue"),
            key = "computer",
            exchange = @Exchange(value = "orderComputerExChange")
    ))
    public void computer(String message) {
        log.info("Receiver: computer {}", message);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "orderFruitExChangeMQQueue"),
            key = "fruit",
            exchange = @Exchange(value = "orderFruitExChange")
    ))
    public void order(String message) {
        log.info("Receiver: fruit {}", message);
    }

    //  绑定 创建队列
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "orderExChangeMQQueue"),
            exchange = @Exchange(value = "orderExChange")
    ))
    public void orderExChangeMQQueue(String message) {
        log.info("Receiver: {}", message);
    }

    //  自动创建队列
    @RabbitListener(queuesToDeclare = @Queue(value = "orderMQQueue"))
    public void orderQueue(String message) {
        log.info("Receiver: {}", message);
    }

    @RabbitListener(queues = {"orderQueue"})
    public void orderQueueTest(String message) {
        log.info("Receiver: {}", message);
    }
}
