package ziv.clyz.spring.colud.spring.colud.eureka.client.order.service;

import ziv.clyz.spring.colud.spring.colud.eureka.client.order.dto.OrderDTO;

public interface OrderService {

    OrderDTO create(OrderDTO order);
}
