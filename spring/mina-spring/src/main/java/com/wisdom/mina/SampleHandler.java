package com.wisdom.mina;

import lombok.extern.slf4j.Slf4j;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

/**
 * IoHandler这个接口负责编写业务逻辑，也就是接收、发送数据的地方。
 * 需要有开发者自己来实现这个接口。
 * IoHandler可以看成是Mina处理流程的终点，每个IoService都需要指定一个IoHandler。
 */
@Slf4j
public class SampleHandler implements IoHandler {

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        log.info("IoHandler::sessionCreated");
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        log.info("IoHandler::sessionOpened");
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {

    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {

    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {

    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {

    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {

    }
}
