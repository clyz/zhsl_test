package com.wisdom.mina;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

/**
 * IoFilter：这个接口定义一组拦截器，
 * 这些拦截器可以包括日志输出、黑名单过滤、数据的编码（write 方向）与解码（read 方向）等功能，
 * 其中数据的encode 与decode是最为重要的、也是你在使用Mina 时最主要关注的地方。
 * <p>
 * 字符编码处理中心
 */
public class CodecFactoryHandler implements ProtocolCodecFactory {
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        return null;
    }

    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        return null;
    }
}
