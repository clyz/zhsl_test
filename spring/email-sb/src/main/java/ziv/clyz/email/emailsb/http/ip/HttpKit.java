package ziv.clyz.email.emailsb.http.ip;

import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.Location;
import com.maxmind.geoip2.record.Subdivision;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HttpKit {

    /**
     * ip地理坐标获取工具类
     */

    public static Map<String, Object> getGenIp(String ipAddr) {
        Map<String, Object> result = new HashMap<>();
        try {
            // 这是GeoIP2 或 GeoLite2 database 文件所在的位置 ，此处从项目resources路径下获取，当然也可以写成绝对路径
            File database = new File("C:\\service_java\\ZHSL\\zhsl_test\\spring\\email-sb\\src\\main\\resources\\GeoLite2-City.mmdb");

            DatabaseReader reader = new DatabaseReader.Builder(database).withCache(new CHMCache()).build();

            InetAddress ipAddress = InetAddress.getByName(ipAddr);

            CityResponse response = reader.city(ipAddress);

            Country country = response.getCountry();
            Subdivision subdivision = response.getMostSpecificSubdivision();
            City city = response.getCity();
            Location location = response.getLocation();

            result.put("lat", location.getLatitude());//纬度
            result.put("long", location.getLongitude()); // 经度
            result.put("country", country.getNames().get("zh-CN"));// 国家名
            result.put("subdivision", subdivision.getNames().get("ja"));//省份
            result.put("city", city.getNames().get("ja")); // 城市


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getRequestRealIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip != null && ip.contains(",")) {
            ip = ip.split(",")[0];
        }

        if (!checkIp(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (!checkIp(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (!checkIp(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (!checkIp(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    private static boolean checkIp(String ip) {
        if (ip == null || ip.length() == 0 || "unkown".equalsIgnoreCase(ip)) {
            return false;
        }
        return true;
    }

}
