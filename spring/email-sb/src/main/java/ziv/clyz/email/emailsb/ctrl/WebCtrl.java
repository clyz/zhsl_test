package ziv.clyz.email.emailsb.ctrl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ziv.clyz.email.emailsb.http.ip.HttpKit;

import javax.servlet.http.HttpServletRequest;

@RestController
public class WebCtrl {

    @GetMapping("ip")
    @ResponseBody
    public String getip(HttpServletRequest request) {
        return HttpKit.getRequestRealIp(request);
    }

}
