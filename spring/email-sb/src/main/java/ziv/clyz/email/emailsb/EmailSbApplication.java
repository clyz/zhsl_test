package ziv.clyz.email.emailsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmailSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmailSbApplication.class, args);
    }

}
