package ziv.clyz.email.mail.support;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;


@ContextConfiguration("classpath:*.xml")
public class MailServiceTest extends AbstractJUnit4SpringContextTests {

    private MailUtil mailUtil;

    @Test
    public void testSendMail() {

        //创建邮件
        MailBean mailBean = new MailBean();
        mailBean.setFrom("wzqdemo@163.com");
        mailBean.setSubject("hello world");
        mailBean.setToEmails(new String[]{"wzqdemo@gmail.com"});
        mailBean.setTemplate("hello ${userName} !<a href='www.baidu.com' >baidu</a>");
        Map map = new HashMap();
        map.put("userName", "Haley Wang");
        mailBean.setData(map);

        //发送邮件
        try {
            mailUtil.send(mailBean);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }
}