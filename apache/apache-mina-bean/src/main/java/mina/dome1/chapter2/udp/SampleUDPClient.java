package mina.dome1.chapter2.udp;

import mina.dome1.chapter2.tcp.SimpleServer;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.nio.NioDatagramConnector;

import java.net.InetSocketAddress;

public class SampleUDPClient extends IoHandlerAdapter {

    private IoSession session;

    public void main() throws Throwable {
        NioDatagramConnector connector = new NioDatagramConnector();
        connector.setHandler(this);
        ConnectFuture connFuture = connector.connect(new InetSocketAddress(SimpleServer.PORT));

        connFuture.addListener( future -> {
            ConnectFuture connFuture1 = (ConnectFuture) future;
            if (connFuture1.isConnected()) {
                session = future.getSession();
                try {
                    sendData();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Not connected...exiting");
            }
        });
    }

    private void sendData() throws InterruptedException {
        for (int i = 0; i < 30; i++) {
            long free = Runtime.getRuntime().freeMemory();
            IoBuffer buffer = IoBuffer.allocate(8);
            buffer.putLong(free);
            buffer.flip();
            session.write(buffer);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new InterruptedException(e.getMessage());
            }
        }
    }


}