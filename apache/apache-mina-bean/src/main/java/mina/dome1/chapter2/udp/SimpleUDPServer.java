package mina.dome1.chapter2.udp;

import mina.dome1.chapter2.handler.MemoryMonitorHandler;
import mina.dome1.chapter2.tcp.SimpleServer;
import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.DatagramSessionConfig;
import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * http://mina.apache.org/mina-project/userguide/ch2-basics/ch2.2-sample-tcp-server.html
 */
public class SimpleUDPServer {

    public List<SocketAddress> list = new ArrayList<SocketAddress>();

    public void main() throws IOException {
        NioDatagramAcceptor acceptor = new NioDatagramAcceptor();

        DefaultIoFilterChainBuilder chain = acceptor.getFilterChain();

        //  连接会话LOG
        chain.addLast("logger", new LoggingFilter());
        //  将二进制或协议特定的数据转换为消息对象
        chain.addLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"))));

        acceptor.setHandler(new MemoryMonitorHandler(this));

        DatagramSessionConfig dcfg = acceptor.getSessionConfig();

        dcfg.setReadBufferSize(2048);
        dcfg.setIdleTime(IdleStatus.BOTH_IDLE, 10);

        dcfg.setReuseAddress(true);
        acceptor.bind(new InetSocketAddress(SimpleServer.PORT));

    }

    public void addClient(SocketAddress remoteAddress) {
        list.add(remoteAddress);
    }

    public void recvUpdate(SocketAddress remoteAddress, long aLong) {
        list.set(Math.toIntExact(aLong), remoteAddress);
    }

    public void removeClient(SocketAddress remoteAddress) {
        list.remove(remoteAddress);
    }
}
