package mina.dome1.chapter2.handler;

import mina.dome1.chapter2.udp.SimpleUDPServer;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import java.net.SocketAddress;

public class MemoryMonitorHandler extends IoHandlerAdapter {

    private SimpleUDPServer server;

    public MemoryMonitorHandler(SimpleUDPServer simpleUDPServer) {
        server = simpleUDPServer;
    }

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        SocketAddress remoteAddress = session.getRemoteAddress();
        server.addClient(remoteAddress);
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        if (message instanceof IoBuffer) {
            IoBuffer buffer = (IoBuffer) message;
            SocketAddress remoteAddress = session.getRemoteAddress();
            server.recvUpdate(remoteAddress, buffer.getLong());
        }
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        System.out.println("Session closed...");
        SocketAddress remoteAddress = session.getRemoteAddress();
        server.removeClient(remoteAddress);
    }


}
