package mina.dome1.chapter2.udp;

public class Main {
    public static void main(String[] args) throws Throwable {
        SimpleUDPServer server = new SimpleUDPServer();
        server.main();

        Thread.sleep(3000);

        SampleUDPClient client = new SampleUDPClient();
        client.main();
    }
}
