import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * @author: ziv
 * @date: 2018/12/6 13:14
 * @version: 0.0.1
 */
public class AppTest {

    @Test
    public void load() {
        Writer writer = null;
        try {
            Velocity.init();
            SAXBuilder builder;
            Document root = null;

            try {
                builder = new SAXBuilder();
                root = builder.build("test.xml");
            } catch (Exception ee) {
                System.out.println("Exception building Document : " + ee);
                return;
            }

            VelocityContext context = new VelocityContext();
            context.put("root", root);
            Template template = Velocity.getTemplate("");

            writer = new BufferedWriter(new OutputStreamWriter(System.out));
            template.merge(context, writer);
        } catch (Exception e) {
            System.out.println("Exception : " + e);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ee) {
                    System.out.println("Exception : " + ee);
                }
            }
        }
    }
}
