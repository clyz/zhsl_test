package com.apa.velocity;

import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class VelocityApplicationTests {

    @Autowired
    private VelocityEngine velocityEngine;

    @Test
    public void velocityTest() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("message", "这是测试的内容。。。");
        model.put("toUserName", "张三");
        model.put("fromUserName", "老许");
        model.put("time", new Date());
        System.out.println(VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "index.vm", "UTF-8", model));
    }

}
