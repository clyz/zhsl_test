package clyz.ziv.cxfsb.service;

import clyz.ziv.cxfsb.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

/**
 * @author YangZemin
 * @date 2020/3/20 14:39
 */
public interface UserService {

    @WebMethod//标注该方法为webservice暴露的方法,用于向外公布，它修饰的方法是webservice方法，去掉也没影响的，类似一个注释信息。
    public User getUser(@WebParam(name = "userId") String userId);

    @WebMethod
    @WebResult(name = "String", targetNamespace = "")
    public String getUserName(@WebParam(name = "userId") String userId);

}