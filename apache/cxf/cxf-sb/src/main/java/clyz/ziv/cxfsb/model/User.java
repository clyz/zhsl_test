package clyz.ziv.cxfsb.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author YangZemin
 * @date 2020/3/20 14:37
 */
@Data
@ToString
public class User implements Serializable {
    private static final long serialVersionUID = -3628469724795296287L;
    private String userId;
    private String userName;
    private String email;
}
