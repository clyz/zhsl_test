package com.wisdom.lucence.core772;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

public class VisLucence {

    @Test
    public void init() throws IOException {
        FSDirectory directory = FSDirectory.open(Paths.get("D:\\xo"));
        Analyzer analyzer = new IKAnalyzer(true);
        IndexWriterConfig iwConfig = new IndexWriterConfig(analyzer);
        iwConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        IndexWriter iwriter = new IndexWriter(directory, iwConfig);

        File file = new File("D:\\uploadFiles");
        Stream.of(Objects.requireNonNull(file.listFiles())).map(f -> {
            Document doc = new Document();
            //  设置 long 数据类型 Field.Store.YES
            doc.add(new StoredField("StoredField", f.length()));
            //  Field.Store.NO,SORT
            doc.add(new NumericDocValuesField("NumericDocValuesField", f.length()));
            //  Field.Store.NO
            doc.add(new LongRange("LongRange", new long[2], new long[2]));
            //  Field.Store.NO
            doc.add(new LongPoint("LongPoint", f.length()));
            //Field.Store.NO,SORT
            doc.add(new FloatDocValuesField("FloatDocValuesField", f.length() / 33));
            doc.add(new FeatureField("FeatureField", "FeatureFieldTerm", 0.9f));
            //不分词
            doc.add(new StringField("StringField", f.getName(), Field.Store.YES));
            //  Field.Store.YES
            doc.add(new StoredField("StoredField", f.isDirectory() ? "0" : "1"));

            FieldType fieldType = new FieldType();
            fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            fieldType.setStored(true);
            doc.add(new Field("StoredFieldX", f.isDirectory() ? "0" : "1", fieldType));

            /*try {
                doc.add(new Field("FieldType", f.getCanonicalPath(), fieldType));
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            /*try {
                doc.add(new Field("FieldType",FileUtils.readFileToString(f,"utf-8"),fieldType));
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            return doc;
        }).forEach(indexableFields -> {
            try {
                System.out.println(indexableFields.get("StringField"));
                iwriter.addDocument(indexableFields);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        iwriter.close();
    }

    @Test
    public void search() throws IOException, ParseException {
        FSDirectory directory = FSDirectory.open(Paths.get("D:\\xo"));
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = new IKAnalyzer(true);

        QueryParser parser = new QueryParser("StringField", analyzer);
        parser.setAllowLeadingWildcard(true);
        Query query = parser.parse("*");
        /**  自定义排序 */
//        TopDocs docs = searcher.search(query, Integer.MAX_VALUE, Sort.INDEXORDER);

        SortField storedField = new SortField("StoredField", SortField.Type.LONG, true);
        storedField.rewrite(searcher);
        Sort sort = new Sort(storedField);
        sort.rewrite(searcher);

        TopDocs docs = searcher.search(query, Integer.MAX_VALUE,
                new Sort(
                        new SortField("StoredField", SortField.Type.LONG, true),
                        SortField.FIELD_SCORE
                )
        );


/*        Query query = new BooleanQuery.Builder()
                .add(new TermQuery(new Term("StringField", "animal-sniffer-annotations-1.14.jar")), BooleanClause.Occur.SHOULD)
                .add(new TermQuery(new Term("StringField", "*.jar")), BooleanClause.Occur.SHOULD)
                .build();
        Query boost = FeatureField.newSaturationQuery("FeatureField", "FeatureFieldTerm");
        Query boostedQuery = new BooleanQuery.Builder()
                .add(query, BooleanClause.Occur.MUST)
                .add(boost, BooleanClause.Occur.SHOULD)
                .build();
        TopDocs docs = searcher.search(boostedQuery, Integer.MAX_VALUE);*/


        /*  权重    */
 /*       Map<String, Float> boosts = new HashMap<>();
        boosts.put("StoredField", 10.0f);
        MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"StringField"}, analyzer, boosts);//指定搜索权重
        parser.setAllowLeadingWildcard(true);
        Query query = parser.parse("StoredField:0 StringField:*.jar");
        TopDocs docs = searcher.search(query, Integer.MAX_VALUE);*/

        ScoreDoc[] scoreDocs = docs.scoreDocs;
        for (int i = 0; i < scoreDocs.length; i++) {
            System.out.print(" DOC::" + scoreDocs[i].doc + "\t\t");
            Document targetDoc = searcher.doc(scoreDocs[i].doc);
            targetDoc.getFields().forEach(f -> {
                System.out.print("[" + f.name() + "] " + f.stringValue() + "\t\t");
            });
            System.out.println();
        }
        reader.close();
    }
}
