package ziv.clyz.lucene.solr.lucenesolrsb.service;

import ziv.clyz.lucene.solr.lucenesolrsb.bean.User;

import java.util.List;

public interface SolrService {
    List<User> addUser();
}
