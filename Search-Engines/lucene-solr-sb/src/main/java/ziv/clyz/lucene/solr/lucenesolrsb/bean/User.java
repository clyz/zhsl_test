package ziv.clyz.lucene.solr.lucenesolrsb.bean;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.SolrDocument;

@Data
@SolrDocument //solrj不需要加这个，加上也不报错，我主要是该项目jpa和solrj都可以，我就把实体类弄成了这样。
public class User {

    @Field
    private String id;

    @Field
    private String name;

    @Field("content_type")
    private String sex;

    @Field("cat")
    private String address;

    @Field("links")
    private Integer host;

}
