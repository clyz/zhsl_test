package ziv.clyz.lucene.solr.lucenesolrsb.Repository;

import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;
import ziv.clyz.lucene.solr.lucenesolrsb.bean.User;

import java.util.List;

public interface MarkSolrRepository  extends SolrCrudRepository<User, String> {

    @Query("*:*")
    List<User> findAll();

}
