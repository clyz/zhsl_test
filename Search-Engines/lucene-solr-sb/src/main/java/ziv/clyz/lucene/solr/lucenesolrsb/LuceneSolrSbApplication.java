package ziv.clyz.lucene.solr.lucenesolrsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@SpringBootApplication
@EnableSolrRepositories
public class LuceneSolrSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuceneSolrSbApplication.class, args);
    }

}
