package ziv.clyz.lucene.solr.lucenesolrsb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LuceneSolrSbApplicationTests {

    @Test
    public void contextLoads() {
    }

}
