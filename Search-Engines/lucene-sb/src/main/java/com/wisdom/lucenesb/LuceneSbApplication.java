package com.wisdom.lucenesb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuceneSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuceneSbApplication.class, args);
    }
}
