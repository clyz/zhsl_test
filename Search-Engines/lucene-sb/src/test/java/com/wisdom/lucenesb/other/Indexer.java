package com.wisdom.lucenesb.other;

import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.nio.file.Paths;

public class Indexer {

    private String descs[] = {
            "科比·布莱恩特，1978年8月23日出生于宾夕法尼亚州费城，前美国著名职业篮球运动员，司职得分后卫小前锋（锋卫摇摆人）。自1996年起，科比一直效力于NBA洛杉矶湖人队，直到2015-2016年赛季结束正式退役。在长达20年的NBA生涯中，科比5次获得NBA总冠军，两次获得总决赛MVP（2009年和2010年），1次常规赛MVP（2008年），17次入选全明星阵容，其中4次获得全明星MVP。[1]NBA总得分达到33643分，场均25分，4.7次助攻，5.2个篮板。[2]曾11次入选NBA最佳阵容一队，9次入选NBA最佳防守阵容一队。在2005-2006赛季对多伦多猛龙的比赛中，拿下单场职业生涯最高的81分，位列NBA历史单场得分第二位（仅次于张伯伦100分）。2008年和2012年代表美国国家队两次获得奥运会金牌。",
            "姚明，1980年9月12日出生于上海市徐汇区，曾经是一位中国职业篮球运动员，司职中锋，曾效力于休斯敦火箭队。现任中职联公司董事长兼总经理，中国篮球运动学院名誉院长。1998年4月，姚明入选王非执教的国家队，开始篮球生涯。姚明7次获得NBA“全明星”，被中国体育总局授予“体育运动荣誉奖章”、“中国篮球杰出贡献奖”。。"

    };
    private Directory dir;
    private Integer ids[] = {1, 2};
    private String names[] = {"科比", "姚明"};

    public static void main(String[] args) throws Exception {
        new Indexer().index("F:\\");
    }

    /**
     * 生成索引
     *
     * @param indexDir
     * @throws Exception
     */
    private void index(String indexDir) throws Exception {
        dir = FSDirectory.open(Paths.get(indexDir));
        IndexWriter writer = getWriter();
        for (int i = 0; i < ids.length; i++) {
            Document doc = new Document();
            doc.add(new IntField("id", ids[i], Field.Store.YES));
            doc.add(new StringField("name", names[i], Field.Store.YES));
            doc.add(new TextField("desc", descs[i], Field.Store.YES));
            writer.addDocument(doc); // 添加文档
        }
        writer.close();
    }

    /**
     * 获取IndexWriter实例
     *
     * @return
     * @throws Exception
     */
    private IndexWriter getWriter() throws Exception {
        //Analyzer analyzer=new StandardAnalyzer(); // 标准分词器
        SmartChineseAnalyzer analyzer = new SmartChineseAnalyzer();//中文分词器
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        IndexWriter writer = new IndexWriter(dir, iwc);
        return writer;
    }

}