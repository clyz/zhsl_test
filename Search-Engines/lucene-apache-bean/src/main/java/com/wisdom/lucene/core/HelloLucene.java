package com.wisdom.lucene.core;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class HelloLucene {

    FSDirectory directory = FSDirectory.open(Paths.get("D:\\luence"));

    String path = "D:\\luence";

    public HelloLucene() throws IOException {
    }

    /**
     * <p>
     * 写入文档内容
     * </p>
     * <p>
     * Field.Store.YES或者NO（存储域选项）
     * YES:表示会把这个域中的内容完全存储到文件中，方便进行还原[对于主键，标题可以是这种方式存储]
     * NO:表示把这个域的内容不存储到文件中，但是可以被索引，此时内容无法完全还原（doc.get()）[对于内容而言，没有必要进行存储，可以设置为No]
     * </p>
     *
     * @throws IOException
     */
    public void indexWriter() throws IOException {
        final Path path = Paths.get("D:\\luence");
        Directory directory = FSDirectory.open(path);

        Analyzer analyzer = new StandardAnalyzer(); //  创建一个分词器
        analyzer.setVersion(Version.LUCENE_8_1_1);

        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);   //  将会影响 删除

        try (IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig)) {
            final File file = new File("D:\\luence\\file");
            for (File f : Objects.requireNonNull(file.listFiles())) {
                try (InputStream inputStream = Files.newInputStream(Paths.get(f.getAbsolutePath()))) {
                    final Document document = new Document();
                    document.add(new TextField("contents",
                            new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))));
                    document.add(new StringField("path", f.getAbsolutePath(), Field.Store.YES));

                    document.add(new Field("name", f.getName(), StringField.TYPE_STORED));
                    indexWriter.addDocument(document);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void indexReader() throws IOException, ParseException {
        final FSDirectory directory = FSDirectory.open(Paths.get("D:/uploadFiles/search_SYSTEM"));
        IndexReader indexReader = DirectoryReader.open(directory);
        final IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        QueryParser parser = new QueryParser("contents", new StandardAnalyzer());

        final TopDocs topDocs = indexSearcher.search(parser.parse("java"), 10);
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            final Document doc = indexSearcher.doc(scoreDoc.doc);
            System.out.print(doc.get("contents") + "\t");
            System.out.print(doc.get("path") + "\t");
            System.out.print(doc.get("name"));
            System.out.println();
        }
        indexReader.close();
    }

    public void indexWriter2() throws IOException {
        final Path path = Paths.get(this.path);
        Directory directory = FSDirectory.open(path);

        Analyzer analyzer = new SmartChineseAnalyzer(); //  创建一个分词器
        analyzer.setVersion(Version.LUCENE_8_1_1);

        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try (IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig)) {
            final File file = new File("D:\\luence\\file");
            int i = 1000;
            for (File f : Objects.requireNonNull(file.listFiles())) {
                final Document document = new Document();
                document.add(new StringField("id", (i++) + "", Field.Store.YES));

                final StringField stringFieldPath = new StringField("path", f.getAbsolutePath(), Field.Store.NO);
                document.add(stringFieldPath);

                /*  索引策略 */
                FieldType fieldType = new FieldType();
                fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
                fieldType.setStored(true);  //  =  Field.Store.YES
                final Field fieldName = new Field("name", f.getName(), fieldType);
                document.add(fieldName);

                /*  索引策略 */
                FieldType fieldTypeText = new FieldType();
                fieldTypeText.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
                fieldTypeText.setStored(false);
                fieldTypeText.setTokenized(true);
                fieldTypeText.freeze();

                final Field fieldContents = new Field("contents",
                        FileUtils.readFileToString(f, Charset.defaultCharset()), fieldTypeText);


                /*TextField fieldContents = new TextField("contents",
                        FileUtils.readFileToString(f, Charset.defaultCharset()),
                        Field.Store.NO
                );*/
                document.add(fieldContents);
                indexWriter.addDocument(document);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void indexReader2(String parserf, String queryString) throws IOException, ParseException {
        final FSDirectory directory = FSDirectory.open(Paths.get(path));
        IndexReader indexReader = DirectoryReader.open(directory);

        final IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        QueryParser parser = new QueryParser(parserf, new SmartChineseAnalyzer());
        //  如果在分析器从空格分隔的文本返回多个术语时将自动生成短语查询，则设置为true。
        parser.setAutoGeneratePhraseQueries(false);
        //  在分析之前，查询文本是否应拆分为空白。
        parser.setSplitOnWhitespace(true);
        //设置为true以允许前导通配符。    耗性能
        parser.setAllowLeadingWildcard(true);
        //设置为true可在结果查询中启用位置增量。
        parser.setEnablePositionIncrements(true);

        Query query = parser.parse(queryString);
//        final Weight weight = query.createWeight(indexSearcher, ScoreMode.TOP_SCORES, 0.5f);

//        Sort sort = new Sort(new SortField("contents", SortField.Type.DOC));//  sort 排序,会影响 score

        ScoreDoc[] hits = indexSearcher.search(query, 10).scoreDocs;
        for (int i = 0; i < hits.length; i++) {
            Document hitDoc = indexSearcher.doc(hits[i].doc);
            System.out.print(hits[i].score + "\t");
            for (IndexableField indexableField : hitDoc) {
                System.out.print(indexableField.stringValue() + "\t");
            }
            System.out.println();
        }
        indexReader.close();
    }

    /**
     * <p>
     * 获取文档 总信息 如同获取 MYSQL 一张表的一条数据信息
     * </p>
     *
     * @throws IOException
     */
    public void query() throws IOException {
        IndexReader indexReader = DirectoryReader.open(directory);
        System.out.println("存储的文档数:" + indexReader.numDocs());
        System.out.println("总存储量:" + indexReader.maxDoc());
        System.out.println("被删除的文档：" + indexReader.numDeletedDocs());
        indexReader.close();
    }

    /**
     * <p>
     * 删除索引 ， 同时 文件模式下
     * 其以
     * cfe,cfs,si
     * 结尾的文件都没啦
     * </p>
     *
     * @throws IOException
     */
    public void deleteAll() throws IOException {
        final IndexWriter indexWriter = getIndexWriter();
        indexWriter.deleteAll();
        indexWriter.close();
    }

    /**
     * <p>
     * 精确查询 Term
     * 删除
     * </p>
     *
     * @throws IOException
     */
    public void deleteTerm(String parserf, String queryString) throws IOException {
        final Term term = new Term(parserf, queryString);
        final IndexWriter indexWriter = getIndexWriter();
        final long l = indexWriter.deleteDocuments(term);
        System.out.println("IndexWriter.deleteDocuments(term) " + l);
        indexWriter.commit();
        indexWriter.close();
    }

    public void deleteQuery(String parserf, String queryString) throws IOException, ParseException {
        final IndexWriter indexWriter = getIndexWriter();
        final QueryParser parser = new QueryParser(parserf, new StandardAnalyzer());
        final Query parse = parser.parse(queryString);
        final long l = indexWriter.deleteDocuments(parse);
        System.out.println("IndexWriter.deleteDocuments(parse): " + l);
        indexWriter.commit();   //  提交
        indexWriter.close();
    }

    public void deleteQueryRollback(String parserf, String queryString) throws IOException, ParseException {
        final IndexWriter indexWriter = getIndexWriter();
        final QueryParser parser = new QueryParser(parserf, new StandardAnalyzer());
        final Query parse = parser.parse(queryString);
        final long l = indexWriter.deleteDocuments(parse);
        System.out.println("IndexWriter.deleteDocuments(parse): " + l);
//        indexWriter.forceMergeDeletes();    //  清空回车站数据,及合并
        indexWriter.rollback(); //  回滚。
        indexWriter.close();
    }

    /**
     * <p>
     * 更新 删除 后在添加
     * </p>
     *
     * @throws IOException
     * @throws ParseException
     */
    public void update() throws IOException, ParseException {
        final IndexWriter indexWriter = getIndexWriter();
        final File file = new File("D:\\luence\\file");

        int i = 1000;
        for (File f : Objects.requireNonNull(file.listFiles())) {
            final Document document = new Document();
            document.add(new StringField("id", i + "", Field.Store.YES));
            document.add(new StringField("path", f.getAbsolutePath(), Field.Store.NO));

            /*  索引策略 */
            FieldType fieldType = new FieldType();
            fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            fieldType.setStored(true);  //  =  Field.Store.YES
            final Field fieldName = new Field("name",
                    f.getName().substring(0, f.getName().lastIndexOf(".")), fieldType);
            document.add(fieldName);

            /*  索引策略 */
            FieldType fieldTypeText = new FieldType();
            fieldTypeText.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            fieldTypeText.setStored(true);
            fieldTypeText.setTokenized(true);
            fieldTypeText.freeze();
            new FileReader(f);

            document.add(new Field("contents",
                    FileUtils.readFileToString(f, Charset.defaultCharset()), fieldTypeText));

            indexWriter.updateDocument(new Term("id", (i++) + ""), document);   //  更新
        }


        indexWriter.commit();   //  提交
        indexWriter.close();
    }

    private IndexWriter getIndexWriter() throws IOException {
        final IndexWriterConfig indexWriterConfig = new IndexWriterConfig();
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        return new IndexWriter(directory, indexWriterConfig);
    }

}
