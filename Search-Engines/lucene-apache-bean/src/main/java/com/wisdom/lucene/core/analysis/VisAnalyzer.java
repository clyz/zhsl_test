package com.wisdom.lucene.core.analysis;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 * <p>
 * 自定义 分词 解析器
 * </p>
 */
public class VisAnalyzer extends Analyzer {

    private CharArraySet stops;

    public VisAnalyzer(String[] sws) {
        //  自定义 分词过滤
        stops = StopFilter.makeStopSet(sws, true);
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        //  为分词器设定 过滤链
        final StandardTokenizer src = new StandardTokenizer();
        src.setMaxTokenLength(255);
        TokenStream tok = new LowerCaseFilter(src);

        //  过滤 {@code stops} 中的字符
        StopFilter stopFilter = new StopFilter(new LowerCaseFilter(tok), stops);

        return new TokenStreamComponents(src, stopFilter);
    }

}
