package com.wisdom.lucene.core.analysis;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

import java.io.IOException;
import java.io.StringReader;

/**
 * <p>
 * 查看分词 的一些 信息
 * </p>
 */
public class AnalysisUtil {

    public static void displayToken(String str, Analyzer analyzer) throws IOException {
        TokenStream tokenStream = analyzer.tokenStream("content", new StringReader(str));
        tokenStream.reset();

        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);    //  分成的词片
        while (tokenStream.incrementToken()) {
            System.out.print("[" + charTermAttribute + "]");
        }
        System.out.println();
        tokenStream.close();
    }

    public static void displayAllTokenInfo(String str, Analyzer analyzer) throws IOException {
        TokenStream tokenStream = analyzer.tokenStream("content", new StringReader(str));
        tokenStream.reset();

        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);    //  分成的词片
        TypeAttribute typeAttribute = tokenStream.addAttribute(TypeAttribute.class);    //  类型信息
        PositionIncrementAttribute positionIncrementAttribute = tokenStream
                .addAttribute(PositionIncrementAttribute.class);    //  位置 信息
        OffsetAttribute offsetAttribute = tokenStream.addAttribute(OffsetAttribute.class);  //  偏移量 信息
        while (tokenStream.incrementToken()) {
            System.out.print("[" + charTermAttribute + "]  ");
            System.out.print(positionIncrementAttribute.getPositionIncrement() + ": ");
            System.out.print("(" + offsetAttribute.startOffset() + "," + offsetAttribute.endOffset() + ")  ");
            System.out.print("//" + typeAttribute.type() + "//");
        }
        System.out.println();
        tokenStream.close();
    }
}
