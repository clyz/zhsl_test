package com.wisdom.lucene.core.analysis;

import com.chenlb.mmseg4j.analysis.MMSegAnalyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.junit.Test;

import java.io.IOException;

public class AnalysisUtilTest {

    @Test
    public void displayAllTokenInfoMMseg() throws IOException {
        MMSegAnalyzer mmSegAnalyzer = new MMSegAnalyzer();
        String txt = "你好呀！我是杨泽民，😄你啦";
        AnalysisUtil.displayAllTokenInfo(txt, mmSegAnalyzer);
    }


    /**
     * <p>
     * 分词器
     * </p>
     *
     * @throws IOException
     */
    @Test
    public void displayToken() throws IOException {
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
//        StopAnalyzer stopAnalyzer = new StopAnalyzer();
        SimpleAnalyzer simpleAnalyzer = new SimpleAnalyzer();
        WhitespaceAnalyzer whitespaceAnalyzer = new WhitespaceAnalyzer();

        String txt = "this is my house,I am come from Chi chuan";
        AnalysisUtil.displayToken(txt, standardAnalyzer);
        AnalysisUtil.displayToken(txt, simpleAnalyzer);
        AnalysisUtil.displayToken(txt, whitespaceAnalyzer);
    }

    @Test
    public void displayAllTokenInfo() throws IOException {
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
//        StopAnalyzer stopAnalyzer = new StopAnalyzer();
        SimpleAnalyzer simpleAnalyzer = new SimpleAnalyzer();
        WhitespaceAnalyzer whitespaceAnalyzer = new WhitespaceAnalyzer();

        String txt = "this is my house,I am come from Chi chuan";
        AnalysisUtil.displayAllTokenInfo(txt, standardAnalyzer);
        AnalysisUtil.displayAllTokenInfo(txt, simpleAnalyzer);
        AnalysisUtil.displayAllTokenInfo(txt, whitespaceAnalyzer);
    }
}