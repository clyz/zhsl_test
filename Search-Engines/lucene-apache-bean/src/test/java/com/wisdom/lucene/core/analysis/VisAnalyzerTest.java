package com.wisdom.lucene.core.analysis;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class VisAnalyzerTest {

    @Test
    public void createComponents() throws IOException {
        String txt = "this is my house,I am come from Chi chuan";
        VisAnalyzer visAnalyzer = new VisAnalyzer(new String[]{"this","CHI","I"});
        AnalysisUtil.displayToken(txt, visAnalyzer);
    }

}