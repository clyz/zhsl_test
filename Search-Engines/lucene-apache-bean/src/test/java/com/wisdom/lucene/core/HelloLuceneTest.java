package com.wisdom.lucene.core;

import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class HelloLuceneTest {

    private HelloLucene helloLucene = null;

    @Before
    public void before() throws IOException {
        helloLucene = new HelloLucene();
    }

    @Test
    public void indexWriter() throws IOException {
        new HelloLucene().indexWriter();
    }

    @Test
    public void indexReader() throws IOException, ParseException {
        new HelloLucene().indexReader();
    }

    @Test
    public void indexWriter2() throws IOException, ParseException {
        new HelloLucene().indexWriter2();
        new HelloLucene().indexReader2("id", "name:哈");
    }

    /**
     * contents,path,name,id
     *
     * @throws IOException
     * @throws ParseException
     */
    @Test
    public void indexReader2() throws IOException, ParseException {
//        new HelloLucene().indexReader2("id", "100"); // id 包含 100
//        new HelloLucene().indexReader2("id", "100*");// 默认查询的域，查询条件
//        new HelloLucene().indexReader2("content", "subject:淳港"); //  name:1 域：内容
//        new HelloLucene().indexReader2("id", "1000 1001"); //   1000 或 1001
        new HelloLucene().indexReader2("id", "name:哈"); //  name:1 域：内容
//        new HelloLucene().indexReader2("id", "contents:sdf AND name:1");    //  contents域包含sdf 并且 name域包含1
//        new HelloLucene().indexReader2("id", "contents:sdf name:1");//  contents域包含sdf 或 name域包含1
//        new HelloLucene().indexReader2("id", "contents:sdf name:?");    //  parser.setAllowLeadingWildcard(true);开启前导通配符
//        new HelloLucene().indexReader2("id", "contents:sdf");
//        new HelloLucene().query();
    }

    @Test
    public void query() throws IOException {
        helloLucene.query();
    }

    @Test
    public void deleteAll() throws IOException {
        helloLucene.deleteAll();
    }

    @Test
    public void deleteTerm() throws IOException, ParseException {
        helloLucene.indexWriter2();
        helloLucene.indexReader2("id", "1000");
        helloLucene.query();
        helloLucene.deleteTerm("id", "1000");
        helloLucene.query();
        helloLucene.indexReader();
    }

    @Test
    public void deleteQuery() throws IOException, ParseException {
        helloLucene.indexWriter2();
        helloLucene.indexReader2("id", "1000");
        helloLucene.query();
        helloLucene.deleteQuery("id", "1000");
        helloLucene.query();
    }

    @Test
    public void deleteQueryRollback() throws IOException, ParseException {
        helloLucene.indexWriter2();
        helloLucene.indexReader2("id", "1000");
        helloLucene.query();
        helloLucene.deleteQueryRollback("id", "1000");
        helloLucene.query();
    }

    @Test
    public void update() throws IOException, ParseException {
        helloLucene.update();
        helloLucene.indexReader2("id", "1000");
    }
}