package ziv.clyz.design.observer.observerwebsocketsb.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ziv.clyz.design.observer.observerwebsocketsb.observer.ImportHolder;
import ziv.clyz.design.observer.observerwebsocketsb.observer.WebSocket;

import java.util.Random;

@Service
@Slf4j
public class ServiceImpl {

    @Autowired
    private WebSocket webSocket;

    @Async
    public void save(Long id){
        log.info("等待处理中，5000s {}",id);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("等待完毕，5000s {}",id);
        ImportHolder holder = new ImportHolder();
        holder.setId(id);

        holder.addObserver(webSocket);
        holder.init(10);
        final Random random = new Random();

        for (int i = 0; i < holder.getTotal(); i++) {
            if (random.nextInt(10) > 5) {
                holder.addOk(i + "");
            } else {
                holder.addError(i + "");
            }
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
