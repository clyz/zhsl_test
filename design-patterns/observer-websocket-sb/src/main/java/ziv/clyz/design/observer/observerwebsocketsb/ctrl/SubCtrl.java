package ziv.clyz.design.observer.observerwebsocketsb.ctrl;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ziv.clyz.design.observer.observerwebsocketsb.vo.Message;
import ziv.clyz.design.observer.observerwebsocketsb.vo.Response;

@Controller
@RequestMapping("sub")
@Slf4j
public class SubCtrl {
    @Autowired
    SimpMessagingTemplate template;

    /**
     * 浏览器发送请求通过@messageMapping 映射/welcome 这个地址。
     * 服务器端有消息时,会订阅@SendTo 中的路径的浏览器发送消息。
     *
     * @param message
     * @return
     * @throws Exception
     */
    @MessageMapping("/welcome")
    @SendTo("/topic/getResponse")
    public Response say(Message message) throws Exception {
        Thread.sleep(1000);
        return new Response("Welcome, " + message.getName() + "!");
    }

    /**
     * 当有客户端订阅"/topic/getResponse"，会收到消息
     *
     * @return
     */
    @SubscribeMapping("/topic/getResponse")
    public Response sub() {
        log.info("XXX用户订阅了我。。。");
        return new Response("感谢你订阅了我。。。");
    }

    @GetMapping("/test")
    String test() {
        return "test";
    }

    /**
     * 可以利用普通http request来主动推送广播消息
     *
     * @return
     */
    @RequestMapping("/welcome")
    @ResponseBody
    public String say02() {
        try {
            template.convertAndSend("/topic/getResponse", new Response("欢迎！"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "OK";
    }
}