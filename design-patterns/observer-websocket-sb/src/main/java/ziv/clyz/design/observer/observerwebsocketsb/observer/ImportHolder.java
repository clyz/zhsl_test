package ziv.clyz.design.observer.observerwebsocketsb.observer;

import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class ImportHolder extends Observable implements Serializable {

    private static final long serialVersionUID = -6451812593150428369L;

    private Long id;

    private List<String> ok;

    private List<String> error;

    private int total;

    public List<String> getOk() {
        return ok;
    }

    public void setOk(List<String> ok) {
        this.ok = ok;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ImportHolder{" +
                "ok_SIZE=" + ok.size() +
                ", error_SIZE=" + error.size() +
                ", total=" + total +
                ",id=" + id +
                '}';
    }

    public void init(int total) {
        this.total = total;
        this.ok = new ArrayList<String>();
        this.error = new ArrayList<String>();
    }

    public void addOk(String e) {
        ok.add(e);
        setChanged();
        notifyObservers();
    }

    public void addError(String e) {
        error.add(e);
        setChanged();
        notifyObservers();
    }


    /**
     * 计算错误数
     *
     * @return 错误数
     */
    public int getErrorNum() {
        if (ObjectUtils.isEmpty(error)) {
            return 0;
        }
        return error.size();
    }

    /**
     * 计算成功数
     *
     * @return 成功数量
     */
    public int getSuccCount() {
        return (total - getErrorNum()) < 0 ? 0 : total - getErrorNum();
    }

}
