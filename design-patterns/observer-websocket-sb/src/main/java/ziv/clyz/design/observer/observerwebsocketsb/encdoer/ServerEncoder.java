package ziv.clyz.design.observer.observerwebsocketsb.encdoer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import ziv.clyz.design.observer.observerwebsocketsb.observer.ImportHolder;

import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

@Slf4j
public class ServerEncoder implements Encoder.Text<ImportHolder> {

    static ObjectMapper objectMapper = new ObjectMapper();

    static {
        // 转换为格式化的json
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        // 如果json中有新增的字段并且是实体类类中不存在的，不报错
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public String encode(ImportHolder holder) {
        log.info("ID: {}  encoder ... ", holder.getId());
        try {
            return objectMapper.writeValueAsString(holder);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
