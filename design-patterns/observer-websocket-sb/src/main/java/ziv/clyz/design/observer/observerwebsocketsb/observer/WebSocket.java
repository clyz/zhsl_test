package ziv.clyz.design.observer.observerwebsocketsb.observer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ziv.clyz.design.observer.observerwebsocketsb.encdoer.ServerEncoder;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;


@ServerEndpoint(value = "/websocket/{sid}",encoders = {ServerEncoder.class})
@Slf4j
@Component
public class WebSocket implements Observer {

    private static Map<Long, Session> sessionMap = new ConcurrentHashMap<>();

    public void sendMessage(ImportHolder holder) {
        log.info(holder.toString());
        try {
            if (sessionMap.containsKey(holder.getId())) {
                sessionMap.get(holder.getId()).getBasicRemote().sendObject(holder);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("sid") long sid) throws Exception {
        log.info("开启一个连接 sid {}", sid);
        if (!sessionMap.containsKey(sid)) {
            sessionMap.put(sid, session);
        }
        log.info("连接成功 {}", sid);
    }

    public void update(Observable o, Object arg) {
        sendMessage((ImportHolder) o);
    }

    @OnClose
    public void onClose() throws Exception {
        System.out.println("Close");
    }

    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

}
