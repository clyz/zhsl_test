package ziv.clyz.design.observer.observerwebsocketsb.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Random;

@Controller
@RequestMapping("/send")
public class H5SendServer {

    public static volatile String A;

    @RequestMapping(value = "/serverSend.do/{id}")
    public void serverSend(HttpServletResponse response, @PathVariable long id) {
        response.setContentType("text/event-stream");
        response.setCharacterEncoding("UTF-8");
        try {
            set();
            response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, accept, content-type, xxxx");
            response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
            response.setHeader("Access-Control-Allow-Origin", "*");
            PrintWriter writer = response.getWriter();
            writer.write("data:" + "{\"id\":\"" + A + "\"} \n\n");//这里需要\n\n，必须要，不然前台接收不到值,键必须为data
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void set() {
        new Thread(() -> {
            while (true) {
                try {
                    final int i = new Random().nextInt(10) * 1000;
                    System.out.println(i);
                    Thread.sleep(i);
                    A = String.valueOf(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
