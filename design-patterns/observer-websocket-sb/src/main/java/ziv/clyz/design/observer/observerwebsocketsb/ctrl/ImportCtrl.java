package ziv.clyz.design.observer.observerwebsocketsb.ctrl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ziv.clyz.design.observer.observerwebsocketsb.service.impl.ServiceImpl;

@Slf4j
@RequestMapping("ctrl")
@RestController
public class ImportCtrl {

    @Autowired
    private ServiceImpl service;



    /**
     * <p>
     * 传入 保存数据
     * </p>
     *
     * @param id
     * @return
     */
    @GetMapping("/socket/{id}")
    public long start(@PathVariable("id") long id) {
        log.info("OK___");
        service.save(id);
        return id;
    }

}
