package ziv.clyz.design.observer.observerwebsocketsb.vo;

/**
 * 客户端发往服务器消息实体
 */
public class Message {
    private String name;

    public String getName() {
        return name;
    }
}