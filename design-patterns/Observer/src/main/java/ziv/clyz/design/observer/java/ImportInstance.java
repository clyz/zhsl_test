package ziv.clyz.design.observer.java;

import java.util.Observable;
import java.util.Observer;

public class ImportInstance implements Observer {

    public void update(Observable o, Object arg) {
        ImportHolder myObserable = (ImportHolder) o;
        System.out.println(myObserable.toString());
        System.out.println(arg);
        System.out.println("--------------------");
        System.out.println("--------------------");
    }

}
