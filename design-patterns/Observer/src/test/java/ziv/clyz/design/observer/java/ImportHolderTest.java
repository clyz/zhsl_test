package ziv.clyz.design.observer.java;

import java.util.Random;

public class ImportHolderTest {

    public static void main(String[] args) throws InterruptedException {

        ImportHolder holder = new ImportHolder();

        holder.addObserver(new ImportInstance());
        holder.init(10);
        final Random random = new Random();

        for (int i = 0; i < holder.getTotal(); i++) {
            if (random.nextInt(10) > 5) {
                holder.addOk(i + "");
            } else {
                holder.addError(i + "");
            }
            Thread.sleep(1500);
        }

    }

}