package ziv.clyz.design.strategy.base.factory;

import ziv.clyz.design.strategy.EX;
import ziv.clyz.design.strategy.base.strategy.Achievement;
import ziv.clyz.design.strategy.base.strategy.AverageAchievement;
import ziv.clyz.design.strategy.base.strategy.ExcellentAchievement;
import ziv.clyz.design.strategy.base.strategy.GoodAchievement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class FactoryStrategy {
    private FactoryStrategy() {

    }

    private final static Map<String, Achievement> conditionMaps;

    private final static Collection<Achievement> conditions;

    static {
        conditions = new ArrayList<>();
        conditions.add(new AverageAchievement());
        conditions.add(new ExcellentAchievement());
        conditions.add(new GoodAchievement());


        conditionMaps = new ConcurrentHashMap<>();
        conditionMaps.put(Type.AVERAGE.getValue(), new AverageAchievement());
        conditionMaps.put(Type.EXCELLENT.getValue(), new ExcellentAchievement());
        conditionMaps.put(Type.GOOD.getValue(), new GoodAchievement());
    }

    public static boolean judge(String context) {
        for (Achievement cond : conditions) {
            if (cond.getAchievement(context)) {
                return true;
            }
        }
        return false;
    }

    public static Achievement achievement(String key) throws EX {
        Optional.ofNullable(key).orElseThrow(() -> new EX());
        if (conditionMaps.containsKey(key)) {
            return conditionMaps.get(key);
        }
        throw new EX();
    }

    public enum Type {
        AVERAGE("C"), EXCELLENT("A"), GOOD("B");

        private String value;

        Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
