package ziv.clyz.design.strategy.base.strategy;

@FunctionalInterface
public interface Achievement {
    boolean getAchievement(String value);
}
