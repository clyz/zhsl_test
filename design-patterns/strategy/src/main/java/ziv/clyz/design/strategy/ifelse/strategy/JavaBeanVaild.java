package ziv.clyz.design.strategy.ifelse.strategy;

import ziv.clyz.design.strategy.ifelse.factory.FactoryStrategy;

public class JavaBeanVaild implements IStrategy {

    @Override
    public boolean click(String type) {
        return FactoryStrategy.Type.JAVA.getValue().equals(type);
    }

}
