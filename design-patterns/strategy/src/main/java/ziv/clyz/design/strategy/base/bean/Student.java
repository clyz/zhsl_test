package ziv.clyz.design.strategy.base.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ziv.clyz.design.strategy.base.strategy.Achievement;
import ziv.clyz.design.strategy.base.strategy.AverageAchievement;
import ziv.clyz.design.strategy.base.strategy.ExcellentAchievement;
import ziv.clyz.design.strategy.base.strategy.GoodAchievement;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private Achievement achievement;

    public boolean getAchievement(String value) {
        return this.achievement.getAchievement(value);
    }

    public boolean getAchievement(AverageAchievement averageAchievement, String value) {
        return averageAchievement.getAchievement(value);
    }

    public boolean getAchievement(ExcellentAchievement excellentAchievement, String value) {
        return excellentAchievement.getAchievement(value);
    }

    public boolean getAchievement(GoodAchievement goodAchievement, String value) {
        return goodAchievement.getAchievement(value);
    }

}
