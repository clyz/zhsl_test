package ziv.clyz.design.strategy.ifelse.factory;

import ziv.clyz.design.strategy.ifelse.strategy.JavaBeanVaild;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;

public class FactoryStrategy {
    private FactoryStrategy() {

    }

    public final static Collection<Function<String, Boolean>> conditions;

    static {
        conditions = new ArrayList<>();
        conditions.add(e -> new JavaBeanVaild().click(e));
        conditions.add(Type.REGEX.getValue()::equals);
        conditions.add(Type.PROCESS.getValue()::equals);
    }

    public static boolean judge(String context) {
        for (Function<String, Boolean> cond : conditions) {
            if (cond.apply(context)) {
                return true;
            }
        }
        return false;
    }

    public enum Type {
        JAVA("java"), REGEX("regex"), PROCESS("process");

        private String value;

        Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
