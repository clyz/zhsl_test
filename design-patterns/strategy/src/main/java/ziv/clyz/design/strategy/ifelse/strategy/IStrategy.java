package ziv.clyz.design.strategy.ifelse.strategy;

@FunctionalInterface
public interface IStrategy {
    public boolean click(String type);
}
