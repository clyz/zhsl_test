package ziv.clyz.design.strategy.base.strategy;

import ziv.clyz.design.strategy.base.factory.FactoryStrategy;

public class AverageAchievement implements Achievement {
    @Override
    public boolean getAchievement(String value) {
        return value.equals(FactoryStrategy.Type.AVERAGE);
    }
}
