package ziv.clyz.design.strategy.ifelse.factory;

import org.junit.Test;

import static org.junit.Assert.*;

public class FactoryStrategyTest {

    @org.junit.Test
    public void judge() {
        final boolean judge = FactoryStrategy.judge(FactoryStrategy.Type.JAVA.getValue());
        System.out.println(judge);
    }

    /**
     * <p>
     *    并行流
     * </p>
     */
    @Test
    public void pipeline(){
        FactoryStrategy.conditions.parallelStream().forEach(System.out::println);
    }
}