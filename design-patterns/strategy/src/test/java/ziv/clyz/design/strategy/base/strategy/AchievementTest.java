package ziv.clyz.design.strategy.base.strategy;

import ziv.clyz.design.strategy.EX;
import ziv.clyz.design.strategy.base.bean.Student;
import ziv.clyz.design.strategy.base.factory.FactoryStrategy;

import static org.junit.Assert.*;

public class AchievementTest {

    public static void main(String[] args) throws EX {
        Student student = new Student(new AverageAchievement());

        System.out.println(student.getAchievement().getAchievement("V"));

        System.out.println(student.getAchievement(new AverageAchievement(),"V"));

        final Achievement achievement = FactoryStrategy.achievement(FactoryStrategy.Type.GOOD.getValue());

        System.out.println(achievement.getAchievement("a"));

    }

}