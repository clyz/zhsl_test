package ziv.clyz.elk.zivelksb.ctrl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.testng.annotations.ITestAnnotation;

@Controller
public class LogCtrl {

    private final static Logger logger = LoggerFactory.getLogger(ITestAnnotation.class);

    @GetMapping("/log")
    public void log() {
        new Thread(() -> {
            logger.info("测试log");
            logger.info("HI 兄弟");
            int i = 1;
            while (true) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                logger.error("HI 朋友");
                logger.error("something wrong. id={}; name=Ryan-{};", i, i);
                i++;
            }
        }).start();
    }
}
