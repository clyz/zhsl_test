package com.wisdom.elasticsearchrestsb.vo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@Document(indexName = "user", type = "_doc")
public class UserVo {
    @Id
    private Long id;
    private String name;
}
