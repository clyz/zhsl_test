package com.wisdom.elasticsearchrestsb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestServiceImpl implements IRestService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${ziv.rest-template.http}")
    private String http;


    @Override
    public Object findAll() {
        HttpHeaders headers = new HttpHeaders();
        return restTemplate.postForEntity(http+"user/_search",new HttpEntity<>(null, headers), String.class);
    }
}
