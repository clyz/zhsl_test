package com.wisdom.elasticsearchrestsb.repository;

import com.wisdom.elasticsearchrestsb.vo.UserVo;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends ElasticsearchCrudRepository<UserVo,Long> {
}
