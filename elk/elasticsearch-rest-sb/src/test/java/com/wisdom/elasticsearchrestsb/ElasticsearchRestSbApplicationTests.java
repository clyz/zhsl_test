package com.wisdom.elasticsearchrestsb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableElasticsearchRepositories
public class ElasticsearchRestSbApplicationTests {

    @Test
    public void contextLoads() {
    }

}
