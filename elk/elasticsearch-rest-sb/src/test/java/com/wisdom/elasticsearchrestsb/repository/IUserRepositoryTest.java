package com.wisdom.elasticsearchrestsb.repository;

import com.wisdom.elasticsearchrestsb.ElasticsearchRestSbApplicationTests;
import com.wisdom.elasticsearchrestsb.service.IRestService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IUserRepositoryTest extends ElasticsearchRestSbApplicationTests {
    @Autowired
    private IRestService restService;

    @Test
    public void test() {
        Object all = restService.findAll();
        System.out.println(all);
    }
}