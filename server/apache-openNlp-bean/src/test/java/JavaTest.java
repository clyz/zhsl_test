import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.model.BaseModel;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JavaTest {
    public static void main(String[] args) throws IOException {
//        sentenceDetect();
        tokenize();
    }

    public static void tokenize() throws IOException {
        InputStream is = new FileInputStream("E:\\java_web\\java201706\\apache-nlp\\en-sent.bin");
        TokenizerModel model = new TokenizerModel(is);
        Tokenizer tokenizer = new TokenizerME(model);
        String tokens[] = tokenizer.tokenize("Hi. How are you? This is Richard. Richard is still single. please help him find his girl");
        for (String a : tokens) System.out.println(a);
        is.close();
    }

    public static void sentenceDetect() throws IOException {
        String paragraph = "Hi. How are you? This is JD_Dog. He is my good friends.He is very kind.but he is no more handsome than me. ";
        InputStream is = new FileInputStream("E:\\java_web\\java201706\\apache-nlp\\en-sent.bin");
        SentenceModel model = new SentenceModel(is);
        SentenceDetector sdetector = new SentenceDetectorME(model);
        String[] sentences = sdetector.sentDetect(paragraph);
        for (String single : sentences) {
            System.out.println(single);
        }
        is.close();
    }
}
