package opennlp.kit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class OpenNLPKit {
    /**
     * 载入词库中的命名实体
     *
     * @param nameListFile
     * @return
     * @throws Exception
     */
    public static List<String> loadNameWords(File nameListFile)
            throws Exception {
        List<String> nameWords = new ArrayList<String>();

        if (!nameListFile.exists() || nameListFile.isDirectory()) {
            System.err.println("不存在那个文件");
            return null;
        }

        BufferedReader br = new BufferedReader(new FileReader(nameListFile));
        String line = null;
        while ((line = br.readLine()) != null) {
            nameWords.add(line);
        }

        br.close();

        return nameWords;
    }

    /**
     * 获取命名实体类型
     *
     * @param nameListFile
     * @return
     */
    public static String getNameType(File nameListFile) {
        String nameType = nameListFile.getName();

        return nameType.substring(0, nameType.lastIndexOf("."));
    }
}
