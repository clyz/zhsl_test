package com.wisdom.jfinal.core.config;

import com.jfinal.config.*;
import com.jfinal.template.Engine;
import com.wisdom.jfinal.core.routes.TomcatRoutes;

public class TomcatConfig extends JFinalConfig {
    @Override
    public void configConstant(Constants me) {

    }

    @Override
    public void configRoute(Routes routes) {
        routes.add(new TomcatRoutes());
    }

    @Override
    public void configEngine(Engine me) {

    }

    @Override
    public void configPlugin(Plugins me) {

    }

    @Override
    public void configInterceptor(Interceptors me) {

    }

    @Override
    public void configHandler(Handlers me) {

    }
}
