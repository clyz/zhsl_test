package com.wisdom.jfinal.core.routes;

import com.jfinal.config.Routes;
import com.wisdom.jfinal.web.ctrl.LoginCtrl;

public class TomcatRoutes extends Routes {
    @Override
    public void config() {
        add("/tomcat/login", LoginCtrl.class);
    }
}
