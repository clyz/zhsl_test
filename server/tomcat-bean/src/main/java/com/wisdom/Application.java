package com.wisdom;

import com.jfinal.kit.PropKit;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class Application {

    public static void main(String[] args) throws LifecycleException {
        PropKit.use("tomcat/tomcat.properties");
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(PropKit.getInt("port"));
        tomcat.addWebapp(PropKit.get("context-path"), new File(PropKit.get("webapp-dirLocation")).getAbsolutePath());
        tomcat.start();
        tomcat.getServer().await();
    }
}
