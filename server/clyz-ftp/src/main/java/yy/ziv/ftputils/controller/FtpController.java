package yy.ziv.ftputils.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import yy.ziv.ftputils.service.FTPClientHelperService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;

@RestController
@Slf4j
public class FtpController {

    @Autowired
    FTPClientHelperService ftpClientHelperService;
    private static final int BUFFER_SIZE = 100 * 1024;

    @PostMapping(value = "/append/plupload")
    public String appendPlupload(MultipartFile file,String name,
                             @RequestParam(defaultValue = "0") Integer chunk,
                             @RequestParam(defaultValue = "0") Integer chunks) throws Exception {
        boolean appendStoreFile = ftpClientHelperService.appendStoreFile("/2018-10-08/0/1/",name, file.getInputStream());
        log.info(appendStoreFile+"");
        return "";
    }

    @PostMapping(value = "/ziv/plupload")
    public String upPlupload(MultipartFile file,String name,
                             @RequestParam(defaultValue = "0") Integer chunk,
                             @RequestParam(defaultValue = "0") Integer chunks,
                             HttpSession session) throws IOException {
        log.info("chunk:[" + chunk + "] chunks:[" + chunks + "]");
        String realPath = session.getServletContext().getRealPath("");
        String relativePath = "\\plupload\\files\\";
        System.out.println(realPath + relativePath);
        File folder = new File(realPath + relativePath);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        //目标文件
        File destFile = new File(folder, name);
        //文件已存在删除旧文件（上传了同名的文件）
        if (chunk == 0 && destFile.exists()) {
            destFile.delete();
            destFile = new File(folder, name);
        }
        //合成文件
        appendFile(file.getInputStream(), destFile);
        if (chunk == chunks - 1) {
            log.info("上传完成");
        }else {
            log.info("还剩["+(chunks-1-chunk)+"]个块文件");
        }
        return "";
    }

    @RequestMapping(value="plupload",method=RequestMethod.POST)
    public String plupload(@RequestParam MultipartFile file, HttpServletRequest request, HttpSession session) {
        try {
            String name = request.getParameter("name");
            Integer chunk = 0, chunks = 0;
            if(null != request.getParameter("chunk") && !request.getParameter("chunk").equals("")){
                chunk = Integer.valueOf(request.getParameter("chunk"));
            }
            if(null != request.getParameter("chunks") && !request.getParameter("chunks").equals("")){
                chunks = Integer.valueOf(request.getParameter("chunks"));
            }
            log.info("chunk:[" + chunk + "] chunks:[" + chunks + "]");
            //检查文件目录，不存在则创建
            String relativePath = "/plupload/files/";
            String realPath = session.getServletContext().getRealPath("");
            System.out.println(realPath + relativePath);
            File folder = new File(realPath + relativePath);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            //目标文件
            File destFile = new File(folder, name);
            //文件已存在删除旧文件（上传了同名的文件）
            if (chunk == 0 && destFile.exists()) {
                destFile.delete();
                destFile = new File(folder, name);
            }
            //合成文件
            appendFile(file.getInputStream(), destFile);
            if (chunk == chunks - 1) {
                log.info("上传完成");
            }else {
                log.info("还剩["+(chunks-1-chunk)+"]个块文件");
            }

        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return "";
    }

    private void appendFile(InputStream in, File destFile) {
        OutputStream out = null;
        try {
            // plupload 配置了chunk的时候新上传的文件append到文件末尾
            if (destFile.exists()) {
                out = new BufferedOutputStream(new FileOutputStream(destFile, true), BUFFER_SIZE);
            } else {
                out = new BufferedOutputStream(new FileOutputStream(destFile),BUFFER_SIZE);
            }
            in = new BufferedInputStream(in, BUFFER_SIZE);

            int len = 0;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (null != in) {
                    in.close();
                }
                if(null != out){
                    out.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    @PostMapping("/up")
    public boolean up(@RequestParam("file") MultipartFile file) throws Exception {
        return ftpClientHelperService.storeFile(file.getOriginalFilename(),file.getInputStream());
    }

    @PostMapping("/ups")
    public boolean ups(@RequestParam("file") MultipartFile file) throws Exception {
        return ftpClientHelperService.storeFile("/img/ox/xo",file.getOriginalFilename(),file.getInputStream());
    }

    @GetMapping("/down/{name}")
    public ResponseEntity<byte[]> down(@PathVariable("name") String name) throws Exception {
        return download(name,ftpClientHelperService.retrieveFileStream(name),MediaType.APPLICATION_OCTET_STREAM);
    }

    @GetMapping("/del/{name}")
    public boolean del(@PathVariable("name") String name){
        try {
            return ftpClientHelperService.deleteFile(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private ResponseEntity<byte[]> download(String filename, byte[] bytes, MediaType mediaType) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        headers.setContentDispositionFormData("attachment", filename);
        return new ResponseEntity<>(bytes, headers, HttpStatus.CREATED);
    }
}