package yy.ziv.ftputils.service;

import java.io.InputStream;

/**
 * 杨泽民
 */
public interface FTPClientHelperService {
    /**
     * 追加图片上传
     * @param remote  /img/2018/ziv/xo
     * @param local
     * @return
     * @throws Exception
     */
    public boolean appendStoreFile(String remote, InputStream local) throws Exception;

    /**
     * 多目录文件上传
     * @param multiDirectory  /img/2018/ziv/xo
     * @param remote
     * @param local
     * @return
     * @throws Exception
     */
    public boolean appendStoreFile(String multiDirectory, String remote, InputStream local) throws Exception;

    /**
     * 下载资源
     * @param remote 下载的文件的全路径如 img/2015/01/1/xx.jpg
     * @return
     * @throws Exception
     */
    public   byte[] retrieveFileStream(String remote) throws Exception;

    /**
     * 删除文件
     * @param pathname 文件的全路径如 img/2015/01/1/xx.jpg
     * @return
     * @throws Exception
     */
    public   boolean deleteFile(String pathname) throws Exception;

    /**
     * 上传文件，只支持一级存在的目录上传
     * @param remote
     * @param local
     * @return
     * @throws Exception
     */
    public   boolean storeFile(String remote, InputStream local) throws Exception;

    /**
     * 删除目录只支持删除一级目录
     * @param pathname
     * @return
     * @throws Exception
     */
    public  boolean removeDirectory(String pathname) throws Exception;

    /**
     * TODO 删除多级目录
     * @param pathname
     * @return
     * @throws Exception
     */
    public  boolean removeDirectorys(String pathname) throws Exception;

    /**
     * 创建目录只支持创建一级目录
     * @param pathname
     * @return
     * @throws Exception
     */
    public boolean makeDirectory(String pathname) throws Exception;

    /**
     * 上传文件，支持多目录上传，
     * @param multiDirectory 路径 img/2018/25/52/x
     * @param remote 文件名称
     * @param local 文件输入流
     * @return
     * @throws Exception
     */
    public boolean storeFile(String multiDirectory, String remote, InputStream local) throws Exception;

    /**
     * 多路径创建文件夹
     * @param pathname asdf/ewrt/wra/asdfwr/asdfw35r
     * @return
     * @throws Exception
     */
    public boolean makeDirectorys(String pathname) throws Exception;
}