package yy.ziv.ftputils.ftp.pool;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import yy.ziv.ftputils.ftp.pool.util.ByteUtil;
import yy.ziv.ftputils.service.FTPClientHelperService;

import java.io.IOException;
import java.io.InputStream;

/**
 * ftp客户端辅助bean
 *
 * @author jelly
 *
 */
@Service
@Slf4j
public class FTPClientHelper implements FTPClientHelperService {
    @Autowired
    private FTPClientPool  ftpClientPool;

    @Value("${pool.ftp.base-path}")
    String basePath;

    /**
     * 下载 remote文件流
     * @param remote 远程文件
     * @return 字节数据
     * @throws Exception
     */
    public byte[] retrieveFileStream(String remote) throws Exception {
        FTPClient client=null;
        InputStream in =null;
        try {
            client=	ftpClientPool.borrowObject();
            in=client.retrieveFileStream(remote);
            return   ByteUtil.inputStreamToByteArray(in);
        }finally{
            if (in != null) {
                in.close();
            }
            if (!client.completePendingCommand()) {
                client.logout();
                client.disconnect();
                ftpClientPool.getPool().invalidateObject(client);
            }
            ftpClientPool.returnObject(client);

        }
    }

    /**
     * 创建目录    单个不可递归
     * @param pathname 目录名称
     * @return
     * @throws Exception
     */
    public boolean makeDirectory(String pathname) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            return  client.makeDirectory(pathname);
        }finally {
                ftpClientPool.returnObject(client);
        }
    }

    /**
     * 上传一个多路径文件
     * @param pathname
     * @return
     * @throws Exception
     */
    public boolean makeDirectorys(String pathname) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            return  this.createMultiDirectory(pathname,client);
        }finally {
            ftpClientPool.returnObject(client);
        }
    }

    /**
     * 删除目录，单个不可递归
     * @param pathname
     * @return
     * @throws IOException
     */
    public  boolean removeDirectory(String pathname) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            return  client.removeDirectory(pathname);
        } finally{
            ftpClientPool.returnObject(client);
        }
    }

    @Override
    public boolean removeDirectorys(String pathname) throws Exception {
        log.info("TODO removeDirectorys...");
        return false;
    }

    /**
     * 删除文件 单个 ，不可递归
     * @param pathname
     * @return
     * @throws Exception
     */
    public   boolean deleteFile(String pathname) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            return  client.deleteFile(pathname);
        }finally{
            ftpClientPool.returnObject(client);
        }
    }

    /**
     * 上传文件
     * @param remote
     * @param local
     * @return
     * @throws Exception
     */
    public   boolean storeFile(String remote, InputStream local) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            return  client.storeFile(remote, local);
        } finally{
            ftpClientPool.returnObject(client);
        }
    }

    public boolean appendStoreFile(String remote, InputStream local) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            return  client.appendFile(remote,local);
        } finally{
            ftpClientPool.returnObject(client);
        }
    }

    public boolean storeFile(String multiDirectory,String remote, InputStream local) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            if(createMultiDirectory(multiDirectory,client))
                return  client.storeFile(remote, local);
            else return false;
        } finally{
            ftpClientPool.returnObject(client);
        }
    }

    public boolean appendStoreFile(String multiDirectory,String remote, InputStream local) throws Exception {
        FTPClient client=null;
        try {
            client=	ftpClientPool.borrowObject();
            if(createMultiDirectory(multiDirectory,client))
                return  client.appendFile(remote,local);
            else return false;
        } finally{
            ftpClientPool.returnObject(client);
        }
    }

    private boolean createMultiDirectory(String multiDirectory,FTPClient ftpClient) throws IOException {
        String[] dirs = multiDirectory.split("/");
//        ftpClient.changeWorkingDirectory(basePath+"/");

        //按顺序检查目录是否存在，不存在则创建目录
       /*for(int i=1; dirs!=null&&i<dirs.length; i++) {
            if(!ftpClient.changeWorkingDirectory(basePath+"/"+dirs[i])) {
                if(ftpClient.makeDirectory(dirs[i])) {
                    if(!ftpClient.changeWorkingDirectory(dirs[i])) {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        }*/

        //切换到上传目录
        if (!ftpClient.changeWorkingDirectory(basePath+multiDirectory)) {
            String tempPath = basePath;
            for (String dir : dirs) {
                if (null == dir || "".equals(dir)) continue;
                tempPath += "/" + dir;
                if (!ftpClient.changeWorkingDirectory(tempPath)) {
                    if (!ftpClient.makeDirectory(tempPath)) {
                        return false;
                    } else {
                        ftpClient.changeWorkingDirectory(tempPath);
                    }
                }
            }
        }

        return true;
    }

    private boolean removeMultiDirectory(String multiDirectory,FTPClient ftpClient) throws IOException {

        return true;
    }

}
