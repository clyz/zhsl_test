package yy.ziv.ftputils.ftp.pool.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import yy.ziv.ftputils.ftp.pool.FTPClientPool;
import yy.ziv.ftputils.ftp.pool.FtpPoolConfig;
import yy.ziv.ftputils.ftp.pool.factory.FTPClientFactory;

@Configuration
public class AutoConfig {
    @ConfigurationProperties( prefix="pool.ftp")
    @Bean
    public FtpPoolConfig ftpPoolConfig(){
        return new FtpPoolConfig();
    }
    @Bean
    public FTPClientFactory ftpClientFactory(){
        return new FTPClientFactory(ftpPoolConfig());
    }
    @Bean
    public FTPClientPool ftpClientPool(){
        return new FTPClientPool(ftpClientFactory());
    }
}
