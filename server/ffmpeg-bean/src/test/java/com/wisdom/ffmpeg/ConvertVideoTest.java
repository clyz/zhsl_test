package com.wisdom.ffmpeg;


import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ConvertVideoTest {

    private final static String VOIDEC_1 = "C:\\Users\\Administrator\\Videos\\1.mp4";
    private final static String VOIDEC_2 = "C:\\Users\\Administrator\\Videos\\2.mp4";
    private final static String VOIDEC_OUT = "C:\\Users\\Administrator\\Videos\\out\\";
    private final static String FFMPEG_EXE = "E:\\java_web\\java201706\\ffmpeg-20190409-0a347ff-win64-static\\bin\\ffmpeg.exe";
    private static final String FFMPEG_PATH = "E:\\java_web\\java201706\\ffmpeg-20190409-0a347ff-win64-static\\bin\\ffmpeg";

    String cut = FFMPEG_EXE + " -i "
            + VOIDEC_2
            + "   -y   -f   image2   -ss   8   -t   0.001   -s   600x500 " + VOIDEC_OUT
            + "c.jpg";

    @Test
    public void commendVideo() throws IOException {
        List<String> commend = new ArrayList<String>();
        commend.add(FFMPEG_PATH);
        commend.add("-i");
        commend.add(VOIDEC_2);
        commend.add("-ab");
        commend.add("56");
        commend.add("-ar");
        commend.add("22050");
        commend.add("-qscale");
        commend.add("8");
        commend.add("-r");
        commend.add("15");
        commend.add("-s");
        commend.add("600x500");
        commend.add(VOIDEC_OUT + "v.mp4");
        ProcessBuilder builder = new ProcessBuilder().command(commend);
        Process process = builder.start();
        OutputStream outputStream = process.getOutputStream();
        InputStream inputStream = process.getInputStream();
    }

    @Test
    public void commendJpg() throws IOException {
        List<String> commend = new ArrayList<String>();
        commend.add(FFMPEG_EXE);
        commend.add("-i");
        commend.add(VOIDEC_1);
        commend.add("-y");
        commend.add("-f");
        commend.add("image2");
        commend.add("-ss");
        commend.add("8");
        commend.add("-t");
        commend.add("0.001");
        commend.add("-s");
        commend.add("600x500");
        commend.add(VOIDEC_OUT + "dc.jpg");
        ProcessBuilder builder = new ProcessBuilder(commend);
        Process process = builder.start();
        System.out.println(process.exitValue());
    }

    @Test
    public void runtime() throws IOException {
        Runtime runtime = Runtime.getRuntime();
        Process exec = runtime.exec(cut);
    }

    @Test
    public void cmd() throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder("C:/Windows/system32/cmd.exe", "dir");
        Process process = processBuilder.start();
        InputStream fis = process.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis,"UTF-8"));
        String s;
        StringBuilder sb = new StringBuilder();
        while ((s = bufferedReader.readLine()) != null && !"".equals(s)) {
            sb.append(s);
        }
        System.out.println("--");
        System.out.println(new String(sb.toString().getBytes(),"UTF-8"));
        System.out.println("--");
//        process.waitFor();
//        System.out.println(process.exitValue());
    }
}