package com.wisdom.red5serversb.red5.server;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.red5.server.net.rtmp.RTMPMinaTransport;

@Slf4j
public class Red5ClassServerTest {

    @Test
    public void load() throws Exception {
        RTMPMinaTransport rtmpMinaTransport = new RTMPMinaTransport();
        rtmpMinaTransport.setAddress("127.0.0.1");
        rtmpMinaTransport.start();
    }
}
