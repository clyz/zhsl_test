package com.wisdom.red5serversb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.properties.PropertyMapping;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@PropertyMapping("red5.properties")
public class Red5ServerSbApplicationTests {
    @Value("${rtmpt.server}")
    String IP;

    @Test
    public void contextLoads() {
        System.out.println(IP);
    }

}
