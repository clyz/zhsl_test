package com.wisdom.red5serversb.state;

import com.wisdom.red5serversb.vo.RoomVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;


/**
 * 房间操作
 */
public class RoomState {

    /**
     * 创建一个房间信息
     */
    public boolean insert(String roomKey, String roomName) {
        try {
            if (!Ram.roomHm.containsKey(roomKey)) {
                RoomVo rVo = new RoomVo();
                rVo.setRoomName(roomName);
                Ram.roomHm.put(roomKey, rVo);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 返回所有房间信息
     */
    public List<RoomVo> selectAll() {
        try {
            List<RoomVo> resultList = new ArrayList<>();
            for (Entry<String, RoomVo> entry : Ram.roomHm.entrySet()) {
                resultList.add(entry.getValue());
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 删除一个房间信息
     */
    public boolean delete(String roomKey) {
        try {
            Ram.roomHm.remove(roomKey);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 统计房间总数
     */
    public int count() {
        return Ram.roomHm.size();
    }

}