package com.wisdom.red5serversb.ctrl;

import com.wisdom.red5serversb.state.Ram;
import com.wisdom.red5serversb.state.RoomState;
import com.wisdom.red5serversb.vo.RoomVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/room/")
@Slf4j
public class RoolCtrl {

    @GetMapping("size")
    public String hello() {
        return "当前房间数: " + Ram.roomHm.size();
    }

    @GetMapping("add")
    public RoomVo insert(String roomKey, String roomName) {
        boolean insert = new RoomState().insert(roomKey, roomName);
        log.info("**添加房间", insert);
        return Ram.roomHm.get(roomKey);
    }
}
