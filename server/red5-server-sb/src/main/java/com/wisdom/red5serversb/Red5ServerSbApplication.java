package com.wisdom.red5serversb;

import org.red5.spring.InetAddressEditor;
import org.red5.spring.Red5ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Red5ServerSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(Red5ServerSbApplication.class, args);
    }

/*    @Bean
    public Red5ApplicationContext red5ApplicationContext(){
        return new Red5ApplicationContext();
    }

    @Bean
    public InetAddressEditor inetAddressEditor(){
        return new InetAddressEditor();
    }*/

}
