package undertow.test.helloworld;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class HelloWorld {
    public static void main(String[] args) {
        Undertow.builder()
                .addHttpListener(8080, "127.0.0.1")
                .setHandler(new HttpHandler() {
                    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
                        httpServerExchange.getResponseHeaders().put(Headers.CONTENT_TYPE,
                                "text/plain");
                        httpServerExchange.getResponseSender().send("Hello World");
                    }
                }).build()
                .start();
        ;
    }
}
