package clyz.ziv.servlet;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;

import javax.servlet.ServletException;


public class Application {

    public static void main(String[] args) {
        Application.start();
    }

    public static void start() {
        ApplicationConfig applicationConfig = ApplicationConfig.builder();
        try {

            DeploymentInfo servletBuilder = Servlets.deployment()
                    .setClassLoader(ApplicationServlet.class.getClassLoader())
                    .setContextPath(applicationConfig.getLocation())
                    .setDeploymentName(applicationConfig.getName() + ".war")
                    .addServlets(
                            Servlets.servlet("applicationServlet", ApplicationServlet.class)
                                    .addInitParam("init", applicationConfig.getIndex())
                                    .addMapping("/*"));

            DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
            manager.deploy();

            HttpHandler servletHandler = manager.start();

            PathHandler path = Handlers.path(Handlers.redirect(applicationConfig.getLocation()))
                    .addPrefixPath(applicationConfig.getLocation(), servletHandler);

            Undertow server = Undertow.builder()
                    .addHttpListener(applicationConfig.getPort(), applicationConfig.getHost())
                    .setHandler(path)
                    .build();

            server.start();
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }
}
