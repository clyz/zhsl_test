package clyz.ziv.servlet;

import clyz.ziv.servlet.common.kit.PropKit;

public class ApplicationConfig {
    private static ApplicationConfig applicationConfig;
    //  项目路径
    private String location;
    private int port;
    private String host;
    //  项目名称
    private String name;
    //  主页
    private String index;

    static {
        PropKit.use("servlet/servlet.properties");
        applicationConfig = new ApplicationConfig();
        applicationConfig.name = PropKit.get("name", "name");
        applicationConfig.location = PropKit.get("location", "/");
        applicationConfig.port = PropKit.getInt("port", 8080);
        applicationConfig.host = PropKit.get("host", "127.0.0.1");
        applicationConfig.index = PropKit.get("index", "");
    }

    private ApplicationConfig() {
    }

    public static ApplicationConfig builder() {
        return applicationConfig;
    }

    public String getLocation() {
        return location;
    }

    public int getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }
}
