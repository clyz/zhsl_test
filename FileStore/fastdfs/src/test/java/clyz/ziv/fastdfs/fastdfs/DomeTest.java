package clyz.ziv.fastdfs.fastdfs;

import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.junit.Test;

import java.io.IOException;

/**
 * @author YangZemin
 * @date 2020/3/13 14:29
 */
public class DomeTest {
    String P = "C:\\service_java\\ZHSL\\zhsl_test\\FileStore\\fastdfs\\src\\main\\resources\\";

    @Test
    public void fileUpload() throws IOException, MyException {
        //加载连接信息（即FastDFS中tracker服务器IP）
        ClientGlobal.init(P + "fdfs_client.properties");
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //获取TrackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //创建StorageServer对象，引用为空
        StorageServer storageServer = null;
        //使用TrackerServer和StorageServer构造StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);
        //使用storageClient上传文件到服务器
        String[] strings = storageClient.upload_file("C:\\Users\\Administrator\\Pictures\\vo\\nihaole.mp4", "mp4", null);
        //上传成功会返回一个字符数组，分别为：文件所在组和文件在组中的位置及名称
        for (String s : strings) {
            System.out.println(s);
        }
    }
}
