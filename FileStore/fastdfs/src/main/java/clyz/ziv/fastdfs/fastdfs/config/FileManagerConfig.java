package clyz.ziv.fastdfs.fastdfs.config;

import java.io.Serializable;

/**
 * @author YangZemin
 * @date 2019/12/19 11:50
 */
public interface FileManagerConfig extends Serializable {

    public static final String FILE_DEFAULT_AUTHOR = "YangZemnin";

    public static final String PROTOCOL = "http://";

    public static final String SEPARATOR = "/";

    public static final String TRACKER_NGNIX_ADDR = "192.168.99.200";

    public static final String TRACKER_NGNIX_PORT = "";

    public static final String CLIENT_CONFIG_FILE = "fdfs_client.conf";
}

