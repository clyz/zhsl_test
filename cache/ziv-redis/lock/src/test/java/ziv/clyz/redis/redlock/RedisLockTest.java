package ziv.clyz.redis.redlock;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@ContextConfiguration(locations = {"classpath:spring-redis.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class RedisLockTest {

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
    @Autowired
    private RedisLock redisLock;

    private String key = "KEY";

    //    @Before
    public void before() {
        log.info("START");
        ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:/spring-redis.xml");
        this.redisTemplate = (RedisTemplate<Object, Object>) ac.getBean("redisTemplate");
    }

    @Test
    public void set() {
        redisTemplate.opsForValue().set("a", "1");
    }

    @Test(expected = Exception.class)
    public void redisLock() throws Exception {
        redisLock.setLockKey(key);
        if (redisLock.lock()) {
            redisTemplate.opsForValue().set(key, "NI_HAO");
            final int i = 1 / 0;
            System.out.println(redisTemplate.opsForValue().get(key));
        }
        redisLock.unlock();
    }

    @Test
    public void lock() throws Exception {
        RedisLock lock = new RedisLock(redisTemplate, key, 10000, 20000);
        if (lock.lock()) {
//                redisTemplate.opsForValue().set(lock.getLockKey(), "1");
            redisTemplate.opsForValue().set(key, "NI_HAO");
            System.out.println(redisTemplate.opsForValue().get(key));
        }
        //为了让分布式锁的算法更稳键些，
        // 持有锁的客户端在解锁之前应该再检查一次自己的锁是否已经超时，再去做DEL操作，
        // 因为可能客户端因为某个耗时的操作而挂起，
        //操作完的时候锁因为超时已经被别人获得，这时就不必解锁了。 ————这里没有做
        lock.unlock();
    }
}