package ziv.clyz.redis.redlock.transaction;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Slf4j
@ContextConfiguration(locations = {"classpath:spring-redis.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class RedisTransactionalTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void transactionalSupportTrue() {
        //  开启事务，用同一个连接
        redisTemplate.setEnableTransactionSupport(false);
        //  MULTI 开启事务
        redisTemplate.multi();
        redisTemplate.opsForValue().set("test_long", 1);
        final int i = 1 / 0;
        redisTemplate.opsForValue().set("test_long", 2);
        //  提交事务
        redisTemplate.exec();
    }

    @Test
    public void transactionalSupportFalse() {
        //  每次使用都是不同的 redisTemplate
        redisTemplate.setEnableTransactionSupport(false);
        redisTemplate.multi();
        redisTemplate.opsForValue().set("test_long", 1);
        final int i = 1 / 0;
        redisTemplate.opsForValue().set("test_long", 2);
        redisTemplate.exec();
    }

    /**
     * <p>
     * Redis通过使用WATCH, MULTI, and EXEC组成的事务来实现乐观锁(注意没有用DISCARD),Redis事务没有回滚操作。
     * 在SpringDataRedis当中通过RedisTemplate的SessionCallback中来支持(否则事务不生效)。
     * discard的话不需要自己代码处理，callback返回null，成的话，返回非null，依据这个来判断事务是否成功(没有抛异常)。
     * </p>
     *
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Test
    public void cas() throws InterruptedException, ExecutionException {
        final String key = "test-cas-1";
        ValueOperations<String, String> strOps = redisTemplate.opsForValue();
        strOps.set(key, "hello");
        ExecutorService pool = Executors.newCachedThreadPool();
        List<Callable<Object>> tasks = new ArrayList();
        for (int i = 0; i < 5; i++) {
            final int idx = i;
            tasks.add(new Callable() {
                public Object call() throws Exception {
                    return redisTemplate.execute(new SessionCallback() {
                        public Object execute(RedisOperations operations) throws DataAccessException {
                            operations.watch(key);
                            String origin = (String) operations.opsForValue().get(key);
                            operations.multi();
                            operations.opsForValue().set(key, origin + idx);
                            Object rs = operations.exec();
                            System.out.println("set:" + origin + idx + " rs:" + rs);
                            return rs;
                        }
                    });
                }
            });
        }
        List<Future<Object>> futures = pool.invokeAll(tasks);
        for (Future<Object> f : futures) {
            System.out.println(f.get());
        }
        pool.shutdown();
        pool.awaitTermination(1000, TimeUnit.MILLISECONDS);
    }

}
