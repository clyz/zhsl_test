package ziv.clyz.redis.redlock.transaction.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Slf4j
@ContextConfiguration(locations = {"classpath:spring-redis.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class IRedisServiceTest {
    @Autowired
    private IRedisService redisService;

    @Test
    public void set() throws Exception {
        redisService.set("xo",1L);
    }

}