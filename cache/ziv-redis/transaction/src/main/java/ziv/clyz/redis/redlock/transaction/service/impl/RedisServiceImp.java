package ziv.clyz.redis.redlock.transaction.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ziv.clyz.redis.redlock.RedisLock;
import ziv.clyz.redis.redlock.transaction.service.IRedisService;

@Service
public class RedisServiceImp implements IRedisService {

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Transactional(rollbackFor = Exception.class)
    public Object set(String key, Object value) throws Exception {
        RedisLock redisLock = new RedisLock(redisTemplate,key);
        Object o = null;
        if (redisLock.lock()) {
            redisTemplate.opsForValue().set(key, value);
            final int i = 1 / 0;
            o = redisTemplate.opsForValue().get(key);
            redisLock.unlock();

        }
        return o;
    }

}
