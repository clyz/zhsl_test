package ziv.clyz.redis.redlock.transaction.service;

public interface IRedisService {
    public Object set(String key ,Object value) throws Exception;
}
