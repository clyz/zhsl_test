package com.wisdom.j2cacheyuansb.service;

import lombok.extern.slf4j.Slf4j;
import net.oschina.j2cache.CacheChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class J2cacheServiceImpl implements Ij2cacheService {
    @Autowired
    CacheChannel cacheChannel;
    @Override
    @Cacheable(value = "save",key = "#value")
    public String save(String value) {
        log.info(value);
        cacheChannel.set("cache",value,value+value+value);
        return value+value;
    }
}
