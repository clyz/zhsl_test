package com.wisdom.j2cacheyuansb.controller;

import com.wisdom.j2cacheyuansb.service.Ij2cacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class J2cacheController {
    @Autowired
    Ij2cacheService ij2cacheService;
    @GetMapping("save")
    public String save(String value){
        return ij2cacheService.save(value);
    }
}
