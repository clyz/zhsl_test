package com.wisdom.j2cacheyuansb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J2cacheYuanSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(J2cacheYuanSbApplication.class, args);
    }
}
