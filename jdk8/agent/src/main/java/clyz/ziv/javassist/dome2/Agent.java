package clyz.ziv.javassist.dome2;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.Arrays;

public class Agent {

    public static void premain(String args, Instrumentation inst) throws Exception {
        inst.addTransformer(new ClassFileTransformer() {
            @Override
            public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
                className = className.replace("/", ".");
                if (className.equals("helloWorld")) {
                    return relaceBytes(className, classfileBuffer);

                }
                return classfileBuffer;
            }
        }, true);
    }

    private static byte[] relaceBytes(String classname, byte[] classbuffer) {
        String bufferStr = Arrays.toString(classbuffer).replace("[", "").replace("]", "");
        System.out.println("classname:" + classname);
        System.out.println("byes:" + bufferStr);

        byte[] findBytes = "hello world".getBytes();
        String findStr = Arrays.toString(findBytes).replace("[", "").replace("]", "");
        System.out.println("world" + findStr);
        byte[] replaceBytes = "hello agent".getBytes();
        String replaceStr = Arrays.toString(replaceBytes).replace("[", "").replace("]", "");
        System.out.println("agent" + replaceStr);
        bufferStr = bufferStr.replace(findStr, replaceStr);

        System.out.println(bufferStr);

        String[] bytearr = bufferStr.split("\\s*,\\s*");

        byte[] bytes = new byte[bytearr.length];

        for (int i = 0; i < bytearr.length; i++) {
            bytes[i] = Byte.parseByte((bytearr[i]));
        }
        System.out.println("new byte :" + Arrays.toString(bytes));
        return bytes;
    }

}