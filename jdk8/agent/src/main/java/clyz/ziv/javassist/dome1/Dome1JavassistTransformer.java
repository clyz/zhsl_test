package clyz.ziv.javassist.dome1;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class Dome1JavassistTransformer implements ClassFileTransformer {

    public static void premain(String agentArgs, Instrumentation instrumentation) {
        System.out.println("开始代理 Agent");

        Dome1JavassistTransformer transformer = new Dome1JavassistTransformer();
        instrumentation.addTransformer(transformer);
    }

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        System.out.println("clyz.ziv.javassist.dome1.Dome1Javassist: " + className);


        return classfileBuffer;
    }

}
