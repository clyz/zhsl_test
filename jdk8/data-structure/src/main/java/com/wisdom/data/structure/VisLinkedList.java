package com.wisdom.data.structure;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.locks.LockSupport;

public class VisLinkedList {

    public VisLinkedList() {
        thread.start();
    }

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public volatile static LinkedList<Content> linkedList = new LinkedList<>();

    public static InheritableThreadLocal<Date> threadLocal = new InheritableThreadLocal();

    private volatile static Thread thread = get();

    //  下一个运行时间
    public volatile static Date nextTime;

    /**
     * <p>
     * 按照 时间 排序 添加到 链表中去
     * </p>
     *
     * @param c
     */
    public synchronized void addSort(Content c){
        linkedList.addLast(c);
        if (linkedList.size() > 1) {
            linkedList.sort(new ContentComparator());
        }

        if (nextTime == null || !linkedList.getFirst().getDate().equals(format.format(nextTime))) {
            init();
        }
    }

    /**
     * 初始化 下一个运行节点
     */
    private void init() {
        try {
            nextTime = format.parse(linkedList.getFirst().getDate());
            System.out.println(nextTime + "新增加执行->NEXT->RUN");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        threadLocal.set(nextTime);
    }

    private static Thread get() {
        return new Thread(new Run());
    }
}

class Run implements Runnable {

    @Override
    public void run() {
        System.err.println(Thread.currentThread().getName() + "TASK_我开始运行了");
        while (true) {
            System.out.print(VisLinkedList.nextTime + "_");
            while (VisLinkedList.nextTime != null && VisLinkedList.linkedList.size() != 0 &&
                    VisLinkedList.nextTime.getTime() < new Date().getTime()) {
                System.out.println(Thread.currentThread().getName() + "TASK__--------------------------------------");
                System.err.println(Thread.currentThread().getName() + "TASK__NEW DATE:" + new Date());
                System.out.println(Thread.currentThread().getName() + VisLinkedList.linkedList.getFirst());
                VisLinkedList.linkedList.removeFirst();
                if (VisLinkedList.linkedList.size() != 0) {
                    try {
                        final Date parse = VisLinkedList.format.parse(VisLinkedList.linkedList.getFirst().getDate());
                        System.out.println("TASK__NEXT  ->> " + parse);
                        VisLinkedList.nextTime = parse;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName() + "TASK__--------------------------------------");
            }
        }
    }
}

class Content {
    private String date;
    private Long id;
    private String content;

    public Content() {
    }

    public Content(Date date, Long id, String content) {
        this.date = VisLinkedList.format.format(date);
        this.id = id;
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Content{" +
                "date='" + date + '\'' +
                ", id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}

class ContentComparator implements Comparator<Content> {


    @Override
    public int compare(Content o1, Content o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}
