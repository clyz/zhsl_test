package com.wisdom.data.structure;

import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.LockSupport;

import static org.junit.Assert.*;

public class VisLinkedListTest {

    VisLinkedList visLinkedList = new VisLinkedList();
    final List<Content> contents = Arrays.asList(
            new Content(new Date(), 1L, "你好一号"),
            new Content(new Date(new Date().getTime() - 8000), 2L, "你好一号 - 8000"),
            new Content(new Date(new Date().getTime() + 70000), 3L, "你好一号 + 70000"),
            new Content(new Date(new Date().getTime() + 6000), 4L, "你好一号+6000"));

    @Test
    public void add() {
        contents.forEach(c -> visLinkedList.addSort(c));
    }

    @Test
    public void run() throws InterruptedException {

        visLinkedList.addSort(new Content(new Date(new Date().getTime() + 6000), 1L, "你好一号+6000"));
        Thread.sleep(3000);
        visLinkedList.addSort(new Content(new Date(new Date().getTime() + 6000), 2L, "你好一号+6000"));
        Thread.sleep(12000);
        visLinkedList.addSort(new Content(new Date(new Date().getTime() + 6000), 3L, "你好一号+6000"));
        visLinkedList.addSort(new Content(new Date(new Date().getTime() + 6000), 4L, "你好一号+6000"));
    }

    @Test
    public void addSort(){
        contents.forEach(c -> visLinkedList.addSort(c));
        final LinkedList<Content> linkedList = VisLinkedList.linkedList;
    }

    @After
    public void after() throws IOException {
        while (true){

        }
    }
}