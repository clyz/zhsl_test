# JVM 调优控制命令
## 参数
### 标准化参数
*例如 通常所见从 控制台命令*
```jshelllanguage
    java -verison
    java -help
```
### 非标准化参数

#### X参数
*默认下*
```jshelllanguage
    C:\Users\23746\Documents\zhsl_test\jdk8>java -version
    java version "1.8.0_202"
    Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
    Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)
```
1. -Xint 解释执行
    ```jshelllanguage
    C:\Users\23746\Documents\zhsl_test\jdk8>java -Xint  -version
    java version "1.8.0_202"
    Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
    Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, interpreted mode)
    ```

2. -Xcomp 第一次使用就编译成本地代码
    ```jshelllanguage
   C:\Users\23746\Documents\zhsl_test\jdk8>java -Xcomp  -version
    java version "1.8.0_202"
    Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
    Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, compiled mode)
    ```
3. -Xmixed 混合模式，JVM自己巨顶是否编译成本地代码(默认就是)
    ```jshelllanguage
    C:\Users\23746\Documents\zhsl_test\jdk8>java -Xmixed  -version
    java version "1.8.0_202"
    Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
    Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)
    ```

#### XX参数
1. Boolean 类型

*格式: -XX:[+-]<name>*表示启用或者禁用name属性
例如
```jshelllanguage
-XX:+UseConcMarkSweepGC
-XX:+UseG1GC
```
2. 非Boolean类型

*格式: -XX<name>=<value>* 表示name属性的值是value

例如 -Xmx(初始化堆的最大值),-Xms(初始化堆的最小值)

```jshelllanguage
-XX:MaxGCPauseMillis=500
-XX:GCTimeRatio=19
```
查看属性值 如:
```jshelllanguage
C:\Users\23746\Documents\zhsl_test>jinfo -flag MaxHeapSize 4884
-XX:MaxHeapSize=4278190080
C:\Users\23746\Documents\zhsl_test>jinfo -flag ThreadStackSize 4884
-XX:ThreadStackSize=0
```
### 查看JVM运行时参数

-XX:+PrintFlagsInitial 查看初始值

-XX:+PrintFlagsFinal 查看最终值

-XX:+UnlockExperimentalVMOptions 解锁实验参数

-XX:+UnlockDiagnosticVMOptions 解锁诊断参数

-XX:+PrintCommandLineFlags 打印命令行参数

说明：=（表示默认值），:= （被用户/JVM修改后的值）

1. PrintFlagsFinal
查看所有
```jshelllanguage
C:\Users\23746\Documents\zhsl_test>java -XX:+PrintFlagsFinal -version
[Global flags]
     intx ActiveProcessorCount                      = -1                                  {product}
    uintx AdaptiveSizeDecrementScaleFactor          = 4                                   {product}
    uintx AdaptiveSizeMajorGCDecayTimeScale         = 10                                  {product}
    uintx AdaptiveSizePausePolicy                   = 0                                   {product}
    uintx AdaptiveSizePolicyCollectionCostMargin    = 50                                  {product}
    uintx AdaptiveSizePolicyInitializingSteps       = 20                                  {product}
    uintx AdaptiveSizePolicyOutputInterval          = 0                                   {product}
    ...
```
将打印出来的存入文件中
```jshelllanguage
C:\Users\23746\Documents\zhsl_test>java -XX:+PrintFlagsFinal -version>1.txt
```

### 常用参数说明

    堆设置
        -Xms:   堆内存的最小大小，默认为物理内存的1/64。
        -Xmx:   堆内存的最大大小，默认为物理内存的1/4
        -Xmn:   堆内新生代的大小。通过这个值也可以得到老生代的大小：-Xmx减去-Xmn
        -Xss:   设置每个线程可使用的内存大小，即栈的大小。在相同物理内存下，减小这个值能生成更多的线程，当然操作系统对一个进程内的线程数还是有限制的，不能无限生成。线程栈的大小是个双刃剑，如果设置过小，可能会出现栈溢出，特别是在该线程内有递归、大的循环时出现溢出的可能性更大，如果该值设置过大，就有影响到创建栈的数量，如果是多线程的应用，就会出现内存溢出的错误。
        -XX:NewRatio:    设置年轻代和年老代的比值。如:为3，表示年轻代与年老代比值为1：3，年轻代占整个年轻代年老代和的1/4
        -XX:SurvivorRatio:  新生代中Eden区与两个Survivor区的比值。注意Survivor区有两个。如：为3，表示Eden：Survivor=3：2，一个Survivor区占整个新生代的1/5
        -XX:MaxTenuringThreshold:   设置转入老年代的存活次数。如果是0，则直接跳过新生代进入老年代
        -XX:MetaspaceSize:  设置元空间最小大小
        -XX:MaxMetaspaceSize:   设置元空间最大大小

    收集器设置
        -XX:+UseSerialGC:   设置串行收集器
        -XX:+UseParallelGC: 设置并行收集器
        -XX:+UseParalledlOldGC: 设置并行老年代收集器
        -XX:+UseConcMarkSweepGC:    设置并发收集器

    垃圾回收统计信息
        -XX:+PrintGC:
        -XX:+PrintGCDetails:
        -XX:+PrintGCTimeStamps:
        -Xloggc:filename:

     并行收集器设置
         -XX:ParallelGCThreads=n:设置并行收集器收集时使用的CPU数。并行收集线程数。
         -XX:MaxGCPauseMillis=n:设置并行收集最大暂停时间
         -XX:GCTimeRatio=n:设置垃圾回收时间占程序运行时间的百分比。公式为1/(1+n)

    并发收集器设置
        -XX:+CMSIncrementalMode:设置为增量模式。适用于单CPU情况。
        -XX:ParallelGCThreads=n:设置并发收集器新生代收集方式为并行收集时，使用的CPU数。并行收集线程数。

    -XX:NewSize     设置新生代最小空间大小。
    -XX:CMSInitiatingPermOccupancyFraction： 当永久区占用率达到这一百分比时，启动CMS回收
    -XX:CMSInitiatingOccupancyFraction： 设置CMS收集器在老年代空间被使用多少后触发
    -XX:+CMSClassUnloadingEnabled：  允许对类元数据进行回收
    -XX:CMSFullGCsBeforeCompaction： 设定进行多少次CMS垃圾回收后，进行一次内存压缩
    -XX:NewRatio:   新生代和老年代的比
    -XX:ParallelCMSThreads： 设定CMS的线程数量
    -XX:ParallelGCThreads：  设置用于垃圾回收的线程数
    -XX:SurvivorRatio：  设置eden区大小和survivior区大小的比例
    -XX:+UseParNewGC：   在新生代使用并行收集器
    -XX:+UseParallelGC ：    新生代使用并行回收收集器
    -XX:+UseParallelOldGC：  老年代使用并行回收收集器
    -XX:+UseSerialGC：   在新生代和老年代使用串行收集器
    -XX:+UseConcMarkSweepGC：    新生代使用并行收集器，老年代使用CMS+串行收集器
    -XX:+UseCMSCompactAtFullCollection： 设置CMS收集器在完成垃圾收集后是否要进行一次内存碎片的整理
    -XX:UseCMSInitiatingOccupancyOnly：  表示只在到达阀值的时候，才进行CMS回收
    -XX:MaxNewSize  设置新生代最大空间大小。
    -XX:PermSize    设置永久代最小空间大小。
    -XX:MaxPermSize 设置永久代最大空间大小。

## 监控调优命令
### JPS
jps(JVM Process Status Tool)：JVM机进程状况工具,
[JPS-API]: https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jps.html

常用参数说明：

    -m 输出传递给main方法的参数，如果是内嵌的JVM则输出为null。

    -l 输出应用程序主类的完整包名，或者是应用程序JAR文件的完整路径。

    -v 输出传给JVM的参数。


## 其他

1. 查看端口的进程
```jshelllanguage
netstat -ano|findstr 8080
```