package clyz.ziv.jdk8.thread.executor.FixedThreadPool;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VisFixedThreadPool {

    private ExecutorService executor = Executors.newFixedThreadPool(4);

    public void executor(Runnable runnable) {
        executor.execute(runnable);
    }

    public void shutdown(){
        executor.shutdown();
    }

}
