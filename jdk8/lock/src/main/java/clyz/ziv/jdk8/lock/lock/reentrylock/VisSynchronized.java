package clyz.ziv.jdk8.lock.lock.reentrylock;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;

/**
 * <p>
 * synchronized 重入锁
 * </p>
 */
public class VisSynchronized {

    public synchronized void get() {
        System.out.println("哈喽");
    }

    public synchronized void say() {
        System.out.println("I am fine!");
    }

    /**
     * <p>
     * 并不会发生死锁
     * </p>
     */
    public synchronized void getSay() {
        System.out.println("SB");
        say();
    }

    public static void insertThrow(Thread thread, ArrayList<Integer> arrayList, Lock lock) throws InterruptedException {
        System.out.println(thread.getName() + "得到了锁");
        for (int i = 0; i < 5; i++) {
            arrayList.add(i);
        }
        Thread.sleep(2000);
    }

    public static void insert(Thread thread, ArrayList<Integer> arrayList, Lock lock) {
        try {
            System.out.println(thread.getName() + "得到了锁");
            for (int i = 0; i < 5; i++) {
                arrayList.add(i);
            }
            Thread.sleep(2000);
        } catch (Exception e) {
        } finally {
            System.out.println(thread.getName() + "释放了锁");
            lock.unlock();
        }
    }

}
