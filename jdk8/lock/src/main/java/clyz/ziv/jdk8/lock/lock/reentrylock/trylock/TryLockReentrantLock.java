package clyz.ziv.jdk8.lock.lock.reentrylock.trylock;

import clyz.ziv.jdk8.lock.lock.reentrylock.VisSynchronized;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock 可重入锁 的 tryLock() 非堵塞尝试拿锁
 */
public class TryLockReentrantLock {
    private ArrayList<Integer> arrayList = new ArrayList<Integer>();
    private Lock lock = new ReentrantLock();    //注意这个地方

    @Test
    public void tryLockReentrantLock() {
        final TryLockReentrantLock test = new TryLockReentrantLock();

        final Runnable target = () -> test.insert(Thread.currentThread());

        new Thread(target).start();
        new Thread(target).start();

        LockSupport.park();
    }

    private void insert(Thread thread) {
        if (lock.tryLock()) {    //  非堵塞尝试拿锁
            VisSynchronized.insert(thread, arrayList, lock);
        } else {
            System.out.println(thread.getName() + "获取锁失败");
        }
    }
}
