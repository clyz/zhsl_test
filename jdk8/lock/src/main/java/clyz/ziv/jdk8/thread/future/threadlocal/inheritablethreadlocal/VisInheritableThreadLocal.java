package clyz.ziv.jdk8.thread.future.threadlocal.inheritablethreadlocal;

/**
 * <p>
 * 将主线程 中的值 专递到子线程中！
 *
 *
 * </p>
 */
public class VisInheritableThreadLocal implements Runnable {

    public static InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal();

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "::" + inheritableThreadLocal.get());
    }

}
