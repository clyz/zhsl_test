package clyz.ziv.jdk8.thread.future.threadlocal;


/**
 * <p>
 * <p>
 * 解决 共享资源 的 并发
 *
 *
 * </p>
 */
public class ThreadLocalExample {

    public static class MyRunnable implements Runnable {

        private ThreadLocal<Integer> threadLocal = new ThreadLocal<>(); //  共享资源 数据不共享

        @Override
        public void run() {
            final double random = Math.random();
            System.out.println(Thread.currentThread().getName() + "::" + random);
            threadLocal.set((int) (random * 100D));

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(threadLocal.get());
        }
    }


    public static class VisRunnable implements Runnable {

        private Integer integer = 0;   //  共享资源 并发

        @Override
        public void run() {
            final double random = Math.random();
            System.out.println(Thread.currentThread().getName() + "::" + random);
            integer = (int) (random * 100D);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(integer);
        }
    }


}
