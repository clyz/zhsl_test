package clyz.ziv.jdk8.lock.readwritelock.ReentrantReadWriteLock;

import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 与 synchronized 同步堵塞 锁 相对比 读（堵塞），写（堵塞）
 * <p>
 * ReentrantReadWriteLock 读写 锁 的 案例
 * <p>
 * 读（非堵塞），写（堵塞）
 */
public class VisReadWriteReentrantReadWriteLock {

    ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();

    @Test
    public void visReadWriteReentrantReadWriteLock() {
        final VisReadWriteReentrantReadWriteLock test = new VisReadWriteReentrantReadWriteLock();

        final Runnable target = () -> test.get(Thread.currentThread());
        new Thread(target).start();
        new Thread(target).start();

    }

    //  堵塞
    public void get(Thread thread) {
        reentrantReadWriteLock.readLock().lock();
        try {
            long start = System.currentTimeMillis();
            while (System.currentTimeMillis() - start <= 10) {
                System.out.println(thread.getName() + "正在进行读操作");
            }
            System.out.println(thread.getName() + "------------读操作完毕");
        } finally {
            reentrantReadWriteLock.readLock().unlock();
        }
    }
}