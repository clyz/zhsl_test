package clyz.ziv.jdk8.thread.future.runnableFuture;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class VisFuture<V> {

    private Thread thread;
    private FutureTask<V> futureTask;

    public VisFuture(Callable<V> callable) {
        futureTask = new FutureTask<V>(callable);
        thread = new Thread(futureTask);
    }

    public void run() {
        thread.start();
    }

    public Object get() throws ExecutionException, InterruptedException {
        return futureTask.get();
    }

    public boolean isDone(){
        return futureTask.isDone();
    }

}
