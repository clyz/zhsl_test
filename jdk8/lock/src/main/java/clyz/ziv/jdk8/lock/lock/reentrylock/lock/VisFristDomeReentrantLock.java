package clyz.ziv.jdk8.lock.lock.reentrylock.lock;

import clyz.ziv.jdk8.lock.lock.reentrylock.VisSynchronized;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * BUG
 * <p>
 * Lock lock = new ReentrantLock();
 * Lock lock() 堵塞 拿锁
 * 线程 A 与 线程 B 不是使用的同一个锁 SO 为能实现锁的效果
 */
public class VisFristDomeReentrantLock {

    private ArrayList<Integer> arrayList = new ArrayList<Integer>();

    @Test
    public void visFristDomeReentrantLock() {
        final VisFristDomeReentrantLock test = new VisFristDomeReentrantLock();

        final Runnable target = () -> test.insert(Thread.currentThread());

        new Thread(target).start();
        new Thread(target).start();
    }

    public void insert(Thread thread) {
        Lock lock = new ReentrantLock();    //注意这个地方
        VisSynchronized.insert(thread, arrayList,lock);
    }
}
