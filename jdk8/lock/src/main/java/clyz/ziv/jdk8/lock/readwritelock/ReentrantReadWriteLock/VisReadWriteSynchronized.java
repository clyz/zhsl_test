package clyz.ziv.jdk8.lock.readwritelock.ReentrantReadWriteLock;

import org.junit.jupiter.api.Test;

/**
 * 与 synchronized 同步堵塞 锁 相对比 读（堵塞），写（堵塞）
 * <p>
 * ReentrantReadWriteLock 读写 锁 的 案例
 * <p>
 * 读（非堵塞），写（堵塞）
 */
public class VisReadWriteSynchronized {

    @Test
    public void visReadWriteSynchronized() {
        final VisReadWriteSynchronized test = new VisReadWriteSynchronized();

        final Runnable target = () -> test.get(Thread.currentThread());
        new Thread(target).start();
        new Thread(target).start();

    }

    //  堵塞
    public synchronized void get(Thread thread) {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start <= 1) {
            System.out.println(thread.getName() + "正在进行读操作");
        }
        System.out.println(thread.getName() + "读操作完毕");
    }
}