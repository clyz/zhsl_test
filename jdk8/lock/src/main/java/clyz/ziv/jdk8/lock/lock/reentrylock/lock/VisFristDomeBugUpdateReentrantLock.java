package clyz.ziv.jdk8.lock.lock.reentrylock.lock;

import clyz.ziv.jdk8.lock.lock.reentrylock.VisSynchronized;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Lock lock() 堵塞 拿锁
 * 解决 VisFristDomeReentrantLock 的 BUG
 */
public class VisFristDomeBugUpdateReentrantLock {
    private ArrayList<Integer> arrayList = new ArrayList<Integer>();
    private Lock lock = new ReentrantLock();    //注意这个地方

    @Test
    public void visFristDomeReentrantLock() {
        final VisFristDomeBugUpdateReentrantLock test = new VisFristDomeBugUpdateReentrantLock();

        final Runnable target = () -> test.insert(Thread.currentThread());

        new Thread(target).start();
        new Thread(target).start();
    }

    public void insert(Thread thread) {
        insert(thread, lock, arrayList);
    }

    static void insert(Thread thread, Lock lock, ArrayList<Integer> arrayList) {
        lock.lock();
        VisSynchronized.insert(thread, arrayList, lock);
    }
}
