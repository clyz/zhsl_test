package clyz.ziv.jdk8.thread.future.forkjointask;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;

/**
 * Created by windwant on 2016/6/3.
 */
public class MyForkJoin extends RecursiveTask<Integer> {

    public Integer num = 0;

    private File file;

    MyForkJoin(File file) {
        this.file = file;
    }

    /**
     * <p>
     * <p>
     * 统计文件的个数
     * </p>
     *
     * @return
     */
    @Override
    protected Integer compute() {
        List<MyForkJoin> taskList = new ArrayList<>();
        if (file.isDirectory()) {
            File[] list = file.listFiles();
            if (list != null) {
                for (File subf : list) {
                    if (subf.isDirectory()) {
                        MyForkJoin mt = new MyForkJoin(subf);
                        taskList.add(mt);
                    } else {
//                        System.out.println(num + ":" + Thread.currentThread().getName() + ":::" + subf.getName());
                        num++;
                    }
                }
            }
        } else {
            num = 1;
        }

        if (!taskList.isEmpty()) {
            final Collection<MyForkJoin> myForkJoins = invokeAll(taskList);
            if (getException() != null) {

            }
            for (MyForkJoin mtask : taskList) {
//                num += mtask.join();

                try {
                    num +=mtask.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
        return num;
    }
}