package clyz.ziv.jdk8.thread.AQS;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * <p>
 * 研究 AQS
 * </p>
 */
public class VisLockAQS implements Lock {

    private Helper helper = new Helper();

    private class Helper extends AbstractQueuedSynchronizer {
        /**
         * 如果第一个线程进入，可以获取到锁 true
         * 如果第二线程进入 ， 不可获取到锁 false
         * <p>
         *
         * 如果是统一线程 能拿到锁
         */
        @Override
        protected boolean tryAcquire(int arg) {
            final int state = getState();  //  获取锁状态
            final Thread thread = Thread.currentThread();
            if (state == 0) { //  第一次进入
                if (compareAndSetState(0, arg)) {  //  CAS 更新状态值
                    setExclusiveOwnerThread(thread);    //  设置当前线程
                    return true;
                }
            } else if (getExclusiveOwnerThread() == thread) {
                setState(state + 1);
                return true;
            }
            return false;
        }

        /**
         * 调用该方法的线程 必定是当前线程
         */
        @Override
        protected boolean tryRelease(int arg) {
            if (Thread.currentThread() != getExclusiveOwnerThread()) {
                throw new RuntimeException("NOT THREAD");
            }

            final int state = getState() - arg;
            boolean flag = false;

            if (state == 0) {  //  当前有一个线程运行
                setExclusiveOwnerThread(null);
                flag = true;
            }
            setState(state);

            return flag;
        }

        Condition newCondition() {
            return new ConditionObject();
        }
    }

    @Override
    public void lock() {
        helper.tryAcquire(1);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        helper.acquireInterruptibly(1);
    }

    @Override
    public boolean tryLock() {
        return helper.tryAcquire(1);
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return helper.tryAcquireNanos(1, unit.toNanos(time));
    }

    @Override
    public void unlock() {
        helper.release(1);
    }

    @Override
    public Condition newCondition() {
        return helper.newCondition();
    }

}
