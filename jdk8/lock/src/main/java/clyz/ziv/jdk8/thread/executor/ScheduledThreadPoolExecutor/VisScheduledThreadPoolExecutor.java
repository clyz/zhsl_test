package clyz.ziv.jdk8.thread.executor.ScheduledThreadPoolExecutor;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * ScheduledThreadPoolExecutor可以用来在给定延时后执行异步任务或者周期性执行任务，
 * 相对于任务调度的Timer来说，其功能更加强大，
 * Timer只能使用一个后台线程执行任务，
 * 而ScheduledThreadPoolExecutor则可以通过构造函数来指定后台线程的个数。
 *
 * @author YangZemin
 * @date 2020/3/3 10:13
 */
public class VisScheduledThreadPoolExecutor {

    /**
     * 任务调度线程池
     */
    private ScheduledThreadPoolExecutor pool = new ScheduledThreadPoolExecutor(5);

    public void schedule(Callable callable,long delay, TimeUnit unit) {
        pool.schedule(callable, delay, unit);
    }

}
