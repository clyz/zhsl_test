package clyz.ziv.jdk8.lock.lock.reentrylock.lockinterruptibly;

import clyz.ziv.jdk8.lock.lock.reentrylock.VisSynchronized;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Lock lockInterruptibly() 当通过这个方法去获取锁时，如果线程正在等待获取锁，则这个线程能够响应中断
 * ，即中断线程的等待状态。
 * <p>
 * 也就使说，当两个线程同时通过lock.lockInterruptibly()想获取某个锁时，
 * 假若此时线程A获取到了锁，而线程B只有在等待，那么对线程B调用threadB.interrupt()方法能够中断线程B的等待过程。
 */
public class LockInterruptiblyReentrantLock {
    private ArrayList<Integer> arrayList = new ArrayList<Integer>();
    private ReentrantLock lock = new ReentrantLock();    //注意这个地方

    @Test
    public void tryLockReentrantLock() throws InterruptedException {
        final LockInterruptiblyReentrantLock test = new LockInterruptiblyReentrantLock();

        final Runnable target = () -> test.insert(Thread.currentThread());

        new Thread(target).start();
        new Thread(target).start();
        final Thread thread = new Thread(target);
        thread.start();
        new Thread(target).start();

        Thread.sleep(2000);
        thread.interrupt();

        LockSupport.park();
    }

    private void insert(Thread thread) {
        try {
            /**
             * //注意，如果需要正确中断等待锁的线程，必须将获取锁放在外面，然后将InterruptedException抛出
             * 中断 式获取锁
             */
            lock.lockInterruptibly();
            VisSynchronized.insertThrow(thread, arrayList, lock);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + "被中断");
        } finally {
            System.out.println(Thread.currentThread().getName() + "执行finally");
            /**
             *   当 其中一个线程被中断时 如果
             * 当前 被锁的线程不是当前线程 unlock() 就会抛出IllegalMonitorStateExceptiony()异常
             *
             * (抛出该异常表明某一线程已经试图等待对象的监视器，或者试图通知其他正在等待对象的监视器，然而本身没有指定的监视器的线程。)
             */
            if(lock.isHeldByCurrentThread()){
                lock.unlock();
            }
            System.out.println(thread.getName() + "释放了锁");
        }
    }
}