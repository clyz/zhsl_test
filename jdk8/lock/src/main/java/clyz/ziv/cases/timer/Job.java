package clyz.ziv.cases.timer;

import java.io.Serializable;

/**
 * @author YangZemin
 * @date 2020/3/3 13:43
 */
public interface Job extends Serializable {

    void execute() throws Exception;

}
