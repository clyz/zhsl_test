package clyz.ziv.cases.timer;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 定时任务框架
 *
 * @author YangZemin
 * @date 2020/3/3 13:39
 */
public class VisQuartz extends Thread {

    private ScheduledThreadPoolExecutor pool = new ScheduledThreadPoolExecutor(5);

    private Queue<Trigger> queue = new ConcurrentLinkedQueue<Trigger>();
    private Thread thread;
    private boolean isSleep;

    /**
     * 注册一个任务调度
     *
     * @param trigger 调度器
     */
    public void add(Trigger trigger) {
        queue.add(trigger);
        if (isSleep){
            LockSupport.unpark(thread);
        }
    }

    @Override
    public void run() {
        thread = Thread.currentThread();
        isSleep = false;
        while (true) {
            if (!queue.isEmpty()) {
                final Trigger trigger = queue.poll();
                pool.schedule(new TriggerExecutor(trigger.getJob()), trigger.delay(), TimeUnit.MILLISECONDS);
            } else {
                isSleep = true;
                LockSupport.park();
            }
        }
    }

    private class TriggerExecutor extends Thread {
        private Job job;

        public TriggerExecutor(Job job) {
            this.job = job;
        }

        @Override
        public void run() {
            try {
                job.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
