package clyz.ziv.cases.timer.job;

import clyz.ziv.cases.timer.Job;

import java.time.LocalDateTime;

/**
 * @author YangZemin
 * @date 2020/3/3 14:15
 */
public class SoutJob implements Job {
    @Override
    public void execute() throws Exception {
        System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now().toString());
    }
}
