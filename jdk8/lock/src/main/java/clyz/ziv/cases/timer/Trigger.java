package clyz.ziv.cases.timer;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author YangZemin
 * @date 2020/3/3 13:41
 */
public class Trigger {
    /**
     * 下一个执行时间
     */
    private LocalDateTime runTime;

    private Job job;

    public Trigger(LocalDateTime runTime, String jobBean) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        this.runTime = runTime;
        this.job = ((Class<Job>) Class.forName(jobBean)).newInstance();
    }

    /**
     *
     * @param runTimeDateString 2011-12-03T10:15:30
     * @param jobBean clyz.ziv.cases.timer.Job
     */
    public Trigger(String runTimeDateString,String jobBean) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        this(LocalDateTime.parse(runTimeDateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME),jobBean);
    }

    /**
     *
     * @param runTimeDateString 2011-12-03T10:15:30
     * @param job clyz.ziv.cases.timer.Job
     */
    public Trigger(String runTimeDateString, Job job) {
        this(LocalDateTime.parse(runTimeDateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME),job);
    }

    public Trigger(LocalDateTime runTime, Job job) {
        this.runTime = runTime;
        this.job = job;
    }

    Job getJob() {
        return job;
    }

    public long delay() {
        final long l = Duration.between(LocalDateTime.now(), runTime).toMillis();
        return l <= 0 ? 0 : l;
    }
}
