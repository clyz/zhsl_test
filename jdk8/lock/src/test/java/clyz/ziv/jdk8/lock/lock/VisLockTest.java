package clyz.ziv.jdk8.lock.lock;

import org.junit.After;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

public class VisLockTest {

    @Test
    public void vis() {
        final VisValue visValue = new VisValue();
        new Thread(() -> {
            while (true) {
                visValue.valueNext();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            while (true) {
                visValue.valueNext();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Test
    public void vislock() {
        final VisValue visValue = new VisValue();
        final Lock lock = new ReentrantLock();

        new Thread(() -> {
            while (true) {
                lock.lock();
                visValue.valueNext();
                lock.unlock();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            while (true) {
                lock.lock();
                visValue.valueNext();
                lock.unlock();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    class VisValue {
        private int value;

        public int valueNext() {
            System.out.println(Thread.currentThread().getName() + " _ " + value);
            return value++;
        }

        public int getValue() {
            return value;
        }
    }

    @After
    public void after() throws IOException {
        LockSupport.park(Thread.currentThread());
    }
}