package clyz.ziv.jdk8.thread.executor.ScheduledThreadPoolExecutor;


import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;


public class VisScheduledThreadPoolExecutorTest {
    private VisScheduledThreadPoolExecutor visScheduledThreadPoolExecutor;

    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;

    @Before
    public void before() {
        visScheduledThreadPoolExecutor = new VisScheduledThreadPoolExecutor();
        scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(5);
    }

    @Test
    public void scheduleThread() throws ExecutionException, InterruptedException {
        scheduledThreadPoolExecutor.schedule(
                new Thread(){
                    @Override
                    public void run() {

                    }
                }
                , 2, TimeUnit.SECONDS
        );
    }

    @Test
    public void schedule() throws ExecutionException, InterruptedException {
        final String s = scheduledThreadPoolExecutor.schedule(
                () -> {
                    return "Zemin.Yang " + LocalDateTime.now().toString();
                }, 2, TimeUnit.SECONDS
        ).get();
        System.out.println(s);
    }

    @Test
    public void scheduleVis() {
        visScheduledThreadPoolExecutor.schedule(
                () -> {
                    System.out.println("Zemin.Yang " + LocalDateTime.now().toString());
                    return "s";
                }, 2, TimeUnit.MILLISECONDS
        );
        LockSupport.park();
    }
}
