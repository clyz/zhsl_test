package clyz.ziv.cases.timer;

import clyz.ziv.cases.timer.job.SoutJob;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.locks.LockSupport;

public class VisQuartzTest {

    @Test
    public void start() throws IllegalAccessException, InstantiationException, ClassNotFoundException, InterruptedException {
        final VisQuartz quartz = new VisQuartz();

        quartz.add(new Trigger("2020-03-03T14:59:30", new SoutJob()));
        quartz.add(new Trigger(LocalDateTime.now(), "clyz.ziv.cases.timer.job.SoutJob"));

        quartz.start();

        Thread.sleep(100);
        quartz.add(new Trigger("2020-03-03T15:00:00", new SoutJob()));

        Thread.sleep(10);
        quartz.add(new Trigger("2020-03-03T15:01:40", new SoutJob()));
        LockSupport.park();
    }
}
