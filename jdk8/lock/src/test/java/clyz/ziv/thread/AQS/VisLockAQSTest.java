package clyz.ziv.jdk8.thread.AQS;

import org.junit.Test;

import java.util.concurrent.locks.LockSupport;

public class VisLockAQSTest {

    private int value = 0;

    final VisLockAQS visLockAQS = new VisLockAQS();

    public int next() {
        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        visLockAQS.lock();
        value++;
        visLockAQS.unlock();
        return value;
    }

    @Test
    public void visLock() {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " " + next());
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " " + next());
            }
        }).start();

        LockSupport.park();
    }
}