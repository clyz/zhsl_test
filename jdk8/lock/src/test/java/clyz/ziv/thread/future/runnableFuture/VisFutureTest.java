package clyz.ziv.jdk8.thread.future.runnableFuture;

import org.junit.Before;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class VisFutureTest {

    private VisFuture visFuture;
    private final static int SLEEP = 3000;

    @Before
    public void before() {
        visFuture = new VisFuture(() -> {
            System.out.println(Thread.currentThread().getName() + ":: 睡觉" + SLEEP + " ms");
            Thread.sleep(SLEEP);
            System.out.println(Thread.currentThread().getName() + ":: 睡醒了返回值了");
            return true;
        });
    }


    @org.junit.Test
    public void run() {
        visFuture.run();
    }

    @org.junit.Test
    public void get() throws ExecutionException, InterruptedException {
        visFuture.run();
        System.out.println("开始取值:");
        System.out.println(visFuture.isDone());
        final Object o = visFuture.get();
        System.out.println(visFuture.isDone());
        System.out.println("值为:" + (Boolean) o);
        System.out.println();
    }


    /**
     * 举个例子：比如去吃早点时，点了包子和凉菜，包子需要等3分钟，凉菜只需1分钟，
     * 如果是串行的一个执行，在吃上早点的时候需要等待4分钟，但是因为你在等包子的时候，
     * 可以同时准备凉菜，所以在准备凉菜的过程中，可以同时准备包子，这样只需要等待3分钟。
     * 那Future这种模式就是后面这种执行模式。
     * ---------------------
     * 作者：石硕页
     * 来源：CSDN
     * 原文：https://blog.csdn.net/u014209205/article/details/80598209
     * 版权声明：本文为博主原创文章，转载请附上博文链接！
     *
     * @param args
     * @throws InterruptedException
     * @throws ExecutionException
     */

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        long start = System.currentTimeMillis();

        // 等凉菜
        FutureTask<String> ft1 = new FutureTask<String>(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "凉菜准备完毕";
        });

        // 等包子 -- 必须要等待返回的结果，所以要调用join方法
        FutureTask<String> ft2 = new FutureTask<>(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "包子准备完毕";
        });

        final Thread th1 = new Thread(ft1);
        final Thread th2 = new Thread(ft2);

        //  让父线程等待子线程结束之后才能继续运行。

        th1.start();
        th1.join(); //  让 主线程 等待 th1 线程执行

        th2.start();

        System.out.println(ft1.get());
        System.out.println(ft2.get());

        long end = System.currentTimeMillis();
        System.out.println("准备完毕时间：" + (end - start));
    }

}