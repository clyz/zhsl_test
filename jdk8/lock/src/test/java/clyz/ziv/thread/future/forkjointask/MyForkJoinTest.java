package clyz.ziv.jdk8.thread.future.forkjointask;

import org.junit.Test;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class MyForkJoinTest {

    @Test
    public void compute() {
        final long l = System.currentTimeMillis();
        MyForkJoin task = new MyForkJoin(new File("C:\\"));
        /**
         * parallelism:4 -> 260128 条数据，17164sm
         * parallelism:50 -> 260123 条数据，17707sm
         * parallelism:100 -> 260124 条数据，18649sm
         */
//        Integer sum = new ForkJoinPool(1000).invoke(task); // invoke 同步
//        System.out.println(sum);


/**
 * parallelism:4 260175 条数据 17959sm
 * parallelism:50 260176 条数据 18022sm
 */
        final ForkJoinTask<Integer> joinTask = new ForkJoinPool(50).submit(task);
        try {
            System.out.println(joinTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
//


       /* new ForkJoinPool(50).execute(task);

        while (true){
            if(task.isDone()){
                System.out.println(task.num);
                break;
            }
        }*/










        System.out.println(System.currentTimeMillis() - l + "sm");

    }
}