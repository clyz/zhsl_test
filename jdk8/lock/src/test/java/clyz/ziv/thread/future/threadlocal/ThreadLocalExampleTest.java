package clyz.ziv.jdk8.thread.future.threadlocal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.locks.LockSupport;

import static org.junit.Assert.*;

public class ThreadLocalExampleTest {

    private Runnable simpleRunnable1 ;
    private Runnable localRunnable ;
    private Runnable simpleRunnable2 ;

    @Before
    public void before(){
        simpleRunnable1 = new ThreadLocalExample.VisRunnable();
        simpleRunnable2 = new ThreadLocalExample.VisRunnable();
        localRunnable = new ThreadLocalExample.MyRunnable();
    }

    @Test
    public void localRunnable() throws InterruptedException {
        Thread thread1 = new Thread(localRunnable);
        Thread thread2 = new Thread(localRunnable);

        thread1.start();
        thread2.start();

//        thread1.join(); //wait for thread 1 to terminate
//        thread2.join(); //wait for thread 2 to terminate
    }

    @Test
    public void setSimpleRunnable() throws InterruptedException {

        Thread thread1 = new Thread(simpleRunnable1);
        Thread thread2 = new Thread(simpleRunnable1);

        thread1.start();
        thread2.start();

        thread1.join(); //wait for thread 1 to terminate
        thread2.join(); //wait for thread 2 to terminate
    }

    @Test
    public void setSimpleRunnable12() throws InterruptedException {

        Thread thread1 = new Thread(simpleRunnable1);
        Thread thread2 = new Thread(simpleRunnable2);

        thread1.start();
        thread2.start();

        thread1.join(); //wait for thread 1 to terminate
        thread2.join(); //wait for thread 2 to terminate
    }

    @After
    public void after() throws InterruptedException {
        LockSupport.unpark(Thread.currentThread());
    }

}