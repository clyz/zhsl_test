package clyz.ziv.jdk8.thread.future.forkjointask;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

public class CountTaskTest {

    @org.junit.Test
    public void compute() {
        ForkJoinPool forkjoinPool = new ForkJoinPool();

        //生成一个计算任务，计算1+2+3+4
        CountTask task = new CountTask(1, 100);

        //执行一个任务
        Future<Integer> result = forkjoinPool.submit(task);

        try {
            System.out.println(result.get());
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}