package clyz.ziv.jdk8.thread.future.threadlocal.inheritablethreadlocal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VisInheritableThreadLocalTest {

    private InheritableThreadLocal<String> inheritableThreadLocal;
    private Thread thread;

    @Before
    public void setUp() throws Exception {
        inheritableThreadLocal = VisInheritableThreadLocal.inheritableThreadLocal;
        inheritableThreadLocal.set(Thread.currentThread().getName() + "_");

        thread = new Thread(new VisInheritableThreadLocal());
    }

    @Test
    public void testUpdate() throws InterruptedException {
        thread.start();


        inheritableThreadLocal.set(Thread.currentThread().getName() + "_11");
        while (thread.isAlive()) {
        }
        thread = new Thread(new VisInheritableThreadLocal());
        thread.start();
    }

    @Test
    public void test() throws InterruptedException {
        thread.start();

    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
    }
}