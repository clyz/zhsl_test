package clyz.ziv.jdk8.stream.spliterator;

import org.junit.After;
import org.junit.Test;

import java.util.Spliterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class NumCounterTest {

    String arr = "12%3 21sdas s34d dfsdz45 🕵  R3 jo34 sjkf8 3$1P 213ikflsd fdg55 kfd 中国";
    NumCounter numCounter;


    /**
     * 字符串中的数字计算器实现
     * <p>
     * <p>
     * <p>
     * 问题：求这个字符串"12%3 21sdas s34d dfsdz45 R3 jo34 sjkf8 3$1P 213ikflsd fdg55 kfd"中所有的数字之和
     * 例子：比如这种"12%sdf3"，和就是12+3=15，这种"12%3 21sdas"和就是12+3+21=36
     * <p>
     * 思路：字符串要用到Stream，只有把整个字符串拆分成一个个Character，而是否是并行，按道理讲只需要改一个标志位即可
     */

    @Test
    public void stream() {
        numCounter = IntStream.range(0, arr.length())
                .mapToObj(arr::charAt).peek(System.out::print)
                .reduce(new NumCounter(0, 0, false),
                        NumCounter::accumulate, NumCounter::combine);
    }

    /**
     * 为什么会执行错误，是因为默认的Spliterator在并行时并不知道整个字符串从哪里开始切割，
     * 由于切割错误，导致把本来完整的数字比如123，可能就切成了12和3，这样加起来的数字肯定不对
     * 若是理解了上诉顺序执行的NumCounter的逻辑，再来看看Spliterator的实现
     */
    @Test
    public void parallel() {
        numCounter = IntStream.range(0, arr.length())
                .parallel()
                .mapToObj(arr::charAt).peek(System.out::print)
                .reduce(new NumCounter(0, 0, false),
                        NumCounter::accumulate, NumCounter::combine);
    }

    @Test
    public void parallelSpliterator(){
        numCounter = StreamSupport.stream(new NumCounterSpliterator(arr), true)
                .reduce(new NumCounter(0, 0, false),
                        NumCounter::accumulate, NumCounter::combine);
    }

    @After
    public void after(){
        System.out.println("\nordered total:" + numCounter.getSum());
    }

}