package clyz.ziv.distributedaffair.atomikos.service;

import clyz.ziv.distributedaffair.atomikos.mapper1.UserMapper1;
import clyz.ziv.distributedaffair.atomikos.mapper2.UserMapper2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ManyService1 {

    @Autowired
    private UserMapper1 userMapper1;

    @Autowired
    private UserMapper2 userMapper2;

    // 开启事务，由于使用jta+atomikos解决分布式事务，所以此处不必再指定事务
    @Transactional
    public int insert(String name, Integer age) {
        int insert = userMapper1.insert(name, age);
        int i = 1 / age;// 赋值age为0故意引发事务
//        throw new RuntimeException();
        return insert;
    }

    // 开启事务，由于使用jta+atomikos解决分布式事务，所以此处不必再指定事务
    @Transactional
    public int insertDb1AndDb2(String name, Integer age) {
        int insert = userMapper1.insert(name, age);
        int insert2 = userMapper2.insert(name, age);
        int i = 1 / age;// 赋值age为0故意引发事务
        return insert + insert2;
    }

}