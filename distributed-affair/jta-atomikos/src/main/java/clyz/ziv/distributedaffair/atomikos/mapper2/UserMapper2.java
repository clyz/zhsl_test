package clyz.ziv.distributedaffair.atomikos.mapper2;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper2 {

    @Insert(value = "INSERT INTO user(name,age) VALUES(#{name},#{age} )")
    int insert(@Param("name") String name, @Param("age") Integer age);

}
