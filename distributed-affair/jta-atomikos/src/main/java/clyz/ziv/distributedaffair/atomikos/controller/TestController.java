package clyz.ziv.distributedaffair.atomikos.controller;

import clyz.ziv.distributedaffair.atomikos.service.ManyService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private ManyService1 manyService1;

    @RequestMapping(value = {"insertDb1AndDb2", "/"})
    public int insertDb1AndDb2(@RequestParam(defaultValue = "Zemin.Yang") String name,
                               @RequestParam(defaultValue = "0") Integer age) {
        return manyService1.insertDb1AndDb2(name, age);
    }
}
