package clyz.ziv.distributedaffair.atomikos.dome;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;

import javax.transaction.SystemException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class AtomikosJTATest {

    private static AtomikosDataSourceBean createAtomikosDataSourceBean(String dbName) {
        Properties p = new Properties();
        p.setProperty("url", "jdbc:mysql://localhost:3306/" + dbName);
        p.setProperty("user", "root");
        p.setProperty("password", "123456");

        AtomikosDataSourceBean atomikosDataSourceBean = new AtomikosDataSourceBean();
        atomikosDataSourceBean.setUniqueResourceName(dbName);
        atomikosDataSourceBean.setXaDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlXADataSource");
        atomikosDataSourceBean.setXaProperties(p);
        atomikosDataSourceBean.setConcurrentConnectionValidation(true);

        return atomikosDataSourceBean;
    }

    public static void main(String[] args) throws SystemException, SQLException {
        AtomikosDataSourceBean a = createAtomikosDataSourceBean("a");
        AtomikosDataSourceBean test = createAtomikosDataSourceBean("test");

        //  全局事务,事务管理器中间协调者
        UserTransactionImp userTransaction = new UserTransactionImp();

        Connection connectionA = null, connectionT = null;
        PreparedStatement statementA = null, statementT = null;
        try {
            userTransaction.begin();

            connectionA = a.getConnection();
            connectionA.setAutoCommit(false);
            statementA = connectionA.prepareStatement("INSERT INTO user(name,age) VALUES ('Zemin.Yang',18)", Statement.RETURN_GENERATED_KEYS);
            statementA.executeUpdate();

            int i = 1 / 0;

            connectionT = test.getConnection();
            connectionT.setAutoCommit(false);
            statementT = connectionT.prepareStatement("INSERT INTO user(name,age) VALUES ('Zemin.Yang',18)");
            statementT.executeUpdate();

            userTransaction.commit();
        } catch (Exception e) {
            userTransaction.rollback();
        } finally {
            if (statementA != null) {
                statementA.close();
            }
            if (statementT != null) {
                statementT.close();
            }
            if (connectionA != null) {
                connectionA.close();
            }
            if (connectionT != null) {
                connectionT.close();
            }
        }
    }
}
