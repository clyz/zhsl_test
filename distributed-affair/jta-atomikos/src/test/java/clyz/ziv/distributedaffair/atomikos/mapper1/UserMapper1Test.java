package clyz.ziv.distributedaffair.atomikos.mapper1;


import clyz.ziv.distributedaffair.atomikos.JtaAtomikosApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserMapper1Test extends JtaAtomikosApplicationTests {

    @Autowired
    private UserMapper1 userMapper1;

    @Test
    public void insert() {
        int insert = userMapper1.insert("test", 0);
        System.out.println(insert);
    }
}
