package clyz.ziv.distributedaffair.atomikos;

import clyz.ziv.distributedaffair.atomikos.config.DBConfig1;
import clyz.ziv.distributedaffair.atomikos.config.DBConfig2;
import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootTest
@EnableConfigurationProperties(value = {DBConfig1.class, DBConfig2.class})
@EnableTransactionManagement
public class JtaAtomikosApplicationTests {

    @Test
    void contextLoads() {
    }


}
