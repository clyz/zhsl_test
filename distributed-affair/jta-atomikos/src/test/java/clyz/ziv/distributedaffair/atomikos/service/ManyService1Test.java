package clyz.ziv.distributedaffair.atomikos.service;

import clyz.ziv.distributedaffair.atomikos.JtaAtomikosApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ManyService1Test extends JtaAtomikosApplicationTests {
    @Autowired
    private ManyService1 manyService1;

    @Test
    public void insert() {
        manyService1.insert("Zore",0);
    }

    @Test
    public void insertDb1AndDb2() {
        manyService1.insertDb1AndDb2("Zore",0);
    }
}
