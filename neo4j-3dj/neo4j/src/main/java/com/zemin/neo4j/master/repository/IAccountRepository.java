package com.zemin.neo4j.master.repository;

import com.zemin.neo4j.master.model.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAccountRepository extends JpaRepository<AccountEntity, String> {

}
