package com.zemin.neo4j.master.model;

import com.zemin.neo4j.config.GeneratedValueId;
import com.zemin.neo4j.constons.NeoConst;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor
@Builder
@NodeEntity
public class WifeEntity implements IPerson {
    @Id
    @GeneratedValue(strategy = GeneratedValueId.class)
    private String id;

    @NonNull
    private String name;

    @Relationship(NeoConst.R_HUSBAND)
    private HusbandEntity husbandEntity;

    @Override
    public String person() {
        return NeoConst.P_WIFE;
    }
}
