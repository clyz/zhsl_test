package com.zemin.neo4j.role.system.model;

import com.zemin.neo4j.config.GeneratedValueId;
import com.zemin.neo4j.role.system.enumeration.JType;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor
@Builder
@NodeEntity
public class JurisdictionEntity {
    @Id
    @GeneratedValue(strategy = GeneratedValueId.class)
    private String id;

    @NonNull
    private JType type;

    @NonNull
    private String name;
}
