package com.zemin.neo4j.role.system.repository;

import com.zemin.neo4j.role.system.model.JurisdictionEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IJurisdictionRepository extends Neo4jRepository<JurisdictionEntity, String> {
}
