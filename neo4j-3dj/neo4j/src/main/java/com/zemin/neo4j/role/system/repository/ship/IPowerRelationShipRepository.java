package com.zemin.neo4j.role.system.repository.ship;

import com.zemin.neo4j.role.system.model.ship.PowerRelationShipEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPowerRelationShipRepository extends Neo4jRepository<PowerRelationShipEntity, String> {
}
