package com.zemin.neo4j.role.system.repository.ship;

import com.zemin.neo4j.role.system.model.ship.RoleRelationShipEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRelationShipRepository extends Neo4jRepository<RoleRelationShipEntity, String> {
}
