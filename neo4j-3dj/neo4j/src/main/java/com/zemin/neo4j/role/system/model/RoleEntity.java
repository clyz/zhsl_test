package com.zemin.neo4j.role.system.model;

import com.zemin.neo4j.config.GeneratedValueId;
import com.zemin.neo4j.constons.NeoConst;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.List;

/**
 * 系统 角色
 *
 * @author Zemin.Yang
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor
@Builder
@NodeEntity
public class RoleEntity {
    @Id
    @GeneratedValue(strategy = GeneratedValueId.class)
    private String id;

    @NonNull
    private String name;

    /**
     * 角色 拥有的权限
     */
    @Relationship(NeoConst.R_JURISDICTION)
    private List<JurisdictionEntity> jurisdictionEntities;
}
