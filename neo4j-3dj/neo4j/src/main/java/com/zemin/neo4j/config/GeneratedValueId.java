package com.zemin.neo4j.config;

import cn.hutool.core.util.IdUtil;
import org.neo4j.ogm.id.IdStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 系统文件 类容 ID 生成。
 * @author Zemin.Yang
 */
@Component
@ConfigurationProperties(prefix = "generated.value.id")
public class GeneratedValueId implements IdStrategy {

    @Value("${generated.value.id.workerId}")
    private long workerId;

    @Value("${generated.value.id.datacenter}")
    private long datacenter;

    @Override
    public String generateId(Object entity) {
        return IdUtil.getSnowflake(workerId,datacenter).nextIdStr();
    }
}
