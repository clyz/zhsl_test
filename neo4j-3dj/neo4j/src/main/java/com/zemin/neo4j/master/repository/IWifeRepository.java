package com.zemin.neo4j.master.repository;

import com.zemin.neo4j.master.model.WifeEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IWifeRepository extends Neo4jRepository<WifeEntity, String> {

}