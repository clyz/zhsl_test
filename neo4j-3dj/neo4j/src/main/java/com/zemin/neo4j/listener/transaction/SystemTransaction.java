package com.zemin.neo4j.listener.transaction;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

public class SystemTransaction extends ApplicationEvent {
    @Getter
    private String name;

    public SystemTransaction(String name, Object source) {
        super(source);
        this.name = name;
    }

}
