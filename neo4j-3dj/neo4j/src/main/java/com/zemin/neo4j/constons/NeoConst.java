package com.zemin.neo4j.constons;

public interface NeoConst {
    String R_WIFE = "妻子";
    String R_HUSBAND = "丈夫";
    String R_ROLE = "角色";
    String R_JURISDICTION = "拥有权限";

    String P_HUSBAND = "husband";
    String P_WIFE = "wife";
}
