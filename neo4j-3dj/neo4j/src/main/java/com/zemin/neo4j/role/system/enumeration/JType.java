package com.zemin.neo4j.role.system.enumeration;

import lombok.Getter;

/**
 * 操作类型
 */
@Getter
public enum JType {
    DEFAULT,
    /*
     * 按钮，操作
     * */
    BTN,
    /*
     * URL 地址跳转（重定向）
     * */
    URL,
    /*
     * API 接口请求地址
     * */
    REST,
    /*
     * 菜单
     * */
    MENU
}
