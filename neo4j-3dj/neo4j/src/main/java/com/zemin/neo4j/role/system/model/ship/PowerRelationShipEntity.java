package com.zemin.neo4j.role.system.model.ship;

import com.zemin.neo4j.config.GeneratedValueId;
import com.zemin.neo4j.constons.NeoConst;
import com.zemin.neo4j.role.system.model.JurisdictionEntity;
import com.zemin.neo4j.role.system.model.RoleEntity;
import lombok.*;
import org.neo4j.ogm.annotation.*;

import java.util.List;

/**
 * 角色 与 操作的关联
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor
@Builder
@RelationshipEntity(type = NeoConst.R_JURISDICTION)
public class PowerRelationShipEntity {
    @Id
    @GeneratedValue(strategy = GeneratedValueId.class)
    private String id;

    @StartNode
    @NonNull
    private RoleEntity roleEntity;

    @EndNode
    @NonNull
    private JurisdictionEntity jurisdictionEntityList;
}
