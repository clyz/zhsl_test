package com.zemin.neo4j.role.system.payload;

import com.zemin.neo4j.config.GeneratedValueId;
import com.zemin.neo4j.constons.NeoConst;
import com.zemin.neo4j.role.system.model.JurisdictionEntity;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.List;

/**
 * 系统 角色
 *
 * @author Zemin.Yang
 */
@Data
@QueryResult
public class RolePayLoad {

    private String id;

    private String name;

    private List<JurisdictionEntity> jurisdictionEntities;
}
