package com.zemin.neo4j.master.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class AccountEntity {
    /**
     * 此ID 与 neo4j中ID 保持一致
     *
     * @see HusbandEntity#getId()
     * @see WifeEntity#getId()
     */
    @Id
    private String id;

    private String account;

    private String password;
}
