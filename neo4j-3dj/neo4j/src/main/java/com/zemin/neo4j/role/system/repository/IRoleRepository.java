package com.zemin.neo4j.role.system.repository;

import com.zemin.neo4j.role.system.model.RoleEntity;
import com.zemin.neo4j.role.system.payload.RolePayLoad;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRoleRepository extends Neo4jRepository<RoleEntity, String> {
    @Query("MATCH (w:WifeEntity{id:{id}})-[:`角色`]->(r:RoleEntity)-[:`拥有权限`]->(j:JurisdictionEntity) RETURN r.id,r.name,collect(distinct j) as jurisdictionEntities")
    List<RolePayLoad> findAllByWifeId(@Param("id") String id);
}
