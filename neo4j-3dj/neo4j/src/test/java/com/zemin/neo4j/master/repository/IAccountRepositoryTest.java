package com.zemin.neo4j.master.repository;

import cn.hutool.core.util.IdUtil;
import com.zemin.neo4j.master.model.AccountEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Slf4j
class IAccountRepositoryTest {
    @Autowired
    private IAccountRepository accountRepository;

    @Test
    @Transactional
    void save() {
        AccountEntity account = accountRepository.save(AccountEntity.builder()
                .account("account")
                .id(IdUtil.simpleUUID())
                .password("11")
                .build());
        account.setPassword("123");
        accountRepository.save(account);
        log.info(account.toString());
    }
}