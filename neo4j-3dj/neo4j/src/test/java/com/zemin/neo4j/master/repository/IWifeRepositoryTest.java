package com.zemin.neo4j.master.repository;

import com.zemin.neo4j.master.model.HusbandEntity;
import com.zemin.neo4j.master.model.WifeEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
@Slf4j
class IWifeRepositoryTest {
    @Autowired
    private IWifeRepository wifeRepository;
    @Autowired
    private IHusbandRepository husbandRepository;

    @Test
    void bindRelationShip() {
        Optional<WifeEntity> wifeEntity = wifeRepository.findById("1316672839792721920");
        wifeEntity.ifPresent(e->{
            Optional<HusbandEntity> husbandEntity = husbandRepository.findById("1316667910961233920");
            e.setHusbandEntity(husbandEntity.orElse(null));

            wifeRepository.save(e);
        });
    }

    @Test
    void saveWife(){
        WifeEntity wifeEntity = WifeEntity.of("糖珑源");
        wifeEntity.setHusbandEntity(husbandRepository.findById("1316667910961233920").orElse(null));
        wifeRepository.save(wifeEntity);
    }
}