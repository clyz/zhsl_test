package com.zemin.neo4j.role.system.repository.ship;

import com.zemin.neo4j.master.repository.IWifeRepository;
import com.zemin.neo4j.role.system.enumeration.JType;
import com.zemin.neo4j.role.system.model.JurisdictionEntity;
import com.zemin.neo4j.role.system.model.RoleEntity;
import com.zemin.neo4j.role.system.model.ship.RoleRelationShipEntity;
import com.zemin.neo4j.role.system.repository.IJurisdictionRepository;
import com.zemin.neo4j.role.system.repository.IRoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
@Slf4j
class IPowerRelationShipRepositoryTest {

    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private IJurisdictionRepository jurisdictionRepository;
    @Autowired
    private IPowerRelationShipRepository powerRelationShipRepository;
    @Autowired
    private IRoleRelationShipRepository roleRelationShipRepository;
    @Autowired
    private IWifeRepository wifeRepository;

    @Test
    void saveRoleAdjudication() {
        RoleEntity roleEntity = RoleEntity.of("SVIP");
        roleEntity.setJurisdictionEntities(
                Arrays.asList(
                        JurisdictionEntity.of(JType.MENU, "裁决权"),
                        JurisdictionEntity.of(JType.MENU, "一票否决权"),
                        JurisdictionEntity.of(JType.MENU, "关系任命权")
                )
        );
        RoleEntity entity = roleRepository.save(roleEntity);
        roleRelationShipRepository.save(RoleRelationShipEntity.of(
                wifeRepository.findById("1318433483260952576").orElse(null),
                entity
        ));
    }

    @Test
    void saveAdminRole() {
        roleRelationShipRepository.save(RoleRelationShipEntity.of(
                wifeRepository.findById("1318433483260952576").orElse(null),
                roleRepository.findById("1318474816847609856").orElse(null)
        ));
    }

    @Test
    void saveAdminRole2() {
        RoleEntity roleEntity = RoleEntity.of("管理者");
        roleEntity.setJurisdictionEntities(Arrays.asList(
                JurisdictionEntity.of(JType.MENU, "管钱"),
                JurisdictionEntity.of(JType.MENU, "管猫咪"),
                JurisdictionEntity.of(JType.MENU, "管老公"),

                JurisdictionEntity.of(JType.BTN, "零花钱"),
                JurisdictionEntity.of(JType.BTN, "打猫咪")
        ));

        RoleEntity entity = roleRepository.save(roleEntity);

        roleRelationShipRepository.save(RoleRelationShipEntity.of(
                wifeRepository.findById("1318433483260952576").orElse(null),
                entity
        ));
    }

}