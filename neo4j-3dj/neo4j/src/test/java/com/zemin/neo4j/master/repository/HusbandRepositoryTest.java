package com.zemin.neo4j.master.repository;

import com.zemin.neo4j.master.model.HusbandEntity;
import com.zemin.neo4j.master.model.WifeEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@Slf4j
class HusbandRepositoryTest {
    @Autowired
    private IWifeRepository wifeRepository;
    @Autowired
    private IHusbandRepository husbandRepository;

    @Test
    void updateHusbandOfWife() {
        Optional<HusbandEntity> husbandEntity = husbandRepository.findById("1316667910961233920");
        husbandEntity.ifPresent(e -> {
            e.setWifeEntities(
                    Arrays.asList(
                            WifeEntity.of("关晓彤"),
                            WifeEntity.of("杨紫"),
                            WifeEntity.of("昆凌")
                    ));
            e.setName("泽民");
            husbandRepository.save(e);
        });
    }

    @Test
    void findHusbandById() {
        Optional<HusbandEntity> husbandEntity = husbandRepository.findById("1316667910961233920");
        husbandEntity.ifPresent(e -> log.info("{}", e));
    }

    @Test
    void saveHusbandOfName() {
        HusbandEntity husbandEntity = HusbandEntity.of("我");
        husbandRepository.save(husbandEntity);
    }

    @Test
    void updateWife(){
        Optional<HusbandEntity> husbandEntity = husbandRepository.findById("1316667910961233920");
        Optional<WifeEntity> wifeEntity = wifeRepository.findById("1318433483260952576");
        husbandEntity.ifPresent(e->{
            e.getWifeEntities().add(wifeEntity.orElse(null));

            husbandRepository.save(e);
        });
    }
}