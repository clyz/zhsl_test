package com.zemin.neo4j.role.system.repository;

import com.zemin.neo4j.role.system.model.RoleEntity;
import com.zemin.neo4j.role.system.payload.RolePayLoad;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
class IRoleRepositoryTest {
    @Autowired
    private IRoleRepository roleRepository;
    @Test
    void save1() {
        roleRepository.save(RoleEntity.of("管理员"));
    }
    @Test
    void findAllByWifeId() {
        List<RolePayLoad> person = roleRepository.findAllByWifeId("1318433483260952576");
        log.info("{}",person);
    }
}