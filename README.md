# zhsl_test
    
    学习测试项目
    
## 软件架构
    tool(工具)
        poi

    task(任务，定时器)
        
        
    mirror-image(镜像)
        ziv-gradle
    
    server(服务器)
        ziv-undertow(服务器)
        
    elk(ElasticSearch,Logstash,Kibana)
        ziv-elk-sb(日志搜集分析 ElasticSearch,Logstash,Kibana)
    
    datasource(各种数据源,数据库连接)
        druid-sb(druid 数据源)
        druid-dbs-mp-sb(多库连接)
        mycat-jdbc-sb(mycat 数据分片)
        mycat-mp-sb
        ssm-sb
        
    ziv-monitor(监控)
        cmd-bat-exec-sb(外部程序调用)
        jvm
            jvm-bean(监控命令)
            
    design-pattern(设计模式)
        observer(🕵)
        observer-websocket-sb
    
    jwt(权限,单点登录)
        ziv-security-jwt-sb(权限控制)
        ziv-jwt-sb(权限控制)
        
    cache
        ziv-redis
            lock(🔒)
            redisson-bean(分布式🔒)
            transaction(事务)
    
    jdk8
        stream(流)
        lock(jdk🔒)
### jdk8
>

#### stream
>

#### lock
> ✔Jdk 并发编程 👀

1. Fork/Join 框架
    >任务拆分。
    ---
    >第一步分割任务。首先我们需要有一个fork类来把大任务分割成子任务，
    有可能子任务还是很大，所以还需要不停的分割，直到分割出的子任务足够小。
    ---
    >第二步执行任务并合并结果。分割的子任务分别放在双端队列里，
    然后几个启动线程分别从双端队列里获取任务执行。子任务执行完的结果都统一放在一个队列里，启动一个线程从队列里拿数据，然后合并这些数据。
    ---
    1. ForkJoinTask：我们要使用ForkJoin框架，必须首先创建一个ForkJoin任务。
    它提供在任务中执行fork()和join()操作的机制，
    通常情况下我们不需要直接继承ForkJoinTask类，而只需要继承它的子类，
    Fork/Join框架提供了以下两个子类
        1. RecursiveAction：用于没有返回结果的任务。
        2. RecursiveTask ：用于有返回结果的任务。
    2. ForkJoinPool ：ForkJoinTask需要通过ForkJoinPool来执行，
    任务分割出的子任务会添加到当前工作线程所维护的双端队列中，进入队列的头部。
    当一个工作线程的队列里暂时没有任务时，它会随机从其他工作线程的队列的尾部获取一个任务。
2. 

### ziv-undertow
> undertow 服务器，springboot 内嵌

### ziv-monitor
> 监控

#### jvm
> jvm的监控

#### jvm-bean
> jvm的 监控 bean 类测试

### design-pattern
> 设计模式

#### Observer
>🕵观察者模式

#### observer-websocket-sb
>🕵观察者模式 websocket 数据推送,java.observer,springboot

#### Strategy
> 策略模式，
1. 清除 if else

### jwt
> 权限管理

#### ziv-security-jwt-sb
>spring security + jwt + springboot 权限管理

#### ziv-jwt-sb
>jwt + springboot 权限管理

### elk

#### ziv-elk-sb
>日志统计中心 ElasticSearch + Logstash + Kibana +springboot
