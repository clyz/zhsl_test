package ziv.clyz.hsql.zivhsqlsb;

import org.hsqldb.util.DatabaseManagerSwing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootApplication
public class ZivHsqlSbApplication {

    public static void main(String[] args) {
        new Thread(() -> {new DatabaseManagerSwing().start();
            try {
                new BufferedReader(new InputStreamReader(System.in)).readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        SpringApplication.run(ZivHsqlSbApplication.class, args);
    }

}
