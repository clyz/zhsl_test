package ziv.clyz.hsql.zivhsqlsb.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ziv.clyz.hsql.zivhsqlsb.bean.User;

@Repository
public interface IUserDao extends JpaRepository<User, Long> {
}
