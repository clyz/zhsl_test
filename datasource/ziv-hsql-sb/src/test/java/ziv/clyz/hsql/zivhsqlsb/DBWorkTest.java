package ziv.clyz.hsql.zivhsqlsb;

import org.hsqldb.util.DatabaseManagerSwing;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;

public class DBWorkTest {

    /**
     * <p>
     * 打开控制台
     * </p>
     *
     * @throws IOException
     */
    @Test
    public void opten() throws IOException {
        DatabaseManagerSwing swing = new DatabaseManagerSwing();
        swing.main();
        swing.start();
        new BufferedReader(new InputStreamReader(System.in)).readLine();
    }

    /**
     * <p>
     * 连接测试
     * </p>
     */
    @Test
    public void connetionTest() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            Connection conn = DriverManager.getConnection("jdbc:hsqldb:mem:.", "sa", "");
            System.out.println("conn = " + conn);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
