package ziv.clyz.hsql.zivhsqlsb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ziv.clyz.hsql.zivhsqlsb.bean.User;
import ziv.clyz.hsql.zivhsqlsb.dao.IUserDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZivHsqlSbApplicationTests {

    @Autowired
    private IUserDao userDao;

    @Test
    public void contextLoads() {
        User ziv = userDao.save(new User(null, "ziv", "123"));
        System.out.println(ziv);
    }

}
