package com.wisdom.druiddbsmpsb;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

/**
 * mp 代码生成器
 *
 * @author 23746
 */
public class MpAuto {
    @Test
    public void start() {
        //	1.全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setActiveRecord(true)    //是否支持AR
                .setAuthor("ziv")    //	作者;
                .setOutputDir("E:\\java_web\\zhsl_obj\\WisdomFW\\FW1.0\\wisdom3A\\src\\main\\java")    //	生成路径,工程路径
                .setFileOverride(true)    //	文件覆盖
                .setIdType(IdType.AUTO)    //	主键策略
                .setServiceName("I%sService")    //	生成service接口的名称的首字母是否为I
                .setMapperName("I%sDao")
                .setControllerName("%sCtrl")
                .setEnableCache(false)
                .setActiveRecord(false)
                .setBaseResultMap(true)    //	生成基本的sql文件
                .setBaseColumnList(true);    //生成sql片段

        //	2.数据源
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.jdbc.Driver")
                .setUrl("jdbc:mysql://192.168.0.188:3306/wisdomhub?useUnicode=true&characterEncoding=UTF-8&tinyInt1isBit=false")
                .setUsername("root")
                .setPassword("hub2015*");

        //	3.策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setCapitalMode(true)    //	开启全局大写命名
                .setDbColumnUnderline(true)    //	数据库表名列明下滑线
                .setNaming(NamingStrategy.underline_to_camel)    //	数据库表命名到实体的命名策咯
//                .setTablePrefix("t_")    //	数据库表名前最
                .setInclude("deploy_log");    //生成的表;

        //	4.包名
        PackageConfig packageConfig = new PackageConfig();
        packageConfig
                .setParent("com.wisdom.wisdom3a")
                .setMapper("dao")
                .setService("service")
                .setController("web")
                .setEntity("entity")
                .setXml("dao");

        //	5.整合
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(globalConfig)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(packageConfig);
        autoGenerator.execute();
    }
}