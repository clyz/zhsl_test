package com.wisdom.druiddbsmpsb.service.impl;

import com.wisdom.druiddbsmpsb.DruidDbsMpSbApplicationTests;
import com.wisdom.druiddbsmpsb.service.IRoleService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Slf4j
public class RoleServiceImplTest extends DruidDbsMpSbApplicationTests {
    @Autowired
    IRoleService roleService;
    @Test
    public void list(){
        Optional.ofNullable(roleService.selectList(null))
                .ifPresent(e->e.forEach(System.out::println));
    }
}