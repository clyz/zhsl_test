package com.wisdom.druiddbsmpsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DruidDbsMpSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(DruidDbsMpSbApplication.class, args);
    }
}
