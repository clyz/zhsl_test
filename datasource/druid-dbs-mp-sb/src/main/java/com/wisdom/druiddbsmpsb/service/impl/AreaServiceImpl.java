package com.wisdom.druiddbsmpsb.service.impl;

import com.wisdom.druiddbsmpsb.config.DataSource;
import com.wisdom.druiddbsmpsb.config.DataSourceEnum;
import com.wisdom.druiddbsmpsb.pojo.Area;
import com.wisdom.druiddbsmpsb.mapper.AreaMapper;
import com.wisdom.druiddbsmpsb.service.IAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地区码表 服务实现类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements IAreaService {
    @Override
    @DataSource(DataSourceEnum.DB2)
    public Area find(Integer id){
        return selectById(id);
    }
}