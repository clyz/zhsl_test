package com.wisdom.druiddbsmpsb.service;

import com.wisdom.druiddbsmpsb.pojo.Area;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 地区码表 服务类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
public interface IAreaService extends IService<Area> {
    Area find(Integer id);
}
