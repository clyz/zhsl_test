package com.wisdom.druiddbsmpsb.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
@Controller
@RequestMapping("/role")
public class RoleController {

}

