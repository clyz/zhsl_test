package com.wisdom.druiddbsmpsb.service.impl;

import com.wisdom.druiddbsmpsb.config.DataSource;
import com.wisdom.druiddbsmpsb.config.DataSourceEnum;
import com.wisdom.druiddbsmpsb.pojo.Role;
import com.wisdom.druiddbsmpsb.mapper.RoleMapper;
import com.wisdom.druiddbsmpsb.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
