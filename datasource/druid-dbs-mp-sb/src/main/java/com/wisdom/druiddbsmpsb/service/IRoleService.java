package com.wisdom.druiddbsmpsb.service;

import com.wisdom.druiddbsmpsb.pojo.Role;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
public interface IRoleService extends IService<Role> {

}
