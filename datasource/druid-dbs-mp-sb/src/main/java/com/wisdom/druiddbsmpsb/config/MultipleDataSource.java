package com.wisdom.druiddbsmpsb.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 多数据源的选择
 */
public class MultipleDataSource extends AbstractRoutingDataSource {
    /**
     * 根据Key获取数据源的信息，上层抽象函数的钩子
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.getDataSource();
    }
}