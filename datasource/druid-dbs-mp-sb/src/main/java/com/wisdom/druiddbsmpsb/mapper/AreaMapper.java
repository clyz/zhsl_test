package com.wisdom.druiddbsmpsb.mapper;

import com.wisdom.druiddbsmpsb.pojo.Area;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 地区码表 Mapper 接口
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
public interface AreaMapper extends BaseMapper<Area> {

}
