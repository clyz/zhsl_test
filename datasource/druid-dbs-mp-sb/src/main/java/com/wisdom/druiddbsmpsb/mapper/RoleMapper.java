package com.wisdom.druiddbsmpsb.mapper;

import com.wisdom.druiddbsmpsb.pojo.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-20
 */
public interface RoleMapper extends BaseMapper<Role> {

}
