package com.wisdom.mycatjdbcsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.wisdom")
public class MycatJdbcSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MycatJdbcSbApplication.class, args);
    }
}
