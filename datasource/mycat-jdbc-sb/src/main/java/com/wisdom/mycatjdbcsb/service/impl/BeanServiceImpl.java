package com.wisdom.mycatjdbcsb.service.impl;

import com.wisdom.mycatjdbcsb.bean.User;
import com.wisdom.mycatjdbcsb.service.IBeanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: ziv
 * @date: 2018/11/28 17:27
 * @version: 0.0.1
 */
@Service
public class BeanServiceImpl implements IBeanService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List getList(String sql, RowMapper rowMapper) {
        return jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean save(User user){
        return jdbcTemplate.update("INSERT INTO cas.user(number,name) VALUES (?,?)",
                user.getNumber(), user.getName()) > 0;
    }
}
