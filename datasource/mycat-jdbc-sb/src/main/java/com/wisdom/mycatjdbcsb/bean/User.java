package com.wisdom.mycatjdbcsb.bean;

/**
 * @author: ziv
 * @date: 2018/11/28 17:35
 * @version: 0.0.1
 */
public class User {
    private Integer id;
    private String name;
    private String number;

    public User() {
    }

    public User(Integer id, String number, String name) {
        this.id = id;
        this.number = number;
        this.name = name;
    }

    public User(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
