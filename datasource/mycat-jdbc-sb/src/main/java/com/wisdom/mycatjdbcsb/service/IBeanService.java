package com.wisdom.mycatjdbcsb.service;

import com.wisdom.mycatjdbcsb.bean.User;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author: ziv
 * @date: 2018/11/28 17:26
 * @version: 0.0.1
 */
public interface IBeanService {
    List getList(String sql, RowMapper rowMapper);

    boolean save(User user);
}
