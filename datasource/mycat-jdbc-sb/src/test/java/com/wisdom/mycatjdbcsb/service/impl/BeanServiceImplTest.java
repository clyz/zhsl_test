package com.wisdom.mycatjdbcsb.service.impl;

import com.wisdom.mycatjdbcsb.MycatJdbcSbApplicationTests;
import com.wisdom.mycatjdbcsb.bean.User;
import com.wisdom.mycatjdbcsb.service.IBeanService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class BeanServiceImplTest extends MycatJdbcSbApplicationTests {

    @Autowired
    IBeanService beanService;

    @Test
    public void getList() {
        beanService.getList("select * from cas.user", (rs, rowNum) -> new User(
                rs.getInt("id"),
                rs.getString("number"),
                rs.getString("name"))).forEach(System.out::println);
    }

    @Test
    public void save() {
        boolean save = beanService.save(new User("1003", "阮阮"));
        System.err.println(save);
    }
}