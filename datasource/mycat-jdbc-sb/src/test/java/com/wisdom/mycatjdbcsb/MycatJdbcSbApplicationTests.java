package com.wisdom.mycatjdbcsb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan("com.wisdom")
public class MycatJdbcSbApplicationTests {
    @Test
    public void contextLoads() {
    }

}
