package com.wisdom.druidsbs.auto;

import com.github.mustfun.RolePo;
import com.github.mustfun.RoleService;
import com.github.pagehelper.Page;
import com.wisdom.druidsbsnull.Result;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author itar
 * @email wuhandzy@gmail.com
 * @date 2018-12-07 09:29:10
 * @since jdk 1.8
 */
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    /**
     * 分页查询数据
     */
    @RequestMapping("/select_paged")
    public Result<Page<RolePo>> selectPaged(RowBounds rowBounds) {
        Result<Page<RolePo>> pageResult = new Result<>();
        Page<RolePo> page = roleService.selectPaged(rowBounds);
        pageResult.setData(page);
        return pageResult;
    }

    /**
     * 通过id查询
     *
     * @return
     */
    @RequestMapping("/select_by_id")
    public Result<RolePo> selectByPrimaryKey(Integer id) {
        Result<RolePo> result = new Result<>();
        RolePo po = roleService.selectByPrimaryKey(id);
        result.setData(po);
        return result;
    }

    /**
     * 通过ID删除
     *
     * @return
     */
    @RequestMapping("/delete_by_id")
    public Result<Integer> deleteByPrimaryKey(Integer id) {
        Result<Integer> result = new Result<>();
        Integer num = roleService.deleteByPrimaryKey(id);
        result.setData(num);
        return result;
    }

    /**
     * 新增数据
     *
     * @return
     */
    @RequestMapping("/save_role")
    public Result<Integer> insert(RolePo role) {
        Result<Integer> result = new Result<>();
        Integer num = roleService.insertSelective(role);
        result.setData(num);
        return result;
    }

    /**
     * 修改数据
     *
     * @return
     */
    @RequestMapping("/update_role")
    public Result<Integer> updateByPrimaryKeySelective(RolePo role) {
        Result<Integer> result = new Result<>();
        Integer num = roleService.updateByPrimaryKeySelective(role);
        result.setData(num);
        return result;
    }


    /**
     * 查询列表
     *
     * @return
     */
    @RequestMapping("/query_list")
    public Result<List<RolePo>> queryByCondition(RolePo role) {
        Result<List<RolePo>> result = new Result<>();
        List<RolePo> list = roleService.query(role);
        result.setData(list);
        return result;
    }

}
