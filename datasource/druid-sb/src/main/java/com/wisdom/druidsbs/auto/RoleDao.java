package com.wisdom.druidsbs.auto;

import com.github.mustfun.RolePo;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 的dao层
 *
 * @author itar
 * @email wuhandzy@gmail.com
 * @date 2018-12-07 09:29:10
 * @since jdk1.8
 */
@Mapper
public interface RoleDao {

    /*<AUTOGEN--BEGIN>*/

    Page<RolePo> selectPaged(RowBounds rowBounds);

    RolePo selectByPrimaryKey(Integer id);

    Integer deleteByPrimaryKey(Integer id);

    Integer insert(RolePo role);

    Integer insertSelective(RolePo role);

    Integer insertSelectiveIgnore(RolePo role);

    Integer updateByPrimaryKeySelective(RolePo role);

    Integer updateByPrimaryKey(RolePo role);

    Integer batchInsert(List<RolePo> list);

    Integer batchUpdate(List<RolePo> list);

    /**
     * 存在即更新
     *
     * @param map
     * @return
     */
    Integer upsert(RolePo role);

    /**
     * 存在即更新，可选择具体属性
     *
     * @param map
     * @return
     */
    Integer upsertSelective(RolePo role);

    List<RolePo> query(RolePo role);

    Long queryTotal();

    Integer deleteBatch(List<Integer> list);

    /*<AUTOGEN--END>*/

}