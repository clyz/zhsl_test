package com.wisdom.druidsbs.auto.bo;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.wisdom.druidsbs.auto.bo.RoleBo;

@Mapper
public interface RoleBoDao {
    int insert(@Param("pojo") RoleBo pojo);

    int insertSelective(@Param("pojo") RoleBo pojo);

    int insertList(@Param("pojos") List<RoleBo> pojo);

    int update(@Param("pojo") RoleBo pojo);
}
