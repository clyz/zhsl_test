package com.wisdom.druidsbs.auto.impl;

import com.github.mustfun.RolePo;
import com.github.mustfun.RoleService;
import com.github.pagehelper.Page;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/po.RoleDao


/**
 * @author itar
 * @email wuhandzy@gmail.com
 * @date 2018-12-07 09:29:10
 * @since jdk 1.8
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
    /*<AUTOGEN--BEGIN>*/

    @Autowired
    public RoleDao roleDao;


    @Override
    public Page<RolePo> selectPaged(RowBounds rowBounds) {
        return roleDao.selectPaged(rowBounds);
    }

    @Override
    public RolePo selectByPrimaryKey(Integer id) {
        return roleDao.selectByPrimaryKey(id);
    }

    @Override
    public Integer deleteByPrimaryKey(Integer id) {
        return roleDao.deleteByPrimaryKey(id);
    }

    @Override
    public Integer insert(RolePo role) {
        return roleDao.insert(role);
    }

    @Override
    public Integer insertSelective(RolePo role) {
        return roleDao.insertSelective(role);
    }

    @Override
    public Integer insertSelectiveIgnore(RolePo role) {
        return roleDao.insertSelectiveIgnore(role);
    }

    @Override
    public Integer updateByPrimaryKeySelective(RolePo role) {
        return roleDao.updateByPrimaryKeySelective(role);
    }

    @Override
    public Integer updateByPrimaryKey(RolePo role) {
        return roleDao.updateByPrimaryKey(role);
    }

    @Override
    public Integer batchInsert(List<RolePo> list) {
        return roleDao.batchInsert(list);
    }

    @Override
    public Integer batchUpdate(List<RolePo> list) {
        return roleDao.batchUpdate(list);
    }

    /**
     * 存在即更新
     *
     * @param map
     * @return
     */
    @Override
    public Integer upsert(RolePo role) {
        return roleDao.upsert(role);
    }

    /**
     * 存在即更新，可选择具体属性
     *
     * @param map
     * @return
     */
    @Override
    public Integer upsertSelective(RolePo role) {
        return roleDao.upsertSelective(role);
    }

    @Override
    public List<RolePo> query(RolePo role) {
        return roleDao.query(role);
    }

    @Override
    public Long queryTotal() {
        return roleDao.queryTotal();
    }

    @Override
    public Integer deleteBatch(List<Integer> list) {
        return roleDao.deleteBatch(list);
    }

    /*<AUTOGEN--END>*/

}
