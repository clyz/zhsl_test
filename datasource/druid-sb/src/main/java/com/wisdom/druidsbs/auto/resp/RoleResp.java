package com.wisdom.druidsbs.auto.resp;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author itar
 * @mail wuhandzy@gmail.com
 * @date 2018-12-07 09:29:10
 * @since jdk1.8
 */
@Setter
@Getter
public class RoleResp implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Integer id;
    /**
     *
     */
    private String name;
}
