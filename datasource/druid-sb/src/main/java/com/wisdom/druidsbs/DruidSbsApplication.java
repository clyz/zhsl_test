package com.wisdom.druidsbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DruidSbsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DruidSbsApplication.class, args);
    }
}
