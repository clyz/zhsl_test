package com.wisdom.interceptor;


import com.wisdom.bean.User;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;

import java.util.Properties;

/**
 * @author: ziv
 * @date: 2018/12/5 09:14
 * @version: 0.0.1
 */
@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {
                MappedStatement.class, Object.class})
})
public class ParamInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        String methodName = invocation.getMethod().getName();

        if(methodName.equals("update")){
            System.err.println("UPDATE******");
            Object parameter = invocation.getArgs()[1];
            if(parameter instanceof User) {
//                boolean id = ((User) parameter).getClass().getField("id").isAnnotationPresent(Snowflake.class);
//                if (id){
//                    ((User) parameter).setId(IdWorkerKit.getId());
//                }
            }
            System.err.println(parameter);

        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
        System.err.println(properties);
    }
}
