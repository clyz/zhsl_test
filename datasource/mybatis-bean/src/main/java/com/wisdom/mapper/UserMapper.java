package com.wisdom.mapper;

import com.wisdom.bean.User;
import com.wisdom.id.Snowflake;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import java.util.List;

/**
 * @author: ziv
 * @date: 2018/12/4 14:15
 * @version: 0.0.1
 */
public interface UserMapper {

    @Select("select * from mp_user")
    List<User> listUser();

    @Insert("INSERT INTO mp_user(id,pwd,name) VALUES(#{id},#{pwd},#{name})")
    @SelectKey(resultType = Long.class,keyColumn = "id",
            statement = "SELECT unix_timestamp(now()) as id",
            keyProperty="id",
            before = true)
    int save(User user);
}
