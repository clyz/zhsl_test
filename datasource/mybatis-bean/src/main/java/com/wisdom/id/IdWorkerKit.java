package com.wisdom.id;

public class IdWorkerKit {
    /**
     * 主机和进程的机器码
     */
    private static final SnowFlakeId WORKER = new SnowFlakeId(1L,20L);

    public static long getId() {
        return WORKER.nextId();
    }

    public static String getIdStr() {
        return String.valueOf(WORKER.nextId());
    }

}