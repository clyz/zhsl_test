package com.wisdom.id;

import java.lang.annotation.*;

/**
 * @author: ziv
 * @date: 2018/12/5 10:48
 * @version: 0.0.1
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Snowflake {
}
