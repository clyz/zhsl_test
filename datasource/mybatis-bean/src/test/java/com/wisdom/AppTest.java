package com.wisdom;

import com.wisdom.bean.User;
import com.wisdom.mapper.UserMapper;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class AppTest {

    public SqlSessionFactory sqlSessionFactory = null;

    @Before
    public void pooledDataSource(){
        PooledDataSource pooledDataSource = new PooledDataSource();
        pooledDataSource.setDriver("com.mysql.jdbc.Driver");
        pooledDataSource.setUrl("jdbc:mysql://localhost:3306/mybatis");
        pooledDataSource.setUsername("root");
        pooledDataSource.setPassword("zemin886520");

        TransactionFactory jdbcTransactionFactory = new JdbcTransactionFactory();

        Environment development = new Environment("development", jdbcTransactionFactory, pooledDataSource);
        Configuration configuration = new Configuration(development);

//        configuration.addMappers("com/wisdom/mapper/UserMapper.xml");
        configuration.getTypeAliasRegistry().registerAlias("mp_user",User.class);
//        configuration.addMapper(UserMapper.class);
        setInterceptor(configuration);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
    }

    @Test
    public void select(){}

    @After
    public void list(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        Optional.ofNullable(mapper.listUser()).ifPresent(System.out::println);
    }

    public void setInterceptor(Configuration configuration){ }
}