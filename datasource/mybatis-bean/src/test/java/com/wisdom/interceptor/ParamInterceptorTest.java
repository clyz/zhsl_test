package com.wisdom.interceptor;

import com.wisdom.AppTest;
import com.wisdom.bean.User;
import com.wisdom.mapper.UserMapper;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class ParamInterceptorTest extends AppTest {
    @Override
    public void setInterceptor(Configuration configuration) {
        configuration.addInterceptor(new ParamInterceptor());
    }

    @Test
    public void save(){
       SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        System.err.println("222");
        User user = new User();
        user.setName("ziv");
        user.setPwd("12345");

        System.err.println(mapper.save(user));
        sqlSession.commit();
    }
}