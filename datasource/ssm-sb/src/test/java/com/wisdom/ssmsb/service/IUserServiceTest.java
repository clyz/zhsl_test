package com.wisdom.ssmsb.service;

import com.wisdom.ssmsb.SsmSbApplicationTests;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IUserServiceTest extends SsmSbApplicationTests {

    @Autowired
    private IUserService userService;

    /**
     * <p>
     * 有一条数据添加成功
     * </p>
     */
    @Test
    public void _() {
        userService._();
    }

    /**
     * <p>
     * 没有数据添加成功
     * </p>
     */
    @Test
    public void __() {
        userService.__();
    }

    /**
     * <p>
     * 没有数据添加成功
     * </p>
     */
    @Test
    public void _transactional() {
        userService._transactional();
    }

    /**
     * <p>
     * 没有数据添加成功
     * </p>
     */
    @Test
    public void _transactional_PROPAGATION_REQUIRES_NEW() {
        userService._transactional_PROPAGATION_REQUIRES_NEW();
    }

    /**
     * <p>
     * 子表数据添加成功
     * </p>
     */
    @Test
    public void __transactional_PROPAGATION_REQUIRES_NEW() {
        userService.__transactional_PROPAGATION_REQUIRES_NEW();
    }


    /**
     * <p>
     * 主数据添加成功
     * </p>
     */
    @Test
    public void __transactional_PROPAGATION_REQUIRES_NEW_TRY() {
        userService.__transactional_PROPAGATION_REQUIRES_NEW_TRY();
    }

    /**
     * <p>
     * 子表数据添加成功
     * </p>
     */
    @Test
    public void ___transactional_PROPAGATION_REQUIRES_NEW() {
        userService.___transactional_PROPAGATION_REQUIRES_NEW();
    }

    /**
     * 无数据
     */
    @Test
    public void _transactional_PROPAGATION_REQUIRES_NEW_TRY() {
        userService._transactional_PROPAGATION_REQUIRES_NEW_TRY();
    }

    /**
     * <p>
     * 主数据添加
     * </p>
     */
    @Test
    public void __transactional_PROPAGATION_REQUIRES_NEW_TRY_AWARE() {
        userService.__transactional_PROPAGATION_REQUIRES_NEW_TRY_AWARE();
    }

}