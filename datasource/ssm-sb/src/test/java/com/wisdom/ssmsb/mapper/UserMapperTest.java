package com.wisdom.ssmsb.mapper;

import com.wisdom.ssmsb.SsmSbApplicationTests;
import com.wisdom.ssmsb.handler.AppContextHandler;
import com.wisdom.ssmsb.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.scripting.defaults.RawSqlSource;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Collection;

@Slf4j
public class UserMapperTest extends SsmSbApplicationTests {
    @Autowired
    UserMapper userMapper;

    @Test
    public void test(){

    }

    @Test
    public void list() {
        userMapper.list().forEach(System.out::println);
    }

    @Test
    public void listSql() throws NoSuchFieldException {
        setMapperSql();
        userMapper.list().forEach(System.out::println);
    }

    @Test
    public void save() {
        userMapper.save(new User(1, "2", "a"));
    }


    private void setMapperSql() throws NoSuchFieldException {
        final Configuration configuration = AppContextHandler.applicationContext.getBean(SqlSessionFactory.class).getConfiguration();
        MappedStatement mappedStatement = configuration.getMappedStatement("com.wisdom.ssmsb.mapper.UserMapper.list");


        final Field source = mappedStatement.getClass().getDeclaredField("sqlSource");
        source.setAccessible(true);
        ReflectionUtils.setField(source, mappedStatement,
                new RawSqlSource(configuration,"SELECT ",null)
                );
        source.setAccessible(false);

/*
        final SqlSource sqlSource = new RawSqlSource(configuration,"SELECT ",null);

        BoundSql boundSql = mappedStatement.getBoundSql(null);

      final MappedStatement build = new MappedStatement.Builder(configuration, mappedStatement.getId(),
                sqlSource,
                mappedStatement.getSqlCommandType()).build();

        mappedStatement = build;
        final MappedStatement mappedStatement1 = configuration.getMappedStatement("com.wisdom.ssmsb.mapper.UserMapper.list");

        boundSql = new BoundSql(configuration,"SELECT ",boundSql.getParameterMappings(),null);*/
    }
}