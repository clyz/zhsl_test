package com.wisdom.ssmsb.handler;

import com.wisdom.ssmsb.SsmSbApplicationTests;
import com.wisdom.ssmsb.service.IUserService;
import com.wisdom.ssmsb.service.imp.UserServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AppContextHandlerTest extends SsmSbApplicationTests {

    @Autowired
    private AppContextHandler appContextHandler;

//    @Autowired
//    private IUserService userService;

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void getClass1() {
        final IUserService aClass = appContextHandler.getClass(IUserService.class);
        aClass._();
    }

    @Test
    public void getClass2() {
        final IUserService aClass = appContextHandler.getClass(userService.getClass());
        System.out.println(aClass);
    }
}