package com.wisdom.ssmsb.service;

/**
 * <p>
 * Spring 的 ken
 * 测试 研究 Spring 的 内置 方法 事务
 * 及 内部方法调用
 *
 * </p>
 */
public interface IUserService {

    public void _();

    public void __();

    public void _transactional();

    public void _transactional_PROPAGATION_REQUIRES_NEW();

    public void __transactional_PROPAGATION_REQUIRES_NEW();

    public void __transactional_PROPAGATION_REQUIRES_NEW_TRY();

    public void __transactional_PROPAGATION_REQUIRES_NEW_TRY_AWARE();

    public void ___transactional_PROPAGATION_REQUIRES_NEW();

    public void _transactional_PROPAGATION_REQUIRES_NEW_TRY();

    public void save();

    public void save_();

    public void save__();

    /**
     * <p>
     *     新增 事务添加
     * </p>
     */
    public void save_PROPAGATION_REQUIRES_NEW();

    public void save_PROPAGATION_REQUIRES_NEW_TRY();
}
