package com.wisdom.ssmsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
public class SsmSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsmSbApplication.class, args);
    }
}
