package com.wisdom.ssmsb.interceptor;

import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.*;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.Properties;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 9:24
 */
@Intercepts({
        @Signature(
                //  Executor、ParameterHandler、StatementHandler、ResultSetHandler
                type = StatementHandler.class,
                // 在定义拦截类的基础之上，在定义拦截的方法
                method = "prepare",
                /* 在定义拦截方法的基础之上在定义拦截的方法对应的参数，
                 JAVA里面方法可能重载，不指定参数，不晓得是那个方法
                 ( statement,  resultHandler)
                 */
                args = {Connection.class, Integer.class}
        )
})
public class TableShardInterceptor implements Interceptor {

    /**
     * 自定义拦截逻辑
     */
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        //对于StatementHandler其实只有两个实现类，一个是RoutingStatementHandler，另一个是抽象类BaseStatementHandler，
        //BaseStatementHandler有三个子类，分别是SimpleStatementHandler，PreparedStatementHandler和CallableStatementHandler，
        //SimpleStatementHandler是用于处理Statement的，PreparedStatementHandler是处理PreparedStatement的，而CallableStatementHandler是
        //处理CallableStatement的。Mybatis在进行Sql语句处理的时候都是建立的RoutingStatementHandler，而在RoutingStatementHandler里面拥有一个
        //StatementHandler类型的delegate属性，RoutingStatementHandler会依据Statement的不同建立对应的BaseStatementHandler，即SimpleStatementHandler、
        //PreparedStatementHandler或CallableStatementHandler，在RoutingStatementHandler里面所有StatementHandler接口方法的实现都是调用的delegate对应的方法。
        //我们在PageInterceptor类上已经用@Signature标记了该Interceptor只拦截StatementHandler接口的prepare方法，又因为Mybatis只有在建立RoutingStatementHandler的时候
        //是通过Interceptor的plugin方法进行包裹的，所以我们这里拦截到的目标对象肯定是RoutingStatementHandler对象。
        if (invocation.getTarget() instanceof RoutingStatementHandler) {
            RoutingStatementHandler statementHandler = (RoutingStatementHandler) invocation.getTarget();
            final Class<? extends RoutingStatementHandler> aClass = statementHandler.getClass();
            final Field declaredField = aClass.getDeclaredField("delegate");
            declaredField.setAccessible(true);

            StatementHandler delegate = (StatementHandler) ReflectionUtils.getField(declaredField, statementHandler);
//            StatementHandler delegate = (StatementHandler) ReflectHelper.getFieldValue(statementHandler, "delegate");
            declaredField.setAccessible(false);
            BoundSql boundSql = delegate.getBoundSql();
            final Field sql = boundSql.getClass().getDeclaredField("sql");
            sql.setAccessible(true);
            ReflectionUtils.setField(sql, boundSql, " SELECT");
            sql.setAccessible(false);
        }
        return invocation.proceed();
    }

    /**
     * 返回代理类
     */
    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
