package com.wisdom.ssmsb.handler;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class AppContextHandler implements ApplicationContextAware {

    public static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        AppContextHandler.applicationContext = applicationContext;
    }

    public <T> T getClass(Class<T> clazz) {
        return (T) applicationContext.getBean(clazz);
    }

    public static <T> T getClassStatic(Class<T> clazz) {
        return (T) applicationContext.getBean(clazz);
    }

}
