package com.wisdom.ssmsb.service.imp;

import com.wisdom.ssmsb.handler.AppContextHandler;
import com.wisdom.ssmsb.mapper.UserMapper;
import com.wisdom.ssmsb.pojo.User;
import com.wisdom.ssmsb.service.IUserService;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public void _() {
        System.out.println("-> _ ::");
        save();
    }

    @Override
    public void __() {
        System.out.println("-> _ ::");
        ((IUserService) AopContext.currentProxy()).save();
    }

    @Override
    @Transactional
    public void _transactional() {
        System.out.println("-> _transactional :: ");
        save_();
        save__();
    }

    @Override
    @Transactional
    public void _transactional_PROPAGATION_REQUIRES_NEW() {
        save_();
        save_PROPAGATION_REQUIRES_NEW();
    }

    @Override
    @Transactional
    public void __transactional_PROPAGATION_REQUIRES_NEW() {
        save_();
        IUserService iUserService = (IUserService) AopContext.currentProxy();
        iUserService.save_PROPAGATION_REQUIRES_NEW();
        TRY();
    }

    @Override
    @Transactional
    public void __transactional_PROPAGATION_REQUIRES_NEW_TRY() {
        save_();
        try {
            ((IUserService) AopContext.currentProxy()).save_PROPAGATION_REQUIRES_NEW_TRY();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void __transactional_PROPAGATION_REQUIRES_NEW_TRY_AWARE() {
        save_();
        try {
            IUserService userService = AppContextHandler.getClassStatic(IUserService.class);
            userService.save_PROPAGATION_REQUIRES_NEW_TRY();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void _transactional_PROPAGATION_REQUIRES_NEW_TRY() {
        save_();
        //  方法 执行 完毕 抛出异常 后 ，异常 未经处理 动态 代理 向上 抛异常  最终 倒置 主业务也失败
        ((IUserService) AopContext.currentProxy()).save_PROPAGATION_REQUIRES_NEW_TRY();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void ___transactional_PROPAGATION_REQUIRES_NEW() {
        save_();
        ((IUserService) AopContext.currentProxy()).save_PROPAGATION_REQUIRES_NEW();
        TRY();
    }

    @Override
    @Transactional
    public void save() {
        userMapper.save(new User(1, "ziv", "123"));
        userMapper.save(new User(1, "ziv", "123"));
    }

    @Override
    @Transactional
    public void save_() {
        userMapper.save(new User(1, "ziv", "123"));
    }

    @Override
    @Transactional()
    public void save__() {
        userMapper.save(new User(1, "ziv", "123"));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void save_PROPAGATION_REQUIRES_NEW() {
        userMapper.save(new User(2, "ziv", "123"));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void save_PROPAGATION_REQUIRES_NEW_TRY() {
        userMapper.save(new User(2, "ziv", "123"));
        TRY();
    }

    private void TRY() {
        final int i = 1 / 0;
    }

}
