package com.wisdom.ssmsb.mapper;

import com.wisdom.ssmsb.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface UserMapper {
    @Select("SELECT * FROM user")
    List<User> list();

    @Insert("INSERT INTO user(id,name,number) VALUES (#{id} ,#{name} ,#{number} )")
    void save(User ziv);
}
