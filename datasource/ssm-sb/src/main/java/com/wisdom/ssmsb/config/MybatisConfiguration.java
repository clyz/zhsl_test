package com.wisdom.ssmsb.config;

import com.wisdom.ssmsb.interceptor.TableShardInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 9:31
 */
@Configuration
public class MybatisConfiguration {

    /**
     * 注册拦截器
     */
    @Bean
    public TableShardInterceptor tableShardInterceptor() {
        TableShardInterceptor interceptor = new TableShardInterceptor();
        Properties properties = new Properties();
        // 可以调用properties.setProperty方法来给拦截器设置一些自定义参数
        interceptor.setProperties(properties);
        return interceptor;
    }


}

