package com.wisdom.mycatmpsb.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
@Controller
@RequestMapping("/user")
public class UserController {

}
