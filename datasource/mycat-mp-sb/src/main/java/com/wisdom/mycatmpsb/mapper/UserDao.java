package com.wisdom.mycatmpsb.mapper;

import com.wisdom.mycatmpsb.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
@Component
public interface UserDao extends BaseMapper<User> {

}
