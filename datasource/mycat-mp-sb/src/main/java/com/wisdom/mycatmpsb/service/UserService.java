package com.wisdom.mycatmpsb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wisdom.mycatmpsb.entity.MpUser;
import com.wisdom.mycatmpsb.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
public interface UserService extends IService<User> {

    boolean saveMpUser(MpUser mpUser);
}
