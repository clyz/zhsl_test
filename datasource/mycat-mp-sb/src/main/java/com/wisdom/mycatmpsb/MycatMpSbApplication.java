package com.wisdom.mycatmpsb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.wisdom.mycatmpsb.mapper")
public class MycatMpSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MycatMpSbApplication.class, args);
    }
}
