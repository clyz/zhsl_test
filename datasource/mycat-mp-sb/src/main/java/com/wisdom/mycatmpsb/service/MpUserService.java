package com.wisdom.mycatmpsb.service;

import com.wisdom.mycatmpsb.entity.MpUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
public interface MpUserService extends IService<MpUser> {

}
