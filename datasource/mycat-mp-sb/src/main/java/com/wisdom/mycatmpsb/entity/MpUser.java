package com.wisdom.mycatmpsb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import org.omg.CORBA.IDLType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
@TableName("mp_user")
public class MpUser extends Model<MpUser> {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ID_WORKER)
    private Long id;

    private String pwd;

    private String name;

    public Long getId() {
        return id;
    }

    public MpUser setId(Long id) {
        this.id = id;
        return this;
    }
    public String getPwd() {
        return pwd;
    }

    public MpUser setPwd(String pwd) {
        this.pwd = pwd;
        return this;
    }
    public String getName() {
        return name;
    }

    public MpUser setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MpUser{" +
        "id=" + id +
        ", pwd=" + pwd +
        ", name=" + name +
        "}";
    }
}
