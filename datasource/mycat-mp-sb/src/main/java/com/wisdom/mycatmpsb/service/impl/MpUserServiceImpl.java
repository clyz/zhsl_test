package com.wisdom.mycatmpsb.service.impl;

import com.wisdom.mycatmpsb.entity.MpUser;
import com.wisdom.mycatmpsb.mapper.MpUserDao;
import com.wisdom.mycatmpsb.service.MpUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
@Service
public class MpUserServiceImpl extends ServiceImpl<MpUserDao, MpUser> implements MpUserService {

}
