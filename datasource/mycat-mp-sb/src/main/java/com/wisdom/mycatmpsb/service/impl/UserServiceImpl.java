package com.wisdom.mycatmpsb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wisdom.mycatmpsb.entity.MpUser;
import com.wisdom.mycatmpsb.entity.User;
import com.wisdom.mycatmpsb.mapper.UserDao;
import com.wisdom.mycatmpsb.service.MpUserService;
import com.wisdom.mycatmpsb.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ziv
 * @since 2018-12-05
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Autowired
    private MpUserService mpUserService;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public boolean saveMpUser(MpUser mpUser){
        log.info("save{}",mpUser.toString());
        mpUserService.save(mpUser);
        User user = new User();
        user.setId(1001);
        user.setNumber(mpUser.getPwd());
        user.setName(mpUser.getName());
        log.info("save{}",user.toString());
        return save(user);
    }

}
