package com.wisdom.mycatmpsb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan
@MapperScan("com.wisdom.mycatmpsb.mapper")
public class MycatMpSbApplicationTests {

    @Test
    public void contextLoads() {
    }

}
