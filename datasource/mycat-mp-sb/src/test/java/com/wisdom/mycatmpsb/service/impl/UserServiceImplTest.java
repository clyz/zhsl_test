package com.wisdom.mycatmpsb.service.impl;

import com.wisdom.mycatmpsb.MycatMpSbApplicationTests;
import com.wisdom.mycatmpsb.entity.MpUser;
import com.wisdom.mycatmpsb.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class UserServiceImplTest extends MycatMpSbApplicationTests {
    @Autowired
    private UserService userService;
    @Test
    public void saveMpUser() {
        MpUser mpUser = new MpUser();
        mpUser.setName("1泽民");
        mpUser.setPwd("123456");
        userService.saveMpUser(mpUser);
    }
}