package com.wisdom.mycatmpsb.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wisdom.mycatmpsb.MycatMpSbApplicationTests;
import com.wisdom.mycatmpsb.entity.MpUser;
import lombok.extern.java.Log;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@Log
public class MpUserDaoTest extends MycatMpSbApplicationTests {
    @Autowired
    MpUserDao mpUserDao;

    @Test
    public void select(){
        QueryWrapper<MpUser> mpUserQueryWrapper = new QueryWrapper<>();
        mpUserQueryWrapper.eq("name","杨泽民");
        Optional.ofNullable(mpUserDao.selectList(mpUserQueryWrapper))
                .ifPresent(System.err::println);
    }

    @Test
    public void add(){
        MpUser mpUser = new MpUser();
        mpUser.setName("杨泽民");
        mpUser.setPwd("123456");
        mpUserDao.insert(mpUser);
    }

    @After
    public void list(){
        Optional.ofNullable(mpUserDao.selectList(null)).ifPresent(System.out::println);
    }
}