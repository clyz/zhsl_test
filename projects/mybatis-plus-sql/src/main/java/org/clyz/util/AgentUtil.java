package org.clyz.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class AgentUtil {
    public final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public final static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static String toStringSql(List<Object> keys, String sql) {
        if (keys.isEmpty()) return sql;
        StringBuilder returnSQL = new StringBuilder();
        String[] subSQL = sql.split("\\?");
        for (int i = 0; i < keys.size(); i++) {
            if (keys.get(i) instanceof LocalDateTime) {
                returnSQL.append(subSQL[i]).append(" '").append(
                        dateTimeFormatter.format((LocalDateTime) keys.get(i))
                ).append("' ");
            } else if (keys.get(i) instanceof LocalDate) {
                returnSQL.append(subSQL[i]).append(" '").append(dateFormatter.format((LocalDate) keys.get(i))).append("' ");
            } else if (keys.get(i) instanceof Date) {
                returnSQL.append(subSQL[i]).append(" '").append(formatter.format((Date) keys.get(i))).append("' ");
            } else {
                returnSQL.append(subSQL[i]).append(" '").append(keys.get(i)).append("' ");
            }
            if (i == keys.size() - 1) {
                try {
                    returnSQL.append(subSQL[keys.size()]);
                } catch (Exception ignored) {

                }
            }
        }
        return returnSQL.toString();
    }
}
