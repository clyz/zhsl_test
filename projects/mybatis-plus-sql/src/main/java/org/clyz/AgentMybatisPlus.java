package org.clyz;

import javassist.*;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.sql.PreparedStatement;

/**
 * -javaagent:D:\zhsl_test\projects\mybatis-plus-sql\target\mybatis-plus-sql-3.0.6.jar=args
 */
public class AgentMybatisPlus {

    private final static String CLASS_PATH = "com/baomidou/mybatisplus/core/MybatisDefaultParameterHandler";

    private static String CLASS_NAME = CLASS_PATH.replace("/", ".");

    public static void premain(String args, Instrumentation inst) throws Exception {
        inst.addTransformer((loader, className, classBeingRedefined, protectionDomain, classfileBuffer) -> {
            if (CLASS_PATH.equals(className)) {
                try {
                    byte[] bytes = relaceBytes(className, classfileBuffer);
                    System.err.println("xss");
                    System.out.println(bytes);
                    System.err.println("xss");
                } catch (NotFoundException e) {
                    e.printStackTrace();
                } catch (CannotCompileException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return classfileBuffer;
        }, true);
    }

    /**
     * 1、拷贝一个新的方法
     * 2、修改原方法名
     * 3、加入监听代码
     */
    private static byte[] relaceBytes(String classname, byte[] classbuffer) throws NotFoundException, CannotCompileException, IOException {
        return classbuffer;
    }

}