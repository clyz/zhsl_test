package vv.ziv.pmc.controller.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import vv.ziv.pmc.entity.TArea;

import java.util.*;

@ControllerAdvice
@RestController
@Slf4j
public class AdviceController {
    @ExceptionHandler(NumberFormatException.class)
    public List<TArea> numberFormatException(NumberFormatException e){
        ArrayList<TArea> list = new ArrayList<>();
        return list;
    }
}
