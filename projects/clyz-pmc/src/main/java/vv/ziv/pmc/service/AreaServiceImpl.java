package vv.ziv.pmc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vv.ziv.pmc.dao.IAreaDao;
import vv.ziv.pmc.entity.TArea;

import java.util.List;

@Service
public class AreaServiceImpl implements IAreaService {
    @Autowired
    private IAreaDao iAreaDao;

    @Override
    public List<TArea> findMunicipal() {
        return iAreaDao.findMunicipal();
    }

    @Override
    public List<TArea> findProvincial() {
        return iAreaDao.findProvincial();
    }

    @Override
    public List<TArea> findCounty() {
        return iAreaDao.findCounty();
    }

    @Override
    public List<TArea> findByParentId(Integer areaId) {
        return iAreaDao.findByParentId(areaId);
    }
}