package vv.ziv.pmc.service;

import vv.ziv.pmc.entity.TArea;

import java.util.List;


public interface IAreaService {
    public List<TArea> findMunicipal();

    public List<TArea> findProvincial();

    List<TArea> findCounty();

    List<TArea> findByParentId(Integer areaId);
}
