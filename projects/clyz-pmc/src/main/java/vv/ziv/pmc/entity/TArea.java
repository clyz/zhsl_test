package vv.ziv.pmc.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_area")
@Data
public class TArea {
    /**
     * 地区Id
     */
    @Id
    private Integer areaId;
    /**
     * 地区编码
     */
    private String areaCode;
    /**
     *  地区名
     */
    private String areaName;
    /**
     * 地区级别（1:省份province,2:市city,3:区县district,4:街道street）
     */
    private Integer level;
    /**
     * 城市编码
     */
    private String cityCode;
    /**
     * 城市中心点（即：经纬度坐标）
     */
    private String center;
    /**
     * 地区父节点 -1 表示无
     */
    private Integer parentId;

}
