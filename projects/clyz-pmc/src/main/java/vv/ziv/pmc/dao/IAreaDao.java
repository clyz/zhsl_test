package vv.ziv.pmc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vv.ziv.pmc.entity.TArea;

import java.util.List;

/**
 * 杨泽民
 * Provincial and Municipal County
 * @Data
 * 数据库jpa
 */
public interface IAreaDao extends JpaRepository<TArea,Integer> {
    /**
     * 查询 父级节点id 的所有地区
     * @param areaId 父级节点id
     * @return
     */
    List<TArea> findByParentId(Integer areaId);
    /**
     * 查询所有父级节点
     * @return
     */
    @Query("select w from TArea w where w.parentId=-1")
    List<TArea> findAllParents();
    /**
     * Provincial 查询所有省
     */
    @Query("select w from TArea w where w.level=1")
    List<TArea> findProvincial();
    /**
     * Municipal 查询所有市
     */
    @Query("select w from TArea w where w.level=2")
    List<TArea> findMunicipal();
    /**
     * County 查询所有市
     */
    @Query("select w from TArea w where w.level=3")
    List<TArea> findCounty();
    /**
     * 查询 Provincial and Municipal County 中每一类型的所有
     */
    List<TArea> findByLevel(Integer level);
}
