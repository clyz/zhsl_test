package vv.ziv.pmc.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vv.ziv.pmc.entity.TArea;
import vv.ziv.pmc.service.IAreaService;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/area")
@CrossOrigin
@Api(value="省市县三级联动接口",tags={"省市县三级联动接口,面向所有用户"})
public class AreaController {
    @Autowired
    private IAreaService iAreaService;

    @GetMapping("/municipal")
    @ApiOperation(value="获取所有县级城市")
    public List<TArea> findMunicipal(){
        return iAreaService.findMunicipal();
    }

    @GetMapping("/provincial")
    @ApiOperation(value="获取所有省级城市")
    public List<TArea> findProvincial(){
        return iAreaService.findProvincial();
    }

    @GetMapping("/county")
    @ApiOperation(value="获取所有市级城市")
    public List<TArea> findCounty(){
        return iAreaService.findCounty();
    }

    @GetMapping("/next")
    @ApiOperation(value="获取所有上级目录下的所有信息",tags={"获取用户信息copy"},notes="传入省级id，返回该省级下的所有市级，无返回为[]")
    List<TArea> findByParentId(@ApiParam(name="areaId",value="上一级的id") Integer areaId){
        return iAreaService.findByParentId(areaId);
    }
}
