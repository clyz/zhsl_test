# clyz-SpringCloud
    Spring Cloud 项目。
    
    clyz-SpringCloud 父级节点。

## server-eureka
    【应用】服务注册中心
    端口：8761

## gateway-zuul
    【应用】服务网关
    端口：8762
    
## common
    【模块】统一的响应处理，统一的异常处理，统一消息转换器
    
## system-advertising
    【父模块】 广告系统
### advertising
    【应用】 广告系统
    端口：9001
### advertising-sponsor
    【应用】 广告投放系统
    端口：9002