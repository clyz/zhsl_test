package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@EnableJpaAuditing
@RunWith(SpringRunner.class)
@Slf4j
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByUserName1() {
        final User byUserName = userRepository.findByUserName("");
        Assert.assertNull(byUserName);
    }

    @Test
    public void findByUserName2() {
        final User byUserName = userRepository.findByUserName("Zemin");
        Assert.assertNotNull(byUserName);
    }

    @Test
    public void save(){
        final User user = new User("Zemin","Zemin.Yang");
        final User save = userRepository.save(user);
        log.info(save.toString());
    }
}