package com.clyz.advertising.sponsor.constant;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:37
 */
public class Constants {
    public static class ErrorMsg {
        public static final String REQUEST_PARAM_ERROR = "请求参数异常";
        public static final String SAME_NAME_ERROR = "存在同名用户异常";

        public static final String SAME_NAME_UNIT_ERROR = "存在同名推广单元";

        public static final String CAN_NOT_FIND_REROR = "找不到数据";

        public static final String SAME_NAME_PLAN_ERROR = "找不到数据";
    }
}
