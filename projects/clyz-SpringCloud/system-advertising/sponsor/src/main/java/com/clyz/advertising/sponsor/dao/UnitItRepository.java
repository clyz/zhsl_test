package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.condition.UnitIt;
import com.clyz.advertising.sponsor.model.po.condition.UnitKeyword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:21
 */
@Repository
public interface UnitItRepository extends JpaRepository<UnitIt,Long> {

}
