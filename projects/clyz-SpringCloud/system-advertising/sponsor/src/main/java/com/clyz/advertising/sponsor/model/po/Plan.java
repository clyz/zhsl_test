package com.clyz.advertising.sponsor.model.po;

import com.clyz.advertising.sponsor.constant.CommonStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * 推广 计划
 *
 * @author Zemin.Yang
 * @date 2020/3/25 14:39
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_plan")
@EntityListeners(AuditingEntityListener.class)
public class Plan {

    public Plan(Long userId, String name, Date startDate, Date endDate) {
        this.userId = userId;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = CommonStatus.VALID.getStatus();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    @Basic
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "plan_name", nullable = false)
    private String name;

    @Column(name = "plan_status", nullable = false)
    private Integer status;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @Column(name = "create_time", updatable = false, nullable = false)
    @Basic
    @CreatedDate
    private Date createTime;

    @Column(name = "update_time", nullable = false)
    @Basic
    @LastModifiedDate
    private Date updateTime;
}
