package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:16
 */
@Repository
public interface UnitRepository extends JpaRepository<Unit, Long> {

    Unit findByNameAndAndPlanId(String name, Long planId);

    List<Unit> findAllByStatus(String status);
}
