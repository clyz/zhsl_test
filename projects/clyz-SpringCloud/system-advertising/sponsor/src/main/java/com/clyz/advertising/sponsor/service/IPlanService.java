package com.clyz.advertising.sponsor.service;

import com.clyz.advertising.sponsor.model.po.Plan;
import com.clyz.advertising.sponsor.model.vo.AddPlanRequest;
import com.clyz.advertising.sponsor.model.vo.AddPlanResponse;
import com.clyz.advertising.sponsor.model.vo.PlanGetRequest;
import com.clyz.common.exception.CommonException;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:50
 */
public interface IPlanService {

    /**
     * <h2>创建计划</h2>
     */
    AddPlanResponse createPlan(AddPlanRequest addPlanRequest) throws CommonException;

    /**
     * <h2>获取计划</h2>
     */
    List<Plan> getPlanByIds(PlanGetRequest planGetRequest) throws CommonException;

    /**
     * <h2>更新计划</h2>
     */
    AddPlanResponse updatePlan(AddPlanRequest addPlanRequest) throws CommonException;

    /**
     * <h2>删除计划</h2>
     */
    void deletePlan(AddPlanRequest addPlanRequest) throws CommonException;
}
