package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest {

    private String userName;

    public boolean validate() {
        return !StringUtils.isEmpty(userName);
    }

}
