package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitDistrictRequest {
    private List<UnitDistrict> unitDistricts;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnitDistrict {
        private Long unitId;

        /**
         * 省
         */
        private String province;

        /**
         * 市
         */
        private String city;
    }
}
