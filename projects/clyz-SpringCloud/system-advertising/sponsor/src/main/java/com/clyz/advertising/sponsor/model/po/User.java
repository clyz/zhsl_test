package com.clyz.advertising.sponsor.model.po;

import com.clyz.advertising.sponsor.constant.CommonStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 14:21
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_user")
@EntityListeners(AuditingEntityListener.class)
public class User {

    public User(String userName, String token) {
        this.userName = userName;
        this.token = token;
        this.status = CommonStatus.VALID.getStatus();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Long id;

    @Basic
    @Column(name = "username",nullable = false)
    private String userName;

    @Column(name = "token",nullable = false)
    @Basic
//    @Transient
    private String token;

    @Column(name = "user_status",nullable = false)
    private Integer status;

    @Column(name = "create_time",updatable = false,nullable = false)
    @Basic
    @CreatedDate
    private Date createTime;

    @Column(name = "update_time",nullable = false)
    @Basic
    @LastModifiedDate
    private Date updateTime;
}
