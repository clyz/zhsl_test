package com.clyz.advertising.sponsor.model.po.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * 推广 单元 的 兴趣 限定词
 *
 * @author Zemin.Yang
 * @date 2020/3/25 14:58
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_unit_it")
@EntityListeners(AuditingEntityListener.class)
public class UnitIt {
    public UnitIt(Long unitId, String itTag) {
        this.unitId = unitId;
        this.itTag = itTag;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    @Column(name = "unit_id", nullable = false)
    private Long unitId;

    /**
     * 兴趣
     */
    @Column(name = "it_tag",nullable = false)
    private String itTag;
}
