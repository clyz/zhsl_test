package com.clyz.advertising.sponsor.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 14:33
 */
@Getter
@AllArgsConstructor
public enum CommonStatus {
    VALID(1, "有效状态"),
    INVALID(0, "无效状态");
    private Integer status;
    private String desc;
}
