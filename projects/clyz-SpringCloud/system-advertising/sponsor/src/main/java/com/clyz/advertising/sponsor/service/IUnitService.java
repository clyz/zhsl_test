package com.clyz.advertising.sponsor.service;

import com.clyz.advertising.sponsor.model.vo.*;
import com.clyz.common.exception.CommonException;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:06
 */
public interface IUnitService {
    /**
     * <h2>新增推广单元</h2>
     */
    UnitResponse createUnit(UnitRequest unitRequest) throws CommonException;

    UnitItResponse createUnitIt(UnitItRequest unitItRequest) throws CommonException;

    UnitKeywordResponse createUnitKeyword(UnitKeywordRequest unitKeywordRequest) throws CommonException;

    UnitDistrictResponse createUnitDistrict(UnitDistrictRequest unitDistrictRequest) throws CommonException;

    /**
     * 新增 创意单元
     *
     * @param creativeUnitRequest request
     * @return response
     * @throws CommonException 异常
     * @see CommonException
     */
    CreativeUnitResponse createCreativeUnit(CreativeUnitRequest creativeUnitRequest) throws CommonException;
}
