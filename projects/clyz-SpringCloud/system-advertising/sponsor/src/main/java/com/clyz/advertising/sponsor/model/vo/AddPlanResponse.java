package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddPlanResponse {
    private Long id;
    private String planName;
}