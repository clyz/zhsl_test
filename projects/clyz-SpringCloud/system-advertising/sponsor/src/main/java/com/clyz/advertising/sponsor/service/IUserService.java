package com.clyz.advertising.sponsor.service;

import com.clyz.advertising.sponsor.model.vo.CreateUserRequest;
import com.clyz.advertising.sponsor.model.vo.CreateUserResponse;
import com.clyz.common.exception.CommonException;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:31
 */
public interface IUserService {
    /**
     * <p>
     *     创建用户
     * </p>
     * @param createUserRequest
     * @return
     * @throws CommonException
     */
    CreateUserResponse createUser(CreateUserRequest createUserRequest) throws CommonException;
}
