package com.clyz.advertising.sponsor.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 15:24
 */
@Getter
@AllArgsConstructor
public enum CreativeType {
    IMAGE(1,"图片"),
    VIDEQ(2,"视频"),
    TEXT(3,"文本");
    private Integer status;
    private String desc;
}
