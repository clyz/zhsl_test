package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.Plan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:12
 */
@Repository
public interface PlanRepository extends JpaRepository<Plan, Long> {
    Plan findByIdAndUserId(Long id, Long userId);

    List<Plan> findAllByIdInAndUserId(List<Long> ids, Long userId);

    Plan findByUserIdAndName(Long userId,String name);

    List<Plan> findAllByStatus(Integer status);
}
