package com.clyz.advertising.sponsor.model.po.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * 推广 单元 的 关键词 限定词
 *
 * @author Zemin.Yang
 * @date 2020/3/25 14:58
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_unit_keyword")
@EntityListeners(AuditingEntityListener.class)
public class UnitKeyword {
    public UnitKeyword(Long unitId, String keyword) {
        this.unitId = unitId;
        this.keyword = keyword;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    @Column(name = "unit_id", nullable = false)
    private Long unitId;

    /**
     * 关键词
     */
    @Column(nullable = false)
    private String keyword;
}
