package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlanGetRequest {
    private Long userId;
    private List<Long> ids;

    public boolean validate() {
        return userId != null &&
                !CollectionUtils.isEmpty(ids);
    }
}
