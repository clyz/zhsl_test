package com.clyz.advertising.sponsor.model.po;

import com.clyz.advertising.sponsor.constant.CreativeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * 创意[与单元多对多]
 *
 * @author Zemin.Yang
 * @date 2020/3/25 13:56
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_creative")
@EntityListeners(AuditingEntityListener.class)
public class Creative {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    private String name;
    /**
     * 创意类型 视频 图片 。。。
     * @see com.clyz.advertising.sponsor.constant.CreativeType
     */
    @Column(nullable = false)
    private Integer type;

    /**
     * 物料类型
     * @see  com.clyz.advertising.sponsor.constant.CreativeMetrialType
     */
    @Column(name = "material_type", nullable = false)
    private Integer materialType;

    @Column(nullable = false)
    private Integer width;
    /**
     * 物料类型 大小
     */
    @Column(nullable = false)
    private Integer size;
    /**
     * 物料类型 时长【只有视频不为0】
     */
    @Column(nullable = false)
    private Integer duration;

    /**
     * 物料 状态
     */
    @Column(name = "audit_status", nullable = false)
    private Integer satatus;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    private String url;

    @Column(name = "create_time", updatable = false, nullable = false)
    @Basic
    @CreatedDate
    private Date createTime;

    @Column(name = "update_time", nullable = false)
    @Basic
    @LastModifiedDate
    private Date updateTime;
}
