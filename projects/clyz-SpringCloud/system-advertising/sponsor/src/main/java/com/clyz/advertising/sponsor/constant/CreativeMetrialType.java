package com.clyz.advertising.sponsor.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 15:24
 */
@Getter
@AllArgsConstructor
public enum CreativeMetrialType {
    JPG(1,"jpg"),
    BMP(2,"bmp"),
    MP4(3,"mp4"),
    TXT(3,"txt")
    ;
    private Integer type;
    private String desc;
}
