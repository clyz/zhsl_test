package com.clyz.advertising.sponsor.service.impl;

import cn.hutool.crypto.digest.MD5;
import com.clyz.advertising.sponsor.constant.Constants;
import com.clyz.advertising.sponsor.dao.UserRepository;
import com.clyz.advertising.sponsor.model.po.User;
import com.clyz.advertising.sponsor.model.vo.CreateUserRequest;
import com.clyz.advertising.sponsor.model.vo.CreateUserResponse;
import com.clyz.advertising.sponsor.service.IUserService;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:35
 */
@Slf4j
@Service
public class UserServiceImpl implements IUserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public CreateUserResponse createUser(CreateUserRequest createUserRequest) throws CommonException {
        if (!createUserRequest.validate()) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        if (userRepository.findByUserName(createUserRequest.getUserName()) != null) {
            throw new CommonException(Constants.ErrorMsg.SAME_NAME_ERROR);
        }
        final User user = userRepository.save(
                new User(createUserRequest.getUserName(), MD5.create().digestHex(createUserRequest.getUserName()))
        );
        return new CreateUserResponse(user.getId(), user.getUserName(), user.getToken());
    }
}
