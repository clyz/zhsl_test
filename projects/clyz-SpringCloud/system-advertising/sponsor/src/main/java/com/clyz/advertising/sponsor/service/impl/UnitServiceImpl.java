package com.clyz.advertising.sponsor.service.impl;

import com.clyz.advertising.sponsor.constant.Constants;
import com.clyz.advertising.sponsor.dao.*;
import com.clyz.advertising.sponsor.model.po.Plan;
import com.clyz.advertising.sponsor.model.po.Unit;
import com.clyz.advertising.sponsor.model.po.condition.CreativeUnit;
import com.clyz.advertising.sponsor.model.po.condition.UnitDistrict;
import com.clyz.advertising.sponsor.model.po.condition.UnitIt;
import com.clyz.advertising.sponsor.model.po.condition.UnitKeyword;
import com.clyz.advertising.sponsor.model.vo.*;
import com.clyz.advertising.sponsor.service.IUnitService;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:11
 */
@Slf4j
@Service
public class UnitServiceImpl implements IUnitService {
    private final UnitRepository unitRepository;
    private final PlanRepository planRepository;
    private final UnitKeywordRepository unitKeywordRepository;
    private final UnitItRepository unitItRepository;
    private final UnitDistrictRepository unitDistrictRepository;

    private final CreativeRepository creativeRepository;
    private final CreativeUnitRepository creativeUnitRepository;

    public UnitServiceImpl(PlanRepository planRepository, UnitRepository unitRepository, UnitKeywordRepository unitKeywordRepository, UnitItRepository unitItRepository, UnitDistrictRepository unitDistrictRepository, CreativeRepository creativeRepository, CreativeUnitRepository creativeUnitRepository) {
        this.planRepository = planRepository;
        this.unitRepository = unitRepository;
        this.unitKeywordRepository = unitKeywordRepository;
        this.unitItRepository = unitItRepository;
        this.unitDistrictRepository = unitDistrictRepository;
        this.creativeRepository = creativeRepository;
        this.creativeUnitRepository = creativeUnitRepository;
    }

    @Override
    @Transactional
    public UnitResponse createUnit(UnitRequest unitRequest) throws CommonException {
        if (!unitRequest.createValidate()) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final Optional<Plan> plan = planRepository.findById(unitRequest.getPlanId());
        if (!plan.isPresent()) {
            throw new CommonException(Constants.ErrorMsg.CAN_NOT_FIND_REROR);
        }
        if (null == unitRepository.findByNameAndAndPlanId(unitRequest.getName(), unitRequest.getPlanId())) {
            throw new CommonException(Constants.ErrorMsg.SAME_NAME_UNIT_ERROR);
        }
        final Unit unit = unitRepository.save(
                new Unit(unitRequest.getPlanId(), unitRequest.getName(), unitRequest.getBudget())
        );
        final UnitResponse unitResponse = new UnitResponse();
        BeanUtils.copyProperties(unit, unitResponse);
        return unitResponse;
    }

    @Override
    public UnitItResponse createUnitIt(UnitItRequest unitItRequest) throws CommonException {
        final List<Long> unitIds = unitItRequest.getUnitIts().stream().map(UnitItRequest.UnitIt::getUnitId).collect(Collectors.toList());
        if (isRelatedUnitExist(unitIds)) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final List<UnitIt> unitIts = new ArrayList<>();
        if (!CollectionUtils.isEmpty(unitItRequest.getUnitIts())) {
            unitItRequest.getUnitIts().forEach(e -> unitIts.add(new UnitIt(e.getUnitId(), e.getItTag())));
            return new UnitItResponse(
                    unitItRepository.saveAll(unitIts).stream().map(UnitIt::getId).collect(Collectors.toList())
            );
        }
        return new UnitItResponse(Collections.emptyList());
    }

    @Override
    public UnitKeywordResponse createUnitKeyword(UnitKeywordRequest unitKeywordRequest) throws CommonException {
        final List<Long> unitIds = unitKeywordRequest.getUnitKeywords().stream().map(UnitKeywordRequest.UnitKeyword::getUnitId).collect(Collectors.toList());
        if (isRelatedUnitExist(unitIds)) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final List<UnitKeyword> unitKeywords = new ArrayList<>();
        if (!CollectionUtils.isEmpty(unitKeywordRequest.getUnitKeywords())) {
            unitKeywordRequest.getUnitKeywords().forEach(e -> unitKeywords.add(
                    new UnitKeyword(e.getUnitId(), e.getKeyword())
            ));
            return new UnitKeywordResponse(unitKeywordRepository.saveAll(unitKeywords)
                    .stream().map(UnitKeyword::getId).collect(Collectors.toList()));
        }
        return new UnitKeywordResponse(Collections.emptyList());
    }

    @Override
    public UnitDistrictResponse createUnitDistrict(UnitDistrictRequest unitDistrictRequest) throws CommonException {
        final List<Long> unitIds = unitDistrictRequest.getUnitDistricts().stream().map(UnitDistrictRequest.UnitDistrict::getUnitId).collect(Collectors.toList());
        if (isRelatedUnitExist(unitIds)) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final List<UnitDistrict> unitDistricts = new ArrayList<>();
        if (!CollectionUtils.isEmpty(unitDistrictRequest.getUnitDistricts())) {
            unitDistrictRequest.getUnitDistricts().forEach(
                    e -> unitDistricts.add(new UnitDistrict(e.getUnitId(), e.getProvince(), e.getCity())));
            return new UnitDistrictResponse(
                    unitDistrictRepository.saveAll(unitDistricts).stream().map(UnitDistrict::getId).collect(Collectors.toList())
            );
        }
        return new UnitDistrictResponse(Collections.emptyList());
    }

    @Override
    public CreativeUnitResponse createCreativeUnit(CreativeUnitRequest creativeUnitRequest) throws CommonException {
        final List<Long> unitIds = creativeUnitRequest.getCreativeUnitItems()
                .stream().map(CreativeUnitRequest.CreativeUnitItem::getUnitId).collect(Collectors.toList());
        final List<Long> creativeIds = creativeUnitRequest.getCreativeUnitItems()
                .stream().map(CreativeUnitRequest.CreativeUnitItem::getCreativeId).collect(Collectors.toList());
        if (!isRelatedCreativeExist(creativeIds)) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        if (!isRelatedUnitExist(unitIds)) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final List<CreativeUnit> creativeUnits = new ArrayList<>();
        creativeUnitRequest.getCreativeUnitItems().forEach(e-> creativeUnits.add(new CreativeUnit(e.getUnitId(),e.getCreativeId())));
        return new CreativeUnitResponse(
                creativeUnitRepository.saveAll(creativeUnits).stream().map(CreativeUnit::getId).collect(Collectors.toList())
        );
    }

    private boolean isRelatedCreativeExist(List<Long> creativeIds) {
        if (CollectionUtils.isEmpty(creativeIds)) {
            return false;
        }
        return creativeRepository.findAllById(creativeIds).size() == creativeIds.size();
    }

    /**
     * 判断推广单元是否 一直
     *
     * @param unitIds 推广单元
     * @return boolean
     */
    private boolean isRelatedUnitExist(List<Long> unitIds) {
        if (CollectionUtils.isEmpty(unitIds)) {
            return true;
        }
        return unitRepository.findAllById(unitIds).size() != new HashSet<>(unitIds).size();
    }
}
