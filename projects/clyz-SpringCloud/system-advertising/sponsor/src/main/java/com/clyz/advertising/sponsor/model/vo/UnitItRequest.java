package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitItRequest {

    private List<UnitIt> unitIts;
    
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnitIt {
        private Long unitId;
        private String itTag;
    }
}
