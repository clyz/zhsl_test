package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitItResponse {
    private List<Long> id;
}