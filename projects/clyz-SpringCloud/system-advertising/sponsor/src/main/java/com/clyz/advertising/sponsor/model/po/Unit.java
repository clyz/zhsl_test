package com.clyz.advertising.sponsor.model.po;

import com.clyz.advertising.sponsor.constant.CommonStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * 推广单元
 *
 * @author Zemin.Yang
 * @date 2020/3/25 13:59
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_creative_nuit")
public class Unit {

    public Unit(Long planId, String name, Long budget) {
        this.planId = planId;
        this.name = name;
        this.budget = budget;
        this.status = CommonStatus.VALID.getStatus();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    @Column(nullable = true)
    private Long planId;

    @Column(name = "nuit_name", nullable = false)
    private String name;

    @Column(name = "nuit_status", nullable = false)
    private Integer status;

    /**
     * 推广单元的位置（开屏，贴片，中贴，暂停贴。。。）
     */
    @Column(nullable = false)
    private Integer positionType;

    /**
     * 预算
     */
    @Column(nullable = false)
    private Long budget;

    @Column(name = "create_time", updatable = false, nullable = false)
    @Basic
    @CreatedDate
    private Date createTime;

    @Column(name = "update_time", nullable = false)
    @Basic
    @LastModifiedDate
    private Date updateTime;
}
