package com.clyz.advertising.sponsor.service;

import com.clyz.advertising.sponsor.model.vo.CreativeReqest;
import com.clyz.advertising.sponsor.model.vo.CreativeResponse;
import com.clyz.common.exception.CommonException;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 13:52
 */
public interface ICreativeService {
    CreativeResponse createCreativce(CreativeReqest creativeReqest) throws CommonException;
}
