package com.clyz.advertising.sponsor.service.impl;

import com.clyz.advertising.sponsor.dao.CreativeRepository;
import com.clyz.advertising.sponsor.model.po.Creative;
import com.clyz.advertising.sponsor.model.vo.CreativeReqest;
import com.clyz.advertising.sponsor.model.vo.CreativeResponse;
import com.clyz.advertising.sponsor.service.ICreativeService;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 14:02
 */
@Slf4j
@Service
public class CreativeServiceImpl implements ICreativeService {
    private final CreativeRepository creativeRepository;

    public CreativeServiceImpl(CreativeRepository creativeRepository) {
        this.creativeRepository = creativeRepository;
    }

    @Override
    public CreativeResponse createCreativce(CreativeReqest creativeReqest) throws CommonException {
        log.error("无校验处理 TODO");
        final Creative creative = creativeRepository.save(creativeReqest.convertToEntity());
        final CreativeResponse creativeResponse = new CreativeResponse();
        BeanUtils.copyProperties(creative, creativeResponse);
        return creativeResponse;
    }

}
