package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddPlanRequest {
    private Long id;
    private Long userId;
    private String planName;
    private String startDate;
    private String endDate;

    /**
     * true 通过验证
     */
    public boolean createValidate() {
        return userId != null &&
                !StringUtils.isEmpty(planName) &&
                !StringUtils.isEmpty(startDate) &&
                !StringUtils.isEmpty(endDate);
    }

    public boolean updateValidate() {
        return userId != null && id != null;
    }

    public boolean deleteValidate() {
        return userId != null && id != null;
    }
}
