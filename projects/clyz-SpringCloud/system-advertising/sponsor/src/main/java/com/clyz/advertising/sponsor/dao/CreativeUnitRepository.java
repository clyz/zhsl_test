package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.condition.CreativeUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 14:18
 */
@Repository
public interface CreativeUnitRepository extends JpaRepository<CreativeUnit, Long> {
}
