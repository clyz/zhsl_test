package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 13:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeResponse {
    private String name;
    private Long id;
}
