package com.clyz.advertising.sponsor.controller;

import com.alibaba.fastjson.JSON;
import com.clyz.advertising.sponsor.model.vo.CreateUserRequest;
import com.clyz.advertising.sponsor.model.vo.CreateUserResponse;
import com.clyz.advertising.sponsor.service.IUserService;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 14:39
 */
@RestController
@Slf4j
public class UserOPController {
    private final IUserService userService;

    public UserOPController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create/user")
    public CreateUserResponse createUser(@RequestBody CreateUserRequest request) throws CommonException {
        log.info("ad-sponsor:  createUser -> {}", JSON.toJSONString(request));
        return userService.createUser(request);
    }
}
