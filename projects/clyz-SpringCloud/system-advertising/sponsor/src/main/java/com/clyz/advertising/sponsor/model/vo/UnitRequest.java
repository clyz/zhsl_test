package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitRequest {
    private Long planId;
    private String name;
    private Integer positionType;
    private Long budget;

    public boolean createValidate() {
        return null != planId &&
                !StringUtils.isEmpty(name) &&
                null != positionType &&
                null != budget;
    }
}
