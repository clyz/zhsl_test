package com.clyz.advertising.sponsor.model.vo;

import com.clyz.advertising.sponsor.constant.CommonStatus;
import com.clyz.advertising.sponsor.model.po.Creative;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 13:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeReqest {
    private String name;
    private String type;
    private Integer materialType;
    private Integer height;
    private Integer width;
    private Long size;
    private Integer duration;
    private Long userId;
    private String url;

    public Creative convertToEntity() {
        final Creative creative = new Creative();
        BeanUtils.copyProperties(this, creative);
        creative.setSatatus(CommonStatus.VALID.getStatus());
        creative.setCreateTime(new Date());
        creative.setUpdateTime(creative.getCreateTime());
        return creative;
    }
}
