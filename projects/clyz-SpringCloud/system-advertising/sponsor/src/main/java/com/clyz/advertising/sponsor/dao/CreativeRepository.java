package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.Creative;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:18
 */
@Repository
public interface CreativeRepository extends JpaRepository<Creative,Long> {

}
