package com.clyz.advertising.sponsor.model.po.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * 推广 单元 的 地域 限定词
 *
 * @author Zemin.Yang
 * @date 2020/3/25 14:58
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_unit_district")
@EntityListeners(AuditingEntityListener.class)
public class UnitDistrict {

    public UnitDistrict(Long unitId, String province, String city) {
        this.unitId = unitId;
        this.province = province;
        this.city = city;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    @Column(name = "unit_id", nullable = false)
    private Long unitId;

    /**
     * 省
     */
    @Column(nullable = false)
    private String province;

    /**
     * 市
     */
    @Column(nullable = false)
    private String city;
}
