package com.clyz.advertising.sponsor.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Zemin.Yang
 * @date 2020/3/26 9:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitResponse {
    private Long id;
    private String name;
}
