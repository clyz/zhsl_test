package com.clyz.advertising.sponsor.service.impl;

import com.clyz.advertising.sponsor.constant.Constants;
import com.clyz.advertising.sponsor.dao.PlanRepository;
import com.clyz.advertising.sponsor.dao.UserRepository;
import com.clyz.advertising.sponsor.model.po.Plan;
import com.clyz.advertising.sponsor.model.po.User;
import com.clyz.advertising.sponsor.model.vo.AddPlanRequest;
import com.clyz.advertising.sponsor.model.vo.AddPlanResponse;
import com.clyz.advertising.sponsor.model.vo.PlanGetRequest;
import com.clyz.advertising.sponsor.service.IPlanService;
import com.clyz.advertising.sponsor.util.CommonUtils;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 16:59
 */
@Service
@Slf4j
public class PlanServiceImpl implements IPlanService {

    private final UserRepository userRepository;
    private final PlanRepository planRepository;

    public PlanServiceImpl(PlanRepository planRepository, UserRepository userRepository) {
        this.planRepository = planRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public AddPlanResponse createPlan(AddPlanRequest addPlanRequest) throws CommonException {
        if (!addPlanRequest.createValidate()) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final Optional<User> user = userRepository.findById(addPlanRequest.getUserId());
        if (!user.isPresent()) {
            throw new CommonException(Constants.ErrorMsg.CAN_NOT_FIND_REROR);
        }
        if (planRepository.findByUserIdAndName(addPlanRequest.getUserId(), addPlanRequest.getPlanName()) != null) {
            throw new CommonException(Constants.ErrorMsg.SAME_NAME_PLAN_ERROR);
        }
        final Plan plan = planRepository.save(
                new Plan(addPlanRequest.getUserId(), addPlanRequest.getPlanName(),
                        CommonUtils.parseStringDate(addPlanRequest.getStartDate()),
                        CommonUtils.parseStringDate(addPlanRequest.getEndDate()))
        );
        return new AddPlanResponse(plan.getId(), plan.getName());
    }

    @Override
    public List<Plan> getPlanByIds(PlanGetRequest planGetRequest) throws CommonException {
        if(!planGetRequest.validate()){
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        return planRepository.findAllByIdInAndUserId(planGetRequest.getIds(),planGetRequest.getUserId());
    }

    @Override
    public AddPlanResponse updatePlan(AddPlanRequest addPlanRequest) throws CommonException {
        if (!addPlanRequest.updateValidate()) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final Plan plan = planRepository.findByUserIdAndName(addPlanRequest.getUserId(), addPlanRequest.getPlanName());
        if(plan==null){
            throw new CommonException(Constants.ErrorMsg.CAN_NOT_FIND_REROR);
        }
        BeanUtils.copyProperties(addPlanRequest,plan);
        final Plan planNew = planRepository.save(plan);
        return new AddPlanResponse(planNew.getId(), planNew.getName());
    }

    @Override
    @Transactional
    public void deletePlan(AddPlanRequest addPlanRequest) throws CommonException {
        if (!addPlanRequest.deleteValidate()) {
            throw new CommonException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        final Plan plan = planRepository.findByUserIdAndName(addPlanRequest.getUserId(), addPlanRequest.getPlanName());
        if(plan==null){
            throw new CommonException(Constants.ErrorMsg.CAN_NOT_FIND_REROR);
        }
        planRepository.delete(plan);
    }
}
