package com.clyz.advertising.sponsor.dao;

import com.clyz.advertising.sponsor.model.po.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 15:29
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUserName(String userName);
}
