package com.clyz.advertising.sponsor.model.po.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * 创意与 单元的 多对多关系
 * @author Zemin.Yang
 * @date 2020/3/25 15:22
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ad_creative_unit")
@EntityListeners(AuditingEntityListener.class)
public class CreativeUnit {
    public CreativeUnit(Long unitId, Long creativeId) {
        this.unitId = unitId;
        this.creativeId = creativeId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Basic
    private Long id;

    @Column(name = "unit_id", nullable = false)
    private Long unitId;
    @Column(name = "creative_id", nullable = false)
    private Long creativeId;
}
