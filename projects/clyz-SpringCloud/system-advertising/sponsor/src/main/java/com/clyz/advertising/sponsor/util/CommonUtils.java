package com.clyz.advertising.sponsor.util;

import com.clyz.common.exception.CommonException;
import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * @author Zemin.Yang
 * @date 2020/3/25 17:09
 */
public class CommonUtils {
    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy/MM/dd", "yyyy.MM.dd"
    };

    public static Date parseStringDate(String dateString) throws CommonException {
        try {
            return DateUtils.parseDate(dateString, parsePatterns);
        } catch (ParseException e) {
            throw new CommonException(e.getMessage());
        }
    }
}
