package com.clyz.advertising.sponsor.controller;

import com.alibaba.fastjson.JSON;
import com.clyz.advertising.sponsor.model.po.Plan;
import com.clyz.advertising.sponsor.model.vo.AddPlanRequest;
import com.clyz.advertising.sponsor.model.vo.AddPlanResponse;
import com.clyz.advertising.sponsor.model.vo.PlanGetRequest;
import com.clyz.advertising.sponsor.service.IPlanService;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 14:43
 */
@Slf4j
@RestController
public class PlanOPController {
    private final IPlanService planService;


    public PlanOPController(IPlanService planService) {
        this.planService = planService;
    }

    @PostMapping("create/plan")
    public AddPlanResponse createPlan(@RequestBody AddPlanRequest request) throws CommonException {
        log.info("ad-sponsor:  createPlan -> {}", JSON.toJSONString(request));
        return planService.createPlan(request);
    }

    @PostMapping("get/plan")
    public List<Plan> getPlan(@RequestBody PlanGetRequest request) throws CommonException {
        log.info("ad-sponsor:  getPlan -> {}", JSON.toJSONString(request));
        return planService.getPlanByIds(request);
    }

    @PutMapping("update/plan")
    public AddPlanResponse updatePlan(AddPlanRequest request) throws CommonException {
        log.info("ad-sponsor:  updatePlan -> {}", JSON.toJSONString(request));
        return planService.updatePlan(request);
    }

    @DeleteMapping("delete/plan")
    public void deletePlan(AddPlanRequest request) throws CommonException {
        log.info("ad-sponsor:  deletePlan -> {}", JSON.toJSONString(request));
        planService.deletePlan(request);
    }

}