package com.clyz.advertising.sponsor.controller;

import com.alibaba.fastjson.JSON;
import com.clyz.advertising.sponsor.model.vo.*;
import com.clyz.advertising.sponsor.service.IUnitService;
import com.clyz.common.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zemin.Yang
 * @date 2020/4/2 14:55
 */
@RestController
@Slf4j
public class UnitOPController {

    private final IUnitService unitService;

    public UnitOPController(IUnitService unitService) {
        this.unitService = unitService;
    }

    @PostMapping("create/unit")
    public UnitResponse createUnit(@RequestBody UnitRequest request) throws CommonException {
        log.info("ad-sponsor:  createUnit -> {}", JSON.toJSONString(request));
        return unitService.createUnit(request);
    }

    @PostMapping("create/unitit")
    public UnitItResponse createUnitIt(@RequestBody UnitItRequest request) throws CommonException {
        log.info("ad-sponsor:  createUnitIt -> {}", JSON.toJSONString(request));
        return unitService.createUnitIt(request);
    }

    @PostMapping("create/unitkeyword")
    public UnitKeywordResponse createUnitKeyword(@RequestBody UnitKeywordRequest request) throws CommonException {
        log.info("ad-sponsor:  createUnitKeyword -> {}", JSON.toJSONString(request));
        return unitService.createUnitKeyword(request);
    }

    @PostMapping("create/unitdistrict")
    public UnitDistrictResponse createUnitDistrict(@RequestBody UnitDistrictRequest request) throws CommonException {
        log.info("ad-sponsor:  createUnitDistrict -> {}", JSON.toJSONString(request));
        return unitService.createUnitDistrict(request);
    }

    @PostMapping("create/creativeunit")
    public CreativeUnitResponse createCreativeUnit(@RequestBody CreativeUnitRequest request) throws CommonException {
        log.info("ad-sponsor:  createCreativeUnit -> {}", JSON.toJSONString(request));
        return unitService.createCreativeUnit(request);
    }
}
