package com.clyz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 13:12
 */
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class GatewayZuul {
    public static void main(String[] args) {
        SpringApplication.run(GatewayZuul.class,args);
    }
}
