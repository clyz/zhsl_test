package com.clyz.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 13:38
 */
@Slf4j
@Component
public class AccesslogFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }

    @Override
    public int filterOrder() {
        //  最后执行
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //  请求上下文
        final RequestContext requestContext = RequestContext.getCurrentContext();
        final Long duration = System.currentTimeMillis() - (Long) requestContext.get("startTime");
        final HttpServletRequest request = requestContext.getRequest();

        //  打印 接口执行消耗时间
        log.info("消耗时间  uri: ", request.getRequestURI() + " duration: " + duration / 100 + "ms ");
        return null;
    }
}
