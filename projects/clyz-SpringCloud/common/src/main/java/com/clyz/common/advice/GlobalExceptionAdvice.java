package com.clyz.common.advice;

import com.clyz.common.exception.CommonException;
import com.clyz.common.vo.CommonResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 15:04
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler(CommonException.class)
    public CommonResponse<String> handlerCommonException(HttpServletRequest request, CommonException commonException) {
        return new CommonResponse<String>(-1, "business error", commonException.getMessage());
    }
}
