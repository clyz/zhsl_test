package com.clyz.common.exception;

/**
 * 统一 异常处理
 *
 * @author Zemin.Yang
 * @date 2020/3/24 15:02
 */
public class CommonException extends Exception {
    public CommonException(String message) {
        super(message);
    }
}
