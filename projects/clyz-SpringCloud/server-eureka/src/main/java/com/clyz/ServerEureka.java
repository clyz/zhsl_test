package com.clyz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 10:33
 */
@SpringBootApplication
@EnableEurekaServer
public class ServerEureka {
    public static void main(String[] args) {
        SpringApplication.run(ServerEureka.class, args);
    }

}