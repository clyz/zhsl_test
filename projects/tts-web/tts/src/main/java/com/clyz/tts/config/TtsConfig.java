package com.clyz.tts.config;

import com.wisdom.tts.analysis.SoundAnalysis;
import com.wisdom.tts.analysis.SoundResources;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * tts 配置中心
 *
 * @author Zemin.Yang
 * @date 2020/4/14 9:09
 */
@Configuration
public class TtsConfig {

    @Bean
    @SneakyThrows
    public SoundAnalysis soundAnalysis() {
        return new SoundAnalysis(soundResources());
    }

    @Bean
    @SneakyThrows
    public SoundResources soundResources() {
        return new SoundResources();
    }

}
