package com.clyz.tts.controller;

import com.wisdom.tts.analysis.SoundAnalysis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/tts")
@Slf4j
public class TtsController {
    @Autowired
    private SoundAnalysis soundAnalysis;

    @GetMapping("money")
    public String base64(BigDecimal money, String code) {
        log.info("moeny:{},code:{}",money,code);
        return soundAnalysis.execute(money, "default", code);
    }
}
