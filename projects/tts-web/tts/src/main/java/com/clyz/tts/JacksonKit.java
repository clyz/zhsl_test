package com.clyz.tts;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

import com.wisdom.framework.kit.StringKit;
import org.springframework.util.ObjectUtils;

public class JacksonKit {
    private static ObjectMapper MAPPER = new ObjectMapper();

    private JacksonKit() {
    }

    public static String writeValueAsString(Object object) throws JsonProcessingException {
        return ObjectUtils.isEmpty(object) ? null : MAPPER.writeValueAsString(object);
    }

    public static Object[] wipeOffServletObject(Object[] args) {
        List<Object> objectList = new ArrayList();
        Object[] var2 = args;
        int var3 = args.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Object servlet = var2[var4];
            if (!(servlet instanceof ServletResponse) && !(servlet instanceof ServletRequest) && !(servlet instanceof HttpSession)) {
                objectList.add(servlet);
            }
        }

        return objectList.toArray();
    }

    public static Map readMapValue(String json) throws IOException {
        return (Map)readValue(json, Map.class);
    }

    public static List readListMapValue(String json) throws IOException {
        return (List)readValue(json, List.class);
    }

    public static <T> T readValue(String json, Class<T> clazz) throws IOException {
        return StringKit.isBlank(json) ? null : MAPPER.readValue(json, clazz);
    }

    public static <T> List<T> readListValue(String json, Class<T> clazz) throws IOException {
        return StringKit.isBlank(json) ? null : (List)MAPPER.readValue(json, MAPPER.getTypeFactory().constructParametricType(ArrayList.class, new Class[]{clazz}));
    }

    public static String[] readStringArrayValue(String json) throws IOException {
        return (String[])readValue(json, String[].class);
    }

    public static boolean isJSON(String json) {
        try {
            MAPPER.readTree(json);
            return true;
        } catch (IOException var2) {
            return false;
        }
    }

    static {
        MAPPER.setSerializationInclusion(Include.NON_NULL);
        MAPPER.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, false);
    }
}
