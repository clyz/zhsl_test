package com.clyz.tts;

import com.wisdom.tts.analysis.SoundAnalysis;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;


@SpringBootTest
public class SoundAnalysisTest {

    @Autowired
    private SoundAnalysis soundAnalysis;

    @SneakyThrows
    @Test
    public void executeDefault() {
        final String base64 = soundAnalysis.executeDefault(new BigDecimal(0.2));
        final Path path = Paths.get("1.wav");
        if (!Files.exists(path)) {
            Files.createFile(path);
        }
        Files.write(path, Base64.getDecoder().decode(base64));
    }

    @Test
    @SneakyThrows
    public void execute() {
        final String base64 = soundAnalysis.execute(new BigDecimal(1000000), "default", "zhifubao");
        final Path path = Paths.get("2.wav");
        if (!Files.exists(path)) {
            Files.createFile(path);
        }
        Files.write(path, Base64.getDecoder().decode(base64));
    }
}