package com.wisdom.tts.analysis.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 扩展音地址
 * 声音 源文件地址信息
 *
 * @author Zemin.Yang
 * @date 2020/4/14 13:46
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SoundInfo implements Serializable {
    /**
     * 备注
     */
    private String remake;
    /**
     * 代码,对应 音频源文件的 代号。
     */
    private String code;

    /**
     * 对应文件名称
     */
    private String name;
}
