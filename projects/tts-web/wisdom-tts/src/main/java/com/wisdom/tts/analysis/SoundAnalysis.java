package com.wisdom.tts.analysis;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>
 * 音频解析
 * </h1>
 *
 * @author Zemin.Yang
 * @date 2020/4/14 13:37
 */
@Slf4j
@AllArgsConstructor
public class SoundAnalysis {

    private SoundResources soundResources;

    @SneakyThrows
    public String executeDefault(BigDecimal decimal) {
        return execute(decimal, soundResources.getDefaultType(), null);
    }

    @SneakyThrows
    public String execute(BigDecimal decimal, String type, String code) {
        final List<String> execute = soundResources.executeTransferred(decimal, type);
        if (StringUtils.isNotBlank(code)) {
            final List<String> command = new ArrayList<>();
            command.add(
                    soundResources.getSoundInfoByCode(type, code).getName()
            );
            command.addAll(execute);
            return soundResources.executeCommand(command, type);
        } else {
            return soundResources.executeCommand(execute, type);
        }
    }

}