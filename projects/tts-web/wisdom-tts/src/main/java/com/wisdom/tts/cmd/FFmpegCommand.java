package com.wisdom.tts.cmd;

import com.wisdom.tts.exception.TtsException;
import com.wisdom.tts.util.CommandUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import pl.touk.throwing.ThrowingConsumer;
import pl.touk.throwing.ThrowingFunction;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Zemin.Yang
 * @date 2020/4/13 16:43
 */
@Slf4j
public class FFmpegCommand {

    public static final String DOT = ".";
    private static final String C_I = "-i";

    private FFmpegBean fFmpegBean;

    public FFmpegCommand(FFmpegBean fFmpegBean) {
        this.fFmpegBean = fFmpegBean;
    }

/*    private static String FFMPEG_EXE_PATH = null;

    static {
        try {
            FFMPEG_EXE_PATH = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX + "ffmpeg-20190909-976617c-win64-static/bin/ffmpeg.exe").getPath().substring(1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }*/

    /**
     * 多个音频文件转为一个音频文件
     * <p>
     * ffmpeg -i 3.wav -i -3.wav -i 2.wav -i -2.wav -i 3.wav -i -1.wav -filter_complex "[0:0] [1:0] concat=n=6:v=0:a=1 [a]" -map [a] output.mp3
     *
     * @param source 源  3,h,2,t,3,
     * @return String Base64 wma
     * @throws TtsException 异常
     */
    public String moreToOne(List<String> source) throws TtsException {
        final Path path = Paths.get(System.getProperty("java.io.tmpdir") + UUID.randomUUID() + "_synthesis" + DOT + fFmpegBean.getFileType());

        try {
            CommandUtil.exec(commandStringSynthesis(source, path.toString()), fFmpegBean.getTimeOut(), TimeUnit.MILLISECONDS);
            if (!Files.exists(path)) {
                throw new TtsException("语音合成失败");
            }
            return Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(path.toFile()));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new TtsException(e.getMessage());
        } finally {
            if (Files.exists(path)) {
                try {
                    FileUtils.forceDeleteOnExit(path.toFile());
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * 将 音频 文件 code 转为 全路径 地址
     *
     * @param source code
     * @return path
     */
    public List<String> soundCode2Path(List<Integer> source) {
        return source.stream().map(ThrowingFunction.unchecked(e -> fFmpegBean.getPosition() + e + DOT + fFmpegBean.getFileType()))
                .collect(Collectors.toList());
    }

    /**
     * @param source      源  3,h,2,t,3,
     * @param outFileName 输出文件名称 路径/xx.type
     * @return 命令字符串
     */
    private List<String> commandStringSynthesis(List<String> source, String outFileName) {
        List<String> cmd = new ArrayList<>();
        if (!fFmpegBean.isConfigurationEnvironment()) {
            cmd.add(fFmpegBean.getFfmpegExePath());
        }
        cmd.add("ffmpeg");
        source.forEach(ThrowingConsumer.unchecked(
                e -> {
                    cmd.add(C_I);
                    cmd.add(e);
                }
        ));

        //  -filter_complex "[0:0] [1:0] concat=n=6:v=0:a=1 [a]" -map [a] output.mp3
        cmd.add("-filter_complex");
        cmd.add("\"[0:0] [1:0] concat=n=" + source.size() + ":v=0:a=1 [a]\"");
        cmd.add("-map");
        cmd.add("\"[a]\"");
        cmd.add(outFileName);
        return cmd;
    }

}
