package com.wisdom.tts.analysis;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 金钱 格式 转义
 * <p>
 * 音频代号规则
 * 0-9 对应 0-9
 * -1 对应 十
 * -2 对应 百
 * -3 对应 千
 * -4 万
 * -5  亿
 * -6 点
 * -7 元
 * ....
 *
 * @author Zemin.Yang
 * @date 2020/4/14 15:38
 */
@Slf4j
public class MoneyTransferred {

    /**
     * 解析 金额数字
     *
     * @param whole      金额的整数
     * @param decimalStr 金额的 小数 00,0,01
     * @return 解析 金额数字 对应的 音频 代号 代号规则
     */
    public List<Integer> transferredMeaning(int whole, String decimalStr) {
        final int decimal = Integer.parseInt(decimalStr);
        if (whole == 0 && decimal == 0) {
            //  返回，0元
            return Arrays.asList(0, -7);
        }
        final List<Integer> value = new ArrayList<>();
        transferredMeaningWhole(whole, value);
        transferredMeaningDecimal(decimal, decimalStr, value);
        value.add(-7);
        return value;
    }

    /**
     * 小数解析
     *
     * @param decimal    小数部分
     * @param decimalStr 金额的  处理01
     * @param data       集合
     */
    private void transferredMeaningDecimal(int decimal, String decimalStr, List<Integer> data) {
        if (decimal == 0) {
            return;
        }
        data.add(-6);
        //  处理  0N  情况
        if (decimalStr.length() == 2 && decimalStr.startsWith("0")) {
            data.add(0);
            data.add(decimal);
        } else {
            if (decimal < 10) {
                data.add(decimal);
            } else {
                data.add(decimal / 10 % 10);
                if (decimal % 10 != 0) {
                    data.add(decimal % 10);
                }
            }
        }
    }

    /**
     * 金额 整数转换
     * <p>
     * 10 不读 一十
     *
     * @param whole 金额整数
     * @param data  集合
     */
    private void transferredMeaningWhole(int whole, List<Integer> data) {
        if (whole == 0) {
            data.add(0);
        }
        //  万元以下,亿元以下
        if (whole < 10_0000) {
            //  [10,20),播放 十
            if (whole >= 10 && whole < 20) {
                data.add(-1);
                transferred(String.valueOf(whole % 10), data);
            } else {
                transferredHundred(String.valueOf(whole), data);
            }
        } else if (whole < 1_0000_0000) {
            if (whole < 20_0000) {
                data.add(-1);
                transferred(String.valueOf(whole / 1_0000 % 10), data);
            } else {
                transferred(String.valueOf(whole / 1_0000), data);
            }
            data.add(-4);
            transferredHundred(String.valueOf(whole % 1_0000), data);
        } else {
            transferred(String.valueOf(whole / 1_0000_0000), data);
            data.add(-5);
            transferred(String.valueOf(whole / 1_0000 % 1_0000), data);
            data.add(-4);
            transferredHundred(String.valueOf(whole % 1_0000), data);
        }

    }

    /**
     * @param thousand 最大 99999 五位字符最大,最小 1
     */
    private void transferred(String thousand, List<Integer> data) {
        final List<Integer> list = new ArrayList<>();
        //  由个位数 向更高为增加
        for (int len = thousand.length(), i = len - 1; i >= 0; i--) {
            final char c = thousand.charAt(i);
            //  是否是0,是0跳过
            if ('0' != c) {
                //  判断是否是个位,是个位跳过
                if (len != i + 1) {
                    list.add(i - (len - 1));
                }
                list.add(c - '0');
            }
        }
        Collections.reverse(list);
        data.addAll(list);
    }

    /**
     * 如 902 读 九百零二元
     *
     */
    private void transferredHundred(String thousand, List<Integer> data) {
        final List<Integer> list = new ArrayList<>();
        //  由个位数 向更高为增加
        for (int len = thousand.length(), i = len - 1; i >= 0; i--) {
            final char c = thousand.charAt(i);
            //  是否是0,是0跳过
            if ('0' != c) {
                //  判断是否是个位,是个位跳过
                if (len != i + 1) {
                    list.add(i - (len - 1));
                }
                list.add(c - '0');
            }
            //  十位等于0 切 个位不为0
            if (i == len - 2 && thousand.charAt(len - 1) != '0') {
                list.add(0);
            }
        }
        Collections.reverse(list);
        data.addAll(list);
    }

}