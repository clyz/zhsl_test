package com.wisdom.tts.analysis.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 声音素材原类别地址
 *
 * @author Zemin.Yang
 * @date 2020/4/14 13:40
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SoundBound implements Serializable {

    public SoundBound(List<SoundInfo> datas) {
        this.type = SoundBoundType.DEFAULT.getCode();
        this.datas = datas;
    }

    public SoundBound(List<SoundInfo> datas, String remarks) {
        this.type = SoundBoundType.DEFAULT.getCode();
        this.datas = datas;
        this.remarks = remarks;
    }

    public SoundBound(List<SoundInfo> datas, String type, String remarks) {
        this.type = type;
        this.datas = datas;
        this.remarks = remarks;
    }

    /**
     * 类型
     */
    private String type;
    /**
     * 声音
     */
    private List<SoundInfo> datas;

    private String author;
    /**
     * 版本   保留字段
     */
    private String version = "0.0.1";

    private String remarks;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 扩展 音频文件类型
     */
    private String dataFileType;

    /**
     * 位置  内置音频
     */
    private String position;

    /**
     * 扩展音频
     */
    private String dataPosition;

    /**
     * 声音类型
     * 默认(女音)，男音
     */
    @AllArgsConstructor
    @Getter
    public static enum SoundBoundType {

        DEFAULT("default");

        private String code;
    }
}