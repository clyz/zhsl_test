package com.wisdom.tts.analysis;

import com.wisdom.framework.kit.JacksonKit;
import com.wisdom.tts.analysis.vo.SoundBound;
import com.wisdom.tts.analysis.vo.SoundInfo;
import com.wisdom.tts.cmd.FFmpegBean;
import com.wisdom.tts.cmd.FFmpegCommand;
import com.wisdom.tts.exception.TtsException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ResourceUtils;
import pl.touk.throwing.ThrowingConsumer;
import pl.touk.throwing.ThrowingFunction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 声音素材原地址
 *
 * @author Zemin.Yang
 * @date 2020/4/14 13:39
 */
@Slf4j
public class SoundResources {

    /**
     * classpath： 路径
     */
    private String path;


    /**
     * 执行超时 设置
     */
    private long timeOut;

    /**
     * 是否启用 服务模式
     */
    private boolean isConfigurationEnvironment;

    /**
     * 文件执行地方
     */
    private String ffmpegExePath;

    /**
     * 默认 语音类型
     */
    private String defaultType;

    /**
     * 音频配置器
     */
    private Map<String, SoundBound> soundBounds = new ConcurrentHashMap<>();

    /**
     * 命令执行 器
     */
    private Map<String, FFmpegCommand> commands = new ConcurrentHashMap<>();

    /**
     * 转义
     */
    private MoneyTransferred transferred = new MoneyTransferred();

    public SoundResources(String type, String ffmpegExePath, boolean isConfigurationEnvironment, long timeOut) throws TtsException {
        try {
            path = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX).getPath().substring(1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        this.ffmpegExePath = ffmpegExePath;
        this.isConfigurationEnvironment = isConfigurationEnvironment;
        this.defaultType = type;
        this.timeOut = timeOut;
        initLoadResources();
    }

    public SoundResources() throws TtsException {
        this(SoundBound.SoundBoundType.DEFAULT.getCode(), null, true, 10_000L);
    }

    public SoundResources(String defaultType) throws TtsException {
        this(defaultType, null, true, 10_000L);
    }

    public SoundResources(String ffmpegExePath, long timeOut) throws TtsException {
        this(SoundBound.SoundBoundType.DEFAULT.getCode(), ffmpegExePath, false, timeOut);
    }

    /**
     * 解析
     *
     * @param decimal 金额 最大 Integer.MAX_VALUE.99
     * @return Base64
     */
    public List<String> executeTransferred(BigDecimal decimal, String type) throws TtsException {
        return getCommandByType(type).soundCode2Path(transferred(decimal));
    }

    private FFmpegCommand getCommandByType(String type) throws TtsException {
        final FFmpegCommand fmpegCommand = commands.get(type);
        if (fmpegCommand == null) {
            throw new TtsException("无 " + type + " 语音类型");
        }
        return fmpegCommand;
    }

    private List<Integer> transferred(BigDecimal decimal) throws TtsException {
        //  为null 返回 0元
        if (decimal == null) {
            return Arrays.asList(0, -7);
        }
        //  解决科学计数法 及 精度问题
        final String[] split = decimal.setScale(2, BigDecimal.ROUND_HALF_UP).toEngineeringString().split("\\.");
        final List<Integer> transferredMeaning = transferred.transferredMeaning(Integer.parseInt(split[0]), split[1]);
        log.info(" Sound Resources {}", transferredMeaning.toString());
        return transferredMeaning;
    }

    public String executeCommand(List<String> command, String type) throws TtsException {
        return getCommandByType(type).moreToOne(command);
    }

    /**
     * 初始化时加载默认资源
     *
     * @throws IOException
     */
    private void initLoadResources() throws TtsException {
        try {
            this.transferred = new MoneyTransferred();
            soundBounds.putAll(Collections.synchronizedMap(
                    JacksonKit.readListValue(
                            FileUtils.readFileToString(
                                    Paths.get(path + "sound_resoucres.json").toFile()),
                            SoundBound.class)
                            .stream().map(ThrowingFunction.unchecked(e -> {

                        //  处理 扩展数据资源
                        e.getDatas().forEach(ThrowingConsumer.unchecked(k -> {
                            if (StringUtils.isBlank(k.getName())) {
                                throw new TtsException("数据资源未找到 文件名称不能为空");
                            }
                            if (k.getName().endsWith(FFmpegCommand.DOT + e.getDataFileType())) {
                                k.setName( path+ e.getDataPosition() + File.separator + k.getName());
                            } else {
                                k.setName(path + e.getDataPosition() + File.separator + k.getName() + FFmpegCommand.DOT + e.getDataFileType());
                            }
                        }));

                        return e;
                    })).collect(Collectors.toMap(SoundBound::getType, e -> e)))
            );
            // 初始化  资源 转义bean
            soundBounds.keySet().forEach(ThrowingConsumer.unchecked(this::sound));

        } catch (Exception e) {
            log.error("Sound Resources init fail", e);
            throw new TtsException(e.getMessage());
        }
    }

    private void sound(String type) {
        final SoundBound soundBound = soundBounds.get(type);
        commands.put(type, new FFmpegCommand(fFmpegBean(soundBound)));
    }

    private FFmpegBean fFmpegBean(SoundBound soundBound) {
        final FFmpegBean fFmpegBean = new FFmpegBean();
        fFmpegBean.setFfmpegExePath(ffmpegExePath);
        fFmpegBean.setConfigurationEnvironment(isConfigurationEnvironment);
        fFmpegBean.setTimeOut(timeOut);

        //  内置 音频数据
        fFmpegBean.setFileType(soundBound.getFileType());
        fFmpegBean.setPosition(path + soundBound.getPosition() + File.separator + soundBound.getType() + File.separator);

        return fFmpegBean;
    }

    public SoundInfo getSoundInfoByCode(String type, String code) throws TtsException {
        final Optional<SoundInfo> soundInfo = soundBounds.get(type).getDatas().stream().filter(e -> code.equalsIgnoreCase(e.getCode())).findFirst();
        return soundInfo.orElseThrow(() -> new TtsException("无" + code + "音频模版"));
    }

    public String getDefaultType() {
        return defaultType;
    }

}
