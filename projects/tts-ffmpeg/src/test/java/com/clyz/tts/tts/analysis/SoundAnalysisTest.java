package com.clyz.tts.tts.analysis;

import com.clyz.tts.tts.exception.TtsException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SoundAnalysisTest {

    @Autowired
    private SoundAnalysis soundAnalysis;

    @Test
    public void executeDefault() throws TtsException, IOException {
        final String base64 = soundAnalysis.executeDefault(new BigDecimal(Integer.MAX_VALUE + 0.09));

        final File file = new File("1.wav");
        file.createNewFile();
        FileUtils.writeByteArrayToFile(file, Base64.getDecoder().decode(base64));
    }

    @Test
    public void execute() throws TtsException, IOException {
        final String base64 = soundAnalysis.execute(new BigDecimal(20)
                , "default", "zhifubao"
        );
        final Path path = Paths.get("default_1.wav");
        if(!Files.exists(path)){
            Files.createFile(path);
        }
        Files.write(path,Base64.getDecoder().decode(base64));
//        FileUtils.writeByteArrayToFile(path.toFile(), Base64.getDecoder().decode(base64));


       /* final String base641 = soundAnalysis.execute(new BigDecimal(13)
                , "default", "weixin"
        );
        final File file1 = new File("gg.wav");
        file.createNewFile();
        FileUtils.writeByteArrayToFile(file1, Base64.getDecoder().decode(base641));*/
    }
}