package com.clyz.tts.tts.cmd;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Base64;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FFmpegCommandTest {

    private FFmpegCommand fFmpegCommand;

    @Test
    public void moreToOne() throws Exception {
       /* final String moreToOne = fFmpegCommand.moreToOne(Arrays.asList(1,2));
        System.out.println(moreToOne);
        final File file = new File("a.wav");
        file.createNewFile();
        FileUtils.writeByteArrayToFile(file, Base64.getDecoder().decode(moreToOne));*/
//        generate(moreToOne, file.getPath());
    }

    public static boolean generate(String imgStr, String path) throws IOException {
        System.out.println(imgStr);
        byte[] b = new BASE64Decoder().decodeBuffer(imgStr);
        for (int i = 0; i < b.length; ++i) {
            if (b[i] < 0) {
                b[i] += 256;
            }
        }
        OutputStream out = new FileOutputStream(path);
        out.write(b);
        out.close();
        out.flush();
        return true;
    }

    @Test
    public void main() throws IOException {
        String p = "C:\\service_java\\ZHSL\\zhsl_test\\projects\\tts-ffmpeg\\src\\main\\resources\\wav\\emotionalFemaleVoice\\";
        final File ifile = new File(p + "-1.wav");
        final String encodeToString = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(ifile));
        System.out.println(encodeToString);
        final File ofile = new File("wer.wav");
        ofile.createNewFile();
        FileUtils.writeByteArrayToFile(ofile, Base64.getDecoder().decode(encodeToString));

    }
}