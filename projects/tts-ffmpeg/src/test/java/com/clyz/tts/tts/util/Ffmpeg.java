package com.clyz.tts.tts.util;

import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Ffmpeg {
    /**
     * 视频转码
     *
     * @param ffmpegPath   转码工具的存放路径
     * @param upFilePath   用于指定要转换格式的文件,要截图的视频源文件
     * @param codcFilePath 格式转换后的的文件保存路径
     * @param mediaPicPath 截图保存路径
     * @return
     * @throws Exception
     */
    public static boolean executeCodecs(String ffmpegPath, String upFilePath, String codcFilePath,
                                        String mediaPicPath) throws Exception {
        // 创建一个List集合来保存转换视频文件为flv格式的命令
        List<String> convert = new ArrayList<String>();
        convert.add(ffmpegPath); // 添加转换工具路径
        convert.add("-i"); // 添加参数＂-i＂，该参数指定要转换的文件
        convert.add(upFilePath); // 添加要转换格式的视频文件的路径
        convert.add("-qscale");     //指定转换的质量
        convert.add("6");
        convert.add("-ab");        //设置音频码率
        convert.add("64");
        convert.add("-ac");        //设置声道数
        convert.add("2");
        convert.add("-ar");        //设置声音的采样频率
        convert.add("22050");
        convert.add("-r");        //设置帧频
        convert.add("24");
        convert.add("-y"); // 添加参数＂-y＂，该参数指定将覆盖已存在的文件
        convert.add(codcFilePath);

        // 创建一个List集合来保存从视频中截取图片的命令
        List<String> cutpic = new ArrayList<String>();
        cutpic.add(ffmpegPath);
        cutpic.add("-i");
        cutpic.add(upFilePath); // 同上（指定的文件即可以是转换为flv格式之前的文件，也可以是转换的flv文件）
        cutpic.add("-y");
        cutpic.add("-f");
        cutpic.add("image2");
        cutpic.add("-ss"); // 添加参数＂-ss＂，该参数指定截取的起始时间
        cutpic.add("2"); // 添加起始时间为第2秒
        cutpic.add("-t"); // 添加参数＂-t＂，该参数指定持续时间
        cutpic.add("0.001"); // 添加持续时间为1毫秒
        cutpic.add("-s"); // 添加参数＂-s＂，该参数指定截取的图片大小
        cutpic.add("800*280"); // 添加截取的图片大小为350*240
        cutpic.add(mediaPicPath); // 添加截取的图片的保存路径

        boolean mark = true;
        ProcessBuilder builder = new ProcessBuilder();
        try {
            builder.command(convert);
            builder.redirectErrorStream(true);
            builder.start();

            builder.command(cutpic);
            builder.redirectErrorStream(true);
            // 如果此属性为 true，则任何由通过此对象的 start() 方法启动的后续子进程生成的错误输出都将与标准输出合并，
            //因此两者均可使用 Process.getInputStream() 方法读取。这使得关联错误消息和相应的输出变得更容易
            Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader inst = new InputStreamReader(is, "GBK");
            BufferedReader br = new BufferedReader(inst);//输入流缓冲区
            String res = null;
            StringBuilder sb = new StringBuilder();
            while ((res = br.readLine()) != null) {//循环读取缓冲区中的数据
                sb.append(res + "\n");
            }
            System.out.println(sb.toString());
        } catch (Exception e) {
            mark = false;
            System.out.println(e);
            e.printStackTrace();
        } finally {
//            关闭流。自己记得关
        }
        return mark;
    }

    public static void main(String orgs[]) throws Exception {
        executeCodecs("G:\\Program Files\\ffmpeg-20190702-231d0c8-win64-static\\bin\\ffmpeg.exe", "C:\\images\\201907\\12346.mp4", "C:\\images\\201907\\1111.flv", "C:\\images\\201907\\12346.jpg");
    }

    @Test
    public void ipConfig() throws Exception {
        ProcessBuilder builder = new ProcessBuilder();
        builder.command("ipconfig");
        final Process process = builder.start();

        //  让当前主线程等待 这个子线程 完成 0代表正常结束;
        // exitValue()可以查看process指向的子进程执行完的退出值，0代表是正常运行结束
        // destroy()和destroyForcibly()可以终止process子进程的运行，后者是强制终止，前者与平台终止进程的具体实现有关。
        if (process.waitFor() != 0) {
            throw new Exception("执行失败");
        }
        final InputStream inputStream = process.getInputStream();
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String temp = null;
        while ((temp = bufferedReader.readLine()) != null) {
            System.out.println(new String(temp.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
        }
    }

    @Test
    public void moreToOne() throws Exception {
        ProcessBuilder builder = new ProcessBuilder();
        final String path = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX + "ffmpeg-20190909-976617c-win64-static/bin/ffmpeg.exe").getPath().substring(1);
        System.out.println(path);
//        builder.command(path+" -i \"concat:123.mp3|124.mp3\" -acodec copy output.mp3");
        builder.command(path,"ffmpeg","-help");
        final Process process = builder.start();

        //  让当前主线程等待 这个子线程 完成 0代表正常结束;
        // exitValue()可以查看process指向的子进程执行完的退出值，0代表是正常运行结束
        // destroy()和destroyForcibly()可以终止process子进程的运行，后者是强制终止，前者与平台终止进程的具体实现有关。
        if (process.waitFor() != 0) {
            throw new Exception("执行失败");
        }
        final InputStream inputStream = process.getInputStream();
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String temp = null;
        while ((temp = bufferedReader.readLine()) != null) {
            System.out.println(new String(temp.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
        }
    }
}
