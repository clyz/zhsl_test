package com.clyz.tts.tts.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CommandUtilTest {

    private String FILE_P = "C:/service_java/ZHSL/zhsl_test/projects/tts-ffmpeg/sound/default/";

    @Test
    public void execOutStr() throws Exception {
        CommandUtil.execOutStr(Arrays.asList("ipconfig"));
    }

    @Test
    public void exec() throws Exception {
        final List<String> ffmpeg = Arrays.asList(
                "ffmpeg", "-i",
                FILE_P + "2.wav",
                "-i",
                FILE_P + "-3.wav",
                "-i",
                FILE_P + "1.wav",
                "-i",
                FILE_P + "-2.wav",
                "-i",
                FILE_P + "-1.wav",
                "-filter_complex",
                "\"[0:0] [1:0] concat=n=5:v=0:a=1 [a]\"",
                "-map", "[a]",
                "outfile.wav"
        );

        System.out.println(ffmpeg.toString().replace(","," "));

        CommandUtil.exec(ffmpeg);
    }

    @Test
    public void exec1() {
    }
}