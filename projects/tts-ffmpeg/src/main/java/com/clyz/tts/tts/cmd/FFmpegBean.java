package com.clyz.tts.tts.cmd;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Zemin.Yang
 * @date 2020/4/14 9:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FFmpegBean {

    /**
     * 没有配置环境时生效,配置环境
     */
    private String ffmpegExePath;

    /**
     * 文件类型
     * 1->wav
     */
    private String fileType;

    /**
     * 配置环境
     */
    private boolean isConfigurationEnvironment;

    private long timeOut;

    private String position;
}
