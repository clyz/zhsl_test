package com.clyz.tts.tts.exception;

/**
 * @author Zemin.Yang
 * @date 2020/4/14 13:04
 */
public class TtsException extends Exception {
    public TtsException(String message) {
        super(message);
    }
}
