package com.clyz.tts.tts.analysis;

import com.clyz.tts.tts.exception.TtsException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>
 * 音频解析
 * </h1>
 *
 * @author Zemin.Yang
 * @date 2020/4/14 13:37
 */
@Slf4j
public class SoundAnalysis {

    private SoundResources soundResources;

    public SoundAnalysis(SoundResources soundResources) {
        this.soundResources = soundResources;
    }

    public String executeDefault(BigDecimal decimal) throws TtsException {
        return execute(decimal, soundResources.getDefaultType(), null);
    }

    public String execute(BigDecimal decimal, String type, String code) throws TtsException {
        final List<String> execute = soundResources.executeTransferred(decimal, type);
        if (StringUtils.isNotBlank(code)) {
            final List<String> command = new ArrayList<>();
            command.add(
                    soundResources.getSoundInfoByCode(type, code).getName()
            );
            command.addAll(execute);
            return soundResources.executeCommand(command, type);
        } else {
            return soundResources.executeCommand(execute, type);
        }
    }

}