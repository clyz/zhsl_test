package com.clyz.tts.tts.util;

import com.clyz.tts.tts.exception.TtsException;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Zemin.Yang
 * @date 2020/4/13 16:53
 */
@Slf4j
public class CommandUtil {

    public static void execOutStr(List<String> command) throws Exception {
        final InputStream inputStream = exec(command).getInputStream();
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String temp = null;
        while ((temp = bufferedReader.readLine()) != null) {
            System.out.println(new String(temp.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
        }
    }

    static Process exec(List<String> command) throws Exception {
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(command);
        final Process process = builder.start();
        //  让当前主线程等待 这个子线程 完成 0代表正常结束;
        // exitValue()可以查看process指向的子进程执行完的退出值，0代表是正常运行结束
        // destroy()和destroyForcibly()可以终止process子进程的运行，后者是强制终止，前者与平台终止进程的具体实现有关。
        if (process.waitFor() != 0) {
            throw new Exception("执行失败");
        }
        return process;
    }

    /**
     * 每个线程 仅仅执行一个
     *
     * @param command
     * @param timeOut
     * @param unit
     * @throws TtsException
     */
    public static void exec(List<String> command, long timeOut, TimeUnit unit) throws TtsException {
        ProcessBuilder builder = null;
        Process process = null;
        Runtime runtime = Runtime.getRuntime();
        try {
            log.info("FFmepg Command {} ", command.toString().replace(",", " "));

            builder = new ProcessBuilder();
            builder.command(command);
            process = builder.start();

            Process finalProcess = process;
            new Thread(() -> {
                log.info(" Run Input Start ------ ");
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(finalProcess.getInputStream()))) {
                    String line;
                    final StringBuilder stringBuilder = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    log.info(stringBuilder.toString());
                } catch (final Exception e) {
                    log.error(" Run Input ", e);
                } finally {
                    log.info(" Run Input End ------ ");
                }
            }).start();

            new Thread(() -> {
                log.info(" Run Error Start ------ ");
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(finalProcess.getErrorStream()))) {
                    String line;
                    final StringBuilder stringBuilder = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    log.error(stringBuilder.toString());
                } catch (final Exception e) {
                    log.error(" Run Error ", e);
                }
                log.info(" Run Error Start ------ ");
            }).start();

            //  让当前主线程等待 这个子线程 完成 0代表正常结束;
            // exitValue()可以查看process指向的子进程执行完的退出值，0代表是正常运行结束
            // destroy()和destroyForcibly()可以终止process子进程的运行，后者是强制终止，前者与平台终止进程的具体实现有关。
            if (!process.waitFor(timeOut, unit)) {
                throw new TtsException("执行超时或失败");
            }
            /*if (process.waitFor() != 0) {
                throw new Exception("执行失败");
            }*/
            process.waitFor();
            close(process);
        } catch (Exception e) {
            log.error("FFmepg exception： " + e.getMessage(), e);
            throw new TtsException(e.getMessage());
        } finally {
            if (null != process) {
                // JVM退出时，先通过钩子关闭FFmepg进程
//                process.destroy();
                runtime.addShutdownHook(new ProcessKiller(process));
            }
        }
    }

    private static void close(Process process) throws IOException {
        try (final InputStream inputStream = process.getInputStream();
             final InputStream errorStream = process.getErrorStream()) {

        }
    }

    /**
     * 在程序退出前结束已有的FFmpeg进程
     */
    private static class ProcessKiller extends Thread {
        private Process process;

        public ProcessKiller(Process process) {
            this.process = process;
        }

        @Override
        public void run() {
            this.process.destroy();
            log.info("--- 已销毁FFmpeg进程 --- 进程名： " + process.toString());
        }
    }

}
