package com.clyz.tts.tts;

import com.clyz.tts.tts.analysis.SoundAnalysis;
import com.clyz.tts.tts.analysis.SoundResources;
import com.clyz.tts.tts.exception.TtsException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Zemin.Yang
 * @date 2020/4/14 9:09
 */
@Configuration
public class TtsConfig {

    @Bean
    public SoundAnalysis soundAnalysis() throws TtsException {
        return new SoundAnalysis(soundResources());
    }

    @Bean
    public SoundResources soundResources() throws TtsException {
        return new SoundResources();
    }

}
