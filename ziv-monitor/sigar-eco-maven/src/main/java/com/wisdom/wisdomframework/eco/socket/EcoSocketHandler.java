package com.wisdom.wisdomframework.eco.socket;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public class EcoSocketHandler extends TextWebSocketHandler {
    private static final ConcurrentMap<String, WebSocketSession> USERS;
    private static final String CLIENT_ID = "ECO";
    static {
        USERS = new ConcurrentHashMap<String, WebSocketSession>();
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("成功建立连接");
        String username= getClientId(session);
        System.out.println("5555"+username);
        if (username != null) {
            USERS.put(username, session);
            session.sendMessage(new TextMessage("成功建立socket连接"));
            System.out.println(username);
            System.out.println(session);
        }
    }

    /**
     * 广播信息
     * @param message
     * @return
     */
    public boolean sendMessageToAllUsers(TextMessage message) {
        boolean allSendSuccess = true;
        Set<String> clientIds = USERS.keySet();
        WebSocketSession session = null;
        for (String clientId : clientIds) {
            try {
                session = USERS.get(clientId);
                if (session.isOpen()) {
                    session.sendMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
                allSendSuccess = false;
            }
        }

        return  allSendSuccess;
    }

    /**
     * 获取用户标识
     * @param session
     * @return
     */
    private String getClientId(WebSocketSession session) {
        try {
            return (String) session.getAttributes().get(CLIENT_ID);
        } catch (Exception e) {
            return null;
        }
    }

}