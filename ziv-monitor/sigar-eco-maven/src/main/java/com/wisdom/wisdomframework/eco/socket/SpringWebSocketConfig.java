package com.wisdom.wisdomframework.eco.socket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;


/**
 * @author ziv
 */
@Configuration
@EnableWebMvc
@EnableWebSocket
public class SpringWebSocketConfig implements WebSocketConfigurer, WebMvcConfigurer {

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(webSocketHandler(),"/ws/eco.ms")
                .addInterceptors(new SpringWebSocketHandlerInterceptor())
                .setAllowedOrigins("*");

        registry.addHandler(webSocketHandler(), "/sockjs/eco.ms")
                .addInterceptors(new SpringWebSocketHandlerInterceptor()).withSockJS();
    }

    @Bean
    public TextWebSocketHandler webSocketHandler(){
        return new EcoSocketHandler();
    }

}