package com.wisdom.wisdomframework.eco.factory;

import org.hyperic.sigar.*;

/**
 * @author: ziv
 * @date: 2018/12/13 14:48
 * @version: 0.0.1
 */
public class Eco {

    private static Sigar sigar = new Sigar();

    public static Cpu getCpu() {
        try {
            return sigar.getCpu();
        } catch (SigarException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static CpuInfo[] getCpuInfo() {
        try {
            return sigar.getCpuInfoList();
        } catch (SigarException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Mem getMem(){
        try {
            return sigar.getMem();
        } catch (SigarException e) {
            e.printStackTrace();
        }
        return null;
    }

}
