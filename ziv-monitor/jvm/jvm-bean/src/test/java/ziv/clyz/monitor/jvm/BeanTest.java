package ziv.clyz.monitor.jvm;

import org.junit.Test;
import ziv.clyz.monitor.jvm.cmd.InputStreamRunnable;
import ziv.clyz.monitor.jvm.entity.JinfoEntity;
import ziv.clyz.monitor.jvm.entity.JpsEntity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BeanTest {

    @Test
    public void jps() {
        Map<String, JpsEntity> map = new HashMap<>();
        String s = execute(new String[]{"jps", "-l", "-v"});
        String[] line = s != null ? s.split("\n") : new String[0];
        for (String aLine : line) {
            String[] one = aLine.split("\\s+");
            //排除sun.tools进程
            if (one[1].contains("sun.tools")){
                continue;
            }
            //格式化控制台输出
            if (!one[1].substring(0, 1).equals("-")) {
                String smallName = one[1].contains(".") ? one[1].substring(one[1].lastIndexOf(".")+1) : one[1];
                smallName = smallName.equalsIgnoreCase("jar")? one[1] : smallName;
                map.put(one[0], new JpsEntity(one[1], smallName, Arrays.stream(one).skip(2).collect(Collectors.toList())));
            } else {
                map.put(one[0], new JpsEntity("NULL","NULL", Arrays.stream(one).skip(1).collect(Collectors.toList())));
            }
            //测试jinfo
/*            JinfoEntity info = Jinfo.info(one[0]);
            System.out.println();
            System.out.println(info);*/
        }
    }


    @Test
    public void exe(){
        String s = execute(new String[]{"jps", "-l", "-v"});
    }

    public String execute(String[] cmd,String... encoding) {
        BufferedReader bufferedReader;
        InputStreamReader inputStreamReader;
        try {
            Process p = Runtime.getRuntime().exec(cmd);

            /* 为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞 */
            Thread t = new Thread(new InputStreamRunnable(p.getErrorStream(), "ErrorStream"));
            t.start();

            /* "标准输出流"就在当前方法中读取 */
            BufferedInputStream bis = new BufferedInputStream(p.getInputStream());

            if (encoding != null && encoding.length != 0) {
                inputStreamReader = new InputStreamReader(bis, encoding[0]);// 设置编码方式
            } else {
                inputStreamReader = new InputStreamReader(bis, "utf-8");
            }
            bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }

            bufferedReader.close();
            p.destroy();
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
