package ziv.clyz.monitor.jvm.runtime;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *     测试
 * </p>
 */
public class RuntimeExecTest {
    public static void main(String[] args) throws IOException {
        new RuntimeExecTest().exec();
    }

    @Test
    public void exec() throws IOException {
        List<String> stringList = new ArrayList<>();
        String[] cmd = {"jps", "-l", "-v"};
        final Process process = Runtime.getRuntime().exec(cmd);

        /* "标准输出流"就在当前方法中读取 */
        BufferedInputStream bis = new BufferedInputStream(process.getInputStream());
        final InputStreamReader inputStreamReader = new InputStreamReader(bis, "utf-8");
        final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            stringList.add(line);
        }
        bufferedReader.close();
        process.destroy();
        stringList.forEach(System.out::println);
    }
}
