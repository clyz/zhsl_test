package com.wisdom.cmdbatexecsb;

import java.io.File;

/**
 * @author: ziv
 * @date: 2018/12/12 16:50
 * @version: 0.0.1
 */
public class Bat {
    public static void main(String[] args) {
        System.out.println(new File("bat").getAbsolutePath());
    }
}
