package com.wisdom.cmdbatexecsb.controller;

import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * @author: ziv
 * @date: 2018/12/12 16:31
 * @version: 0.0.1
 */
@RestController
@RequestMapping("/exec/bat/")
public class BaseController {
    private static final String BAT = ".bat";
    private static ConcurrentHashMap<String, File> FILE;

    static {
        FILE = new ConcurrentHashMap();
        Stream.of(new File("bat").listFiles())
                .forEach(file -> FILE.put(file.getName(), file));
    }

    private static void add(File file) {
        FILE.put(file.getName(), file);
    }

    @PostMapping("createbat")
    public Callable createBat(String name, String con) {
        return () -> {
            if (StringUtils.isEmpty(name) || StringUtils.isEmpty(con)) {
                return false;
            }
            File file = getFile(name);
            FileUtils.writeStringToFile(file, con, "UTF-8");
            return true;
        };
    }

    private static File getFile(String name) throws IOException {
        File file = new File("bat/" + name + BAT);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    @PostMapping("createexe")
    public Callable createExe(String path) {
        return () -> {
            if (StringUtils.isEmpty(path)) {
                return false;
            }
            File file = isFile(path);
            FILE.put(file.getName(), file);
            return file;
        };
    }

    private static File isFile(String path) throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            throw new IOException();
        }
        return file;
    }

    @GetMapping("list")
    public Callable list() {
        return () -> FILE;
    }

    @PostMapping("run")
    public void run(String name, @RequestParam(defaultValue = BAT) String type) throws IOException {
        if (StringUtils.isEmpty(name)) {
            return;
        }
        if (!name.contains(".")) {
            name = FILE.get(name + type).getAbsolutePath();
        }
        Runtime.getRuntime().exec(name);
    }
}