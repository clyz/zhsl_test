package com.wisdom.cmdbatexecsb;

import com.github.xiaour.api_scanner.annotation.Sapi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Sapi(controllers = "com.wisdom.cmdbatexecsb.controller")
public class CmdBatExecSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmdBatExecSbApplication.class, args);
    }
}
