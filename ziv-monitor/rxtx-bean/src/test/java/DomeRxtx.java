import com.wisdom.kit.SerialIO;
import gnu.io.*;

import java.io.*;
import java.util.Enumeration;
import java.util.TooManyListenersException;

public class DomeRxtx {

    public static SerialPort serialPort;

    public static void main(String[] args) throws PortInUseException, NoSuchPortException, TooManyListenersException, UnsupportedCommOperationException, IOException {
       a();
    }

    public static  void  a() throws IOException {
/*        SerialIO serialIO = new SerialIO();
        boolean open = serialIO.open(2, 9600);
        System.out.println("write is open " + open);
        byte[] writeByte = new byte[]{'1', '2', '3'};
        serialIO.write(writeByte);
        System.out.println("write: " + new String(writeByte));*/
//        serialIO.close();


        SerialIO serialIORead = new SerialIO();
        boolean open1 = serialIORead.open(2, 9600);
        System.out.println("read is open " + open1);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while ((n = serialIORead.read(buffer)) > 0) {
            System.out.println(n);
            out.write(buffer, 0, n);
        }
//        serialIORead.close();
        System.out.println("read: " + new String(out.toByteArray()));
        out.close();
    }

    public static void test() {
        Enumeration<CommPortIdentifier> em = CommPortIdentifier.getPortIdentifiers();
        while (em.hasMoreElements()) {
            String name = em.nextElement().getName();
            System.out.println(":" + name);
        }
    }

    public static void open() throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException, TooManyListenersException {
        //打开串口
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier("COM1");//COM4是串口名字
        CommPort commPort = portIdentifier.open("COM1", 2000);    //2000是打开超时时间
        serialPort = (SerialPort) commPort;
        //设置参数（包括波特率，输入/输出流控制，数据位数，停止位和齐偶校验）
        serialPort.setSerialPortParams(9600,
                SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
        //监听串口事件
        serialPort.addEventListener(new Abc()); //Abc是实现SerialPortEventListener接口的类，具体读操作在里面进行
        // 设置当有数据到达时唤醒监听接收线程
        serialPort.notifyOnDataAvailable(true);
        // 设置当通信中断时唤醒中断线程
        serialPort.notifyOnBreakInterrupt(true);
        System.out.println("READ:"+new String(readFromPort(serialPort)));
        //    in.close(); //关闭串口
    }

    public static  byte[] read(InputStream in) throws IOException {
        byte[] bytes = {};
        // 缓冲区大小为一个字节
        byte[] readBuffer = new byte[1];
        int bytesNum = in.read(readBuffer);
        while (bytesNum > 0) {
            bytes = DomeRxtx.concat(bytes, readBuffer);
            bytesNum = in.read(readBuffer);
        }
        return bytes;
    }

    public static byte[] readFromPort(SerialPort serialPort) {
        InputStream in = null;
        byte[] bytes = {};
        try {
            in = serialPort.getInputStream();
            // 缓冲区大小为一个字节
            byte[] readBuffer = new byte[1];
            int bytesNum = in.read(readBuffer);
            while (bytesNum > 0) {
                bytes = DomeRxtx.concat(bytes, readBuffer);
                bytesNum = in.read(readBuffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bytes;
    }

    public static void out(byte[] data) throws IOException {
        OutputStream out = serialPort.getOutputStream();
        out.write(data); //byte[] data;
        out.flush();
    }
    private static byte[] concat(byte[] firstArray, byte[] secondArray) {
        if (firstArray == null || secondArray == null) {
            if (firstArray != null)
                return firstArray;
            if (secondArray != null)
                return secondArray;
            return null;
        }
        byte[] bytes = new byte[firstArray.length + secondArray.length];
        System.arraycopy(firstArray, 0, bytes, 0, firstArray.length);
        System.arraycopy(secondArray, 0, bytes, firstArray.length, secondArray.length);
        return bytes;
    }
}

class Abc implements SerialPortEventListener {
    public void serialEvent(SerialPortEvent arg0) {
        //对以下内容进行判断并操作
        /*
        BI -通讯中断
    　　CD -载波检测
    　　CTS -清除发送
    　　DATA_AVAILABLE -有数据到达
    　　DSR -数据设备准备好
    　　FE -帧错误
    　　OE -溢位错误
    　　OUTPUT_BUFFER_EMPTY -输出缓冲区已清空
    　　PE -奇偶校验错
        RI -　振铃指示
        */
        //switch多个，if单个
        if (arg0.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                InputStream in = null;
                byte[] bytes = null;
                in = DomeRxtx.serialPort.getInputStream();

                int bufflenth = in.available();
                while (bufflenth != 0) {
                    // 初始化byte数组为buffer中数据的长度
                    bytes = new byte[bufflenth];
                    in.read(bytes);
                    System.out.println(new String(bytes));
                    bufflenth = in.available();
                }
                OutputStream out = DomeRxtx.serialPort.getOutputStream();
                out.write(bytes != null ? bytes : new byte[0]); //byte[] data;
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 合并数组
     *
     * @param firstArray  第一个数组
     * @param secondArray 第二个数组
     * @return 合并后的数组
     */
}
