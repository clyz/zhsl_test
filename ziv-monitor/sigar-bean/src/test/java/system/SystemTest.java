package system;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

/**
 * @author: ziv
 * @date: 2018/12/6 15:52
 * @version: 0.0.1
 * @see System#getenv()    返回一个不能修改的当前系统环境的字符串映射视图。
 * @see InetAddress#getLocalHost() 本地主机信息
 * @see System#getProperties() 确定当前的系统属性。
 */
public class SystemTest {
    @Test
    public void getProperties(){
        Properties properties = System.getProperties();
        Enumeration<?> enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()){
            String key = (String) enumeration.nextElement();
            System.out.println(key+" :: "+ properties.getProperty(key));
        }
    }
    @Test
    public void getenv() {
        Map<String, String> getenv = System.getenv();
        getenv.keySet().forEach(key->{
            System.out.println(key +" :: "+getenv.get(key));
        });
    }

    @Test
    public void localHost() throws UnknownHostException {
        InetAddress localHost = InetAddress.getLocalHost();
        System.out.println(localHost.getHostAddress());
        System.out.println(localHost.getHostName());
    }
}
