package system;

import org.hyperic.sigar.*;
import org.junit.Test;

/**
 * @author: ziv
 * @date: 2018/12/6 16:21
 * @version: 0.0.1
 *
 */
public class CpuTest {
    Sigar sigar = new Sigar();


    @Test
    public void cpu() throws SigarException {
        CpuInfo[] cpuInfoList = sigar.getCpuInfoList();
        CpuPerc[] cpuPercList = sigar.getCpuPercList();
        Cpu cpu = sigar.getCpu();
    }

    /**
     * @see Sigar#getCpuInfoList()
     *
     * @throws SigarException 异常
     */
    @Test
    public void getCpuInfoList() throws SigarException {
        CpuInfo[] cpuInfoList = sigar.getCpuInfoList();
        for (CpuInfo cpuInfo: cpuInfoList) {
            System.out.println(cpuInfo);
        }
    }
}
