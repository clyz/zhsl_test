package ziv.clyz.jwt.zivjwtsb.service.Imp;

import org.springframework.stereotype.Service;
import ziv.clyz.jwt.zivjwtsb.bean.User;
import ziv.clyz.jwt.zivjwtsb.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static List<User> DB;

    static {
        DB = new ArrayList<>();
        DB.add(new User("admin", "123", 1L));
        DB.add(new User("ziv", "123", 2L));
    }

    @Override
    public User findByUsername(User user) throws Exception {
        return Optional.of(DB.stream().filter(e -> user.getUsername().equals(e.getUsername())).collect(Collectors.toList()))
                .orElseThrow(() -> new Exception("不为空"))
                .get(0);
    }

    @Override
    public User findUserById(long userId) throws Exception {
        return Optional.of(DB.stream().filter(e -> userId == e.getId()).collect(Collectors.toList()))
                .orElseThrow(() -> new Exception("不为空"))
                .get(0);
    }
}
