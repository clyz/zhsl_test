package ziv.clyz.jwt.zivjwtsb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZivJwtSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZivJwtSbApplication.class, args);
    }

}
