package ziv.clyz.jwt.zivjwtsb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ziv.clyz.jwt.zivjwtsb.annontation.CheckToken;
import ziv.clyz.jwt.zivjwtsb.annontation.LoginToken;
import ziv.clyz.jwt.zivjwtsb.bean.User;
import ziv.clyz.jwt.zivjwtsb.service.UserService;
import ziv.clyz.jwt.zivjwtsb.util.JwtUtil;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    //登录
    @PostMapping("/login")
    @LoginToken
    public Map<String, Object> login(@Valid User user) throws Exception {
        Map<String, Object> hashMap = new HashMap<>();
        User userForBase = userService.findByUsername(user);
        if (userForBase == null) {
            hashMap.put("message", "登录失败,用户不存在");
        } else {
            if (!userForBase.getPassword().equals(user.getPassword())) {
                hashMap.put("message", "登录失败,密码错误");
            } else {
                String token = JwtUtil.createJWT(6000000, userForBase);
                hashMap.put("token", token);
                hashMap.put("user", userForBase);
                hashMap.put("message", "OK");
            }
        }
        return hashMap;
    }

    //查看个人信息
    @CheckToken
    @GetMapping("/getMessage")
    public String getMessage() {
        return "你已通过验证";
    }


}