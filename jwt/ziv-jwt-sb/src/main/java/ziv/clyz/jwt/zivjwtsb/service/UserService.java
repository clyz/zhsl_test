package ziv.clyz.jwt.zivjwtsb.service;

import ziv.clyz.jwt.zivjwtsb.bean.User;

import javax.validation.constraints.NotNull;

public interface UserService {

    User findByUsername(@NotNull User user) throws Exception;

    User findUserById(@NotNull long userId) throws Exception;
}
