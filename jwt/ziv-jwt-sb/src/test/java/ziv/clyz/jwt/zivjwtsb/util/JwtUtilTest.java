package ziv.clyz.jwt.zivjwtsb.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class JwtUtilTest {

    @Test
    public void contextLoads() {
        String subject = "admin";
        final HashMap<String, Object> claims = new HashMap<>();
        claims.put("name", subject);
        //下面就是在为payload添加各种标准声明和私有声明了
        //这里其实就是new一个JwtBuilder，设置jwt的body
        JwtBuilder builder = Jwts.builder()
                //如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的
                .setClaims(claims)
                //设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。
                .setId(UUID.randomUUID().toString())
                //iat: jwt的签发时间
                .setIssuedAt(new Date())
                //代表这个JWT的主体，即它的所有人，这个是一个json格式的字符串，可以存放什么userid，roldid之类的，作为什么用户的唯一标志。
                // 生成签发人
                .setSubject(subject)
                //设置签名使用的签名算法和签名使用的秘钥
                .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encode(subject.getBytes()));
        //设置过期时间
        builder.setExpiration(new Date(new Date().getTime() + 100000000L));
        final String token = builder.compact();

        System.out.println(token);


        //得到DefaultJwtParser
        final Claims body = Jwts.parser()
                //设置签名的秘钥
                .setSigningKey(Base64.getEncoder().encode(subject.getBytes()))
                //设置需要解析的jwt
                .parseClaimsJws(token)
                .getBody();

        System.out.println(body.get("name"));
    }

}