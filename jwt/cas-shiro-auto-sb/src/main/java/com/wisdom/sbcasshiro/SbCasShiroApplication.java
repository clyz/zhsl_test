package com.wisdom.sbcasshiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbCasShiroApplication {
    public static void main(String[] args) {
        SpringApplication.run(SbCasShiroApplication.class, args);
    }
}
