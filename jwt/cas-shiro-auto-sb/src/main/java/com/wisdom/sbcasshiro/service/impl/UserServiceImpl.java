package com.wisdom.sbcasshiro.service.impl;

import com.wisdom.sbcasshiro.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

/**
 * Created by wwu on 2017/4/13.
 */
@Service("userService")
@Slf4j
public class UserServiceImpl implements UserService {
    @Override
    public String getUsername() {
        Subject subject = SecurityUtils.getSubject();
        if (subject == null || subject.getPrincipals() == null) {
            log.info("登陆者用户名不存在");
            return "xo";
        }
        return (String) subject.getPrincipals().getPrimaryPrincipal();
    }
}
