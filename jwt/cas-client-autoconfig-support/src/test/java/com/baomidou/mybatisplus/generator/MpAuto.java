package com.baomidou.mybatisplus.generator;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

/**
 * mp 代码生成器
 *
 * @author 23746
 */
public class MpAuto {
    @Test
    public void start() {
        //	1.全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setActiveRecord(true)    //是否支持AR
                .setAuthor("杨泽民")    //	作者;
                .setOutputDir("E:\\java_web\\zhsl_obj\\zhsl_test\\cas-client-autoconfig-support\\src\\main\\java")    //	生成路径,工程路径
                .setFileOverride(true)    //	文件覆盖
                .setIdType(IdType.AUTO)    //	主键策略
                .setServiceName("I%sService")    //	生成service接口的名称的首字母是否为I
                .setBaseResultMap(true)    //	生成基本的sql文件
                .setBaseColumnList(true);    //生成sql片段

        //	2.数据源
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.jdbc.Driver")
                .setUrl("jdbc:mysql://127.0.0.1:3306/cas?useUnicode=true&characterEncoding=UTF-8&tinyInt1isBit=false")
                .setUsername("root")
                .setPassword("zemin886520");

        //	3.策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setCapitalMode(true)    //	开启全局大写命名
                .setDbColumnUnderline(true)    //	数据库表名列明下滑线
                .setNaming(NamingStrategy.underline_to_camel)    //	数据库表命名到实体的命名策咯
                .setTablePrefix("sec_");    //	数据库表名前最
//			.setInclude("tb_employee");	//生成的表;

        //	4.包名
        PackageConfig packageConfig = new PackageConfig();
        packageConfig
                .setParent("yy")
                .setMapper("mapper")    //	实体类位置
                .setService("service")
                .setController("controller")
                .setEntity("pojo")
                .setXml("mapper");

        //	5.整合
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(globalConfig)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(packageConfig);
        autoGenerator.execute();

        /*ConfigBuilder configBuilder = new ConfigBuilder(packageConfig, dataSourceConfig, strategyConfig, null, globalConfig);
        configBuilder = autoGenerator.pretreatmentConfigBuilder(configBuilder);

        System.err.println("over");*/
    }
}