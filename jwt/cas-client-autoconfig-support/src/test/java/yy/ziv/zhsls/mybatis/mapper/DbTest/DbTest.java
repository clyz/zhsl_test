package yy.ziv.zhsls.mybatis.mapper.DbTest;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import yy.ziv.zhsls.ZhslsApplicationTests;
import yy.ziv.zhsls.system.mapper.UserMapper;
import yy.ziv.zhsls.system.pojo.User;
import yy.ziv.zhsls.system.service.IUserService;

@Slf4j
public class DbTest extends ZhslsApplicationTests {
    @Autowired
    UserMapper userMapper;
    @Autowired
    IUserService userService;

    @Test
    public void select(){
        EntityWrapper entity = new EntityWrapper();
        entity.eq("account","admin");
        log.info("{}",userService.selectOne(entity));
    }
    @Test
    public void getId(){
        log.info("Worker:{}",IdWorker.getId());
    }
    @Test
    public void get(){
        userMapper.selectList(null).forEach(System.out::println);
    }
    @Test
    public void save(){
        Integer ziv = userMapper.insert(new User("123", "admin"));
        log.info("is ok {}",ziv>0);
    }
}
