package yy.ziv.zhsls.system.service.impl;

import yy.ziv.zhsls.system.pojo.Role;
import yy.ziv.zhsls.system.mapper.RoleMapper;
import yy.ziv.zhsls.system.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
