package yy.ziv.zhsls.system.service;

import yy.ziv.zhsls.system.pojo.Role;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-19
 */
public interface IRoleService extends IService<Role> {

}
