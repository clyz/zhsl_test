package yy.ziv.zhsls.system.mapper;

import yy.ziv.zhsls.system.pojo.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-19
 */
public interface RoleMapper extends BaseMapper<Role> {

}
