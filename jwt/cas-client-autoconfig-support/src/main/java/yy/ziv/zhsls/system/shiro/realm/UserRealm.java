package yy.ziv.zhsls.system.shiro.realm;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import yy.ziv.zhsls.system.pojo.User;
import yy.ziv.zhsls.system.service.IUserService;

@Slf4j
public class UserRealm extends AuthorizingRealm {
    @Autowired
    IUserService userService;
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String nickname = (String) token.getPrincipal();
        User user = userService.selectByName(nickname);
        log.info("NAME{},USER:{}",nickname,user);
        return new SimpleAuthenticationInfo(user, user.getNumber(), getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        User user = (User) principals.getPrimaryPrincipal();
        log.info("USER:{}",user);
        return new SimpleAuthorizationInfo();
    }
}
