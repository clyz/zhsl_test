package yy.ziv.zhsls.common.basecontroller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import yy.ziv.zhsls.system.pojo.User;
import yy.ziv.zhsls.system.shiro.config.ShiroConfig;

import javax.servlet.http.HttpSession;

@Controller
@Slf4j
@RequestMapping("/")
public class BaseController {

    @GetMapping(value = "login")
    public String loginForm() {
        return "redirect:" + ShiroConfig.loginUrl;
    }

    @GetMapping(value = "logout")
    public String loginout() {
        return "redirect:" + ShiroConfig.logoutUrl;
    }
}
