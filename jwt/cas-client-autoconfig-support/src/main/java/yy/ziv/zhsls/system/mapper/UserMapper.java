package yy.ziv.zhsls.system.mapper;

import yy.ziv.zhsls.system.pojo.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-19
 */
public interface UserMapper extends BaseMapper<User> {

}
