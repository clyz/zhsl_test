package yy.ziv.zhsls.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import yy.ziv.zhsls.system.pojo.User;
import yy.ziv.zhsls.system.mapper.UserMapper;
import yy.ziv.zhsls.system.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 杨泽民
 * @since 2018-11-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public User selectByName(String nickname) {
        EntityWrapper<User> objectEntityWrapper = new EntityWrapper<>();
        objectEntityWrapper.eq("name", nickname);
        return selectOne(objectEntityWrapper);
    }
}
