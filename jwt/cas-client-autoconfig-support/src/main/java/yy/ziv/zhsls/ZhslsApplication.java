package yy.ziv.zhsls;

import com.github.xiaour.api_scanner.annotation.Sapi;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableCasClient
@MapperScan("yy.ziv.zhsls.system.mapper")
@Sapi(controllers = {"yy.ziv.zhsls.common.basecontroller","yy.ziv.zhsls.system.controller"})
public class ZhslsApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZhslsApplication.class, args);
    }
}