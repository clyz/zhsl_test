package ziv.clyz.security.jwt.securityjwtsb.util.beanmap;

import org.junit.Test;
import ziv.clyz.security.jwt.securityjwtsb.bean.Role;
import ziv.clyz.security.jwt.securityjwtsb.bean.User;

import java.util.Arrays;
import java.util.Map;

public class BeanMapUtilTest {

    @Test
    public void toMap() throws Exception {
        final User user = new User();
        user.setId(1L);
        user.setPassword("123");
        user.setUsername("admin");
        user.setRoles(Arrays.asList(
                new Role(1L, "ADMIN"),
                new Role(2L, "USER")
        ));
        final Map<String, Object> stringObjectMap = BeanMapUtil.toMap(user);
        System.out.println(stringObjectMap);
    }
}