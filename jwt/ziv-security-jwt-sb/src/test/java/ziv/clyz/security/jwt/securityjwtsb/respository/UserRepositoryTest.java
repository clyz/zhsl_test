package ziv.clyz.security.jwt.securityjwtsb.respository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ziv.clyz.security.jwt.securityjwtsb.bean.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Test
    public void contextLoads() {
/*
        final User user = new User();
        user.setUsername("admin");
        user.setPassword("123");
        final User save = userRepository.save(user);
        System.out.println(save);
*/

        final boolean admin = userRepository.existsUserByUsername("admin");
        System.out.println(admin);

        final User byUsername = userRepository.getByUsername("admin");

        System.out.println(byUsername);
    }
}