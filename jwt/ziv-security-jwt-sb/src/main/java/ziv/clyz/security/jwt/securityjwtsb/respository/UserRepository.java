package ziv.clyz.security.jwt.securityjwtsb.respository;

import org.springframework.data.repository.Repository;
import ziv.clyz.security.jwt.securityjwtsb.bean.User;

@org.springframework.stereotype.Repository
public interface UserRepository extends Repository<User,Long> {

    User getByUsername(String username);

    boolean existsUserByUsername(String username);

    User save(User userToAdd);
}
