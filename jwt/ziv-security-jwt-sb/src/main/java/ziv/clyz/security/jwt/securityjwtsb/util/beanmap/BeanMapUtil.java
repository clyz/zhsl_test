package ziv.clyz.security.jwt.securityjwtsb.util.beanmap;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class BeanMapUtil {

    public static Map<String, Object> toMap(Object obj) throws Exception {
        Class clazz = obj.getClass();
        //获得属性
        Field[] fields = obj.getClass().getDeclaredFields();
        Map<String, Object> hashMap = new HashMap<>(fields.length);
        for (Field field : fields) {
            if (field.getAnnotation(Need.class) == null && field.getAnnotation(NeedIterative.class) == null) {
                continue;
            }
            PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
            //获得get方法
            Method getMethod = pd.getReadMethod();
            //执行get方法返回一个Object
            Object exeValue = getMethod.invoke(obj);
            //获取的注解不为空，那么说明此处返回的结果是对象，那么需要迭代处理
            if (field.getAnnotation(NeedIterative.class) != null) {
                if (exeValue instanceof Collection) {
                    List<Map<String, Object>> fieldList = new ArrayList<>();
                    for (Object o : ((Collection) exeValue)) {
                        fieldList.add(toMap(o));
                    }
                    hashMap.put(field.getName(), fieldList);
                }else {
                    hashMap.put(field.getName(), toMap(exeValue));
                }
            } else {
                hashMap.put(field.getName(), exeValue);
            }
        }
        return hashMap;
    }
}
