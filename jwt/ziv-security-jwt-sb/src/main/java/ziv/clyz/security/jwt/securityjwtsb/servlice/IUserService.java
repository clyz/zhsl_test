package ziv.clyz.security.jwt.securityjwtsb.servlice;

import ziv.clyz.security.jwt.securityjwtsb.bean.User;
import ziv.clyz.security.jwt.securityjwtsb.exception.JwtException;

public interface IUserService {

    User register(User userToAdd) throws JwtException;

    String login(String username, String password) throws Exception;

    User loadUserByUsername(String userName);

    String refreshToken(String token) throws Exception;
}
