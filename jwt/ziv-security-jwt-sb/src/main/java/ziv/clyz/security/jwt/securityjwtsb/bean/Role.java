package ziv.clyz.security.jwt.securityjwtsb.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ziv.clyz.security.jwt.securityjwtsb.util.beanmap.Need;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue
    @Need
    private Long id;

    @Need
    private String name;

}
