package ziv.clyz.security.jwt.securityjwtsb.servlice.imp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ziv.clyz.security.jwt.securityjwtsb.bean.User;
import ziv.clyz.security.jwt.securityjwtsb.exception.JwtException;
import ziv.clyz.security.jwt.securityjwtsb.jwt.JwtTokenComponent;
import ziv.clyz.security.jwt.securityjwtsb.respository.UserRepository;
import ziv.clyz.security.jwt.securityjwtsb.servlice.IUserService;

import java.util.Optional;

@Service
@Slf4j
public class IUserServiceImpl implements IUserService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenComponent jwtTokenComponent;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    // 登录
    @Override
    public String login(String username, String password) {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = userRepository.getByUsername(username);
        return jwtTokenComponent.generateToken(userDetails);
    }

    // 注册
    @Override
    public User register(User userToAdd) throws JwtException {
        if (userRepository.existsUserByUsername(userToAdd.getUsername())) {
            log.info("用户名称已经存在！");
            throw new JwtException("用户名称已经存在！");
        }
        userToAdd.setPassword(passwordEncoder.encode(userToAdd.getPassword()));
        return userRepository.save(userToAdd);
    }

    @Override
    public User loadUserByUsername(String userName) {
        return Optional.ofNullable(userRepository.getByUsername(userName)).orElseThrow(() -> new UsernameNotFoundException("无该用户"));
    }

    @Override
    public String refreshToken(String oldToken) throws Exception {
        String token = oldToken.substring(tokenHead.length());
        if (jwtTokenComponent.canRefresh(token)) {
            return jwtTokenComponent.refreshToken(token);
        }
        log.info("不可清除！");
        throw new JwtException("不可清除！");
    }
}
