package ziv.clyz.security.jwt.securityjwtsb.exception;

public class JwtException extends Exception {
    public JwtException(String s) {
        super(s);
    }
}
