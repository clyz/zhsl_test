package ziv.clyz.security.jwt.securityjwtsb;

import com.github.xiaour.api_scanner.annotation.Sapi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@Sapi(controllers = {"ziv.clyz.security.jwt.securityjwtsb.controller"})
public class SecurityJwtSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityJwtSbApplication.class, args);
    }

}
