package ziv.clyz.security.jwt.securityjwtsb.util.beanmap;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;

//作用于方法上，运行时保留
@Target(value={FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Need {
    //参数没有作用
    String value() default "";
}
