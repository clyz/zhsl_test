package ziv.clyz.security.jwt.securityjwtsb.controller;

import com.github.xiaour.api_scanner.annotation.SapiGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import ziv.clyz.security.jwt.securityjwtsb.bean.User;
import ziv.clyz.security.jwt.securityjwtsb.exception.JwtException;
import ziv.clyz.security.jwt.securityjwtsb.servlice.IUserService;

import java.security.Principal;

@RestController
@SapiGroup(title = "JWT _")
public class JwtAuthController {
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Autowired
    private IUserService authService;

    /**
     * <p>
     * 测试普通权限
     * </p>
     *
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_NORMAL')")
    @RequestMapping(value = "/normal/test", method = RequestMethod.GET)
    public String test1() {
        return "ROLE_NORMAL /normal/test接口调用成功！";
    }

    /**
     * <p>
     * 测试管理员权限
     * </p>
     *
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/test", method = RequestMethod.GET)
    public String test2() {
        return "ROLE_ADMIN /admin/test接口调用成功！";
    }

    /**
     * <p>
     * 刷新 token
     * </p>
     *
     * @param token
     * @return
     * @throws JwtException
     */
    @PreAuthorize("hasAuthority('ROLE_NORMAL')")
    @PostMapping("/refresh")
    public String refresh(@RequestHeader(name = "ziv") String token) throws Exception {
        return authService.refreshToken(token);
    }

    /**
     * <p>
     * 登录
     * </p>
     *
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/authentication/login", method = RequestMethod.POST)
    public String createToken(String username, String password) throws Exception {
        return authService.login(username, password); // 登录成功会返回JWT Token给用户
    }

    /**
     * <p>
     * 注册
     * </p>
     *
     * @param addedUser
     * @return
     * @throws AuthenticationException
     * @throws JwtException
     */
    @RequestMapping(value = "/authentication/register", method = RequestMethod.POST)
    public User register(User addedUser) throws AuthenticationException, JwtException {
        return authService.register(addedUser);
    }

    /**
     * <p>
     * 开放的接口
     * </p>
     *
     * @param x
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/authentication/xo", method = RequestMethod.POST)
    public String xo(String x) throws AuthenticationException {
        return x;
    }

    /**
     * <p>
     * 获取 用户详情
     * </p>
     *
     * @param principal
     * @return
     */
    @GetMapping(value = "/info")
    @ResponseBody
    public Object getAdminInfo(Principal principal) {
        return authService.loadUserByUsername(principal.getName());
    }
}
