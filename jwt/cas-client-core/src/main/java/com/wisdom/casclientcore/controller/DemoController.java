package com.wisdom.casclientcore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class DemoController {

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "redirect:https://s.ziv:1315/cas/logout?service=https://localhost:1318";
    }
}