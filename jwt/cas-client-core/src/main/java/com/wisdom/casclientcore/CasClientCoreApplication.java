package com.wisdom.casclientcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasClientCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasClientCoreApplication.class, args);
    }
}
