package com.wisdom.casclientcore.filter;


import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LocalUserInfoFilter implements Filter {

    Logger logger =  LoggerFactory.getLogger(LocalUserInfoFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException, IOException {
        HttpServletRequest request_ = (HttpServletRequest)request;
        String loginName = CASUtil.getAccountNameFromCas(request_);
        if(!StringUtils.isEmpty(loginName)){
            logger.info("访问者 ：" +loginName);
            request_.getSession().setAttribute("loginName", loginName);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

/**
 * cas client常用工具类
 *
 * @author weiller
 */
class CASUtil {

    /**
     * 从cas中获取用户名
     *
     * @param request
     * @return
     */
    public static String getAccountNameFromCas(HttpServletRequest request) {
        Assertion assertion = (Assertion) request.getSession().getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
        if(assertion!= null){
            AttributePrincipal principal = assertion.getPrincipal();
            return principal.getName();
        }else{
            return null;
        }

    }
}