package com.wisdom.caspac4jshirosb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasPac4jShiroSbApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasPac4jShiroSbApplication.class, args);
    }
}
