package com.wisdom.caspac4jshirosb.service.impl;

import com.wisdom.caspac4jshirosb.service.UserService;
import io.buji.pac4j.subject.Pac4jPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

/**
 * Created by wwu on 2017/4/13.
 */
@Service("userService")
@Slf4j
public class UserServiceImpl implements UserService {
    @Override
    public String getUsername() {
        Subject subject = SecurityUtils.getSubject();
        if (subject == null || subject.getPrincipals() == null) {
            log.info("未登录");
            return null;
        }
        Pac4jPrincipal principal = (Pac4jPrincipal) subject.getPrincipals().getPrimaryPrincipal();
        return principal.getName();
    }
}
