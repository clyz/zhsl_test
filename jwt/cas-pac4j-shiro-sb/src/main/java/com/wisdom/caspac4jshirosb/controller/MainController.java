package com.wisdom.caspac4jshirosb.controller;

import com.wisdom.caspac4jshirosb.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by wwu on 2017/4/13.
 */
@Controller
@RequestMapping("/")
@Slf4j
public class MainController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public void index(ModelMap model){
        log.info("login...{}",userService.getUsername());
        model.addAttribute("username",userService.getUsername());
    }
    @RequiresPermissions("xo")
    @RequestMapping("xo")
    public void ox(){
        log.info("per");
    }
}