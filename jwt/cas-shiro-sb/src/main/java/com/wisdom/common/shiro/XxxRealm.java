package com.wisdom.common.shiro;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cas.CasRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.HashSet;
import java.util.Set;

public class XxxRealm extends CasRealm {
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        Set<String> strings = new HashSet<>();
        strings.add("USER");
        //这里获取用户角色和权限的逻辑，每个项目不一样，所以没写实现代码
        //设置用户拥有的角色
        authorizationInfo.setRoles(strings);
        //设置用户拥有的权限
        authorizationInfo.setStringPermissions(strings);
        return authorizationInfo;
    }
}