package com.wisdom.cassecurity1sb;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class CasSecurity1SbApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasSecurity1SbApplication.class, args);
    }

    @Data
    @ConfigurationProperties(prefix = "security.cas.server")
    public class CasServerConfig {
        private String host;
        private String login;
        private String logout;

    }

    @Data
    @ConfigurationProperties(prefix = "security.cas.service")
    public class CasServiceConfig {
        private String host;
        private String login;
        private String logout;
        private Boolean sendRenew = false;
    }
}
