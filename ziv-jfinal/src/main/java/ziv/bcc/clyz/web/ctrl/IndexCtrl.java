package ziv.bcc.clyz.web.ctrl;

import com.jfinal.core.Controller;

public class IndexCtrl extends Controller {
    public String index() {
        return "index.html";
    }

    public void orCode() {
        renderQrCode("杨泽民哼帅", 200, 200);
    }

    public void captcha() {
        renderCaptcha();
    }

    public void down() {
        renderFile("xo.json");
    }
}
