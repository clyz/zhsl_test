package ziv.bcc.clyz;

import com.jfinal.server.undertow.UndertowServer;
import ziv.bcc.clyz.config.ClyzConfig;

public class Application {
    public static void main(String[] args) {
        UndertowServer.create(ClyzConfig.class, "undertow-server.txt")
                .start();
    }
}
