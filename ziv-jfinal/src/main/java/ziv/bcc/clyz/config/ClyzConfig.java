package ziv.bcc.clyz.config;

import com.jfinal.captcha.CaptchaCache;
import com.jfinal.config.*;
import com.jfinal.kit.PathKit;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import ziv.bcc.clyz.config.md.ClyzMdRoutes;
import ziv.bcc.clyz.web.ctrl.IndexCtrl;

public class ClyzConfig extends JFinalConfig {

    public void configConstant(Constants constants) {
        constants.setDevMode(true);
        constants.setEncoding("utf-8");
        constants.setBaseDownloadPath(PathKit.getWebRootPath() + "/download");
        constants.setBaseUploadPath(PathKit.getWebRootPath() + "/upload");
        constants.setCaptchaCache(new CaptchaCache());
        constants.setFreeMarkerTemplateUpdateDelay(0);
        constants.setReportAfterInvocation(true);
        constants.setTokenCache(null);
        constants.setUrlParaSeparator("-");
        constants.setViewType(ViewType.FREE_MARKER);
        constants.setI18nDefaultBaseName("i18n");
        constants.setI18nDefaultLocale("zh_CN");
        constants.setMaxPostSize(1024 * 1024 * 1024);
        constants.setViewExtension(".html");
    }

    public void configRoute(Routes routes) {
        routes.add("/", IndexCtrl.class);
        //  MD 业务
        routes.add(new ClyzMdRoutes());
        routes.setBaseViewPath("/");
    }

    public void configEngine(Engine engine) {

    }

    public void configPlugin(Plugins plugins) {

    }

    public void configInterceptor(Interceptors interceptors) {

    }

    public void configHandler(Handlers handlers) {

    }
}
