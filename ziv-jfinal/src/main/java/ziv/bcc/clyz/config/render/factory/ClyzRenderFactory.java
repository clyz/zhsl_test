package ziv.bcc.clyz.config.render.factory;

import com.jfinal.config.Constants;
import com.jfinal.render.ContentType;
import com.jfinal.render.IRenderFactory;
import com.jfinal.render.Render;
import com.jfinal.template.Engine;

import javax.servlet.ServletContext;
import java.io.File;

public class ClyzRenderFactory implements IRenderFactory {
    public void init(Engine engine, Constants constants, ServletContext servletContext) {

    }

    public Render getRender(String view) {
        return null;
    }

    public Render getTemplateRender(String view) {
        return null;
    }

    public Render getFreeMarkerRender(String view) {
        return null;
    }

    public Render getJspRender(String view) {
        return null;
    }

    public Render getVelocityRender(String view) {
        return null;
    }

    public Render getJsonRender() {
        return null;
    }

    public Render getJsonRender(String key, Object value) {
        return null;
    }

    public Render getJsonRender(String[] attrs) {
        return null;
    }

    public Render getJsonRender(String jsonText) {
        return null;
    }

    public Render getJsonRender(Object object) {
        return null;
    }

    public Render getTextRender(String text) {
        return null;
    }

    public Render getTextRender(String text, String contentType) {
        return null;
    }

    public Render getTextRender(String text, ContentType contentType) {
        return null;
    }

    public Render getDefaultRender(String view) {
        return null;
    }

    public Render getErrorRender(int errorCode, String view) {
        return null;
    }

    public Render getErrorRender(int errorCode) {
        return null;
    }

    public Render getFileRender(String fileName) {
        return null;
    }

    public Render getFileRender(String fileName, String downloadFileName) {
        return null;
    }

    public Render getFileRender(File file) {
        return null;
    }

    public Render getFileRender(File file, String downloadFileName) {
        return null;
    }

    public Render getRedirectRender(String url) {
        return null;
    }

    public Render getRedirectRender(String url, boolean withQueryString) {
        return null;
    }

    public Render getRedirect301Render(String url) {
        return null;
    }

    public Render getRedirect301Render(String url, boolean withQueryString) {
        return null;
    }

    public Render getNullRender() {
        return null;
    }

    public Render getJavascriptRender(String jsText) {
        return null;
    }

    public Render getHtmlRender(String htmlText) {
        return null;
    }

    public Render getXmlRender(String view) {
        return null;
    }

    public Render getCaptchaRender() {
        return null;
    }

    public Render getQrCodeRender(String content, int width, int height) {
        return null;
    }

    public Render getQrCodeRender(String content, int width, int height, char errorCorrectionLevel) {
        return null;
    }
}
