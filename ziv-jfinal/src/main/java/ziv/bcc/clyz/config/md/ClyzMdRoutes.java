package ziv.bcc.clyz.config.md;

import com.jfinal.config.Routes;
import ziv.bcc.clyz.web.ctrl.md.MdCtrtl;

public class ClyzMdRoutes extends Routes {
    public void config() {
        add("/md", MdCtrtl.class);
    }
}
