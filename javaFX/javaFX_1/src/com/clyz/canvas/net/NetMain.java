package com.clyz.canvas.net;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * 网 图
 */
public class NetMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("网");
        Group root = new Group();
        Canvas canvas = new Canvas(800, 800);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawShapes(gc);
        final Boolean[] who = {true};
        Pot pot = new Pot();


        canvas.setOnMouseClicked(e -> {
            // 鼠标左键单击
            if (e.getButton() == MouseButton.PRIMARY) {
                if (who[0]) {
                    gc.setFill(Color.RED);
                } else {
                    gc.setFill(Color.BLACK);
                }
                who[0] = !who[0];

                double sceneX = e.getSceneX();
                double sceneY = e.getSceneY();
                if (sceneX < 40 || sceneX > 800 || sceneY < 40 || sceneY > 800) {
                    return;
                }
                double x = getLocation(sceneX);
                double y = getLocation(sceneY);

                boolean b = pot.set(!who[0], x, y);
                if (!b) {
                    return;
                }
                gc.fillArc(x - 15, y - 15, 30, 30, 0, 360, ArcType.ROUND);//绘制完整的原型
                boolean valid = pot.valid(!who[0], x, y);
                if (valid) {
                    gc.setFont(new Font(28));
                    gc.strokeText("OVER", 300, 300);
                }
            } else if (e.getButton() == MouseButton.SECONDARY) {
                gc.clearRect(0, 0, 800, 800);
                pot.pots.clear();
                drawShapes(gc);
            }
            //刷新，否则不能接收到键盘事件了
            canvas.requestFocus();
        });

        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private double getLocation(double l) {
        double v = l % 40;
        double v1 = Math.floor(l / 40);
        return v > 20.0 ? v1 * 40 + 40 : v1 * 40;
    }

    private void drawShapes(GraphicsContext gc) {
        gc.setStroke(Color.GREY);
        gc.setLineWidth(1);

        for (int i = 40; i < 800; i = i + 40) {
            gc.strokeLine(i, 40, i, 760);
        }

        for (int i = 40; i < 800; i = i + 40) {
            gc.strokeLine(40, i, 760, i);
        }

    }

    class Pot {
        /**
         * 记录 棋子点位 0/1_x_y
         * 0/1 表示 黑/白
         * x    x 坐标 小标 实际 位置 为 x*40 从1开始
         */
        private Set<String> pots = new ConcurrentSkipListSet<>();

        public String getKey(Boolean bool, double x, double y) {
            int i = bool ? 1 : 0;
            double indexX = Math.floor(x / 40);
            double indexY = Math.floor(y / 40);
            return i + "_" + indexX + "_" + indexY;
        }

        public boolean set(Boolean bool, double x, double y) {
            String key = getKey(bool, x, y);
            String key1 = getKey(!bool, x, y);
            if (pots.contains(key) || pots.contains(key1)) {
                return false;
            } else {
                pots.add(key);
                return true;
            }
        }

        public boolean valid(Boolean bool, double x, double y) {
            int r = bool ? 1 : 0;
            double indexX = Math.floor(x / 40);
            double indexY = Math.floor(y / 40);

            boolean heng = heng(r, indexX, indexY);
            if (heng) return true;
            boolean su = su(r, indexX, indexY);
            if (su) return true;
            boolean xieZheng = xieZheng(r, indexX, indexY);
            if (xieZheng) return true;
            return xieSu(r, indexX, indexY);
        }

        private boolean xieZheng(int r, double x, double y) {
            StringBuilder buffer = new StringBuilder();
            for (int i = (int) (x > 4 ? x - 4 : 1); i <= (int) (x > 15 ? 19 : x + 4); i++) {
                buffer.append(have(r, i, x + y - i));
            }
            return buffer.toString().contains("11111");
        }

        private boolean xieSu(int r, double x, double y) {
            StringBuilder buffer = new StringBuilder();
            for (int i = (int) (y > 4 ? y - 4 : 1); i <= (int) (y > 15 ? 19 : y + 4); i++) {
                buffer.append(have(r, x - (y - i), i));
            }
            return buffer.toString().contains("11111");
        }

        private boolean su(int r, double x, double y) {
            //  横 x 不变
            StringBuilder buffer = new StringBuilder();
            for (int i = (int) (y > 4 ? y - 4 : 1); i <= (int) (y > 15 ? 19 : y + 4); i++) {
                buffer.append(have(r, x, i));
            }
            return buffer.toString().contains("11111");
        }

        private boolean heng(int r, double x, double y) {
            //  横 y 不变
            StringBuilder buffer = new StringBuilder();
            for (int i = (int) (x > 4 ? x - 4 : 1); i <= (int) (x > 15 ? 19 : x + 4); i++) {
                buffer.append(have(r, i, y));
            }
            return buffer.toString().contains("11111");
        }

        private String have(int r, double x, double y) {
            return pots.contains(r + "_" + x + "_" + y) ? "1" : "0";
        }

    }
}
