JavaFX SpringBoot

## JavaFx

## JavaFX SpringBoot

### 参考文献及地址

[javafx-boot-demo]: https://github.com/spartajet/javafx-boot-demo



### 搭建项目

1. 创建一个正常的Spring Boot 项目

2. maven 包

   ```xml
   <dependency>
     <groupId>de.roskenet</groupId>
     <artifactId>springboot-javafx-support</artifactId>
     <version>${springboot-javafx-support.version}</version>
   </dependency>
   ```

3. 修改 应用启动类

   ```java
   @SpringBootApplication
   public class Javafx2SbApplication extends AbstractJavaFxApplicationSupport {

       public static void main(String[] args) {
           launch(Javafx2SbApplication.class, Javafx2SbView.class, args);
       }
   }
   ```

   其中，Javafx2SbApplication 应用程序类。

   Javafx2SbView 主窗口视图。

   ```java
   @FXMLView(value = "/fxml/index/index.fxml")
   public class Javafx2SbView extends AbstractFxmlView {
   }
   ```

4. 视图文件配置

   ```XML
   <?xml version="1.0" encoding="UTF-8"?>

   <?import javafx.scene.layout.BorderPane?>
   <BorderPane id="javaFXSb" xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1" fx:controller="clyz.ziv.javafx.javafx_2_sb.index.Javafx2SbController">

   </BorderPane>
   ```

   对应 Javafx2SbView 主窗口视图 中的 /view/Javafx2SbView.fxml。视图文件(view/Javafx2SbView.fxml) 创建在资源目录下。配置视图的controller

   ```java
   @FXMLController
   public class Javafx2SbController implements Initializable {

       @Override
       public void initialize(URL location, ResourceBundle resources) {

       }
   }
   ```

## 视图配置说明

## DOME

### Hello World

```java
@SpringBootApplication
public class Javafx2SbApplication extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launch(Javafx2SbApplication.class, Javafx2SbView.class, args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        Scene scene = new Scene(root, 300, 250);
        stage.setTitle("Hello World!");
        stage.setScene(scene);
        stage.show();
    }
}

```



## 注解说明

| 名称              | 说明   |
| :-------------- | ---- |
| @FXMLController |      |
| @FXMLView       |      |
|                 |      |

## 类及接口说明

| 名称                               | 类型   | 说明             |
| :------------------------------- | ---- | -------------- |
| AbstractJavaFxApplicationSupport | 类    |                |
| Initializable                    | 接口   | 控制器初始化接口<br /> |
|                                  |      |                |