package clyz.ziv.javafx.javafx_2_sb.controller.index;

import clyz.ziv.javafx.javafx_2_sb.Main;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndexViewTest extends AbstractJavaFxApplicationSupport {
    public static void main(String[] args) {
        launch(Main.class, IndexView.class, args);
    }
}