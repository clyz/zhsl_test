package clyz.ziv.javafx.javafx_2_sb.controller.about;

import clyz.ziv.javafx.javafx_2_sb.Main;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AboutViewTest extends AbstractJavaFxApplicationSupport {
    public static void main(String[] args) {
        launch(Main.class, AboutView.class, args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        super.start(stage);
        stage.setResizable(false);
    }
}