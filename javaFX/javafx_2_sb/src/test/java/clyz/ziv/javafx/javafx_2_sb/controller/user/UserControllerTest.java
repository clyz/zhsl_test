package clyz.ziv.javafx.javafx_2_sb.controller.user;

import clyz.ziv.javafx.javafx_2_sb.Main;
import clyz.ziv.javafx.javafx_2_sb.config.AppConfig;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(AppConfig.class)
public class UserControllerTest extends AbstractJavaFxApplicationSupport {
    public static void main(String[] args) {
        launch(Main.class, UserView.class, args);
    }
}