package clyz.ziv.javafx.javafx_2_sb.controller.user;

import clyz.ziv.javafx.javafx_2_sb.entity.User;
import clyz.ziv.javafx.javafx_2_sb.service.IUserService;
import de.felixroske.jfxsupport.FXMLController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

@FXMLController
public class UserController implements Initializable {
    private final IUserService userService;
    @FXML
    public TableView<User> userTableView;
    @FXML
    public TableColumn<User, String> nameTableColumn;
    @FXML
    public TableColumn<User, String> pwdTableColumn;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        pwdTableColumn.setCellValueFactory(new PropertyValueFactory<>("pwd"));

        ObservableList<User> users = FXCollections.observableArrayList();
        users.addAll(userService.list());
        userTableView.setItems(users);
    }

}