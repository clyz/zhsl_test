package clyz.ziv.javafx.javafx_2_sb.controller.about;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


/**
 * 关于我们
 */
@FXMLView("/fxml/about/about.fxml")
public class AboutView extends AbstractFxmlView {
}
