package clyz.ziv.javafx.javafx_2_sb.service.impl;

import clyz.ziv.javafx.javafx_2_sb.config.AppConfig;
import clyz.ziv.javafx.javafx_2_sb.entity.User;
import clyz.ziv.javafx.javafx_2_sb.exception.LoginRuntime;
import clyz.ziv.javafx.javafx_2_sb.service.IUserService;
import org.springframework.stereotype.Service;
import pl.touk.throwing.ThrowingPredicate;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    private final AppConfig appConfig;

    public UserServiceImpl(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    @Override
    public User findByUserName(String name) {
        return null;
    }

    @Override
    public boolean findByUsernameAndPassword(String name, String pwd) {
        try {
            return appConfig.getUsers().stream().anyMatch(ThrowingPredicate.unchecked(e -> e.getPwd().equals(pwd) && e.getName().equals(name)));
        } catch (Exception e) {
            throw new LoginRuntime("账号或密码错误");
        }
    }

    @Override
    public List<User> list() {
        return appConfig.getUsers();
    }
}
