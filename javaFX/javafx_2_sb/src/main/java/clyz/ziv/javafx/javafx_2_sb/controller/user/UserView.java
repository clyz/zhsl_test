package clyz.ziv.javafx.javafx_2_sb.controller.user;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView("/fxml/user/user.fxml")
public class UserView extends AbstractFxmlView {
}
