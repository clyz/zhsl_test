package clyz.ziv.javafx.javafx_2_sb.controller.login;

import clyz.ziv.javafx.javafx_2_sb.Main;
import clyz.ziv.javafx.javafx_2_sb.controller.index.IndexView;
import clyz.ziv.javafx.javafx_2_sb.exception.LoginRuntime;
import clyz.ziv.javafx.javafx_2_sb.service.IUserService;
import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;

import java.net.URL;
import java.util.ResourceBundle;

@FXMLController
public class LoginController implements Initializable {
    @FXML
    public TextField nameTextField;
    @FXML
    public PasswordField pwdPasswordField;
    @FXML
    public Label tipLabel;

    private final IUserService userService;

    public LoginController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void login(ActionEvent actionEvent) {
        try {
            boolean bool = userService.findByUsernameAndPassword(nameTextField.getText(), pwdPasswordField.getText());
            if (!bool) {
                tipLabel.setText("账号或密码错误");
            } else {
                tipLabel.setText("登录成功");
                Main.showView(IndexView.class, Modality.APPLICATION_MODAL);
            }
        } catch (LoginRuntime e) {
            tipLabel.setText(e.getMessage());
        }
    }

    @FXML
    public void exit(ActionEvent actionEvent) {
        Platform.exit();
    }
}
