package clyz.ziv.javafx.javafx_2_sb.exception;

public class LoginRuntime extends RuntimeException{
    public LoginRuntime(String message) {
        super(message);
    }
}
