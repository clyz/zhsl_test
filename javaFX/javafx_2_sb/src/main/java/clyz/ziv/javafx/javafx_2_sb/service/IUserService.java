package clyz.ziv.javafx.javafx_2_sb.service;

import clyz.ziv.javafx.javafx_2_sb.entity.User;

import java.util.List;

public interface IUserService {

    User findByUserName(String name);

    boolean findByUsernameAndPassword(String name,String pwd);

    List<User> list();

}
