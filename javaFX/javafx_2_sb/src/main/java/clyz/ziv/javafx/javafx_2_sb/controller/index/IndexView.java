package clyz.ziv.javafx.javafx_2_sb.controller.index;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * 首页
 * @author Zemin.Yang
 * @date 2020/3/10 15:52
 */
@FXMLView(value = "/fxml/index/index.fxml")
public class IndexView extends AbstractFxmlView {

}