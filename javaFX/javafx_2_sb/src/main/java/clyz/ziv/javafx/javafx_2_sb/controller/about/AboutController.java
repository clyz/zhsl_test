package clyz.ziv.javafx.javafx_2_sb.controller.about;

import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.HTMLEditor;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@FXMLController
public class AboutController implements Initializable {
    @FXML
    public HTMLEditor htmlEditor;

    @Autowired
    public AboutView aboutView;

    private File file;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            file = ResourceUtils.getFile("classpath:fxml/about/about");
            htmlEditor.setHtmlText(FileUtils.readFileToString(file));
        } catch (Exception ignored) {
        }
        htmlEditor.addEventFilter(KeyEvent.KEY_PRESSED, ke -> {
            if (ke.isControlDown() && ke.getCode() == KeyCode.S) {
                try {
                    FileUtils.writeStringToFile(file, htmlEditor.getHtmlText());
                } catch (IOException ignored) {
                }
                ke.consume();
            }
        });
    }
}
