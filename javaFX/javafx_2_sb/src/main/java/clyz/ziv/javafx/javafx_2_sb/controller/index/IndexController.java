package clyz.ziv.javafx.javafx_2_sb.controller.index;

import clyz.ziv.javafx.javafx_2_sb.controller.about.AboutView;
import clyz.ziv.javafx.javafx_2_sb.controller.user.UserView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Zemin.Yang
 * @date 2020/3/10 15:58
 */
@FXMLController
public class IndexController implements Initializable {
    @FXML
    public Pane contentPane;
    private final UserView userView;
    private final AboutView aboutView;

    public IndexController(UserView userView, AboutView aboutView) {
        this.userView = userView;
        this.aboutView = aboutView;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void exit(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void userView(ActionEvent actionEvent) {
        contentPane.getChildren().clear();
        contentPane.getChildren().add(userView.getView());
    }

    public void helpAbout(ActionEvent actionEvent) {
        contentPane.getChildren().clear();
        contentPane.getChildren().add(aboutView.getView());
    }
}
