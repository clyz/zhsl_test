package clyz.ziv.javafx.javafx_2_sb.config;

import clyz.ziv.javafx.javafx_2_sb.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统环境参数配置
 */
@Component
@ConfigurationProperties("app")
@Getter
@Setter
public class AppConfig {
    /**
     * 登录用户
     */
    private List<User> users;
}
