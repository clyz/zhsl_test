package com.clyz.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 15:14
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 消息转换器
     */
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.clear();
        //  Java 对象 转为 Json 对象 转换器
        converters.add(
                new MappingJackson2HttpMessageConverter()
        );
    }

}
