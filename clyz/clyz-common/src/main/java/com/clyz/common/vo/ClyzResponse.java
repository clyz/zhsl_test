package com.clyz.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 14:44
 */
@Data
@NoArgsConstructor
public class ClyzResponse<T> implements Serializable {

    private Integer code;
    private String message;
    private T data;

    public ClyzResponse(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ClyzResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
