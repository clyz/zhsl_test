package com.clyz.common.exception;

/**
 * 统一 异常处理
 *
 * @author Zemin.Yang
 * @date 2020/3/24 15:02
 */
public class ClyzException extends Exception {
    public ClyzException(String message) {
        super(message);
    }
}
