package com.clyz.common.advice;

import com.clyz.common.exception.ClyzException;
import com.clyz.common.vo.ClyzResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Zemin.Yang
 * @date 2020/3/24 15:04
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler(ClyzException.class)
    public ClyzResponse<String> handlerCommonException(HttpServletRequest request, ClyzException clyzException) {
        return new ClyzResponse<String>(-1, "business error", clyzException.getMessage());
    }
}
