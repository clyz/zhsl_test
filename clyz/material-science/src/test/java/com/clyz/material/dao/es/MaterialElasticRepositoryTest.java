package com.clyz.material.dao.es;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.clyz.material.entity.es.Material;
import lombok.extern.java.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Log
class MaterialElasticRepositoryTest {

    @Autowired
    private MaterialElasticRepository materialElasticRepository;
    private final Pageable pageable = PageRequest.of(0, 10);

    @Test
    void save(){
        Material material = new Material();
        material.setContent("DB SQL 持久化");
        material.setType(1);
        material.setTag("节点");
        Material save = materialElasticRepository.save(material);
        log.info(save.toString());
    }

    @Test
    void findAll(){
        Iterable<Material> all = materialElasticRepository.findAll();
        log.info(all.toString());
    }

    @Test
    void findByContent() {
        Page<Material> page = materialElasticRepository.findByContent("杨泽民", pageable);
        log.info(page.getContent().toString());
    }

    @Test
    void remove(){
        materialElasticRepository.deleteAll();
    }

    @Test
    void findAllParamPageable(){
        Page<Material> page = materialElasticRepository.findAll(pageable);
        log.info(page.getContent().toString());
    }

    @Test
    void findByContentAndType(){
        Page<Material> page = materialElasticRepository.findByContentAndType("*", 1, pageable);
        log.info(page.getContent().toString());
    }

    @Test
    void search(){
        Page<Material> page = materialElasticRepository.search("节点", 1, pageable);
        log.info(page.getContent().toString());
    }

    /**
     * 导出
     */
    @Test
    public void simpleWrite() {
        // 写法1
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭

        // 如果这里想使用03 则 传入excelType参数即可
        List<Material> content = materialElasticRepository.findAll(pageable).getContent();

       /* EasyExcel.write("write.xlsx", Material.class).sheet("模板")
                .doWrite(content);

        ExcelWriterBuilder write = EasyExcel.write("write.xlsx", Material.class);
        ExcelWriterSheetBuilder builder = write.sheet("1");
        builder.doWrite(content);*/


        // 这里 需要指定写用哪个class去写
        ExcelWriter excelWriter = null;
        try {
            excelWriter = EasyExcel.write("write.xlsx", Material.class).build();
            WriteSheet writeSheet = EasyExcel.writerSheet("1").build();
            excelWriter.write(content, writeSheet);

            WriteSheet writeSheet1 = EasyExcel.writerSheet("2").build();
            excelWriter.write(content, writeSheet1);

        } finally {
            // 千万别忘记finish 会帮忙关闭流
            if (excelWriter != null) {
                excelWriter.finish();
            }

        }
    }
}