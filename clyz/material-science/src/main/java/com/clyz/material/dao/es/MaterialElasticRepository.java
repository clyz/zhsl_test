package com.clyz.material.dao.es;

import com.clyz.material.entity.es.Material;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * <h1></h1>
 *
 * @author Zemin.Yang
 */
@Repository
public interface MaterialElasticRepository extends ElasticsearchRepository<Material, String> {
    //默认的注释
    //@Query("{\"bool\" : {\"must\" : {\"field\" : {\"content\" : \"?\"}}}}")
    Page<Material> findByContent(String content, Pageable pageable);

    Page<Material> findByContentAndType(String content, Integer type, Pageable pageable);

    @Query("{" +
            "        \"bool\":{" +
            "            \"must\":[" +
            "                {" +
            "                    \"match\":{" +
            "                        \"type\":?1" +
            "                    }" +
            "                }," +
            "                {" +
            "                    \"match_phrase\":{" +
            "                        \"content\":\"?0\"" +
            "                    }" +
            "                }" +
            "            ]" +
            "        }" +
            "}")
    Page<Material> search(String content, Integer type, Pageable pageable);
}
