package com.clyz.material.common;

public interface IEnum {
    Integer getIndex();

    String getMsg();
}
