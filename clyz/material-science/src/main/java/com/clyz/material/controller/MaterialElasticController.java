package com.clyz.material.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.util.FileUtils;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.clyz.material.entity.es.Material;
import com.clyz.material.service.es.IMaterialElasticService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

/**
 * <h1></h1>
 *
 * @author Zemin.Yang
 */
@Log4j2
@RestController
@RequestMapping("es/material")
@AllArgsConstructor
@Api(tags = {"资料 ES "})
public class MaterialElasticController {
    private final IMaterialElasticService elasticService;

    @PutMapping("addOrUpdateBatch")
    @ApiOperation(value = "批量更新及新增")
    public List<Material> addOrUpdateBatch(@RequestBody List<Material> materials) {
        return elasticService.addOrUpdateBatch(materials);
    }

    @GetMapping("findById/{id}")
    public Material findById(@PathVariable String id) {
        return elasticService.findById(id);
    }

    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable String id) {
        elasticService.delete(id);
    }

    @GetMapping("search")
    public Page<Material> search(String content,
                                 @RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "10") int size) {
        return elasticService.search(content, 1, page, size);
    }

    @SneakyThrows
    @GetMapping("down")
    public ResponseEntity<byte[]> down() {
        int size = 1000;
        Page<Material> page = elasticService.page(0, size);
        if (!page.hasContent()) return ResponseEntity.status(HttpStatus.HTTP_VERSION_NOT_SUPPORTED).build();

        String tmpDirMaterialFileName = System.getProperty("java.io.tmpdir") + File.pathSeparator + UUID.randomUUID().toString() + "materials.xlsx";

        HttpHeaders headers = new HttpHeaders();        //设置头信息
        headers.setContentDispositionFormData("attachment", new String("materials.xlsx".getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));         //设置下载的附件
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);        //设置MIME类型

        File file = new File(tmpDirMaterialFileName);
        ExcelWriter excelWriter = null;
        try {
            excelWriter = EasyExcel.write(tmpDirMaterialFileName, Material.class).build();

            WriteSheet writeSheet = EasyExcel.writerSheet(page.getPageable().getPageNumber() + 1).build();
            excelWriter.write(page.getContent(), writeSheet);

            for (int i = page.getPageable().getPageNumber() + 1; i < page.getTotalPages(); i++) {
                page = elasticService.page(page.getPageable().getPageNumber() + 1, size);
                if (!page.hasContent()) break;
                writeSheet = EasyExcel.writerSheet(page.getPageable().getPageNumber() + 1).build();
                excelWriter.write(page.getContent(), writeSheet);
            }
        }finally {
            if (excelWriter != null) {
                excelWriter.finish();
            }
        }
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
        FileUtils.delete(file);
        return responseEntity;
    }
}
