package com.clyz.material.service.es;

import com.clyz.material.entity.es.Material;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * <h1></h1>
 *
 * @author Zemin.Yang
 * @date 2020.07.06 13:19
 */
public interface IMaterialElasticService{
    /**
     * 批量更新及新增
     * eg:
     *  当ID eq null 时 新增
     *  当ID eq not null 时 exist 修改: 新增
     * @param materials 操作得源对象
     */
    List<Material> addOrUpdateBatch(List<Material> materials);

    Material findById(String id);

    Page<Material> search(String content, Integer type, int page, int size);

    Page<Material> page(int page, int size);

    void delete(String id);
}
