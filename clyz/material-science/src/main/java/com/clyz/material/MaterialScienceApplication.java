package com.clyz.material;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaterialScienceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaterialScienceApplication.class, args);
    }

}
