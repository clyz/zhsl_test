package com.clyz.material.common.statics;

import com.clyz.material.common.IEnum;

public enum MATERIAL_TYPE implements IEnum {
    /*本地类容*/
    URL(1, "网址"),
    LOCAL(2, "本地"),
    ;
    private Integer index;
    private String msg;

    MATERIAL_TYPE(int index, String msg) {
        this.index = index;
        this.msg = msg;
    }

    @Override
    public Integer getIndex() {
        return index;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}