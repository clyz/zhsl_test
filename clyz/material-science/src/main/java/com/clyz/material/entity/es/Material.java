package com.clyz.material.entity.es;

import com.alibaba.excel.annotation.ExcelProperty;
import com.clyz.material.easyexecl.converter.MaterialTypeConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * <h1></h1>
 *
 * @author Zemin.Yang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Document(indexName = "material", shards = 1, replicas = 0)
public class Material {
    @Id
    @ExcelProperty("序列号")
    private String id;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    @ExcelProperty("搜索内容")
    private String content;

    @Field(type = FieldType.Keyword)
    @ExcelProperty("地址")
    private String address;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    @ExcelProperty("标题")
    private String tag;

    @Field(type = FieldType.Integer)
    @ExcelProperty(value = "类型", converter = MaterialTypeConverter.class)
    private Integer type = 1;
}
