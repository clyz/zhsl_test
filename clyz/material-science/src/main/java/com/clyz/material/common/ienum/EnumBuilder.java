package com.clyz.material.common.ienum;

import com.clyz.material.common.IEnum;

public class EnumBuilder {
    public static <T extends IEnum> String getMsgByIndex(T[] values, Integer index) {
        for (T value : values) {
            if (value.getIndex().equals(index)) return value.getMsg();
        }
        return null;
    }
}