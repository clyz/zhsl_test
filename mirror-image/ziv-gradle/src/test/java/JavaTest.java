import cn.hutool.crypto.digest.BCrypt;

public class JavaTest {
    public static void main(String[] args) {
        String gensalt = BCrypt.gensalt();
        System.out.println(gensalt);
        String hashpw = BCrypt.hashpw("123");
        System.out.println(hashpw);
    }
}
