package clyz.ziv.service;

import clyz.ziv.ApplicationTest;
import clyz.ziv.params.LoginParam;
import clyz.ziv.security.token.AuthToken;
import org.junit.Test;

public class AdminServiceTest extends ApplicationTest {
    @Test
    public void authenticate() {
        LoginParam loginParam = new LoginParam();
        loginParam.setPassword("123");
        loginParam.setUsername("admin");
        AuthToken authenticate = adminService.authenticate(loginParam);
    }
}