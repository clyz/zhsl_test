package clyz.ziv.repository;

import clyz.ziv.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@Slf4j
public class UserRepositoryTest {

    @Autowired
    public UserRepository userRepository;

    @Test
    public void load() {
        userRepository.findAll().forEach(System.out::println);
    }
}