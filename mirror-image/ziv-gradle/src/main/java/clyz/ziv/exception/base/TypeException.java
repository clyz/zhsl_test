package clyz.ziv.exception.base;

public enum TypeException {
    ALREADY_EXISTS,
    AUTHENTICATION,
    BAD_REQUEST,
    BEAN_UTILS,
    NOT_FOUND,
    FORBIDDEN
}
