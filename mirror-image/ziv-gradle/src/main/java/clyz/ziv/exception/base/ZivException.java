package clyz.ziv.exception.base;

import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class ZivException extends RuntimeException {

    private Object errorData;

    private TypeException type;

    public ZivException(String message, @NonNull TypeException type) {
        super(message);
        this.type = type;
    }

    public ZivException(String message, Throwable cause) {
        super(message, cause);
    }

    @NonNull
    public HttpStatus getStatus() {
        if (type.equals(TypeException.BAD_REQUEST)) {
            return HttpStatus.BAD_REQUEST;
        }
        return null;
    }

    @Nullable
    public Object getErrorData() {
        return errorData;
    }

    @NonNull
    public ZivException setErrorData(@Nullable Object errorData) {
        this.errorData = errorData;
        return this;
    }
}
