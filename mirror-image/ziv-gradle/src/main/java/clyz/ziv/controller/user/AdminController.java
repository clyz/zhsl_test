package clyz.ziv.controller.user;

import clyz.ziv.params.LoginParam;
import clyz.ziv.security.token.AuthToken;
import clyz.ziv.service.AdminService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Admin controller.
 *
 * @author johnniang
 * @date 3/19/19
 */
@Slf4j
@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("login")
    @ApiOperation("Login")
//    @CacheLock(autoDelete = false)
    public AuthToken auth(@Valid LoginParam loginParam) {
        return adminService.authenticate(loginParam);
    }

}
