package clyz.ziv.service.impl;

import clyz.ziv.entity.User;
import clyz.ziv.exception.base.TypeException;
import clyz.ziv.exception.base.ZivException;
import clyz.ziv.params.LoginParam;
import clyz.ziv.security.context.SecurityContextHolder;
import clyz.ziv.security.token.AuthToken;
import clyz.ziv.service.AdminService;
import clyz.ziv.service.UserService;
import clyz.ziv.utils.ZivUtils;
import cn.hutool.core.lang.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.concurrent.TimeUnit;

/**
 * Admin service implementation.
 *
 * @author johnniang
 * @author ryanwang
 * @date 19-4-29
 */
@Slf4j
@Service
public class AdminServiceImpl implements AdminService {

    private final UserService userService;

    public AdminServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public AuthToken authenticate(LoginParam loginParam) {
        Assert.notNull(loginParam, "Login param must not be null");

        String username = loginParam.getUsername();
        String mismatchTip = "用户名或者密码不正确";
        final User user;
        try {
            // Get user by username or email
            user = Validator.isEmail(username) ?
                    userService.getByEmailOfNonNull(username) : userService.getByUsernameOfNonNull(username);
        } catch (ZivException e) {
            log.error("Failed to find user by name: " + username, e);
            throw new ZivException(mismatchTip, TypeException.BAD_REQUEST);
        }

        userService.mustNotExpire(user);

        if (!userService.passwordMatch(user, loginParam.getPassword())) {
            throw new ZivException(mismatchTip, TypeException.BAD_REQUEST);
        }

        if (SecurityContextHolder.getContext().isAuthenticated()) {
            throw new ZivException("You have been logged in, do not log in repeatedly please", TypeException.BAD_REQUEST);
        }

        return buildAuthToken(user);
    }

    @NonNull
    private AuthToken buildAuthToken(@NonNull User user) {
        Assert.notNull(user, "User must not be null");

        // Generate new token
        AuthToken token = new AuthToken();

        token.setAccessToken(ZivUtils.randomUUIDWithoutDash());
        token.setExpiredIn(ACCESS_TOKEN_EXPIRED_SECONDS);
        token.setRefreshToken(ZivUtils.randomUUIDWithoutDash());

        return token;
    }

}
