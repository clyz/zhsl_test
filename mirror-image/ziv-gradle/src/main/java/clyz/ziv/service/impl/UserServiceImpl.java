package clyz.ziv.service.impl;

import clyz.ziv.entity.User;
import clyz.ziv.exception.base.TypeException;
import clyz.ziv.exception.base.ZivException;
import clyz.ziv.repository.UserRepository;
import clyz.ziv.repository.base.AbstractCrudService;
import clyz.ziv.service.UserService;
import clyz.ziv.utils.DateUtils;
import clyz.ziv.utils.ZivUtils;
import cn.hutool.crypto.digest.BCrypt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * UserService implementation class
 *
 * @author ryanwang
 * @date : 2019-03-14
 */
@Service
public class UserServiceImpl extends AbstractCrudService<User, Integer> implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void mustNotExpire(User user) {
        Assert.notNull(user, "User must not be null");

        Date now = DateUtils.now();
        if (user.getExpireTime() != null && user.getExpireTime().after(now)) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds(user.getExpireTime().getTime() - now.getTime());
            throw new ZivException("You have been temporarily disabled，please try again " + ZivUtils.timeFormat(seconds) + " later",TypeException.FORBIDDEN).setErrorData(seconds);
        }
    }

    @Override
    public boolean passwordMatch(User user, String plainPassword) {
        Assert.notNull(user, "User must not be null");
        return !StringUtils.isBlank(plainPassword) && BCrypt.checkpw(plainPassword, user.getPassword());
    }

    @Override
    public User getByUsernameOfNonNull(String username) {
        return getByUsername(username).orElseThrow(() -> new ZivException("The username dose not exist", TypeException.NOT_FOUND).setErrorData(username));
    }


    @Override
    public User getByEmailOfNonNull(String email) {
        return getByEmail(email).orElseThrow(() -> new ZivException("The email dose not exist", TypeException.NOT_FOUND).setErrorData(email));
    }

}
