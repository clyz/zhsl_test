package clyz.ziv.service;

import clyz.ziv.params.LoginParam;
import clyz.ziv.security.token.AuthToken;
import org.springframework.lang.NonNull;


public interface AdminService {

    int ACCESS_TOKEN_EXPIRED_SECONDS = 24 * 3600;

    int REFRESH_TOKEN_EXPIRED_DAYS = 30;

    String ACCESS_TOKEN_CACHE_PREFIX = "halo.admin.access_token.";

    String REFRESH_TOKEN_CACHE_PREFIX = "halo.admin.refresh_token.";

    @NonNull
    AuthToken authenticate(@NonNull LoginParam loginParam);
}
