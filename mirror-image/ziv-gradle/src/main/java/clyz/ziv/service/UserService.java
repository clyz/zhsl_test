package clyz.ziv.service;

import clyz.ziv.entity.User;
import clyz.ziv.repository.base.CrudService;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Optional;

public interface UserService extends CrudService<User, Integer> {

    @NonNull
    User getByUsernameOfNonNull(@NonNull String username);

    @NonNull
    User getByEmailOfNonNull(@NonNull String email);

    @NonNull
    Optional<User> getByEmail(String email);

    @NonNull
    Optional<User> getByUsername(String username);

    void mustNotExpire(@NonNull User user);

    boolean passwordMatch(@NonNull User user, @Nullable String plainPassword);
}
