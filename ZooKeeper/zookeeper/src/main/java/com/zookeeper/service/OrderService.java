package com.zookeeper.service;

import com.zookeeper.entity.Order;
import com.zookeeper.entity.Product;
import com.zookeeper.mapper.IOrderMapper;
import com.zookeeper.mapper.IProductMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.concurrent.TimeUnit;


@Service
@AllArgsConstructor
public class OrderService {
    private final IProductMapper IProductMapper;
    private final IOrderMapper IOrderMapper;


    @Transactional
    public void reduceStock(Integer id) {
        // 1.    获取库存
        Product product = IProductMapper.getProduct(id);

        // 模拟耗时业务处理
        sleep(1000); // 其他业务处理

        if (product.getStock() <= 0) {
            throw new RuntimeException("out of stock");
        }

        // 2.    减库存
        int i = IProductMapper.deductStock(id);
        if (i == 1) {
            Order order = new Order();
            order.setUserId(UUID.randomUUID().toString());
            order.setPid(id);
            IOrderMapper.insert(order);
        } else {
            throw new RuntimeException("deduct stock fail, retry.");
        }

    }

    /**
     * 模拟耗时业务处理
     *
     * @param wait
     */
    public void sleep(long wait) {
        try {
            TimeUnit.MILLISECONDS.sleep(wait);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
