package com.zookeeper.controller;

import com.zookeeper.service.OrderService;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.recipes.locks.InterProcessReadWriteLock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 *
 */
@RestController
public class ZooKeeperController {
    private final CuratorFramework curatorFramework;
    private final OrderService orderService;
    @Value("${server.port}")
    private String port;

    public ZooKeeperController(CuratorFramework curatorFramework, OrderService orderService) {
        this.curatorFramework = curatorFramework;
        this.orderService = orderService;
    }

    @PostMapping("/stock/deduct")
    public Object reduceStock(Integer id) {
        try {
            orderService.reduceStock(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ok:" + port;
    }

    @PostMapping("/stock/deductLock")
    public Object deductLock(Integer id) throws Exception {
        //ZooKeeper
        // ephemeral -seq
        InterProcessMutex lock = new InterProcessMutex(curatorFramework, "/product_" + id);
        try {
            lock.acquire(); // 加锁
            orderService.reduceStock(id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.release();
        }
        return "ok:" + port;
    }


    @GetMapping("/update")
    public String update(String id, Integer sec) {
        long start = System.currentTimeMillis();
        InterProcessReadWriteLock i = new InterProcessReadWriteLock(curatorFramework, "/" + id);
        InterProcessMutex writeLock = i.writeLock();
        try {
            writeLock.acquire();
            TimeUnit.SECONDS.sleep(sec);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                writeLock.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long end = System.currentTimeMillis();
        return "ok:" + (end - start) / 1000;
    }

    @GetMapping("/get")
    public String update(String id, int sec) {
        long start = System.currentTimeMillis();
        InterProcessReadWriteLock i = new InterProcessReadWriteLock(curatorFramework, "/" + id);
        InterProcessMutex readLock = i.readLock();
        try {
            readLock.acquire();
            TimeUnit.SECONDS.sleep(sec);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                readLock.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();

        return "ok:" + (end - start) / 1000;
    }


}
