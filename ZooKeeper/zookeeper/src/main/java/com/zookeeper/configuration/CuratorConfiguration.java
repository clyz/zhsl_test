package com.zookeeper.configuration;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ZooKeeper Client Configuration
 *
 * @author Zemin.Yang
 */
@Configuration
public class CuratorConfiguration {

    /**
     * @return ZooKeeper 客服端，启动时调用
     * @see CuratorFramework#start() 开始
     */
    @Bean(initMethod = "start")
    public CuratorFramework curatorFramework() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", retryPolicy);
        return client;
    }

    @Bean
    public IdentifierGenerator identifierGenerator(){
        return new DefaultIdentifierGenerator();
    }

}
