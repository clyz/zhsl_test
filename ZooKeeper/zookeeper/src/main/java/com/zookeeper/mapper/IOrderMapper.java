package com.zookeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zookeeper.entity.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrderMapper extends BaseMapper<Order> {

}
