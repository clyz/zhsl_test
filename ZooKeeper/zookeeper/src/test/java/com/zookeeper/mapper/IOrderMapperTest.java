package com.zookeeper.mapper;

import com.baomidou.mybatisplus.test.autoconfigure.MybatisPlusTest;
import com.zookeeper.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import java.util.UUID;

@ExtendWith(value = {SpringExtension.class})
@Slf4j
@MybatisPlusTest
@Rollback(value = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class IOrderMapperTest {
    @Autowired
    private IOrderMapper mapper;

    @Test
    void save() {
        Order order = new Order();
        order.setPid(1);
        order.setUserId(UUID.randomUUID().toString());
        int insert = mapper.insert(order);
        Assert.isTrue(insert == 1, "OK");
    }
}