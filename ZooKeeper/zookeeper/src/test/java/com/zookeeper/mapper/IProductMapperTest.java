package com.zookeeper.mapper;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.test.autoconfigure.MybatisPlusTest;
import com.zookeeper.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

@ExtendWith(value = {SpringExtension.class})
@Slf4j
@MybatisPlusTest
@Rollback(value = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class IProductMapperTest {
    @Autowired
    private IProductMapper productMapper;
    @Autowired
    private IOrderMapper orderMapper;

    @Test
    void init() {
        productMapper.delete(Wrappers.emptyWrapper());
        orderMapper.delete(Wrappers.emptyWrapper());
        insert();
    }

    @Test
    public void insert() {
        Product product = new Product();
        product.setId(1);
        product.setProductName("西瓜");
        product.setStock(5);
        product.setVersion(1);
        int insert = productMapper.insert(product);
        Assert.isTrue(insert == 1, "ok");
    }

    @Test
    public void selectById() {
        Product product = productMapper.selectById(1);
        log.info("{}", product);
        Assert.notNull(product, "ok");
    }
}