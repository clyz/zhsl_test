# Docker

## 常用命令

### 进入镜像

```bash
docker run -t -i [容器ID/容器name] /bin/bash
```

​	exec代表使用docker运行容器里面的某一个命令 这个命令可以执行bash alpine这个系统中默认没有bash。
​	-it表示已交互式方式执行。

### 退出镜像

​	ctrl+d可以直接退出容器 

### 拷贝文件

向容器中拷贝

```shell
docker pc [系统_path] [容器ID/容器name]:[容器_path]
```

向系统中拷贝

```shell
docker pc [容器_path] [容器ID/容器name]:[系统_path]
```

### VIM 使用

#### 	安装

```shell
apt-get update
apt-get install vim
```





