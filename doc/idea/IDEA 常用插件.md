# IDEA 常用插件

## Mybatis Log Plugin

​	用于查看 Mybatis 执行的 SQL。

##  Grep Console

​	让日志有颜色类别，不同的日志类别有不同的颜色。

## FindBugs-IDEA

​	寻找代码中的BUG。

## RestfulToolkit

1. 根据 URL 直接跳转到对应的方法定义 ( Ctrl \ or Ctrl Alt N );
2. 提供了一个 Services tree 的显示窗口;

   3. 一个简单的 http 请求工具;
    4. 在请求方法上添加了有用功能: 复制生成 URL;,复制方法参数...
   5. 其他功能: java 类上添加 Convert to JSON 功能，格式化 json 数据 ( Windows: Ctrl + Enter; Mac: Command + Enter )

## Maven Helper

​	分析依赖冲突插件

## Codota

​	可查询代码案例。

## Material Theme UI

​	美化

## Iedis

​	redis 可视化界面

## Alibaba Java Coding Guidelines

​	阿里代码规约检测

