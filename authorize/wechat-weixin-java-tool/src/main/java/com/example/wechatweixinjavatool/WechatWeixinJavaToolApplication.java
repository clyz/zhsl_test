package com.example.wechatweixinjavatool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WechatWeixinJavaToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(WechatWeixinJavaToolApplication.class, args);
    }

}
