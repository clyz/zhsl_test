package com.example.wechatweixinjavatool.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMenuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公众号 菜单
 *
 * @author Zemin.Yang
 * @date 2020/4/29 10:11
 */
@RestController
@RequestMapping("/wx/runtime/gwx/manger/")
@AllArgsConstructor
@Slf4j
@Api(tags = "菜单")
public class GwxMenuController {

    private WxMpMenuService wxMpMenuService;

    @GetMapping("menuCreate")
    @ApiOperation(value = "创建菜单", notes = "连接 公众号 的 认证对比接口")
    public String menuCreate(@RequestBody WxMenu wxMenu) throws WxErrorException {
        log.info("Request  menuCreate {}",wxMenu);
        return wxMpMenuService.menuCreate(wxMenu);
    }


}
