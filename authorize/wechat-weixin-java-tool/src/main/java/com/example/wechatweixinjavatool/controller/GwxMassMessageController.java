package com.example.wechatweixinjavatool.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMassMessageService;
import me.chanjar.weixin.mp.api.WxMpMenuService;
import me.chanjar.weixin.mp.bean.WxMpMassPreviewMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static com.example.wechatweixinjavatool.conts.GwxConts.OPEN_ID;

/**
 * 公众号 菜单
 *
 * @author Zemin.Yang
 * @date 2020/4/29 10:11
 */
@RestController
@RequestMapping("/wx/runtime/gwx/manger/")
@AllArgsConstructor
@Slf4j
@Api(tags = "MassMessage")
public class GwxMassMessageController {

    private WxMpMenuService wxMpMenuService;
    private WxMpMassMessageService wxMpMassMessageService;

    @GetMapping("massMessagePreviewText")
    @ApiOperation(value = "创建菜单", notes = "连接 公众号 的 认证对比接口")
    public void massMessagePreviewText() throws WxErrorException, IOException {
        // 发送群发消息
        WxMpMassPreviewMessage massMessage = new WxMpMassPreviewMessage();
        massMessage.setMsgType(WxConsts.MassMsgType.TEXT);
        massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:Hello World");
        massMessage.setToWxUserOpenid(OPEN_ID);
        log.info("{}",massMessage);
        wxMpMassMessageService.massMessagePreview(massMessage);
    }


}
