package com.example.wechatweixinjavatool.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.UUID;

import static com.example.wechatweixinjavatool.conts.GwxConts.USER_INFO_URL;

/**
 * 公众号 认证
 *
 * @author Zemin.Yang
 * @date 2020/4/29 10:11
 */
@RestController
@RequestMapping("/wx/runtime/gwx/manger/")
@AllArgsConstructor
@Slf4j
@Api(tags = "认证")
public class GwxAuthorizeController {

    private WxMpService wxMpService;


    @GetMapping("authorize")
    @ApiOperation(value = "认证", notes = "连接 公众号 的 认证对比接口")
    public void authorize(@ApiIgnore HttpServletRequest request, @ApiIgnore HttpServletResponse response) {
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");

        boolean flag = wxMpService.checkSignature(timestamp, nonce, signature);

        try (PrintWriter out = response.getWriter()) {
            if (flag) {
                out.print(echostr);
            }
        } catch (IOException e) {
            log.error("微信授权连接失败", e);
        }
    }

    @GetMapping("oauth2")
    @ApiOperation("用户授权")
    public ModelAndView authorize(String state) throws Exception {
        log.info("Request userInfo {}", state);

        String redirectUrl = wxMpService.oauth2buildAuthorizationUrl(USER_INFO_URL, WxConsts.OAuth2Scope.SNSAPI_BASE,
                URLEncoder.encode(UUID.randomUUID().toString()));

        log.info("Request redirectUrl {}", redirectUrl);

        return new ModelAndView("redirect:" + redirectUrl);
    }

    @GetMapping("userInfo")
    @ApiOperation("获取用户信息")
    @ResponseBody
    public WxMpUser userInfo(@RequestParam("code") String code, @RequestParam("state") String state) throws Exception {
        log.info("Request userInfo {}", Arrays.asList(code, state));
        WxMpOAuth2AccessToken accessToken = wxMpService.oauth2getAccessToken(code);
        final WxMpOAuth2AccessToken auth2AccessToken = wxMpService.oauth2refreshAccessToken(accessToken.getRefreshToken());
        log.info(auth2AccessToken.toString());
        return wxMpService.oauth2getUserInfo(auth2AccessToken, "zh_CN");
    }


}
