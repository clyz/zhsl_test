package com.example.wechatweixinjavatool.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zemin.Yang
 * @date 2020/4/30 10:38
 */
@RestController
public class TxtController {
    @GetMapping("MP_verify_dinb5JSsLk3XQa8d.txt")
    public String txt(){
        return "dinb5JSsLk3XQa8d";
    }
}
