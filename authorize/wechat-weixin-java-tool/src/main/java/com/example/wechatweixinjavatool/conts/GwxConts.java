package com.example.wechatweixinjavatool.conts;

import java.util.Arrays;
import java.util.List;

/**
 * 公众号
 *
 * @author Zemin.Yang
 * @date 2020/4/29 10:40
 */
public class GwxConts {
    public final static String USER_INFO_URL = "http://6bibjr.natappfree.cc/wx/runtime/gwx/manger/userInfo";

    public final static String MENU = "{\n" +
            "  \"button\": [\n" +
            "    {\n" +
            "      \"type\": \"click\",\n" +
            "      \"name\": \"今日歌曲\",\n" +
            "      \"key\": \"V1001_TODAY_MUSIC\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"菜单\",\n" +
            "      \"sub_button\": [\n" +
            "        {\n" +
            "          \"type\": \"view\",\n" +
            "          \"name\": \"搜索\",\n" +
            "          \"url\": \"http://www.soso.com/\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"type\": \"miniprogram\",\n" +
            "          \"name\": \"wxa\",\n" +
            "          \"url\": \"http://mp.weixin.qq.com\",\n" +
            "          \"appid\": \"wx286b93c14bbf93aa\",\n" +
            "          \"pagepath\": \"pages/lunar/index.html\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"type\": \"click\",\n" +
            "          \"name\": \"赞一下我们\",\n" +
            "          \"key\": \"V1001_GOOD\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public final static String MENU_FILE_PATH = "C:\\service_java\\ZHSL\\zhsl_test\\authorize\\wechat-weixin-java-tool\\src\\main\\resources\\static\\menu.json";

    public final static String GIF_PATH = "C:\\Users\\Administrator\\Pictures\\1243.gif";

    public final static String JPG_PATH="C:\\Users\\Administrator\\Pictures\\鼻子\\aa.jpg";

    /*测试时要用到 openId 从 用户中获取到的*/
    public final static String OPEN_ID="omswo5wl1bcbqK_IvESZVtHeSuEw";

    public final static List OPEN_IDS = Arrays.asList("omswo55_jdP79GCqxQg0GpZX43NA","omswo5zWgrS6iZcx-WN00SCSEdCw","omswo5zuFCX0_GCrVQAtEvbojCPo","omswo5x84g81BAdWrcCLBj5gbMq8","omswo5zbVZYvLWTPzORmM-1UsX1k","omswo5_CfKjzQa-vN6k0wE0qSRQE","omswo5045vQg3_4vM1voWzZ0X9Ww","omswo57PR2dM9ib564ffHg6EUCFI","omswo5y2oJi-nu_cZdislyqTBViw","omswo538Y54qX-0eNFslcKSb-668","omswo5y9PpHERffB-Jfb1FNjrixA","omswo54SqrpAHxhnOySMgWZ02n24","omswo53yZxk3l7NycC8A60MKbrl8","omswo53bodsNDy0fft9dHdaf8mY4","omswo5xvBUtsKgAE7bYcegtz5Z7w","omswo55b9ch41hzSBgSWyJY-T21g","omswo5zI4kz2PTV-F6wfCpuZe1bM","omswo501YKcMNAtQIP41spmBNns4","omswo5xOmnuKcXqsXGSoaGYSzMRw","omswo5wl1bcbqK_IvESZVtHeSuEw");

}
