package com.example.wechatweixinjavatool.config;

import me.chanjar.weixin.mp.api.*;
import me.chanjar.weixin.mp.api.impl.*;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Zemin.Yang
 * @date 2020/4/29 9:59
 */
@Configuration
public class WeChatConfig {

    @Bean
    public WxMpConfigStorage wxMpConfigStorage() {
        final WxMpDefaultConfigImpl defaultConfig = new WxMpDefaultConfigImpl();
        //  Zemin 账号
        /*defaultConfig.setAppId("wx3e0daea92664a5ea");
        defaultConfig.setAesKey("KPnK1cIGrs2deDihs0MgfJjI5qt3H8DpjCn1ugCSK05");
        defaultConfig.setSecret("97d08597bf627a2db70ddf296f8bf489");*/

        //  服务账号
        defaultConfig.setAppId("wx3e0daea92664a5ea");
//        defaultConfig.setAesKey("KPnK1cIGrs2deDihs0MgfJjI5qt3H8DpjCn1ugCSK05");
        defaultConfig.setSecret("97d08597bf627a2db70ddf296f8bf489");


        defaultConfig.setToken("zh2019sl");
        return defaultConfig;
    }

    @Bean
    public WxMpService wxMpService() {
        final WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }

    @Bean
    public WxMpMenuService wxMpMenuService() {
        final WxMpMenuService menuService = new WxMpMenuServiceImpl(wxMpService());
        return menuService;
    }

    /**
     * 群发消息服务类.
     */
    @Bean
    public WxMpMassMessageService wxMpMassMessageService() {
        final WxMpMassMessageServiceImpl wxMpMassMessageService = new WxMpMassMessageServiceImpl(wxMpService());
        return wxMpMassMessageService;
    }

    /**
     * 媒体库
     */
    @Bean
    public WxMpMaterialService wxMpMaterialService() {
        final WxMpMaterialService wxMpMaterialService = new WxMpMaterialServiceImpl(wxMpService());
        return wxMpMaterialService;
    }

    /**
     * 用户
     * @return
     */
    @Bean
    public WxMpUserService wxMpUserService(){
        final WxMpUserServiceImpl wxMpUserService = new WxMpUserServiceImpl(wxMpService());
        return wxMpUserService;
    }

    /**
     * 用户标签
     */
    @Bean
    public WxMpUserTagService wxMpUserTagService(){
        final WxMpUserTagService wxMpUserService = new WxMpUserTagServiceImpl(wxMpService());
        return wxMpUserService;
    }

}
