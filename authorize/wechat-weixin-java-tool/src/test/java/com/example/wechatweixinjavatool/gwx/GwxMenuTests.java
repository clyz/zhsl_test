package com.example.wechatweixinjavatool.gwx;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMenuService;
import me.chanjar.weixin.mp.api.WxMpService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Slf4j
class GwxMenuTests {

    @Autowired
    private WxMpMenuService wxMpMenuService;

    @Test
    void menuCreate() throws WxErrorException {
        WxMenu menu = createMenu();
        log.info("Menu {}", menu);
        final String s = wxMpMenuService.menuCreate(menu);
        log.info(s);
    }

    private WxMenu createMenu() {
        final WxMenu wxMenu = new WxMenu();
        /*final WxMenuRule menuRule = new WxMenuRule();
        wxMenu.setMatchRule(menuRule);*/
        List<WxMenuButton> buttons = new ArrayList<>();

        final WxMenuButton menuButton = new WxMenuButton();
        menuButton.setType("click");
        menuButton.setName("今日歌曲");
        menuButton.setKey("V1001_TODAY_MUSIC");
        buttons.add(menuButton);

        final WxMenuButton menuButton1 = new WxMenuButton();
        menuButton1.setType("view");
        menuButton1.setName("搜索");
        menuButton1.setUrl("http://www.soso.com/");
        buttons.add(menuButton1);
        wxMenu.setButtons(buttons);
        return wxMenu;

    }

}
