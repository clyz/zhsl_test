package com.example.wechatweixinjavatool.gwx;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpUserService;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Zemin.Yang
 * @date 2020/4/29 13:23
 */
@SpringBootTest
@Slf4j
public class GwxUserTests {
    @Autowired
    private WxMpUserService wxMpUserService;

    @Test
    void listUser() throws WxErrorException {
        final WxMpUserList wxMpUserList = wxMpUserService.userList(null);
        log.info(wxMpUserList.toString());
    }
}
