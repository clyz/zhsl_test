package com.example.wechatweixinjavatool.gwx;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpUserService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class GwxAuthorizeTests {

    @Autowired
    private WxMpService wxMpService;
    @Autowired
    private WxMpUserService userService;

    @Test
    void userInfo() throws WxErrorException {
        WxMpUser user = userService.userInfo("oqNBds3vjb5HGVJTNH8umjKoX2jk");
        log.info(user.toString());
        log.info(JSONUtil.toJsonStr(user));
    }

    @Test
    void infoUser() throws WxErrorException {
        WxMpOAuth2AccessToken accessToken = wxMpService.oauth2getAccessToken("1");
        final WxMpOAuth2AccessToken auth2AccessToken = wxMpService.oauth2refreshAccessToken(accessToken.getRefreshToken());
        log.info(auth2AccessToken.toString());
        final WxMpUser user = wxMpService.oauth2getUserInfo(auth2AccessToken, "zh_CN");
        log.info(user.toString());
    }

}
