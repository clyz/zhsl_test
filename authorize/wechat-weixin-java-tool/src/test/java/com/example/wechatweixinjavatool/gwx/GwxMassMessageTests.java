package com.example.wechatweixinjavatool.gwx;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.result.WxMediaUploadResult;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMassMessageService;
import me.chanjar.weixin.mp.api.WxMpMaterialService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpUserTagService;
import me.chanjar.weixin.mp.bean.WxMpMassNews;
import me.chanjar.weixin.mp.bean.WxMpMassOpenIdsMessage;
import me.chanjar.weixin.mp.bean.WxMpMassPreviewMessage;
import me.chanjar.weixin.mp.bean.WxMpMassTagMessage;
import me.chanjar.weixin.mp.bean.result.WxMpMassSendResult;
import me.chanjar.weixin.mp.bean.result.WxMpMassUploadResult;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;

import static com.example.wechatweixinjavatool.conts.GwxConts.JPG_PATH;
import static com.example.wechatweixinjavatool.conts.GwxConts.OPEN_ID;

@SpringBootTest
@Slf4j
class GwxMassMessageTests {

    @Autowired
    private WxMpService wxMpService;
    @Autowired
    private WxMpMassMessageService wxMpMassMessageService;
    @Autowired
    private WxMpMaterialService wxMpMaterialService;
    @Autowired
    private WxMpConfigStorage wxMpConfigStorage;
    @Autowired
    private WxMpUserTagService wxMpUserTagService;

    /**
     * 上传 要发送的内容
     */
    @Test
    void massNewsUpload() throws WxErrorException, IOException {
        // 上传照片到媒体库
        InputStream inputStream = new FileInputStream(Paths.get(JPG_PATH).toFile());
        WxMediaUploadResult uploadMediaRes = wxMpMaterialService.mediaUpload(WxConsts.MediaFileType.IMAGE, "jpg", inputStream);

        // 上传图文消息
        WxMpMassNews news = new WxMpMassNews();
        WxMpMassNews.WxMpMassNewsArticle article1 = new WxMpMassNews.WxMpMassNewsArticle();
        article1.setTitle("标题1");
        article1.setContent("内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1");
        article1.setThumbMediaId(uploadMediaRes.getMediaId());
        news.addArticle(article1);

        /*WxMpMassNews.WxMpMassNewsArticle article2 = new WxMpMassNews.WxMpMassNewsArticle();
        article2.setTitle("标题2");
        article2.setContent("内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2内容2");
//        article2.setThumbMediaId(uploadMediaRes.getMediaId());
        article2.setShowCoverPic(true);
        article2.setAuthor("作者2");
        article2.setContentSourceUrl("www.baidu.com");
        article2.setDigest("摘要2");
        news.addArticle(article2);*/

        WxMpMassUploadResult massUploadResult = wxMpMassMessageService.massNewsUpload(news);

        log.info(massUploadResult.toString());
        assert inputStream != null;
        inputStream.close();
    }

    @Test
    void massOpenIdsMessageSend() throws WxErrorException, IOException {
        InputStream inputStream = new FileInputStream(Paths.get(JPG_PATH).toFile());
        WxMediaUploadResult uploadMediaRes = wxMpMaterialService.mediaUpload(WxConsts.MediaFileType.IMAGE, "jpg", inputStream);

        // 上传图文消息
        WxMpMassNews news = new WxMpMassNews();
        WxMpMassNews.WxMpMassNewsArticle article1 = new WxMpMassNews.WxMpMassNewsArticle();
        article1.setTitle("标题1");
        article1.setContent("内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1内容1");
        article1.setThumbMediaId(uploadMediaRes.getMediaId());
        article1.setAuthor("Zemin.Yang");
        news.addArticle(article1);
        WxMpMassUploadResult massUploadResult = wxMpMassMessageService.massNewsUpload(news);

        log.info(massUploadResult.toString());
        assert inputStream != null;
        inputStream.close();

        // 发送群发消息
        WxMpMassOpenIdsMessage massMessage = new WxMpMassOpenIdsMessage();
        /*massMessage.setMsgType(WxConsts.MassMsgType.IMAGE);
        massMessage.setMediaId(massUploadResult.getMediaId());
        massMessage.setContent("xx");*/

        massMessage.setMsgType(WxConsts.MassMsgType.TEXT);
        massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:" );

        massMessage.getToUsers().add(OPEN_ID);
        massMessage.getToUsers().add("");
        WxMpMassSendResult massResult = wxMpMassMessageService.massOpenIdsMessageSend(massMessage);
    }

    @Test
    void massOpenIdsMessageSendTEXT() throws WxErrorException {
        WxMpMassOpenIdsMessage massMessage = new WxMpMassOpenIdsMessage();
        massMessage.setMsgType(WxConsts.MassMsgType.TEXT);
        massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:<a href=\"http://www.baidu.com\">Hello World</a>");
        massMessage.getToUsers().add(OPEN_ID);
        massMessage.getToUsers().add("");
        WxMpMassSendResult massResult = wxMpMassMessageService.massOpenIdsMessageSend(massMessage);
        log.info(massResult.toString());
    }

    @Test
    void massGroupMessageSend() throws WxErrorException {
        WxMpMassTagMessage massMessage = new WxMpMassTagMessage();
        massMessage.setMsgType(WxConsts.MassMsgType.TEXT);
        massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:Hello World");
        final List<WxUserTag> userTags = wxMpUserTagService.tagGet();

        log.info("{}",userTags);

        massMessage.setTagId(userTags.get(0).getId());

        wxMpMassMessageService.massGroupMessageSend(massMessage);
    }

    /**
     * 开发 发送 消息 预览接口
     */
    @Test
    void massMessagePreviewText() throws WxErrorException, IOException {
        // 发送群发消息
        WxMpMassPreviewMessage massMessage = new WxMpMassPreviewMessage();
        massMessage.setMsgType(WxConsts.MassMsgType.TEXT);
        massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:Hello World");
        massMessage.setToWxUserOpenid(OPEN_ID);

        log.info("{}",massMessage);

        wxMpMassMessageService.massMessagePreview(massMessage);
    }

    @Test
    void massMessagePreviewImage() throws WxErrorException, IOException {
        InputStream inputStream = new FileInputStream(Paths.get(JPG_PATH).toFile());
        WxMediaUploadResult uploadMediaRes = wxMpMaterialService.mediaUpload(WxConsts.MediaFileType.IMAGE, "jpg", inputStream);

        // 上传图文消息
        WxMpMassNews news = new WxMpMassNews();
        WxMpMassNews.WxMpMassNewsArticle article1 = new WxMpMassNews.WxMpMassNewsArticle();
        article1.setTitle("标题1");
        article1.setContent("内容1内容1内容1内容1内容");
        article1.setThumbMediaId(uploadMediaRes.getMediaId());
        article1.setAuthor("Zemin.Yang");
        news.addArticle(article1);
        WxMpMassUploadResult massUploadResult = wxMpMassMessageService.massNewsUpload(news);

        log.info(massUploadResult.toString());
        assert inputStream != null;
        inputStream.close();

        // 发送群发消息
        WxMpMassPreviewMessage massMessage = new WxMpMassPreviewMessage();
        massMessage.setMsgType(WxConsts.MassMsgType.IMAGE);
        massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:Hello World");
        massMessage.setMediaId(massUploadResult.getMediaId());
        massMessage.setToWxUserOpenid(OPEN_ID);

        log.info("{}",massMessage);

        wxMpMassMessageService.massMessagePreview(massMessage);
    }

}
