package zlsy.ziv.authorize.wechatpublicaddress.util;

import lombok.extern.java.Log;
import org.junit.Test;
import zlsy.ziv.authorize.wechatpublicaddress.bean.AccessToken;

@Log
public class JsonMapperTest {

    private String json = "{\"access_token\":\"27_3EfndYvDISKt0KT3reAOmPm6mRmYhN28UB3cDPlMY0h4bmWVC4lt_tobonDAt453Yrney1ULythKQ4THI_d4bjsXXKtWNZXXyFBt7U5ZHOM\",\"expires_in\":7200,\"refresh_token\":\"27_Ci0w9MubvFnW1eJ1ZpGb6Ot7cJZhkpaOKTUuIWYIPR89HUwiWhw1cRhzvEt5PO8HWqjnVBYsWadwaZS7sL0J9qnK3ap342FTeYydguzT5Q0\",\"openid\":\"omswo5wl1bcbqK_IvESZVtHeSuEw\",\"scope\":\"snsapi_userinfo\"}";

    @Test
    public void objectToJson() {
    }

    @Test
    public void objectToJsonPretty() {
    }


    @Test
    public void jsonToObject() {
        final AccessToken accessToken = JsonMapper.jsonToObject(json, AccessToken.class);
        log.info(accessToken.toString());
    }

    @Test
    public void jsonToObject1() {
    }

    @Test
    public void jsonToObject2() {
    }
}
