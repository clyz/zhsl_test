package zlsy.ziv.authorize.wechatpublicaddress.bean;

import lombok.Data;

/**
 * {
 * "openid":" OPENID",
 * " nickname": NICKNAME,
 * "sex":"1",
 * "province":"PROVINCE"
 * "city":"CITY",
 * "country":"COUNTRY",
 * "headimgurl":       "http://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46",
 * "privilege":[ "PRIVILEGE1" "PRIVILEGE2"     ],
 * "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
 * }
 *
 * @author YangZemin
 * @date 2019/11/29 18:01
 */
@Data
public class UserInfo {
    private String openid;
    private String nickname;
    private String sex;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private String[] privilege;
    private String unionid;

}
