package zlsy.ziv.authorize.wechatpublicaddress.bean;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author YangZemin
 * @date 2019/11/29 17:00
 */
@ConfigurationProperties(prefix = "wx")
@Component
@Data
public class WeChat {

    private String appId;

    private String redirectUri;

    private String scope;

    private String responseType;

    private String appSecret;
}
