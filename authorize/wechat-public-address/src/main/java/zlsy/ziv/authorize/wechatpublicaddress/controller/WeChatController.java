package zlsy.ziv.authorize.wechatpublicaddress.controller;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import zlsy.ziv.authorize.wechatpublicaddress.bean.AccessToken;
import zlsy.ziv.authorize.wechatpublicaddress.bean.RefreshToken;
import zlsy.ziv.authorize.wechatpublicaddress.bean.UserInfo;
import zlsy.ziv.authorize.wechatpublicaddress.bean.WeChat;
import zlsy.ziv.authorize.wechatpublicaddress.util.JsonMapper;

/**
 * 微信 公众号 授权
 *
 * @author YangZemin
 * @date 2019/11/29 16:53
 */
@Controller
@Log
@RequestMapping("/wx")
public class WeChatController {

    @Autowired
    private WeChat wxChat;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/index")
    @ResponseBody
    public String index() {
        return "OK";
    }

    /**
     * 1.https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx49bca42252058294&redirect_uri=http://132.232.234.237/zentao/user-login-L3plbnRhby9wcm9qZWN0LXRhc2stMi1hc3NpZ25lZHRvbWUuaHRtbA==.html&response_type=code&scope=SCOPE&state=STATE&connect_redirect=1#wechat_redirect
     */
    @GetMapping("/login/{state}")
    public String login(@PathVariable("state") String state, String r) {
        final String url = "redirect:https://open.weixin.qq.com/connect/oauth2/authorize?" +
                "appid=" + wxChat.getAppId() +
                "&redirect_uri=" + ((r == null || "".equals(r.trim())) ? wxChat.getRedirectUri() : r) + "/accessToken" +
                "&response_type=code" +
                "&scope=" + wxChat.getScope() +
                "&state=" + state +
                "&connect_redirect=1#wechat_redirect";
        log.info(url);
        return url;
    }

    /**
     * {
     * "access_token":"ACCESS_TOKEN",
     * "expires_in":7200,
     * "refresh_token":"REFRESH_TOKEN",
     * "openid":"OPENID",
     * "scope":"SCOPE"
     * }
     * <p>
     * 页面将跳转至 redirect_uri/?code=CODE&state=STATE
     * https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
     */
    @GetMapping("/accessToken")
    @ResponseBody
    public String accessToken(String code, String state) {
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

        HttpEntity<AccessToken> entity = new HttpEntity<>(headers);

        String urlAccessToken = "https://api.weixin.qq.com/sns/oauth2/access_token?" +
                "appid=" + wxChat.getAppId() +
                "&secret=" + wxChat.getAppSecret() +
                "&code=" + code +
                "&grant_type=authorization_code";
        log.info(urlAccessToken);

//        AccessToken accessToken = restTemplate.getForObject(urlAccessToken,AccessToken.class);

        final String forObject = restTemplate.getForObject(urlAccessToken, String.class);
        log.info(forObject);

        AccessToken accessToken = JsonMapper.jsonToObject(forObject, AccessToken.class);

        String urlRefreshToken = "https://api.weixin.qq.com/sns/oauth2/refresh_token?" +
                "appid=" + wxChat.getAppId() +
                "&grant_type=refresh_token" +
                "&refresh_token=" + accessToken.getRefresh_token();

//        RefreshToken refreshToken = restTemplate.getForObject(urlRefreshToken,RefreshToken.class);
        log.info(urlRefreshToken);
        final String forObject1 = restTemplate.getForObject(urlRefreshToken, String.class);
        log.info(forObject1);

        final RefreshToken refreshToken = JsonMapper.jsonToObject(forObject1, RefreshToken.class);

        String urlUserInfo = "https://api.weixin.qq.com/sns/userinfo?" +
                "access_token=" + refreshToken.getAccess_token() +
                "&openid=" + refreshToken.getOpenid() +
                "&lang=zh_CN";

        log.info(urlUserInfo);
        final String forObject2 = restTemplate.getForObject(urlUserInfo, String.class);
        log.info(forObject2);
        final UserInfo userInfo = JsonMapper.jsonToObject(forObject2, UserInfo.class);

        /**
         * 检查 授权是否成功
         */
        final String urlAuth = "https://api.weixin.qq.com/sns/auth?access_token={0}&openid={1}";
        assert userInfo != null;
        final String forObject3 = restTemplate.getForObject(urlAuth, String.class, accessToken.getAccess_token(), userInfo.getOpenid());
        log.info(forObject3);
        return userInfo.getHeadimgurl();
    }

}