package zlsy.ziv.authorize.wechatpublicaddress.bean;

import lombok.Data;

/**
 * {
 * "access_token":"ACCESS_TOKEN",
 * "expires_in":7200,
 * "refresh_token":"REFRESH_TOKEN",
 * "openid":"OPENID",
 * "scope":"SCOPE"
 * }
 *
 * @author YangZemin
 * @date 2019/11/29 17:58
 */
@Data
public class RefreshToken {
    private String access_token;

    private String expires_in;

    private String refresh_token;

    private String openid;
    private String scope;

}
