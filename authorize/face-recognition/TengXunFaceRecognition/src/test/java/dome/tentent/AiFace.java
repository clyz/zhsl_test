package dome.tentent;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.iai.v20180301.IaiClient;
import com.tencentcloudapi.iai.v20180301.models.AnalyzeFaceRequest;
import com.tencentcloudapi.iai.v20180301.models.AnalyzeFaceResponse;
import com.tencentcloudapi.iai.v20180301.models.SearchFacesRequest;
import com.tencentcloudapi.iai.v20180301.models.SearchFacesResponse;
import dome.K;

public class AiFace {

    public static void main(String[] args) throws TencentCloudSDKException {
        searchFaces();
    }

    // 人脸搜索
    public static void searchFaces() throws TencentCloudSDKException {
        Credential cred = new Credential("AKIDDECwRgXRwBAyWoRmPnvnfIfmfIgoNV3N", "EF1YpuqWxXu2uWs30iCSSDiy4va0B7nN");

        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("iai.tencentcloudapi.com");

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);

        IaiClient client = new IaiClient(cred, "ap-chengdu", clientProfile);

        String params = "{\"GroupIds\":[\"g26WZAJNpKMbDHhIZO4\"],\"Url\":\"http://132.232.150.196/PCdemo/develop_use/48E2370B3F955BAE265DC4D2533C6319.jpg\",\"NeedPersonInfo\":1}";
        SearchFacesRequest req = SearchFacesRequest.fromJsonString(params, SearchFacesRequest.class);

        SearchFacesResponse resp = client.SearchFaces(req);
        System.out.println(SearchFacesRequest.toJsonString(resp));
    }


    public static void analyzeFace() throws TencentCloudSDKException {
        Credential cred = new Credential(K.SECRET_ID, K.SECRET_KEY);
        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint(K.ENDPOINT);
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        IaiClient client = new IaiClient(cred, K.AP_CHENGDU, clientProfile);
        String params = "{}";
        AnalyzeFaceRequest req = AnalyzeFaceRequest.fromJsonString(params, AnalyzeFaceRequest.class);
        AnalyzeFaceResponse resp = client.AnalyzeFace(req);
        System.out.println(AnalyzeFaceRequest.toJsonString(resp));
    }

}