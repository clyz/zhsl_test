package dome;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.cvm.v20170312.CvmClient;
import com.tencentcloudapi.cvm.v20170312.models.CreateImageRequest;
import com.tencentcloudapi.cvm.v20170312.models.CreateImageResponse;
import com.tencentcloudapi.cvm.v20170312.models.DescribeZonesRequest;
import com.tencentcloudapi.cvm.v20170312.models.DescribeZonesResponse;
import com.tencentcloudapi.iai.v20180301.IaiClient;
import com.tencentcloudapi.iai.v20180301.models.DetectFaceRequest;
import com.tencentcloudapi.iai.v20180301.models.DetectFaceResponse;

// 导入对应产品模块的client
// 导入要请求接口对应的request response类

public class DescribeZones {
    private Credential cred = new Credential("AKIDDECwRgXRwBAyWoRmPnvnfIfmfIgoNV3N", "EF1YpuqWxXu2uWs30iCSSDiy4va0B7nN");
    private CvmClient client = new CvmClient(cred, "ap-chengdu");

    public static void main(String[] args) throws TencentCloudSDKException {
        new DescribeZones().createImageRequest();
    }

    private void createImageRequest() throws TencentCloudSDKException {
        final CreateImageRequest createImageRequest = new CreateImageRequest();
        createImageRequest.setImageName("yangzemin");
        final CreateImageResponse createImageResponse = client.CreateImage(createImageRequest);
        System.out.println(DescribeZonesResponse.toJsonString(createImageResponse));
    }

    private void dome1() {
        try {// 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey
            Credential cred = new Credential("AKIDDECwRgXRwBAyWoRmPnvnfIfmfIgoNV3N", "EF1YpuqWxXu2uWs30iCSSDiy4va0B7nN");
            // 实例化要请求产品(以 CVM 为例)的 client 对象
            CvmClient client = new CvmClient(cred, "ap-chengdu");// 实例化一个请求对象
            DescribeZonesRequest req = new DescribeZonesRequest();
            // 通过 client 对象调用想要访问的接口，需要传入请求对象
            DescribeZonesResponse resp = client.DescribeZones(req);
            // 输出 JSON 格式的字符串回包
            System.out.println(DescribeZonesRequest.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }

    private void dome() {
        try {
            Credential cred = new Credential("AKIDDECwRgXRwBAyWoRmPnvnfIfmfIgoNV3N", "EF1YpuqWxXu2uWs30iCSSDiy4va0B7nN");
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("iai.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            IaiClient client = new IaiClient(cred, "ap-chengdu", clientProfile);
            String params = "{}";
            DetectFaceRequest req = DetectFaceRequest.fromJsonString(params, DetectFaceRequest.class);
            DetectFaceResponse resp = client.DetectFace(req);
            System.out.println(DetectFaceRequest.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }

    }

    public void dome2() {
        try {
            // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey
            Credential cred = new Credential("AKIDDECwRgXRwBAyWoRmPnvnfIfmfIgoNV3N", "EF1YpuqWxXu2uWs30iCSSDiy4va0B7nN");

            // 实例化要请求产品(以cvm为例)的client对象
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
            CvmClient client = new CvmClient(cred, "ap-guangzhou", clientProfile);

            // 实例化一个请求对象
            DescribeZonesRequest req = new DescribeZonesRequest();

            // 通过client对象调用想要访问的接口，需要传入请求对象
            DescribeZonesResponse resp = client.DescribeZones(req);

            // 输出json格式的字符串回包
            System.out.println(DescribeZonesRequest.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }
    }

}