package ziv.clyz.authorize.facerecognition.wechat.facerecognition;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import ziv.clyz.authorize.facerecognition.wechat.facerecognition.config.tencent.AiFaceBean;

@SpringBootTest
@ComponentScan
class TengXunFaceRecognitionApplicationTests {

    @Autowired
    private AiFaceBean aiFaceBean;

    @Test
    void contextLoads() {
        System.out.println(aiFaceBean);
    }

}
