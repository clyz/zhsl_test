package ziv.clyz.authorize.facerecognition.wechat.facerecognition.config.tencent;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;

/**
 * @author YangZemin
 * @date 2019/10/17 18:51
 */
@Component
@Data
@ConfigurationProperties(prefix = "sdk.ai.face")
public class AiFaceBean {
    private Charset charset;
    private String secretId;
    private String secretKey;
    private String ctJson;
}
