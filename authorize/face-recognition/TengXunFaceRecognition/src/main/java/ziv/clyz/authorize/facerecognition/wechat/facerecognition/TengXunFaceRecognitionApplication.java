package ziv.clyz.authorize.facerecognition.wechat.facerecognition;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.iai.v20180301.IaiClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ziv.clyz.authorize.facerecognition.wechat.facerecognition.util.K;

@SpringBootApplication
public class TengXunFaceRecognitionApplication {

    public static void main(String[] args) {
        SpringApplication.run(TengXunFaceRecognitionApplication.class, args);
    }

    @Bean
    public IaiClient iaiClient() {
        Credential cred = new Credential(K.SECRET_ID, K.SECRET_KEY);

        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint(K.ENDPOINT);

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);

        return new IaiClient(cred, K.AP_CHENGDU, clientProfile);
    }

}
