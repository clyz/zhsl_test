package ziv.clyz.authorize.facerecognition.wechat.facerecognition.controller;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.iai.v20180301.IaiClient;
import com.tencentcloudapi.iai.v20180301.models.CompareFaceRequest;
import com.tencentcloudapi.iai.v20180301.models.CompareFaceResponse;
import com.tencentcloudapi.iai.v20180301.models.SearchFacesRequest;
import com.tencentcloudapi.iai.v20180301.models.SearchFacesResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author YangZemin
 * @date 2019/10/18 9:32
 */
@RestController
@Api(value = "人脸AI 接口")
public class AiFaceController {
    @Autowired
    private IaiClient client;

    @GetMapping("compareFace")
    @ApiOperation("人脸对比")
    public CompareFaceResponse compareFace(@RequestBody CompareFaceRequest req) throws TencentCloudSDKException {
        return client.CompareFace(req);
    }

    @PostMapping("searchFaces")
    @ApiOperation("人脸搜索")
    public SearchFacesResponse searchFaces(@RequestBody SearchFacesRequest req) throws TencentCloudSDKException {
        return client.SearchFaces(req);
    }

    public static void main(String[] args) {
        try {

            Credential cred = new Credential("AKIDDECwRgXRwBAyWoRmPnvnfIfmfIgoNV3N", "EF1YpuqWxXu2uWs30iCSSDiy4va0B7nN");

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("iai.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            IaiClient client = new IaiClient(cred, "ap-chengdu", clientProfile);

            String params = "{\"GroupIds\":[\"MsujupdlO7oZOTdAYaN\"],\"Url\":\"http://132.232.150.196/PCdemo/develop_use/48E2370B3F955BAE265DC4D2533C6319.jpg\"}";
            SearchFacesRequest req = SearchFacesRequest.fromJsonString(params, SearchFacesRequest.class);

            SearchFacesResponse resp = client.SearchFaces(req);

            System.out.println(SearchFacesRequest.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }

    }

}
