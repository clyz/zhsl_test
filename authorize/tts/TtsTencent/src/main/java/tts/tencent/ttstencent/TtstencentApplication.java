package tts.tencent.ttstencent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TtstencentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TtstencentApplication.class, args);
    }

}
