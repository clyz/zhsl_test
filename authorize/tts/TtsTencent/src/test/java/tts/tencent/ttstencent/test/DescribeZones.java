package tts.tencent.ttstencent.test;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.tts.v20190823.TtsClient;
import com.tencentcloudapi.tts.v20190823.models.TextToVoiceRequest;
import com.tencentcloudapi.tts.v20190823.models.TextToVoiceResponse;
import sun.misc.BASE64Decoder;
import tts.tencent.ttstencent.basic.AsrBaseConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

// 导入对应产品模块的client
// 导入要请求接口对应的request response类

/**
 * @author YangZemin
 * @date 2020/4/9 10:00
 */
public class DescribeZones {
    public static void main(String[] args) throws TencentCloudSDKException, IOException {
        // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey
        Credential cred = new Credential(AsrBaseConfig.secretId, AsrBaseConfig.secretKey);

        // 实例化要请求产品(以cvm为例)的client对象
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
        TtsClient client = new TtsClient(cred, "ap-chengdu", clientProfile);

        final TextToVoiceRequest voiceRequest = new TextToVoiceRequest();
        voiceRequest.setText("清空大甩卖了，大甩卖，全场  ");
        voiceRequest.setModelType(1L);
        voiceRequest.setSessionId(UUID.randomUUID().toString());
        voiceRequest.setVoiceType(5L);
//        voiceRequest.setSpeed(1.0f);
        final TextToVoiceResponse voiceResponse = client.TextToVoice(voiceRequest);
        final File file = new File("suaimai.wav");
        file.createNewFile();
        generate(voiceResponse.getAudio(), file.getPath());


        /*for (int i = 0; i < 10; i++) {
            // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey
            Credential cred = new Credential(AsrBaseConfig.secretId, AsrBaseConfig.secretKey);

            // 实例化要请求产品(以cvm为例)的client对象
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
            TtsClient client = new TtsClient(cred, "ap-chengdu", clientProfile);

            final TextToVoiceRequest voiceRequest = new TextToVoiceRequest();
            voiceRequest.setText(i+"");
            voiceRequest.setModelType(1L);
            voiceRequest.setSessionId(UUID.randomUUID().toString());
            voiceRequest.setVoiceType(5L);
            voiceRequest.setSpeed(1.5f);
            final TextToVoiceResponse voiceResponse = client.TextToVoice(voiceRequest);
            final File file = new File(i+".wav");
            file.createNewFile();
            generate(voiceResponse.getAudio(), file.getPath());
        }*/
    }

    public static boolean generate(String imgStr, String path) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] b = decoder.decodeBuffer(imgStr);
        for (int i = 0; i < b.length; ++i) {
            if (b[i] < 0) {
                b[i] += 256;
            }
        }
        OutputStream out = new FileOutputStream(path);
        out.write(b);
        out.close();
        out.flush();
        return true;
    }
}
