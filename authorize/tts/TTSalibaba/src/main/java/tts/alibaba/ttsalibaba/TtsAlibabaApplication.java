package tts.alibaba.ttsalibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TtsAlibabaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TtsAlibabaApplication.class, args);
    }

}
