package workflow.flowable.controller;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
@Slf4j
@RunWith(SpringRunner.class)
public class ExpenseControllerTest {
    private MockMvc mockMvc;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private ProcessEngine processEngine;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(
                new ExpenseController(runtimeService, taskService, repositoryService, processEngine)
        ).build();
    }

    /**
     * @see ExpenseController#addExpense(String, Integer, String)
     */
    @Test
    public void addExpense() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/expense/add")
                .param("userid", "User-ID")
                .param("money", "12");
        final ResultActions perform = mockMvc.perform(requestBuilder);
        perform.andExpect(MockMvcResultMatchers.status().isOk());

        final MockHttpServletResponse response = perform.andReturn().getResponse();
        log.info(response.getContentAsString());
    }

    @Test
    public void list() {
    }

    @Test
    public void apply() {
    }

    @Test
    public void reject() {
    }

    @Test
    public void genProcessDiagram() {
    }
}