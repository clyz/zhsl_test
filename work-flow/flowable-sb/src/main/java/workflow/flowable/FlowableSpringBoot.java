package workflow.flowable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author YangZemin
 * @date 2020/6/5 9:28
 */
@SpringBootApplication
public class FlowableSpringBoot {
    public static void main(String[] args) {
        SpringApplication.run(FlowableSpringBoot.class, args);
    }
}
