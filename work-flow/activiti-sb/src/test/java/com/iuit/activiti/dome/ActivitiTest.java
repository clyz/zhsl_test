package com.iuit.activiti.dome;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivitiTest {

    @Autowired
    private ProcessEngine engine;

    @Test
    public void dome1() {
        //  创建流程引擎
//        final ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();

        final RepositoryService repositoryService = engine.getRepositoryService();
        final RuntimeService runtimeService = engine.getRuntimeService();
        final TaskService taskService = engine.getTaskService();

        //  部署流程文件
        repositoryService.createDeployment().addClasspathResource("processes/vacation.bpmn").deploy();

        //  查询第一个任务
        Task task = taskService.createTaskQuery().singleResult();

        //  完成第一个任务
        taskService.complete(task.getId());
        //  查询第二个任务
        task = taskService.createTaskQuery().singleResult();
        System.out.println("第二个任务完成前，当前任务名称：" + task.getName());

        //  完成第二个任务
        taskService.complete(task.getId());
        System.out.println("流程结束后，查询任务：" + task);

        System.exit(0);
    }

}